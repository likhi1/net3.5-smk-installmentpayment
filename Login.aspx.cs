using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


public partial class UserControl_AutoLogin : System.Web.UI.Page
{
    public string strAppName = System.Configuration.ConfigurationManager.AppSettings["appName"];
    public string strAppPath = System.Configuration.ConfigurationManager.AppSettings["appName"].ToString();
    protected void Page_Load(object sender, EventArgs e)
    {
        MasterDataManager cm = new MasterDataManager();
        DataSet dsMisUser;
        ProfileData loginData;
        litJS.Text = "";
        if (!Page.IsPostBack)
        {
            Page.Title = "�Թ�յ�͹�Ѻ�������к�";
            txtUserName.Focus();
            Session.RemoveAll();
            ViewState.Clear();
        }
        
       
    }
    protected void btnLogin_Click(object sender, EventArgs e)
    {
        string strRet = "";
        string menuList = "";
        ProfileData loginData;
        DataSet ds;
        DataSet dsMenu;
        DataSet dsTmp;
        DataSet dsBranch;
        string[] arrBranch ; 

        MasterDataManager cm = new MasterDataManager();
     
        strRet = cm.doLogin(txtUserName.Text, txtPassword.Text);
        litScript.Text = "";
        if (strRet.StartsWith("1"))
        {
            litScript.Text += "alert('" + strRet.Split(':')[1] + "');\n";
        }
        else if (strRet.StartsWith("2"))
        {
            litScript.Text += "alert('" + strRet.Split(':')[1] + "');\n";
            txtPassword.Focus();
        }
        else if (strRet.StartsWith("3"))
        {
            litScript.Text += "alert('" + strRet.Split(':')[1] + "');\n";
            txtUserName.Focus();
        }
        else if (strRet.StartsWith("0"))
        {
            litScript.Text += "alert('" + strRet.Split(':')[1] + "');\n";
            litScript.Text += "document.getElementById('divChangePassword').style.display = 'inline';\n";
        }

        if (litScript.Text == "")
        {
            loginData = new ProfileData();
            ds = cm.getDataUserByLogin(txtUserName.Text);
            loginData = new ProfileData();
            if (ds.Tables[0].Rows.Count > 0)
            {
                loginData.loginName = ds.Tables[0].Rows[0]["user_login"].ToString();
                loginData.userName = ds.Tables[0].Rows[0]["user_fullname"].ToString();
                loginData.roleID = ds.Tables[0].Rows[0]["role_id"].ToString();
                loginData.roleName = ds.Tables[0].Rows[0]["role_name"].ToString();
                loginData.teamCode = ds.Tables[0].Rows[0]["team_id"].ToString();
                loginData.teamName = ds.Tables[0].Rows[0]["team_name"].ToString();
                loginData.level = ds.Tables[0].Rows[0]["user_level"].ToString();

                // branch


                //loginData.userBranch = ds.Tables[0].Rows[0]["user_branch"].ToString();
                //dsTmp = cm.getDataLevelByID(loginData.levelID);
                //if (dsTmp.Tables[0].Rows.Count > 0)
                //    loginData.levelName = dsTmp.Tables[0].Rows[0]["level_name"].ToString();

                // role_menu
                dsMenu = cm.getDataMenuByRoleID(loginData.roleID);
                for (int i = 0; i <= dsMenu.Tables[0].Rows.Count - 1; i++)
                {
                    menuList += dsMenu.Tables[0].Rows[i]["menu_id"].ToString() + ", ";
                }
                // user_menu
                dsMenu = cm.getDataMenuByuserID(loginData.loginName);
                for (int i = 0; i <= dsMenu.Tables[0].Rows.Count - 1; i++)
                {
                    if (menuList.IndexOf(dsMenu.Tables[0].Rows[i]["menu_id"].ToString()) == -1)
                        menuList += dsMenu.Tables[0].Rows[i]["menu_id"].ToString() + ", ";
                }

                if (strRet.EndsWith(", "))
                    menuList = strRet.Substring(0, strRet.Length - 2);
               // loginData.menuList = menuList;
                Session.Add("LoginData", loginData);


                Response.Redirect("../"+strAppName+"/module/MasterData/MainPage.aspx");
               // Response.Redirect(  "/module/MasterData/MainPage.aspx");
               
            }
            else
            {
                litScript.Text += "alert('�������ö�����ҹ�к���');\n";
                litScript.Text = "<script>\n" + litScript.Text;
                litScript.Text += "</script>\n";
            }
        }
        else
        {
            litScript.Text = "<script>\n" + litScript.Text;
            litScript.Text += "</script>\n";
        }      
      
    }
}
