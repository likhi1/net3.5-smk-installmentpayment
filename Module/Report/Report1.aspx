﻿<%@ Page Language="C#" MasterPageFile="~/MasterData/MainMasterPage.master" AutoEventWireup="true" CodeFile="Report1.aspx.cs" Inherits="Module_Report_Report1" Title="Untitled Page" %>
<%@ Register src="../../UserControl/ucPageNavigator.ascx" tagname="ucPageNavigator" tagprefix="uc1" %>
<%@ Register src="../../UserControl/ucTxtDate.ascx" tagname="ucTxtDate" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" Runat="Server">
    <script>
        function goPage(strPage)
        {
            window.location= strPage;
            return false;
        }
        function validateSearchForm() {
            var isRet = true;
            var strMsg = "";

            if (strMsg != "") {
                strMsg = "! กรุณาระบุเงื่อนไขการดึงข้อมูล\n" + strMsg;
            }

            if (isRet == false)
                alert(strMsg);
            return isRet;
        }
        function validateForm()
        {
            var isRet = true;
            var strMsg = "";
            
 
            if (strMsg != "")
            {
                strMsg = "! Please check\n" + strMsg;
            }            
                                    
            if (isRet == false)
                alert(strMsg);

            window.open("../../images/Tmp/notice_smk.gif");
            isRet = false;
            return isRet;
        }
        function openZoom() {
            openShadowBox("../../Module/Zoom/Zoom1.aspx", 500, 600, 'Zoom');
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div id="page-inside">     
    <table width="100%">
        <tr>
            <td>
                <div class="heading1">:: ออกจดหมายทวงหนี้ ::</div>
            </td>
        </tr>
        <tr>
             <td colspan="1">
                <asp:Button ID="btnSave" runat="server" Text="บันทึก" CssClass="awesome" onclick="btnSave_Click" OnClientClick="return validateForm();"/>            
                <asp:Button ID="btnClear" runat="server" Text="เริ่มใหม่" CssClass="awesome" OnClientClick="return goPage('../../Module/Report/Report1.aspx');"/>            
                <asp:Button ID="btnExit" runat="server" Text="ออก" CssClass="awesome"  OnClientClick="return goPage('../../Module/MasterData/MainPage.aspx');"/>
            </td>
        </tr>
        <tr>
            <td>
                <div id="divSearchPolicy">
                <fieldset >
                    <legend>เงื่อนไขการดึงข้อมูล</legend>
                    <table width="75%">
                        <tr>
                            <td align="right" width="20%">ประเภท :</td>
                            <td align="left" width="30%">
                                <asp:RadioButton ID="rdoTypeAll" GroupName="rdoPolType" runat="server" Checked/>ทั้งหมด &nbsp;
                                <asp:RadioButton ID="rdoMotor" GroupName="rdoPolType" runat="server" />ระบบ Motor&nbsp;
                                <asp:RadioButton ID="rdoNonMotor" GroupName="rdoPolType" runat="server" />ระบบ Non-Motor
                            </td>
                            <td width="20%">                            
                            </td>
                            <td width="30%">                            
                            </td>                  
                        </tr>   
                        <tr>
                            <td align="right" >เลขกรมธรรม์ :</td>
                            <td align="left">
                                <asp:TextBox ID="txtSearchPolicy" runat="server" CssClass="TEXTBOX" Width="100" MaxLength="15" ></asp:TextBox>
                            </td>
                            <td align="right"></td>
                            <td>
                                
                            </td> 
                        </tr>
                        <tr>
                            <td align="right">วันที่รับกรมธรรม์ตั้งแต่ :</td>
                            <td align="left">
                                <uc1:ucTxtDate ID="txtPolicyDateFrom" runat="server" />
                            </td>
                            <td align="right">ถึง :</td>
                            <td >
                                <uc1:ucTxtDate ID="txtPolicyDateTo" runat="server" />
                            </td>
                            <td>
                                
                            </td> 
                        </tr>
                        <tr>
                            <td align="right">วันที่ครบกำหนดตั้งแต่ :</td>
                            <td align="left">
                                <uc1:ucTxtDate ID="txtDueDateFrom" runat="server" />
                            </td>
                            <td align="right">ถึง :</td>
                            <td>
                                <uc1:ucTxtDate ID="txtDueDateTo" runat="server" />
                            </td>
                            <td>
                                
                            </td> 
                        </tr>
                        <tr>
                            <td></td>
                            <td align="right"></td>
                            <td align="right"></td>
                            <td>
                                <asp:Button ID="btnSearch" runat="server" Text="ค้นหาข้อมูล" CssClass="awesome" onclick="btnSearch_Click" OnClientClick="return validateSearchForm();"/>
                                <asp:Button ID="btnClearSearch" runat="server" Text="เคลียร์" CssClass="awesome" OnClientClick="goPage('../../Module/Report/Report1.aspx');"/>
                            </td>
                        </tr>                                             
                    </table>
                </fieldset>
                </div>
            </td>
        </tr> 
        <tr>
            <td>    
                <div id="divPolicy" style="display:none">
                <fieldset>
                    <legend>ผลการค้นหา</legend>
                    <table width="100%">
                        <tr>
                            <td>
                                <asp:Label ID="lblSearch" runat="server" Text="Label"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>                            
                                
                            </td>
                        </tr>                        
                        <tr>
                            <td>
                                <asp:DataGrid ID="dtgData" runat="server" AutoGenerateColumns="false" Width="100%"
                                    HeaderStyle-CssClass="gridhead" ItemStyle-CssClass="gridrow0" AlternatingItemStyle-CssClass="gridrow1"
                                    AllowPaging="true" PagerStyle-Visible="false">
                                    <Columns>
                                        <asp:TemplateColumn>
					                        <HeaderTemplate>
                                                <%#ColumnTitle[0]%>					                
					                        </HeaderTemplate>
					                        <ItemTemplate>
					                            <%# Container.DataSetIndex +1%>
					                        </ItemTemplate>
				                        </asp:TemplateColumn>
                                        <asp:TemplateColumn>
					                        <HeaderTemplate>
						                        <%#ColumnTitle[1]%>						                        
					                        </HeaderTemplate>
					                        <ItemTemplate>
					                            <%#FormatStringApp.FormatDate(Eval(ColumnName[1]))%>
					                        </ItemTemplate>
				                        </asp:TemplateColumn>
				                        <asp:TemplateColumn>
					                        <HeaderTemplate>
						                        <%#ColumnTitle[2]%>
					                        </HeaderTemplate>
					                        <ItemTemplate>
					                            <%#Eval(ColumnName[2])%>
					                        </ItemTemplate>
				                        </asp:TemplateColumn>
				                        <asp:TemplateColumn>
					                        <HeaderTemplate>
						                        <%#ColumnTitle[3]%>
					                        </HeaderTemplate>
					                        <ItemTemplate>
					                            <%#(Eval(ColumnName[3]))%> 
					                        </ItemTemplate>
				                        </asp:TemplateColumn>
				                        <asp:TemplateColumn>
					                        <HeaderTemplate>
						                        <%#ColumnTitle[4]%>
					                        </HeaderTemplate>
					                        <ItemTemplate>
					                            <%#(Eval(ColumnName[4]))%> / <%#(Eval(ColumnName[4]))%>
					                        </ItemTemplate>
				                        </asp:TemplateColumn>
				                        <asp:TemplateColumn>
					                        <HeaderTemplate>
						                        <%#ColumnTitle[5]%>
					                        </HeaderTemplate>
					                        <ItemTemplate>
					                             <%#FormatStringApp.FormatDate(Eval(ColumnName[5]))%>
					                        </ItemTemplate>
				                        </asp:TemplateColumn>
				                        <asp:TemplateColumn>
					                        <HeaderTemplate>
						                        <%#ColumnTitle[6]%>
					                        </HeaderTemplate>
					                        <ItemTemplate>
					                            <%#FormatStringApp.FormatMoney(Eval(ColumnName[6]))%>
					                        </ItemTemplate>
				                        </asp:TemplateColumn>
				                        <asp:TemplateColumn>
					                        <HeaderTemplate>
						                        <%#ColumnTitle[7]%>
					                        </HeaderTemplate>
					                        <ItemTemplate>
					                             <%#FormatStringApp.FormatMoney(Eval(ColumnName[7]))%>
					                        </ItemTemplate>
				                        </asp:TemplateColumn>
				                        <asp:TemplateColumn>
					                        <HeaderTemplate>
						                        <%#ColumnTitle[8]%>
					                        </HeaderTemplate>
					                        <ItemTemplate>
					                             <%#FormatStringApp.FormatDate(Eval(ColumnName[8]))%>
					                        </ItemTemplate>
				                        </asp:TemplateColumn>
				                        <asp:TemplateColumn>
					                        <HeaderTemplate>
						                        <%#ColumnTitle[9]%>
					                        </HeaderTemplate>
					                        <ItemTemplate>
					                            <asp:CheckBox ID="chkPrint" runat="server" />
					                        </ItemTemplate>
				                        </asp:TemplateColumn>
				                    </Columns>
                                </asp:DataGrid>
                            </td>
                        </tr>
                    </table>
                </fieldset>        
                </div>        
            </td>
        </tr>  
        <tr>
            <td>
            <div style="display:none">
                <asp:TextBox ID="txtMode" runat="server"></asp:TextBox>
            </div>
            </td>
        </tr>          
        <tr>
            <td>            
            </td>
        </tr>
        <tr>
            <td>
                <asp:Literal ID="litJavaScript" runat="server"></asp:Literal>
            </td>
        </tr> 
    </table>    
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
</asp:Content>

