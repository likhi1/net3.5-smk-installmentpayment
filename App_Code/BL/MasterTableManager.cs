﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for MasterTableManager
/// </summary>
public class MasterTableManager : TDS.BL.BLWorker_MSSQL
{
	public MasterTableManager()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public DataSet getAllDataPolType()
    {
        DataSet ds = new DataSet();
        string strSQL = "";
        strSQL = "SELECT * FROM mst_policytype";
        

        ds = executeQuery(strSQL);
        return ds;
    }
}
