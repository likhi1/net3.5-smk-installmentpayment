﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Module_Report_Report1 : System.Web.UI.Page
{
    public string[] ColumnName = { "NO", "policy_date", "policy_no", "policy_insurername", "settle_installments", "settle_startdate", "settle_balance", "settle_arrear", "notice_date", "notice_flageprint" };
    public string[] ColumnType = { "string", "string", "string", "string", "string", "string", "string", "string", "string", "string" };
    public string[] ColumnTitle = { "ลำดับที่", "วันที่รับกรมธรรม์", "เลขกรมธรรม์", "ชื่อผู้เอาประกันภัย", "งวดที่", "กำหนดชำระ", "จำนวนเงิน", "ยอดค้าง", "วันที่ออกจดหมาย", "พิมพ์" };
    string[] ColumnWidth = { "5%", "10%", "10%", "15%", "10%", "10%", "10%", "10%", "10%", "5%" };
    string[] ColumnAlign = { "center", "left", "left", "left", "center", "center", "right", "right", "center", "center" };

    public string tranPicUrl = "";
    DataView dv = new DataView();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            initialDataGrid();
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        SettlementManager cmSettle = new SettlementManager();
        DataSet ds = new DataSet();
        lblSearch.Text = "วันที่รับกรมธรรม์ : 01/11/2014 - 31/11/2014   วันที่ครบกำหนดชำระ: 01/11/2014 - 31/11/2014";
        litJavaScript.Text += "<script>\n";
        litJavaScript.Text += "     \tdocument.getElementById('divSearchPolicy').style.display = 'none';\n";
        litJavaScript.Text += "     \tdocument.getElementById('divPolicy').style.display = 'inline';\n";
        litJavaScript.Text += "</script>\n";

        ds = cmSettle.searchDataSettlement("", "",
                                "", "",
                                "", "");
        ViewState.Remove("SortDirection");
        ViewState.Remove("SortString");
        ViewState.Remove("SortIndex");
        ViewState.Add("DSMasterTable", ds);
        ShowData();
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {

    }

    public void initialDataGrid()
    {
        DataSet ds = new DataSet();
        DataTable dt = new DataTable();
        int iPageSize = 0;
        dt = TDS.Utility.MasterUtil.AddColumnToDataTable(dt, ColumnType, ColumnName);
        ds.Tables.Add(dt);
        ViewState.Remove("SortDirection");
        ViewState.Remove("SortString");
        ViewState.Remove("SortIndex");
        ViewState.Add("DSMasterTable", ds);
        ShowData();
        GridViewUtil.setGridStyle(dtgData, ColumnWidth, ColumnAlign, ColumnTitle);
        try
        {
            iPageSize = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["pageSize"].ToString());
        }
        catch (Exception e)
        {
            iPageSize = 10;
        }
        dtgData.PageSize = iPageSize;
    }
    public void ShowData()
    {
        DataSet ds = new DataSet();
        if (ViewState["DSMasterTable"] != null)
            ds = (DataSet)ViewState["DSMasterTable"];

        string sortBy = "";
        if (ViewState["SortString"] != null)
            sortBy = (String)ViewState["SortString"];

        if (ds.Tables.Count > 0)
        {
            dv.Table = ds.Tables[0];
            dv.Sort = sortBy;

            int newPageCount = (Int32)(Math.Ceiling((double)dv.Table.Rows.Count / (double)dtgData.PageSize));
            if (dtgData.CurrentPageIndex >= newPageCount && newPageCount > 0)
                dtgData.CurrentPageIndex = newPageCount - 1;
        }
        dtgData.DataSource = dv;
        dtgData.DataBind();

        GridViewUtil.ItemDataBound(dtgData.Items);

        //ucPageNavigator1.dvData = ds.Tables[0].DefaultView;
        //ucPageNavigator1.dtgData = dtgData;
    }
}
