function getRadioValue(radObj){
	var returnValue = "";
	for(var i=0; i<radObj.length; i++){
		if(radObj[i].checked){
			returnValue = radObj[i].value;
			break;
		}
	}
	for(var i=0; i<radObj.length; i++){
		if(radObj[i].disabled){
			returnValue = "";
			break;
		}
	}
	return returnValue;
}
function Upper(e,r)
{
   if (e.keyCode > 96 && e.keyCode < 123)
         r.value = r.value.toUpperCase();
 }