<%@ Page Language="C#" MasterPageFile="~/MasterData/MainMasterPage.master" AutoEventWireup="true" CodeFile="UserForm.aspx.cs" Inherits="Module_MasterData_UserForm"%>
<%@ Register src="../../UserControl/ucPageNavigator.ascx" tagname="ucPageNavigator" tagprefix="uc1" %>
<%@ Register src="../../UserControl/ucTxtDate.ascx" tagname="ucTxtDate" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" Runat="Server">
<script>
        function goPage(strPage)
        {
            window.location= strPage;
            return false;
        }
        function validateForm()
        {
            var isRet = true;
            var strMsg = "";
            if (document.getElementById("<%=txtUserName.ClientID %>").value == ""){
                isRet = false;
                strMsg += "\t-  ชื่อผู้ใช้งาน\n";
            }
            if (document.getElementById("<%=txtMode.ClientID %>").value == "A" && document.getElementById("<%=txtPassword.ClientID %>").value == "") {
                isRet = false;
                strMsg += "\t-  รหัสผ่าน\n";
            }
            if (document.getElementById("<%=txtStartDate.txtID %>").value == "") {
                isRet = false;
                strMsg += "\t-  วันที่เริ่มต้นใช้งาน\n";
            }           
            if (document.getElementById("<%=txtFName.ClientID %>").value == ""){
                isRet = false;
                strMsg += "\t-  ชื่อ\n";
            }
            if (document.getElementById("<%=txtEmail.ClientID %>").value == "") {
                isRet = false;
                strMsg += "\t-  อีเมล\n";
            }
            if (document.getElementById("<%=ddlRole.ClientID %>").value == ""){
                isRet = false;
                strMsg += "\t-  กลุ่มผู้ใช้งาน\n";
            }            
            if (document.getElementById("<%=ddlStatus.ClientID %>").value == ""){
                isRet = false;
                strMsg += "\t-  สถานะ\n";
            }
            if (document.getElementById("<%=ddllevel.ClientID %>").value == "") {
                isRet = false;
                strMsg += "\t-  ระดับ\n";
            }
            if (document.getElementById("<%=ddlteam.ClientID %>").value == "") {
                isRet = false;
                strMsg += "\t-  ทีม\n";
            }
            if (document.getElementById("<%=txtMode.ClientID %>").value == "A") {
                if (document.getElementById("<%=txtPassword.ClientID %>").value.length < 8) {
                    isRet = false;
                    strMsg += "! รหัสผ่านต้องมีความยาวอย่างน้อย 8 ตัวอักษร\n";
                }
                if (document.getElementById("<%=txtPassword.ClientID %>").value != document.getElementById("<%=txtRePassword.ClientID %>").value) {
                    isRet = false;
                    strMsg += "! รหัสผ่าน ไม่ตรงกับ ยืนยันรหัสผ่าน\n";
                }
            }
 
            if (strMsg != "")
            {
                strMsg = "! Please check\n" + strMsg;
            }            
                                    
            if (isRet == false)
                alert(strMsg);
            return isRet;
        }
        function validateChangePassword() {
            var isRet = true;
            var strMsg = "";
            if (document.getElementById("<%=txtNewPassword.ClientID %>").value == "") {
                isRet = false;
                strMsg += "\t-  รหัสผ่านใหม่\n";
            }
            if (document.getElementById("<%=txtRenewPassword.ClientID %>").value == "") {
                isRet = false;
                strMsg += "\t-  ยืนยันรหัสผ่านใหม่\n";
            }
            if (document.getElementById("<%=txtNewPassword.ClientID %>").value.length < 8) {
                isRet = false;
                strMsg += "! รหัสผ่านต้องมีความยาวอย่างน้อย 8 ตัวอักษร\n";
            }
            if (document.getElementById("<%=txtNewPassword.ClientID %>").value != document.getElementById("<%=txtRenewPassword.ClientID %>").value) {
                isRet = false;
                strMsg += "! รหัสผ่านใหม่ ไม่ตรงกับ ยืนยันรหัสผ่านใหม่\n";
            }

            if (isRet == false)
                alert("! กรุณาระบุข้อมูล\n" + strMsg);
            return isRet;
        }                
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div id="page-inside">     
<table width="100%">
    <tr>
        <td>
            <div class="heading1">:: ข้อมูลผู้ใช้งาน ::</div>
        </td>
    </tr>
    <tr>
         <td colspan="1">
            <asp:Button ID="btnback" runat="server" Text="ย้อนกลับ" CssClass="awesome" OnClientClick="return goPage('../../Module/MasterData/UserList.aspx');"/>
            <asp:Button ID="btnAdd" runat="server" Text="เพิ่มสาขา" CssClass="awesome" onclick="btnAddItem_Click"/>  
            <asp:Button ID="btnAdd2" runat="server" Text="เพิ่มเมนู" CssClass="awesome" onclick="btnAddItem2_Click"/>      
            <asp:Button ID="btnSave" runat="server" Text="บันทึก" CssClass="awesome" onclick="btnSave_Click" OnClientClick="return validateForm();"/>            
            <asp:Button ID="btnClear" runat="server" Text="ล้างข้อมูล" CssClass="awesome" OnClientClick="return goPage('../../Module/MasterData/UserForm.aspx');"/>            
            <asp:Button ID="btnAddNew" runat="server" Text="เพิ่มข้อมูล" CssClass="awesome" OnClientClick="return goPage('../../Module/MasterData/UserForm.aspx');"/>
            <asp:Button ID="btnExit" runat="server" Text="ออกจากระบบ" CssClass="awesome"  OnClientClick="return goPage('../../Module/MasterData/MainPage.aspx');"/>
            <asp:Button ID="btnChangePwd" runat="server" Text="เปลี่ยนรหัสผ่าน" CssClass="awesome" 
                                OnClientClick="document.getElementById('divChangePassword').style.display = 'block'; return false;"/>
                      
        </td>
    </tr>
    <tr>
        <td>
            <fieldset >
                <legend>รายละเอียดผู้ใช้งาน</legend>
                <table width="100%">
                    <tr>
                        <td align="right" width="20%"><div class="labelRequireField">ผู้ใช้งาน :</div></td>
                        <td width="30%">
                            <asp:TextBox ID="txtUserName" runat="server" CssClass="TEXTBOX" Width="100" MaxLength="15" ></asp:TextBox>
                        </td>
                        <td width="20%"></td>                        
                        <td width="30%"></td>
                    </tr>
                    <tr>
                        <td align="right" style="height:0px"><div id="divLablePassword" class="labelRequireField" runat="server">รหัสผ่าน :</div></td>
                        <td style="height:0px">
                            <div id="divtxtPassword" runat="server">
                            <asp:TextBox ID="txtPassword" runat="server" CssClass="TEXTBOX" Width="100" MaxLength="13" TextMode="Password"></asp:TextBox>
                            </div>
                        </td>
                        <td align="right" style="height:0px"><div id="divLblRePassword" class="labelRequireField" runat="server">ยืนยันรหัสผ่าน :</div></td>
                        <td style="height:0px">
                            <div id="divtxtRePassword" runat="server">
                            <asp:TextBox ID="txtRePassword" runat="server" CssClass="TEXTBOX" Width="100" MaxLength="13" TextMode="Password"></asp:TextBox>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td align="right"><div class="labelRequireField">ชื่อ  :</div></td>
                        <td>
                            <asp:TextBox ID="txtFName" runat="server" CssClass="TEXTBOX" Width="150" MaxLength="100" ></asp:TextBox>
                        </td>
                        <td align="right">นามสกุล  :</td>
                        <td>
                            <asp:TextBox ID="txtLName" runat="server" CssClass="TEXTBOX" Width="150" MaxLength="100" ></asp:TextBox>
                        </td>
                    </tr>                                        
                    <tr>
                        <td align="right"><div class="labelRequireField">อีเมล  :</div></td>
                        <td>
                            <asp:TextBox ID="txtEmail" runat="server" CssClass="TEXTBOX" Width="150" MaxLength="100" ></asp:TextBox>
                        </td>
                    </tr> 
                    <tr>
                        <td align="right"><div class="labelRequireField">กลุ่มผู้ใช้งาน  :</div></td>
                        <td>
                            <asp:DropDownList ID="ddlRole" runat="server">
                                </asp:DropDownList>
                        </td>  
                        <td align="right"><div class="labelRequireField">สถานะ :</div></td>
                        <td>
                            <asp:DropDownList ID="ddlStatus" runat="server">                            
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                            <td align="right"><div class="labelRequireField">วันที่เริ่มต้นใช้งาน :</div></td>
                            <td>                                
                                <uc1:ucTxtDate ID="txtStartDate" runat="server" />                                
                            </td>
                            <td align="right">วันที่สิ้นสุด : </td>
                            <td>                                
                                <uc1:ucTxtDate ID="txtEndDate" runat="server" />                                
                            </td>
                    </tr>
                    <tr>
                        <td align="right"><div class="labelRequireField">ระดับ :</div></td>
                        <td>
                            <asp:DropDownList ID="ddllevel" runat="server">                            
                            </asp:DropDownList>
                        </td>
                        <td align="right"><div class="labelRequireField">ทีม :</div></td>
                        <td>
                            <asp:DropDownList ID="ddlteam" runat="server">                            
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                            <td align="right">วันfที่เข้าใช้งานล่าสุด : </td>
                            <td>
                                <uc1:ucTxtDate ID="txtLastLogin" runat="server" Mode="view" />
                            </td>                                    
                    </tr>                       
                    <tr>
                        <td align="right" valign="top">สาขา :</td>
                        <td >
                            <asp:DataGrid ID="dtgData" runat="server" AutoGenerateColumns="false" Width="100%"
                                HeaderStyle-CssClass="gridhead" ItemStyle-CssClass="gridrow0" AlternatingItemStyle-CssClass="gridrow1"
                                AllowPaging="false" PagerStyle-Visible="false" 
                                onitemdatabound="dtgData_ItemDataBound">
                                <Columns>
                                    <asp:TemplateColumn>
					                    <HeaderTemplate>
                                            <%#ColumnTitle[0]%>					                
					                    </HeaderTemplate>
					                    <ItemTemplate>
					                        <%# Container.DataSetIndex +1%>
					                    </ItemTemplate>
				                    </asp:TemplateColumn>
                                    <asp:TemplateColumn>
					                    <HeaderTemplate>
						                    <%#ColumnTitle[1]%>
					                    </HeaderTemplate>
					                    <ItemTemplate>
					                        <%#Eval(ColumnName[1])%>
					                    </ItemTemplate>
					                    <EditItemTemplate>
                                            <asp:DropDownList ID="ddlBranchCode" runat="server" >                            
                                            </asp:DropDownList>
					                    </EditItemTemplate>
				                    </asp:TemplateColumn>		                    
				                    <asp:TemplateColumn>
					                    <HeaderTemplate>						                    
					                    </HeaderTemplate>
					                    <ItemTemplate>
                                            <asp:ImageButton ID="imgEdit" runat="server" ImageUrl="../../images/btnEDIT.GIF" ToolTip="Edit" onclick="imgEdit_Click" dsindex="<%# Container.DataSetIndex%>"/>
                                            <asp:ImageButton ID="imgDelete" runat="server" ImageUrl="../../images/btnDelete.GIF" ToolTip="Delete" onclick="imgDelete_Click" dsindex="<%# Container.DataSetIndex%>"/>
                                            
					                    </ItemTemplate>
					                    <EditItemTemplate>
					                        <asp:ImageButton ID="imgSave" runat="server" ImageUrl="../../images/img_save.gif" ToolTip="Save" onclick="imgSave_Click" dsindex="<%# Container.DataSetIndex%>"/>
					                        <asp:ImageButton ID="imgCancel" runat="server" ImageUrl="../../images/btnDelete.GIF" ToolTip="Cancel" onclick="imgCancel_Click" dsindex="<%# Container.DataSetIndex%>"/>                                            
					                    </EditItemTemplate>
				                    </asp:TemplateColumn>				                    
				                </Columns>
                            </asp:DataGrid>
                        </td>
                    </tr>     
                    <tr>
                        <td align="right" valign="top">เมนู :</td>
                        <td colspan="2">
                            <asp:DataGrid ID="dtgData2" runat="server" AutoGenerateColumns="false" Width="100%"
                                HeaderStyle-CssClass="gridhead" ItemStyle-CssClass="gridrow0" AlternatingItemStyle-CssClass="gridrow1"
                                AllowPaging="false" PagerStyle-Visible="false" 
                                onitemdatabound="dtgData_ItemDataBound2">
                                <Columns>
                                    <asp:TemplateColumn>
					                    <HeaderTemplate>
                                            <%#ColumnTitle2[0]%>					                
					                    </HeaderTemplate>
					                    <ItemTemplate>
					                        <%# Container.DataSetIndex +1%>
					                        <div style="display:none">
                                                <asp:TextBox ID="txtMenuMode" runat="server"></asp:TextBox>
                                            </div>
					                    </ItemTemplate>
				                    </asp:TemplateColumn>
                                    <asp:TemplateColumn>
					                    <HeaderTemplate>
						                    <%#ColumnTitle2[1]%>
					                    </HeaderTemplate>
					                    <ItemTemplate>
					                        <%#Eval(ColumnName2[1])%>
					                    </ItemTemplate>				                    
					                    <EditItemTemplate>
                                            <asp:DropDownList ID="ddlMenu" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlMenu_SelectedIndexChanged" indexItem="<%# Container.DataSetIndex%>">                            
                                            </asp:DropDownList>
                                            <div style="display:none">
                                                <asp:TextBox ID="txtMenu" runat="server" Text="<%#Eval(ColumnName2[7]) %>"></asp:TextBox>
                                            </div>
					                    </EditItemTemplate>				                    
				                    </asp:TemplateColumn>
				                    <asp:TemplateColumn>
					                    <HeaderTemplate>
					                        <%#ColumnTitle2[2]%>
					                    </HeaderTemplate>
					                    <ItemTemplate> 
					                        <%#Eval(ColumnName2[2])%>
					                    </ItemTemplate>
					                    <EditItemTemplate>
                                            <asp:CheckBox runat="server" id="chkFlagAdd" flag_id="<%#Eval(ColumnName2[2])%>" Text="<%#ColumnTitle2[2]%>"></asp:CheckBox>    
                                            <div style="display:none">
                                                <asp:TextBox ID="txtFlagAdd" runat="server" Text="<%#Eval(ColumnName2[2]) %>"></asp:TextBox>
                                            </div>
					                   </EditItemTemplate>				                   
				                    </asp:TemplateColumn>	
				                     <asp:TemplateColumn>
					                    <HeaderTemplate>
					                        <%#ColumnTitle2[3]%>
					                    </HeaderTemplate>
					                    <ItemTemplate> 
					                        <%#Eval(ColumnName2[3])%>
					                    </ItemTemplate>
					                    <EditItemTemplate>
					                            <asp:CheckBox runat="server" id="chkFlagEdit" flag_id="<%#Eval(ColumnName2[3])%>"  Text="<%#ColumnTitle2[3]%>"></asp:CheckBox>   
					                        <div style="display:none">
                                                <asp:TextBox ID="txtFlagEdit" runat="server" Text="<%#Eval(ColumnName2[3]) %>"></asp:TextBox>
                                            </div>
					                    </EditItemTemplate>
				                    </asp:TemplateColumn>
				                     <asp:TemplateColumn>
					                    <HeaderTemplate>
					                        <%#ColumnTitle2[4]%>
					                    </HeaderTemplate>
					                    <ItemTemplate> 
					                        <%#Eval(ColumnName2[4])%>
					                    </ItemTemplate>
					                    <EditItemTemplate>
                                            <asp:CheckBox runat="server" id="chkFlagDelete" flag_id="<%#Eval(ColumnName2[4])%>" Text="<%#ColumnTitle2[4]%>"></asp:CheckBox> 
                                            <div style="display:none">
                                                <asp:TextBox ID="txtFlagDelete" runat="server" Text="<%#Eval(ColumnName2[4]) %>"></asp:TextBox>
                                            </div>
					                    </EditItemTemplate>
				                    </asp:TemplateColumn>
				                     <asp:TemplateColumn>
					                    <HeaderTemplate>
					                        <%#ColumnTitle2[5]%>
					                    </HeaderTemplate>
					                    <ItemTemplate> 
					                        <%#Eval(ColumnName2[5])%>
					                    </ItemTemplate>
					                    <EditItemTemplate>
                                            <asp:CheckBox runat="server" id="chkFlagInquiry" flag_id="<%#Eval(ColumnName2[5])%>" Text="<%#ColumnTitle2[5]%>"></asp:CheckBox>  
                                            <div style="display:none">
                                                <asp:TextBox ID="txtFlagInquiry" runat="server" Text="<%#Eval(ColumnName2[5]) %>"></asp:TextBox>
                                            </div>
					                    </EditItemTemplate>
				                    </asp:TemplateColumn>	                    
				                    <asp:TemplateColumn>
					                    <HeaderTemplate>						                    
					                    </HeaderTemplate>
					                    <ItemTemplate>
                                            <asp:ImageButton ID="imgEdit2" runat="server" ImageUrl="../../images/btnEDIT.GIF" ToolTip="Edit" onclick="imgEdit2_Click" dsindex="<%# Container.DataSetIndex%>"/>
                                            <asp:ImageButton ID="imgDelete2" runat="server" ImageUrl="../../images/btnDelete.GIF" ToolTip="Delete" onclick="imgDelete2_Click" dsindex="<%# Container.DataSetIndex%>"/>
                                            
					                    </ItemTemplate>
					                    <EditItemTemplate>
					                        <asp:ImageButton ID="imgSave2" runat="server" ImageUrl="../../images/img_save.gif" ToolTip="Save" onclick="imgSave2_Click" dsindex="<%# Container.DataSetIndex%>"/>
					                        <asp:ImageButton ID="imgCancel2" runat="server" ImageUrl="../../images/btnDelete.GIF" ToolTip="Cancel" onclick="imgCancel2_Click" dsindex="<%# Container.DataSetIndex%>"/>                                            
					                    </EditItemTemplate>
				                    </asp:TemplateColumn>				                    
				                </Columns>
                            </asp:DataGrid>
                        </td>
                    </tr>                                        
                </table>
            </fieldset>
        </td>
    </tr> 
    <tr>
            <td>
            <div style="display:none">
                <asp:TextBox ID="txtMode" runat="server"></asp:TextBox>
                <asp:HiddenField ID="hddPassword" runat="server" />
                <table>
                    <tr>
                            <td align="right">ผู้ที่บันทึกรายการ :</td>
                            <td>
                                <asp:TextBox ID="txtInsertLogin" runat="server" CssClass="TEXTBOXDIS" ReadOnly="true"></asp:TextBox>
                            </td>
                            <td align="right">วันที่บันทึกรายการ :</td>
                            <td>
                                <asp:TextBox ID="txtInsertDate" runat="server" CssClass="TEXTBOXDIS" ReadOnly="true"></asp:TextBox>
                            </td>
                    </tr>
                    <tr>
                            <td align="right">ผู้ที่แก้ไขรายการล่าสุด :</td>
                            <td>
                                <asp:TextBox ID="txtUpdateLogin" runat="server" CssClass="TEXTBOXDIS" ReadOnly="true"></asp:TextBox>
                            </td>
                            <td align="right">วันที่แก้ไขรายการล่าสุด :</td>
                            <td>
                                <asp:TextBox ID="txtUpdateDate" runat="server" CssClass="TEXTBOXDIS" ReadOnly="true"></asp:TextBox>
                            </td>
                    </tr>
                </table>
            </div>
            </td>
    </tr>      
    
    <tr>
        <td>
            
        </td>
    </tr>
    <tr>
        <td>
            <asp:Literal ID="litJavaScript" runat="server"></asp:Literal>
        </td>
    </tr> 
</table>    
</div>   
<div style="position:absolute; display:none; top:380px; left:430px; z-index:2;border: 2px solid;width:350px" id="divChangePassword">
    <table class="bgPopupLayer" width="100%">
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td width="20px">&nbsp;</td>
            <td>รหัสผ่านใหม่ :</td>
            <td>
                <asp:TextBox ID="txtNewPassword" runat="server" CssClass="TEXTBOX" Width="100" MaxLength="13" TextMode="Password"></asp:TextBox>
            </td>
            <td width="20px">&nbsp;</td>
        </tr>
        <tr>
            <td width="20px">&nbsp;</td>
            <td>ยืนยันรหัสผ่านใหม่ :</td>
            <td>
                <asp:TextBox ID="txtRenewPassword" runat="server" CssClass="TEXTBOX" Width="100" MaxLength="13" TextMode="Password"></asp:TextBox>
            </td>
            <td width="20px">&nbsp;</td>
        </tr>
        <tr>
            <td></td>        
            <td colspan="2">
                <asp:Button ID="btnConfirmChangePwd" runat="server" Text="ตกลง" 
                    CssClass="awesome" OnClientClick="return validateChangePassword();" 
                    onclick="btnConfirmChangePwd_Click" />
                <input id="Button1" name="btnClear" type="button" value="ยกเลิก" class="awesome" onclick="document.getElementById('divChangePassword').style.display = 'none';" />
            </td>
            <td></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
    </table>
 </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
</asp:Content>

