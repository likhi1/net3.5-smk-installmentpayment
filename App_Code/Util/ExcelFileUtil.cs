using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Xml;

/// <summary>
/// Summary description for ExcelFileUtil
/// </summary>
public class ExcelFileUtil : TDS.BL.BLWorker_MSAccess
{
	public ExcelFileUtil()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public DataSet getDataExcelFile(string strFilePath, string strSheetName)
    {
        string strConn = "";
        string strSQL = "";
        DataSet ds = new DataSet();
        DataTable dt = new DataTable();
        System.Data.OleDb.OleDbDataAdapter myCommand;
        System.Data.OleDb.OleDbConnection myConn;

        ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;" + "Data Source=" + strFilePath + ";" + "Extended Properties=Excel 8.0;";
        myConn = getDbConnection();
        try
        {
            dt = myConn.GetOleDbSchemaTable(System.Data.OleDb.OleDbSchemaGuid.Tables, null);
        }
        catch (Exception ee)
        {
        }
        finally
        {
            myConn.Close();
            myConn.Dispose();
        }
        if (dt.Rows.Count > 0)
        {
            if (dt.Rows[0]["TABLE_NAME"].ToString().Substring(0,dt.Rows[0]["TABLE_NAME"].ToString().Length - 1) == strSheetName)
            {
                strSQL = "SELECT * FROM [" + dt.Rows[0]["TABLE_NAME"].ToString().Replace("\'", "") + "]";
                //myCommand = new System.Data.OleDb.OleDbDataAdapter("SELECT * FROM [" + dt.Rows[0]["TABLE_NAME"].ToString().Replace("\'", "") + "]", myConn);
                try
                {
                    //myCommand.Fill(ds, "ExcelInfo");
                    ds = executeQuery(strSQL);
                    
                }
                catch (Exception ex)
                {
                    TDS.Utility.MasterUtil.writeError("Error occured when reading excel file", ex.ToString());
                    throw new Exception("! Can not load data, Please check your excel file.");                    
                }
            }
            else
            {
                throw new Exception("! Please check excel file.");                                    
            }
        }
        else
        {
            throw new Exception("! Not found any sheet, Please check your excel file.");                                                
        }
        return ds;
    }
    public StringWriter createDocumentFile(DataSet ds, string strCompany, string strDivision)
    {
        string strSQL = "";
        string strRet = "";
        bool isRet = true;
        string strTmp = "";
        int iFixColumns = 10;
        int iMaxColumns = 0;
        int iHeaderRows = 21;
        StringWriter outStream = new StringWriter();
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();
        DataSet dsXML = new DataSet();

        #region tmp
        /*
        #region find show max columns
        dsXML.ReadXml(new XmlTextReader(new StringReader(ds.Tables[0].Rows[ds.Tables[0].Rows.Count -1]["XMLData"].ToString())));
        iMaxColumns = iFixColumns + dsXML.Tables[0].Columns.Count;
        #endregion        
        // header and style
        #region no change
        outStream.WriteLine("<?xml version=\"1.0\"?>");
        outStream.WriteLine("<?mso-application progid=\"Excel.Sheet\"?>");
        outStream.WriteLine("<Workbook xmlns=\"urn:schemas-microsoft-com:office:spreadsheet\"");
        outStream.WriteLine("xmlns:o=\"urn:schemas-microsoft-com:office:office\"");
        outStream.WriteLine("xmlns:x=\"urn:schemas-microsoft-com:office:excel\"");
        outStream.WriteLine("xmlns:ss=\"urn:schemas-microsoft-com:office:spreadsheet\"");
        outStream.WriteLine(" xmlns:html=\"http://www.w3.org/TR/REC-html40\">");
        //outStream.WriteLine(" <DocumentProperties xmlns=\"urn:schemas-microsoft-com:office:office\">");
        //outStream.WriteLine("  <Author>T.D. Software</Author>");
        //outStream.WriteLine("  <LastAuthor>T.D. Software</LastAuthor>");
        //outStream.WriteLine("  <Created></Created>");
        //outStream.WriteLine("  <LastSaved></LastSaved>");
        //outStream.WriteLine("  <Company>T.D. Software Co., Ltd.</Company>");
        //outStream.WriteLine("  <Version></Version>");
        //outStream.WriteLine(" </DocumentProperties>");
        outStream.WriteLine(" <ExcelWorkbook xmlns=\"urn:schemas-microsoft-com:office:excel\">");
        outStream.WriteLine("  <WindowHeight>9210</WindowHeight>");
        outStream.WriteLine("  <WindowWidth>15195</WindowWidth>");
        outStream.WriteLine("  <WindowTopX>1290</WindowTopX>");
        outStream.WriteLine("  <WindowTopY>-75</WindowTopY>");
        outStream.WriteLine("  <ProtectStructure>False</ProtectStructure>");
        outStream.WriteLine("  <ProtectWindows>False</ProtectWindows>");
        outStream.WriteLine(" </ExcelWorkbook>");
        outStream.WriteLine(" <Styles>");
        outStream.WriteLine("  <Style ss:ID=\"Default\" ss:Name=\"Normal\">");
        outStream.WriteLine("   <Alignment ss:Vertical=\"Bottom\"/>");
        outStream.WriteLine("   <Borders/>");
        outStream.WriteLine("   <Font ss:FontName=\"Arial\"/>");
        outStream.WriteLine("   <Interior/>");
        outStream.WriteLine("   <NumberFormat/>");
        outStream.WriteLine("   <Protection/>");
        outStream.WriteLine("  </Style>");
        outStream.WriteLine("  <Style ss:ID=\"s62\">");
        outStream.WriteLine("   <Font ss:FontName=\"Arial\" x:Family=\"Swiss\" ss:Bold=\"1\"/>");
        outStream.WriteLine("   <Interior ss:Color=\"#B8CCE4\" ss:Pattern=\"Solid\"/>");
        outStream.WriteLine("  </Style>");
        outStream.WriteLine("  <Style ss:ID=\"s66\">");
        outStream.WriteLine("   <Font ss:FontName=\"Arial\" x:Family=\"Swiss\" ss:Bold=\"1\"/>");
        outStream.WriteLine("  </Style>");
        outStream.WriteLine("  <Style ss:ID=\"s67\">");
        outStream.WriteLine("   <Font ss:FontName=\"Arial\" x:Family=\"Swiss\" ss:Bold=\"1\"/>");
        outStream.WriteLine("   <Interior ss:Color=\"#DBEEF3\" ss:Pattern=\"Solid\"/>");
        outStream.WriteLine("  </Style>");
        outStream.WriteLine("  <Style ss:ID=\"s69\">");
        outStream.WriteLine("   <NumberFormat ss:Format=\"#,##0.00;[Black]-#,##0.00\"/>");
        outStream.WriteLine("  </Style>");
        outStream.WriteLine("  <Style ss:ID=\"s70\">");
        outStream.WriteLine("   <Alignment ss:Horizontal=\"Right\" ss:Vertical=\"Center\"/>");
        outStream.WriteLine("   <Font ss:FontName=\"Arial\" x:Family=\"Swiss\" ss:Bold=\"1\"/>");
        outStream.WriteLine("   <Interior ss:Color=\"#B8CCE4\" ss:Pattern=\"Solid\"/>");
        outStream.WriteLine("   <NumberFormat ss:Format=\"#,##0.00;[Black]#,##0.00\"/>");
        outStream.WriteLine("  </Style>");
        outStream.WriteLine("  <Style ss:ID=\"s71\">");
        outStream.WriteLine("   <Font ss:FontName=\"Arial\" x:Family=\"Swiss\" ss:Bold=\"1\"/>");
        outStream.WriteLine("   <NumberFormat ss:Format=\"#,##0.00;[Black]#,##0.00\"/>");
        outStream.WriteLine("  </Style>");
        outStream.WriteLine("  <Style ss:ID=\"s92\">");
        outStream.WriteLine("   <Interior ss:Color=\"#DBEEF3\" ss:Pattern=\"Solid\"/>");
        outStream.WriteLine("  </Style>");
        outStream.WriteLine("  <Style ss:ID=\"s93\">");
        outStream.WriteLine("   <Alignment ss:Vertical=\"Center\"/>");
        outStream.WriteLine("   <Font ss:FontName=\"Arial\" x:Family=\"Swiss\" ss:Bold=\"1\"/>");
        outStream.WriteLine("   <Interior ss:Color=\"#DBEEF3\" ss:Pattern=\"Solid\"/>");
        outStream.WriteLine("  </Style>");
        outStream.WriteLine("  <Style ss:ID=\"s94\">");
        outStream.WriteLine("   <Alignment ss:Horizontal=\"Center\"/>");
        outStream.WriteLine("   <Font ss:FontName=\"Arial\" x:Family=\"Swiss\" ss:Size=\"16\" ss:Bold=\"1\"/>");
        outStream.WriteLine("   <Interior ss:Color=\"#DBEEF3\" ss:Pattern=\"Solid\"/>");
        outStream.WriteLine("  </Style>");
        outStream.WriteLine("  <Style ss:ID=\"s95\">");
        outStream.WriteLine("   <Alignment ss:Horizontal=\"Left\" ss:Vertical=\"Bottom\"/>");
        outStream.WriteLine("   <Font ss:FontName=\"Arial\" x:Family=\"Swiss\" ss:Size=\"11\" ss:Bold=\"1\"/>");
        outStream.WriteLine("   <Interior ss:Color=\"#DBEEF3\" ss:Pattern=\"Solid\"/>");
        outStream.WriteLine("  </Style>");
        outStream.WriteLine("  <Style ss:ID=\"s96\">");
        outStream.WriteLine("   <Alignment ss:Horizontal=\"Center\" ss:Vertical=\"Bottom\"/>");
        outStream.WriteLine("   <Font ss:FontName=\"Arial\" x:Family=\"Swiss\" ss:Size=\"14\" ss:Bold=\"1\"/>");
        outStream.WriteLine("   <Interior ss:Color=\"#DBEEF3\" ss:Pattern=\"Solid\"/>");
        outStream.WriteLine("  </Style>");
        outStream.WriteLine("  <Style ss:ID=\"s107\">");
        outStream.WriteLine("   <Font ss:FontName=\"Arial\" x:Family=\"Swiss\" ss:Bold=\"1\"/>");
        outStream.WriteLine("   <Interior ss:Color=\"#B8CCE4\" ss:Pattern=\"Solid\"/>");
        outStream.WriteLine("  </Style>");
        outStream.WriteLine("  <Style ss:ID=\"s108\">");
        outStream.WriteLine("   <Alignment ss:Horizontal=\"Right\" ss:Vertical=\"Center\"/>");
        outStream.WriteLine("   <Font ss:FontName=\"Arial\" x:Family=\"Swiss\" ss:Bold=\"1\"/>");
        outStream.WriteLine("   <Interior ss:Color=\"#B8CCE4\" ss:Pattern=\"Solid\"/>");
        outStream.WriteLine("  </Style>");
        outStream.WriteLine("  <Style ss:ID=\"s109\">");
        outStream.WriteLine("   <Alignment ss:Horizontal=\"Right\" ss:Vertical=\"Center\"/>");
        outStream.WriteLine("   <Font ss:FontName=\"Arial\" x:Family=\"Swiss\" ss:Bold=\"1\"/>");
        outStream.WriteLine("   <Interior ss:Color=\"#DBEEF3\" ss:Pattern=\"Solid\"/>");
        outStream.WriteLine("  </Style>");
        outStream.WriteLine(" </Styles>");
        outStream.WriteLine(" <Worksheet ss:Name=\"Sheet1\">");
        #endregion
        #region set column width of data
        //outStream.WriteLine("  <Table ss:ExpandedColumnCount=\"8\" ss:ExpandedRowCount=\"" + CStr(RowNO + 16) + "\" x:FullColumns=\"1\"");
        outStream.WriteLine("  <Table ss:ExpandedColumnCount=\"" + iMaxColumns + "\" ss:ExpandedRowCount=\"" + Convert.ToString(ds.Tables[0].Rows.Count + iHeaderRows) + "\" x:FullColumns=\"1\"");
        outStream.WriteLine("   x:FullRows=\"1\">");
        for (int i = 0; i <= iMaxColumns - 1; i++)
        {
            outStream.WriteLine("   <Column ss:AutoFitWidth=\"0\" ss:Width=\"120\"/>");
        }
        #endregion
        #region set header
        outStream.WriteLine("   <Row ss:AutoFitHeight=\"0\" ss:Height=\"23.25\">");
        outStream.WriteLine("    <Cell ss:StyleID=\"s95\"><Data ss:Type=\"String\">Price list :</Data></Cell>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s95\"><Data ss:Type=\"String\">" + ds.Tables[0].Rows[0]["PHPSNO"].ToString() + "</Data></Cell>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("   </Row>");
        outStream.WriteLine("   <Row ss:AutoFitHeight=\"0\" ss:Height=\"23.25\">");
        outStream.WriteLine("    <Cell ss:StyleID=\"s95\"><Data ss:Type=\"String\">Customer name :</Data></Cell>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s95\"><Data ss:Type=\"String\">" + ds.Tables[0].Rows[0]["PHCUNO"].ToString() + " : " + ds.Tables[0].Rows[0]["cust_name"].ToString() + "</Data></Cell>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("   </Row>");
        outStream.WriteLine("   <Row ss:AutoFitHeight=\"0\" ss:Height=\"23.25\">");
        outStream.WriteLine("    <Cell ss:StyleID=\"s95\"><Data ss:Type=\"String\">Term of trade :</Data></Cell>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s95\"><Data ss:Type=\"String\">" + ds.Tables[0].Rows[0]["trad_name"].ToString() + "</Data></Cell>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s95\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("   </Row>");
        outStream.WriteLine("   <Row ss:AutoFitHeight=\"0\" ss:Height=\"23.25\">");
        outStream.WriteLine("    <Cell ss:StyleID=\"s95\"><Data ss:Type=\"String\">Departure port :</Data></Cell>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s95\"><Data ss:Type=\"String\">" + ds.Tables[0].Rows[0]["departureport_name"].ToString() + "</Data></Cell>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("   </Row>");
        outStream.WriteLine("   <Row ss:AutoFitHeight=\"0\" ss:Height=\"23.25\">");
        outStream.WriteLine("    <Cell ss:StyleID=\"s95\"><Data ss:Type=\"String\">Destination port :</Data></Cell>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s95\"><Data ss:Type=\"String\">" + ds.Tables[0].Rows[0]["destinationport_name"].ToString() + "</Data></Cell>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("   </Row>");
        outStream.WriteLine("   <Row ss:AutoFitHeight=\"0\" ss:Height=\"23.25\">");
        outStream.WriteLine("    <Cell ss:StyleID=\"s95\"><Data ss:Type=\"String\">Mode of shipment :</Data></Cell>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s95\"><Data ss:Type=\"String\">" + ds.Tables[0].Rows[0]["shty_name"].ToString() + "</Data></Cell>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("   </Row>");
        outStream.WriteLine("   <Row ss:AutoFitHeight=\"0\" ss:Height=\"23.25\">");
        outStream.WriteLine("    <Cell ss:StyleID=\"s95\"><Data ss:Type=\"String\">Unit of shipment :</Data></Cell>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s95\"><Data ss:Type=\"String\">" + ds.Tables[0].Rows[0]["PLSPUN"].ToString() + "</Data></Cell>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("   </Row>");
        outStream.WriteLine("   <Row ss:AutoFitHeight=\"0\" ss:Height=\"23.25\">");
        outStream.WriteLine("    <Cell ss:StyleID=\"s95\"><Data ss:Type=\"String\">Invoice currency :</Data></Cell>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s95\"><Data ss:Type=\"String\">" + ds.Tables[0].Rows[0]["PHCUCD"].ToString() + "</Data></Cell>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("   </Row>");
        outStream.WriteLine("   <Row ss:AutoFitHeight=\"0\" ss:Height=\"23.25\">");
        outStream.WriteLine("    <Cell ss:StyleID=\"s95\"><Data ss:Type=\"String\">Inv exchange rate :</Data></Cell>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s95\"><Data ss:Type=\"String\">" + ds.Tables[0].Rows[0]["PHARAT"].ToString() + "</Data></Cell>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("   </Row>");
        outStream.WriteLine("   <Row ss:AutoFitHeight=\"0\" ss:Height=\"23.25\">");
        outStream.WriteLine("    <Cell ss:StyleID=\"s95\"><Data ss:Type=\"String\">Valid date from - to :</Data></Cell>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s95\"><Data ss:Type=\"String\">" + FormatStringApp.FormatDateNumber(ds.Tables[0].Rows[0]["PHSTDT"].ToString()) + " - " + FormatStringApp.FormatDateNumber(ds.Tables[0].Rows[0]["PHLVDT"].ToString()) + "</Data></Cell>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("   </Row>");
        outStream.WriteLine("   <Row ss:AutoFitHeight=\"0\" ss:Height=\"23.25\">");
        outStream.WriteLine("    <Cell ss:StyleID=\"s95\"><Data ss:Type=\"String\">W/H :</Data></Cell>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s95\"><Data ss:Type=\"String\">" + (ds.Tables[0].Rows[0]["PHWHLO"].ToString()) + "</Data></Cell>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("   </Row>");
        outStream.WriteLine("   <Row ss:AutoFitHeight=\"0\" ss:Height=\"23.25\">");
        outStream.WriteLine("    <Cell ss:StyleID=\"s95\"><Data ss:Type=\"String\">Date created :</Data></Cell>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s95\"><Data ss:Type=\"String\">" + FormatStringApp.FormatDateNumber(ds.Tables[0].Rows[0]["PHRGDT"].ToString()) + " " + FormatStringApp.FormatTimeNumber(ds.Tables[0].Rows[0]["PHRGTM"].ToString()) + "</Data></Cell>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("   </Row>");
        outStream.WriteLine("   <Row ss:AutoFitHeight=\"0\" ss:Height=\"23.25\">");
        outStream.WriteLine("    <Cell ss:StyleID=\"s95\"><Data ss:Type=\"String\">Created by :</Data></Cell>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s95\"><Data ss:Type=\"String\">" + ds.Tables[0].Rows[0]["create_name"].ToString() + "</Data></Cell>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("   </Row>");
        outStream.WriteLine("   <Row ss:AutoFitHeight=\"0\" ss:Height=\"23.25\">");
        outStream.WriteLine("    <Cell ss:StyleID=\"s95\"><Data ss:Type=\"String\">Date updated :</Data></Cell>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s95\"><Data ss:Type=\"String\">" + FormatStringApp.FormatDateNumber(ds.Tables[0].Rows[0]["PHCHDT"].ToString()) + " " + FormatStringApp.FormatTimeNumber(ds.Tables[0].Rows[0]["PHCHTM"].ToString()) + "</Data></Cell>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("   </Row>");
        outStream.WriteLine("   <Row ss:AutoFitHeight=\"0\" ss:Height=\"23.25\">");
        outStream.WriteLine("    <Cell ss:StyleID=\"s95\"><Data ss:Type=\"String\">Updated by :</Data></Cell>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s95\"><Data ss:Type=\"String\">" + ds.Tables[0].Rows[0]["update_name"].ToString() + "</Data></Cell>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("   </Row>");        
        outStream.WriteLine("   <Row ss:AutoFitHeight=\"0\" ss:Height=\"23.25\">");
        outStream.WriteLine("    <Cell ss:StyleID=\"s95\"><Data ss:Type=\"String\"></Data></Cell>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s95\"><Data ss:Type=\"String\">" + "" + "</Data></Cell>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("    <Cell ss:StyleID=\"s93\"/>");
        outStream.WriteLine("   </Row>");
        #endregion
        #region set column header of data
        outStream.WriteLine("   <Row ss:StyleID=\"s66\">");
        for (int iColumns = 0; iColumns <= iMaxColumns - 1; iColumns++)
        {
            outStream.WriteLine("   <Cell ss:StyleID=\"s107\"><Data ss:Type=\"String\">" + ColumnTitle[iColumns] + "</Data></Cell>");
        }
        outStream.WriteLine("   </Row>");
        #endregion
        #region data
        for (int iRows = 0; iRows <= ds.Tables[0].Rows.Count - 1; iRows++)
        {
            outStream.WriteLine("   <Row>");
            for (int iColumns = 0; iColumns <= iMaxColumns - 1; iColumns++)
            {
                outStream.WriteLine("   <Cell><Data ss:Type=\"String\">" + ds.Tables[0].Rows[iRows][ColumnName[iColumns]].ToString() + "</Data></Cell>");
            }

            //outStream.WriteLine("    <Cell><Data ss:Type=\"Number\">" + Convert.ToString(i + 1) + "</Data></Cell>");
            //outStream.WriteLine("    <Cell><Data ss:Type=\"String\">" + drVONO[0]["EPVONO"].ToString() + "</Data></Cell>");
            //outStream.WriteLine("    <Cell><Data ss:Type=\"String\">" + FormatStringApp.showDate(drVONO[0]["P3PYDT"].ToString().Insert(4, "/").Insert(7, "/")) + "</Data></Cell>");
            //outStream.WriteLine("    <Cell><Data ss:Type=\"String\">" + drVONO[0]["EPSPYN"].ToString() + "</Data></Cell>");
            //outStream.WriteLine("    <Cell><Data ss:Type=\"String\">" + drVONO[0]["IDSUNM"].ToString() + "</Data></Cell>");
            //outStream.WriteLine("    <Cell><Data ss:Type=\"String\">" + drVONO[0]["SUPFIAN"].ToString() + "</Data></Cell>");
            //outStream.WriteLine("    <Cell><Data ss:Type=\"String\">" + drVONO[0]["SUPBANA"].ToString() + "</Data></Cell>");
            //outStream.WriteLine("    <Cell ss:StyleID=\"s69\"><Data ss:Type=\"Number\">" + FormatStringApp.FormatMoney(dInvoice) + "</Data></Cell>"); //(-1)Invoice Amount
            //outStream.WriteLine("    <Cell ss:StyleID=\"s69\"><Data ss:Type=\"Number\">" + drData[i][""].ToString() + "</Data></Cell>") '(-1)WHT Amount
            //outStream.WriteLine("    <Cell ss:StyleID=\"s69\"><Data ss:Type=\"Number\">" + FormatStringApp.FormatMoney(dPaid) + "</Data></Cell>"); //Paid Amount
            //outStream.WriteLine("    <Cell><Data ss:Type=\"String\">" + drVONO[0]["EGVTXT"].ToString() + "</Data></Cell>");
            outStream.WriteLine("   </Row>");
        }
        #endregion
        #region footer

        #region total
        //outStream.WriteLine("   <Row ss:Index=\"" + Convert.ToString(drData.Length + 7) + "\">");
        //outStream.WriteLine("    <Cell ss:Index=\"7\" ss:StyleID=\"s62\"><Data ss:Type=\"String\">" + drFixExcel[0]["FXValue"].ToString() + "  :</Data></Cell>");
        ////outStream.WriteLine("    <Cell ss:StyleID=\"s71\"><Data ss:Type=\"Number\">" + FormatStringApp.FormatMoney(dTotalInvoice) + "</Data></Cell>"); // summary Invoiced            
        //outStream.WriteLine("    <Cell ss:StyleID=\"s70\"><Data ss:Type=\"Number\">" + FormatStringApp.FormatMoney(dTotalPaid) + "</Data></Cell>"); // summary Paid            
        //outStream.WriteLine("   </Row>");
        #endregion

        #region footer
        //outStream.WriteLine("   <Row ss:Index=\"" + Convert.ToString(drData.Length + 12) + "\">");
        //outStream.WriteLine("    <Cell ss:Index=\"2\"><Data ss:Type=\"String\">" + drFixExcel[1]["FXValue"].ToString() + "       " + strUserName + "</Data></Cell>");
        //outStream.WriteLine("    <Cell ss:Index=\"7\"><Data ss:Type=\"String\">" + drFixExcel[2]["FXValue"].ToString() + " ____________________________</Data></Cell>");
        //outStream.WriteLine("   </Row>");
        //outStream.WriteLine("   <Row ss:Index=\"" + Convert.ToString(drData.Length + 16) + "\">");
        //outStream.WriteLine("    <Cell ss:Index=\"2\"><Data ss:Type=\"String\">" + drFixExcel[3]["FXValue"].ToString() + " ____________________________</Data></Cell>");
        //outStream.WriteLine("    <Cell ss:Index=\"7\"><Data ss:Type=\"String\">" + drFixExcel[4]["FXValue"].ToString() + " ____________________________</Data></Cell>");
        //outStream.WriteLine("   </Row>");
        outStream.WriteLine("  </Table>");
        outStream.WriteLine("  <WorksheetOptions xmlns=\"urn:schemas-microsoft-com:office:excel\">");
        outStream.WriteLine("   <Print>");
        outStream.WriteLine("    <ValidPrinterInfo/>");
        outStream.WriteLine("    <PaperSizeIndex>9</PaperSizeIndex>");
        outStream.WriteLine("    <HorizontalResolution>600</HorizontalResolution>");
        outStream.WriteLine("    <VerticalResolution>600</VerticalResolution>");
        outStream.WriteLine("   </Print>");
        outStream.WriteLine("   <Selected/>");
        outStream.WriteLine("   <Panes>");
        outStream.WriteLine("    <Pane>");
        outStream.WriteLine("     <Number>3</Number>");
        outStream.WriteLine("     <ActiveRow>7</ActiveRow>");
        outStream.WriteLine("     <ActiveCol>5</ActiveCol>");
        outStream.WriteLine("    </Pane>");
        outStream.WriteLine("   </Panes>");
        outStream.WriteLine("   <ProtectObjects>False</ProtectObjects>");
        outStream.WriteLine("   <ProtectScenarios>False</ProtectScenarios>");
        outStream.WriteLine("  </WorksheetOptions>");
        outStream.WriteLine(" </Worksheet>");
        outStream.WriteLine("</Workbook>");
        #endregion
        #endregion footer
        */
        #endregion tmp
        return outStream;
    }
}
