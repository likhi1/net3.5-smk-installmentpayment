﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

/// <summary>
/// Summary description for SendDocManager
/// </summary>
public class SendDocManager : TDS.BL.BLWorker_MSSQL
{
	public SendDocManager()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public DataSet searchDataSendDoc(string strPolNo, string strInsurerName,
                                        string strBranchCode, string strAgentCode,
                                        string strPeriodFromDate1, string strPeriodFromDate2)
    {
        DataSet ds = new DataSet();
        string strSQL = "";
        string strWhere = "";
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();


        strSQL = "SELECT *  ";
        strSQL = "SELECT *,spol_custpre + spol_custfirstname + ' ' + spol_custlastname as spol_custname ";

        strSQL += " FROM sync_policy A INNER JOIN pay_senddoc B ON A.spol_policy = B.policy_no ";
        strSQL += " WHERE A.spol_policy != '' AND ";

          
      
        if (strPolNo != "")
            strSQL += " A.policy_no = '" + strPolNo + "' AND ";
        if (strInsurerName != "")
            strSQL += " A.policy_insurername = '" + strInsurerName + "' AND ";
        if (strBranchCode != "")
            strSQL += " policy_branch = '" + strBranchCode + "' AND ";
        if (strAgentCode != "")
            strSQL += " policy_agent = '" + strAgentCode + "' AND ";
        if (strPeriodFromDate1 != "__/__/____" && strPeriodFromDate1 != "")
            strSQL += " policy_periodfrom >= '" + cmDate.convertDateStringForDB(strPeriodFromDate1) + "' AND ";
        if (strPeriodFromDate2 != "__/__/____" && strPeriodFromDate2 != "")
            strSQL += " policy_periodfrom <= '" + cmDate.convertDateStringForDB(strPeriodFromDate2) + "' AND ";
        if (strSQL.EndsWith("AND ") == true)
            strSQL = strSQL.Substring(0, strSQL.Length - 4);
       // strSQL += " ORDER BY A.spol_policy asc ";
        ds = executeQuery(strSQL);
        return ds;

    }




    public DataSet getDataSendDocByPolNo(string strPolNo)
    {
        DataSet ds = new DataSet();
        string strSQL = "";
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();

        strSQL = "SELECT * , spol_custpre + spol_custfirstname + ' ' + spol_custlastname as spol_custname ,  spol_compre + spol_comfirstname + ' ' + spol_comlastname as spol_comname  ";
        strSQL += " FROM sync_policy A  INNER JOIN pay_senddoc B  ON A.spol_policy = B.policy_no ";
        strSQL += " WHERE B.policy_no != '' AND ";
        strSQL += " B.policy_no = '" + strPolNo + "' ";
        ds = executeQuery(strSQL);
        return ds;
    }



    //\\\\\\\\\\\\\\\\\\\\  Update Data
    public string addDataSendDoc(ref DataSet dsData)
    {
        string strRet = "";
        string strPayID = "";
        string strInstID = "";
        string strSQL = "";
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();
        SqlConnection conn;
        SqlTransaction trans;

        conn = getDbConnection();
        trans = conn.BeginTransaction();
        try
        {
            strRet = getMaxID("settle_id", "pay_settlement", "", 8, true, true, conn, trans);
            dsData.Tables[0].Rows[0]["settle_id"] = strRet;
             
            //\\\\\\\\\\\ Update synv_policy.spol_settlementdate \\\\\\\
            strSQL = "UPDATE pay_senddoc SET ";
            strSQL += " send_name ='"+dsData.Tables[0].Rows[0]["send_name"].ToString()+"' ";
            strSQL += " , send_phoneno ='" + dsData.Tables[0].Rows[0]["send_phoneno"].ToString() + "' ";
            strSQL += " , send_address1 ='" + dsData.Tables[0].Rows[0]["send_address1"].ToString() + "' ";
            strSQL += " , send_address2 ='" + dsData.Tables[0].Rows[0]["send_address2"].ToString() + "' ";
            strSQL += " , send_postcode ='" + dsData.Tables[0].Rows[0]["send_postcode"].ToString() + "' ";
            strSQL += " , update_date = getDate() ";
            strSQL += " , update_login = '" + dsData.Tables["settlement"].Rows[0]["LOGINNAME"].ToString() + "' ";
   
            strSQL += " , WHERE policy_no = '" + dsData.Tables[0].Rows[0]["policy_no"].ToString() + "'  "; 
            executeUpdate(strSQL, conn, trans);
             
            trans.Commit();
        }
        catch (Exception e )
        {
            strRet = "ERROR";
            trans.Rollback();

            TDS.Utility.MasterUtil.writeLog("SendDocManager.addDataSendDoc() - ERROR", e.ToString());
            TDS.Utility.MasterUtil.writeLog("SendDocManager.addDataSendDoc() - SQL", strSQL);
        }
        finally
        {
            trans.Dispose();
            conn.Dispose();
            conn.Close();
        }


        return strRet;
    }


}