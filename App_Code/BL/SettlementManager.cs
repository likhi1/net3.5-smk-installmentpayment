﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

/// <summary>
/// Summary description for SettlementManager
/// </summary>
public class SettlementManager : TDS.BL.BLWorker_MSSQL
{
	public SettlementManager()
	{
		//
		// TODO: Add constructor logic here
		//
    }

    public DataSet searchDataSettlement(string strPolNo, string strInsurerName,
                                        string strBranchCode, string strAgentCode,
                                        string strPeriodFromDate1, string strPeriodFromDate2)
    {
        DataSet ds = new DataSet();
        string strSQL = "";
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();

        strSQL = "SELECT *, null as notice_date ";
        strSQL += " FROM ins_policy A INNER JOIN pay_settlement B ON A.policy_no = B.policy_no ";
        strSQL += " WHERE A.policy_no != '' AND ";

        if (strPolNo != "")
            strSQL += " A.policy_no = '" + strPolNo + "' AND ";
        if (strInsurerName != "")
            strSQL += " A.policy_insurername = '" + strInsurerName + "' AND ";
        if (strBranchCode != "")
            strSQL += " policy_branch = '" + strBranchCode + "' AND ";
        if (strAgentCode != "")
            strSQL += " policy_agent = '" + strAgentCode + "' AND ";
        if (strPeriodFromDate1 != "__/__/____" && strPeriodFromDate1 != "")
            strSQL += " policy_periodfrom >= '" + cmDate.convertDateStringForDB(strPeriodFromDate1) + "' AND ";
        if (strPeriodFromDate2 != "__/__/____" && strPeriodFromDate2 != "")
            strSQL += " policy_periodfrom <= '" + cmDate.convertDateStringForDB(strPeriodFromDate2) + "' AND ";
        if (strSQL.EndsWith("AND ") == true)
            strSQL = strSQL.Substring(0, strSQL.Length - 4);
        ds = executeQuery(strSQL);
        return ds;
    }
    public DataSet getDataSettlementByPolNo(string strPolNo)
    {
        DataSet ds = new DataSet();
        string strSQL = "";
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();

        strSQL = "SELECT * ";
        strSQL += " FROM pay_settlement B  ";
        strSQL += " WHERE B.policy_no != '' AND ";
        strSQL += " B.policy_no = '" + strPolNo + "' ";
        ds = executeQuery(strSQL);
        return ds;
    }

    public string addDataSettlement(ref DataSet dsData)
    {
        string strRet = "";
        string strPayID = "";
        string strInstID = "";
        string strSQL = "";
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();
        SqlConnection conn;
        SqlTransaction trans;

        conn = getDbConnection();
        trans = conn.BeginTransaction();
        try
        {
            strRet = getMaxID("settle_id", "pay_settlement", "", 8, true, true, conn, trans);
            dsData.Tables[0].Rows[0]["settle_id"] = strRet;
            // 1. insert pay_settlement
            strSQL = "INSERT INTO pay_settlement(";
            strSQL += " settle_id, settle_date, policy_no, settle_premium, settle_compremium " + System.Environment.NewLine;
            strSQL += " , settle_endorsepremium, settle_totalpremium, settle_discountrate, settle_discount, settle_balance " + System.Environment.NewLine;
            strSQL += " , settle_paid,settle_arrear, settle_installments, settle_startdate, settle_months " + System.Environment.NewLine;
            strSQL += " , settle_arno,insert_date, insert_login, update_date, update_login) " + System.Environment.NewLine;
            strSQL += " VALUES( " + System.Environment.NewLine;
            strSQL += " '" + dsData.Tables[0].Rows[0]["settle_id"].ToString() + "' " ;
            strSQL += " ,'" + cmDate.convertDateStringForDB(dsData.Tables[0].Rows[0]["settle_date"].ToString()) + "' " ;
            strSQL += " ,'" + dsData.Tables["settlement"].Rows[0]["policy_no"].ToString() + "' " ;
            strSQL += " ,'" + dsData.Tables["settlement"].Rows[0]["settle_premium"].ToString().Replace(",", "") + "' " ;
            strSQL += " ,'" + dsData.Tables["settlement"].Rows[0]["settle_compremium"].ToString().Replace(",", "") + "' " + System.Environment.NewLine;
            strSQL += " ,'" + dsData.Tables["settlement"].Rows[0]["settle_endorsepremium"].ToString().Replace(",", "") + "' " ;
            strSQL += " ,'" + dsData.Tables["settlement"].Rows[0]["settle_totalpremium"].ToString().Replace(",", "") + "' ";
            strSQL += " ,'" + dsData.Tables["settlement"].Rows[0]["settle_discountrate"].ToString() + "' ";
            strSQL += " ,'" + dsData.Tables["settlement"].Rows[0]["settle_discount"].ToString().Replace(",", "") + "' ";
            strSQL += " ,'" + dsData.Tables["settlement"].Rows[0]["settle_balance"].ToString().Replace(",", "") + "' " + System.Environment.NewLine;
            strSQL += " ,'" + dsData.Tables["settlement"].Rows[0]["settle_paid"].ToString().Replace(",", "") + "' " ;
            strSQL += " ,'" + dsData.Tables["settlement"].Rows[0]["settle_arrear"].ToString().Replace(",", "") + "' ";
            strSQL += " ,'" + dsData.Tables["settlement"].Rows[0]["settle_installments"].ToString() + "' ";
            strSQL += " ,'" + cmDate.convertDateStringForDB(dsData.Tables["settlement"].Rows[0]["settle_startdate"].ToString()) + "' ";
            strSQL += " ,'" + dsData.Tables["settlement"].Rows[0]["settle_months"].ToString() + "' " + System.Environment.NewLine;
            strSQL += " ,null " + System.Environment.NewLine; // arno
            strSQL += " ,getDate() " + System.Environment.NewLine;
            strSQL += " ,'" + dsData.Tables["settlement"].Rows[0]["LOGINNAME"].ToString() + "' " + System.Environment.NewLine;
            strSQL += " ,null " + System.Environment.NewLine; // update_date
            strSQL += " ,'' "; // update_login
            strSQL += " ) " + System.Environment.NewLine;
            executeUpdate(strSQL, conn, trans);
            // 2. insert pay_senddoc
            strSQL = "INSERT INTO pay_senddoc (" + System.Environment.NewLine;
            strSQL += " policy_no, send_name, send_address1, send_address2, send_province " + System.Environment.NewLine;
            strSQL += " ,send_postcode, insert_date, insert_login, update_date, update_login" + System.Environment.NewLine;
            strSQL += " ) VALUES ( " + System.Environment.NewLine;
            strSQL += " '" + dsData.Tables["settlement"].Rows[0]["policy_no"].ToString() + "' ";
            strSQL += " ,'" + TDS.Utility.MasterUtil.replaceSpecialSQL(dsData.Tables["senddoc"].Rows[0]["send_name"].ToString()) + "' ";
            strSQL += " ,'" + TDS.Utility.MasterUtil.replaceSpecialSQL(dsData.Tables["senddoc"].Rows[0]["send_address1"].ToString()) + "' ";
            strSQL += " ,'" + TDS.Utility.MasterUtil.replaceSpecialSQL(dsData.Tables["senddoc"].Rows[0]["send_address2"].ToString()) + "' ";
            strSQL += " ,'" + dsData.Tables["senddoc"].Rows[0]["send_province"].ToString() + "' " + System.Environment.NewLine;
            strSQL += " ,'" + dsData.Tables["senddoc"].Rows[0]["send_postcode"].ToString() + "' ";
            strSQL += " ,getDate() ";
            strSQL += " ,'" + dsData.Tables["settlement"].Rows[0]["LOGINNAME"].ToString() + "' ";
            strSQL += " ,null "; // update_date
            strSQL += " ,'' "; // update_login
            strSQL += " ) " + System.Environment.NewLine;
            executeUpdate(strSQL, conn, trans);
            // 3. loop for pay_payin
            // 3.1 insert into informix >> chq_accept2, chq_cheque2, cash2, credit_card2, transfer2
            // 3.2 insert into pay_payin
            strPayID = getMaxID("pay_id", "pay_payin", "", 8, true, true, conn, trans);
            dsData.Tables["payin"].Rows[0]["pay_id"] = strPayID;
            strSQL = "INSERT INTO pay_payin ("  + System.Environment.NewLine;
            strSQL += "  pay_id, pay_docno, policy_no, pay_amt, pay_date " + System.Environment.NewLine;
            strSQL += " , insert_date, insert_login, update_date, update_login " + System.Environment.NewLine;
            strSQL += " ) VALUES ( " + System.Environment.NewLine;
            strSQL += " '" + dsData.Tables["payin"].Rows[0]["pay_id"].ToString() + "' ";
            strSQL += " ,'" + dsData.Tables["payin"].Rows[0]["pay_docno"].ToString() + "' ";
            strSQL += " ,'" + dsData.Tables["payin"].Rows[0]["policy_no"].ToString() + "' ";
            strSQL += " ,'" + dsData.Tables["payin"].Rows[0]["pay_amt"].ToString().Replace(",", "") + "' " + System.Environment.NewLine;
            strSQL += " ,'" + cmDate.convertDateStringForDB(dsData.Tables["payin"].Rows[0]["pay_date"].ToString()) + "' ";
            strSQL += " ,getDate() ";
            strSQL += " ,'" + dsData.Tables["settlement"].Rows[0]["LOGINNAME"].ToString() + "' ";
            strSQL += " ,null "; // update_date
            strSQL += " ,'' "; // update_login
            strSQL += " ) " + System.Environment.NewLine;
            executeUpdate(strSQL, conn, trans);
            // 4. loop for insert pay_installments
            for (int iInst = 0; iInst <= dsData.Tables["installment"].Rows.Count - 1; iInst++)
            {
                strInstID = getMaxID("inst_id", "pay_installments", "", 8, true, true, conn, trans);
                dsData.Tables["installment"].Rows[iInst]["inst_id"] = strInstID;
                strSQL = "INSERT INTO pay_installments( ";
                strSQL += " inst_id, settle_id, policy_no, inst_no, inst_duedate " + System.Environment.NewLine;
                strSQL += " ,inst_rate, inst_net, inst_rvno, inst_paiddate, inst_paid " + System.Environment.NewLine;
                strSQL += " ,inst_remark, pay_id, insert_date, insert_login, update_date, update_login" + System.Environment.NewLine;
                strSQL += " ) VALUES ( " + System.Environment.NewLine;
                strSQL += " '" +  dsData.Tables["installment"].Rows[iInst]["inst_id"].ToString() + "' ";
                strSQL += " ,'" +  dsData.Tables["installment"].Rows[iInst]["settle_id"].ToString() + "' ";
                strSQL += " ,'" +  dsData.Tables["installment"].Rows[iInst]["policy_no"].ToString() + "' ";
                strSQL += " ,'" +  dsData.Tables["installment"].Rows[iInst]["inst_no"].ToString() + "' ";
                if (dsData.Tables["installment"].Rows[iInst]["inst_duedate"].ToString() == "__/__/____" ||
                    dsData.Tables["installment"].Rows[iInst]["inst_duedate"].ToString() == "")
                {
                    strSQL += " , null ";
                }
                else
                {
                    strSQL += " ,'" + cmDate.convertDateStringForDB(dsData.Tables["installment"].Rows[iInst]["inst_duedate"].ToString()) + "' " + System.Environment.NewLine;
                }
                strSQL += " ,'" +  dsData.Tables["installment"].Rows[iInst]["inst_rate"].ToString() + "' ";
                strSQL += " ,'" +  dsData.Tables["installment"].Rows[iInst]["inst_net"].ToString().Replace(",", "") + "' "; 
                strSQL += " ,'" +  dsData.Tables["installment"].Rows[iInst]["inst_rvno"].ToString() + "' ";
                if (dsData.Tables["installment"].Rows[iInst]["inst_paiddate"].ToString() == "__/__/____" ||
                    dsData.Tables["installment"].Rows[iInst]["inst_paiddate"].ToString() == "")
                {
                    strSQL += " , null ";
                }
                else
                {
                    strSQL += " ,'" + cmDate.convertDateStringForDB(dsData.Tables["installment"].Rows[iInst]["inst_paiddate"].ToString()) + "' ";
                }
                strSQL += " ,'" +  dsData.Tables["installment"].Rows[iInst]["inst_paid"].ToString().Replace(",", "") + "' " + System.Environment.NewLine;
                strSQL += " ,'" +  TDS.Utility.MasterUtil.replaceSpecialSQL(dsData.Tables["installment"].Rows[iInst]["inst_remark"].ToString()) + "' ";
                strSQL += " ,'" + strPayID + "' ";
                strSQL += " ,getDate() ";
                strSQL += " ,'" + dsData.Tables["installment"].Rows[iInst]["LOGINNAME"].ToString() + "' ";
                strSQL += " ,null "; // update_date
                strSQL += " ,'' "; // update_login
                strSQL += " ) " + System.Environment.NewLine;
                executeUpdate(strSQL, conn, trans);
            }
            //5. update synv_policy.spol_settlementdate
            strSQL = "UPDATE sync_policy SET ";
            strSQL += " spol_settlementdate = '" + cmDate.convertDateStringForDB(dsData.Tables[0].Rows[0]["settle_date"].ToString()) + "' ";            
            strSQL += " , update_date = getDate() ";
            strSQL += " , update_login = '" + dsData.Tables["settlement"].Rows[0]["LOGINNAME"].ToString() + "' ";
            strSQL += " WHERE spol_policy = '" + dsData.Tables["settlement"].Rows[0]["policy_no"].ToString() + "' ";
            executeUpdate(strSQL, conn, trans);

            trans.Commit();
        }
        catch (Exception e)
        {
            strRet = "ERROR";
            trans.Rollback();

            TDS.Utility.MasterUtil.writeLog("SettlementManager.addDataSettlement() - ERROR", e.ToString());
            TDS.Utility.MasterUtil.writeLog("SettlementManager.addDataSettlement() - SQL", strSQL);
        }
        finally
        {
            trans.Dispose();
            conn.Dispose();
            conn.Close();
        }


        return strRet;
    }


    public DataSet searchDataPayInstallment(string strPolNo, string strInsurerName,
                                        string strBranchCode, string strAgentCode,
                                        string strPeriodFromDate1, string strPeriodFromDate2,
                                        string strPayFromDate, string strPayToDate)
    {
        DataSet ds = new DataSet();
        string strSQL = "";
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();

        strSQL = "SELECT B.policy_no, A.spol_custfirstname + ' ' + A.spol_custlastname as policy_insurername " + System.Environment.NewLine;
        strSQL += " , A.spol_effectivefromdate as policy_periodfrom, A.spol_effectivetodate as policy_periodto " + System.Environment.NewLine;
        strSQL += " , B.settle_totalpremium, B.settle_balance, B.settle_installments, B.settle_paid, B.settle_arrear " + System.Environment.NewLine;
        strSQL += " , C.pay_amt, C.pay_date, C.pay_id " + System.Environment.NewLine;        
        strSQL += " , null as notice_date ";
        strSQL += " FROM sync_policy A  ";
        strSQL += "     INNER JOIN pay_payin C ON A.spol_policy = C.policy_no ";
        strSQL += "     INNER JOIN pay_settlement B ON A.spol_policy = B.policy_no ";        
        strSQL += " WHERE A.spol_policy != '' AND ";
        if (strPolNo != "")
            strSQL += " A.policy_no like '%" + strPolNo + "%' AND ";
        if (strInsurerName != "")
            strSQL += " A.spol_custfirstname + ' ' + A.spol_custlastname like '%" + strInsurerName + "%' AND ";
        if (strBranchCode != "")
            strSQL += " policy_branch = '" + strBranchCode + "' AND ";
        if (strAgentCode != "")
            strSQL += " policy_agent = '" + strAgentCode + "' AND ";
        if (strPeriodFromDate1 != "__/__/____" && strPeriodFromDate1 != "")
            strSQL += " policy_periodfrom >= '" + cmDate.convertDateStringForDB(strPeriodFromDate1) + "' AND ";
        if (strPeriodFromDate2 != "__/__/____" && strPeriodFromDate2 != "")
            strSQL += " policy_periodfrom <= '" + cmDate.convertDateStringForDB(strPeriodFromDate2) + "' AND ";
        if (strPayFromDate != "__/__/____" && strPayFromDate != "")
            strSQL += " pay_date >= '" + cmDate.convertDateStringForDB(strPayFromDate) + "' AND ";
        if (strPayToDate != "__/__/____" && strPayToDate != "")
            strSQL += " pay_date <= '" + cmDate.convertDateStringForDB(strPayToDate) + "' AND ";

        if (strSQL.EndsWith("AND ") == true)
            strSQL = strSQL.Substring(0, strSQL.Length - 4);
        ds = executeQuery(strSQL);
        return ds;
    }
    
    public DataSet getDataPayInstallmentByPayID(string strPayID)
    {
        DataSet ds = new DataSet();
        string strSQL = "";
        string strWhere = "";
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();

        strSQL = "SELECT *,spol_custpre + spol_custfirstname + ' ' + spol_custlastname as spol_custname ";
        strSQL += " ,spol_compre + spol_comfirstname + ' ' + spol_comlastname as spol_comname ";
        strSQL += " FROM sync_policy A  ";
        strSQL += "     INNER JOIN pay_payin C ON A.spol_policy = C.policy_no ";
        strSQL += "     INNER JOIN pay_settlement B ON A.spol_policy = B.policy_no ";
        strSQL += " WHERE C.pay_id = '" + strPayID + "' ";


        ds = executeQuery(strSQL);
        return ds;
    }

    public DataSet searchDataEndorse(string strEndorseNo,
                                        string strPolNo, string strInsurerName,
                                        string strBranchCode, string strAgentCode,
                                        string strPeriodFromDate1, string strPeriodFromDate2,
                                        string strEndorseDate1, string strEndorsseDate2)
    {
        DataSet ds = new DataSet();
        string strSQL = "";
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();

        strSQL = "SELECT * ";
        strSQL += " FROM ins_policy A INNER JOIN ins_endorsement B ON A.policy_no = B.policy_no ";
        strSQL += " WHERE A.policy_no != '' AND ";
        if (strEndorseNo != "")
            strSQL += " B.endorse_no = '" + strEndorseNo + "' AND ";
        if (strPolNo != "")
            strSQL += " A.policy_no = '" + strPolNo + "' AND ";
        if (strInsurerName != "")
            strSQL += " A.policy_insurername = '" + strInsurerName + "' AND ";
        if (strBranchCode != "")
            strSQL += " A.policy_branch = '" + strBranchCode + "' AND ";
        if (strAgentCode != "")
            strSQL += " A.policy_agent = '" + strAgentCode + "' AND ";
        if (strPeriodFromDate1 != "__/__/____" && strPeriodFromDate1 != "")
            strSQL += " A.policy_periodfrom >= '" + cmDate.convertDateStringForDB(strPeriodFromDate1) + "' AND ";
        if (strPeriodFromDate2 != "__/__/____" && strPeriodFromDate2 != "")
            strSQL += " A.policy_periodfrom <= '" + cmDate.convertDateStringForDB(strPeriodFromDate2) + "' AND ";
        if (strEndorseDate1 != "__/__/____" && strEndorseDate1 != "")
            strSQL += " B.endorse_date >= '" + cmDate.convertDateStringForDB(strEndorseDate1) + "' AND ";
        if (strEndorsseDate2 != "__/__/____" && strEndorsseDate2 != "")
            strSQL += " B.endorse_date <= '" + cmDate.convertDateStringForDB(strEndorsseDate2) + "' AND ";
        if (strSQL.EndsWith("AND ") == true)
            strSQL = strSQL.Substring(0, strSQL.Length - 4);
        ds = executeQuery(strSQL);
        return ds;
    }

}
