using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Module_MasterData_RoleForm : System.Web.UI.Page
{
    public string[] ColumnNameMenu = { "MODU_ID", "MODU_NAME" };
    public string[] ColumnTypeMenu = { "string", "string" };
    public string[] ColumnNameTable = { "MENU_ID", "MENU_NAME" };
    public string[] ColumnTypeTable = { "string", "string" };
    public string[] ColumnNameJob = { "RGJ_ID", "RGJ_NAME" };
    public string[] ColumnTypeJob = { "string", "string" };
    public string[] ColumnBranchTitle = { "No","สาขา", "" };
    public string[] ColumnBranchName = { "branch_code", "branch_name","" };
    public string[] ColumnBranchType = { "string","string","string"};
    public string[] ColumnBranchAlign = { "center","center","center"};
    public string[] ColumnBranchWidth = { "20%","60%","20%"};
    public string[] ColumnDSBranchName = { "branch_name","branch_code","role_id","robr_status","insert_date", "insert_login","update_date","update_login"};
    public string[] ColumnDSBranchType = { "string","string","string","string","datetime","string","datetime","string"};
    public string CurrentRoleID = "";
    DataView dv = new DataView();

    protected void Page_Load(object sender, EventArgs e)
    {
        ProfileData loginData = FormatStringApp.getLoginSession(Page, Session);
        MasterDataManager cm = new MasterDataManager();
        DataSet ds = new DataSet();
        if (!IsPostBack)
        {
            initialDropDownList();
            IntitialGrid();
            ddlStatus.SelectedValue = "A";
            txtMode.Text = "A";
            showData();
            if (Request.QueryString["roleid"] != null)
            {
                txtMode.Text = "E";
                txtRoleID.Text = Request.QueryString["roleid"];
                ds = cm.getDataRoleByID(txtRoleID.Text);
                docToUI(ds);
                CurrentRoleID = txtRoleID.Text;
            }
            if (RadioS.Checked == true)
            {
                btnAddBranch.Visible = true;
                DtgBranch.Visible = true;
            }
            else 
            {
                btnAddBranch.Visible = false;
                DtgBranch.Visible = false;
            }
            setMode();
        }
        else
        {
            //setValueDiabledControl();
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        string strRet = "";
        bool isRet = true;
        DataSet ds;
        DataSet dsBranch = (DataSet)ViewState["DSMasterTable"];
        ProfileData loginData = FormatStringApp.getLoginSession(Page, Session);
        MasterDataManager cm = new MasterDataManager();
        litJavaScript.Text = "<script>";
        ds = UIToDoc();
        if (txtMode.Text == "A")
        {
            strRet = cm.addDataRole(ds,dsBranch);
            if (strRet.StartsWith("ERROR"))
            {
                litJavaScript.Text += "alert('" + cm.getDataErrorByPageNameNo("", "002") + "');";
            }
            else
            {
                litJavaScript.Text += "alert('" + cm.getDataErrorByPageNameNo("", "001") + "');";
                txtMode.Text = "E";
                ds.Tables[0].Rows[0]["ROLE_ID"] = strRet;
                txtRoleID.Text = strRet;
            }
        }
        else if (txtMode.Text == "E")
        {
            isRet = cm.editDataRole(ds,dsBranch);
            if (isRet == false)
            {
                litJavaScript.Text += "alert('" + cm.getDataErrorByPageNameNo("", "002") + "');";
            }
            else
            {
                litJavaScript.Text += "alert('" + cm.getDataErrorByPageNameNo("", "001") + "');";
            }
        }
        litJavaScript.Text += "</script>";
        docToUI(ds);
        setMode();
    }
    protected void imgEdit_Click(object sender, ImageClickEventArgs e)
    {
        litJavaScript.Text = "";
        ImageButton imgClick = (ImageButton)sender;
        DtgBranch.EditItemIndex = Convert.ToInt32(imgClick.Attributes["dsindex"]);
        showBranch();
        GridViewUtil.ItemDataBound(DtgBranch.Items);
    }
    protected void imgDelete_Click(object sender, ImageClickEventArgs e)
    {
        litJavaScript.Text = "";
        ImageButton imgClick = (ImageButton)sender;
        int iIndex = Convert.ToInt32(imgClick.Attributes["dsindex"]);
        DataSet ds;
        ds = (DataSet)ViewState["DSMasterTable"];
        ds.Tables[0].Rows.RemoveAt(iIndex);
        ViewState["DSMasterTable"] = ds;
        DtgBranch.EditItemIndex = -1;
        showBranch();
    }
    protected void imgSave_Click(object sender, ImageClickEventArgs e)
    {
        ImageButton imgClick = (ImageButton)sender;
        DataSet ds;
        DropDownList ddlBranchCode;
        ProfileData loginData = FormatStringApp.getLoginSession(Page, Session);

        //DropDownList ddlStatus;
        litJavaScript.Text = "";
        int iIndex = Convert.ToInt32(imgClick.Attributes["dsindex"]);
        ds = (DataSet)ViewState["DSMasterTable"];
        ddlBranchCode = (DropDownList)DtgBranch.Items[iIndex].FindControl("ddlBranchCode");
        if (ddlBranchCode.SelectedValue == "")
        {
            litJavaScript.Text = "<script>alert('!ยังไม่ได้ระบุสาขาที่ต้องการ ');</script>";
        }
        else if (ds.Tables[0].Select("branch_code = '" + ddlBranchCode.SelectedValue + "'", "").Length > 0)
        {
            litJavaScript.Text = "<script>alert('!สาขาที่ต้องการ มีการกำหนดสิทธิ์แล้ว ');</script>";
        }
        else
        {
            ds.Tables[0].Rows[iIndex]["branch_name"] = ddlBranchCode.SelectedItem;
            ds.Tables[0].Rows[iIndex]["branch_code"] = ddlBranchCode.SelectedValue;
            ds.Tables[0].Rows[iIndex]["role_id"] = txtRoleID.Text ;
            ds.Tables[0].Rows[iIndex]["robr_status"]="A";
            ds.Tables[0].Rows[iIndex]["insert_date"] = DateTime.Now;
            ds.Tables[0].Rows[iIndex]["insert_login"] = loginData.loginName ;
            ds.Tables[0].Rows[iIndex]["update_date"] = DateTime.Now;
            ds.Tables[0].Rows[iIndex]["update_login"] = loginData.loginName ;
            ViewState["DSMasterTable"] = ds;
            DtgBranch.EditItemIndex = -1;
            showBranch();
        }
    }
    protected void imgCancel_Click(object sender, ImageClickEventArgs e)
    {
        litJavaScript.Text = "";
        ImageButton imgClick = (ImageButton)sender;
        int iIndex = Convert.ToInt32(imgClick.Attributes["dsindex"]);
        DataSet ds;
        ds = (DataSet)ViewState["DSMasterTable"];
        DropDownList  ddlBranchCode = (DropDownList)DtgBranch.Items[iIndex].FindControl("ddlBranchCode");
        if ((ddlBranchCode.SelectedValue == "" && ds.Tables[0].Rows[iIndex]["branch_code"]=="") || (ds.Tables[0].Rows[iIndex]["branch_code"]=="")) { 
            ds.Tables[0].Rows.RemoveAt(iIndex);
        }
        ViewState["DSMasterTable"] = ds;
        DtgBranch.EditItemIndex = -1;
        showBranch();
    }
    protected void btnAddBranch_Click(object sender, EventArgs e)
    {
        DataSet ds;
        ds = (DataSet)ViewState["DSMasterTable"];
        ds.Tables[0].Rows.InsertAt(ds.Tables[0].NewRow(), 0);
        ViewState["DSMasterTable"] = ds;
        DtgBranch.EditItemIndex = 0;
        showBranch();
    }

    protected void dtg_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        MasterDataManager cm = new MasterDataManager();        
        DataSet ds;
        DataGrid dtgTmp;
        TextBox txtDtgModule;

        txtDtgModule = (TextBox)e.Item.Cells[0].FindControl("txtModule");


        if (e.Item.ItemType == ListItemType.Item ||
            e.Item.ItemType == ListItemType.AlternatingItem)
        {
            dtgTmp = (DataGrid)e.Item.Cells[0].FindControl("dtgTable");
            ds = cm.getMenuByModule(txtDtgModule.Text);
            if (dtgTmp != null)
            {
                dtgTmp.DataSource = ds;
                dtgTmp.DataBind();
            }            
        }
    }

    protected void DtgBranch_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        MasterDataManager cm = new MasterDataManager();
        DataSet ds;
        DropDownList ddlBranchCode;

        ProfileData loginData = FormatStringApp.getLoginSession(Page, Session);
        if (e.Item.ItemType == ListItemType.EditItem)
        {
            // ddlBranchCode = (DropDownList)e.Item.FindControl("ddlBranchCode");
            ds = cm.getAllDataBranch();
            ddlBranchCode = (DropDownList)e.Item.FindControl("ddlBranchCode");
            ddlBranchCode.DataSource = ds;
            ddlBranchCode.DataTextField = "branch_name";
            ddlBranchCode.DataValueField = "branch_code";
            ddlBranchCode.DataBind();
            ddlBranchCode.Items.Insert(0, new ListItem("-- choose -- ", ""));
        }
    }

    protected void dtgTable_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        MasterDataManager cm = new MasterDataManager();
        DataSet ds;
        DataGrid dtgTmp;
        CheckBox chkDtgMenu;
        CheckBox chkAdd;
        CheckBox chkEdit;
        CheckBox chkDelete;
        CheckBox chkInquiry;
        string strMenuid="";

        

        if (e.Item.ItemType == ListItemType.Item ||
            e.Item.ItemType == ListItemType.AlternatingItem)
        {
            chkDtgMenu = (CheckBox)e.Item.Cells[0].FindControl("chkTable");
            strMenuid = chkDtgMenu.Attributes["table_id"];
           // dtgTmp = (DataGrid)e.Item.Cells[0].FindControl("dtgTable");
            ds = cm.getModuleByID(strMenuid);
            chkAdd = (CheckBox)e.Item.Cells[0].FindControl("chkFlagAdd");
            chkEdit = (CheckBox)e.Item.Cells[0].FindControl("chkFlagEdit");
            chkDelete = (CheckBox)e.Item.Cells[0].FindControl("chkFlagDelete");
            chkInquiry = (CheckBox)e.Item.Cells[0].FindControl("chkFlagInquiry");

            if (ds.Tables[0].Rows[0]["menu_flagadd"].ToString() != "Y")
            {
                chkAdd.Visible = false;
            }
            if (ds.Tables[0].Rows[0]["menu_flagedit"].ToString() != "Y")
            {
                chkEdit.Visible = false;
            }
            if (ds.Tables[0].Rows[0]["menu_flagdelete"].ToString() != "Y")
            {
                chkDelete.Visible = false;
            }
            if (ds.Tables[0].Rows[0]["menu_flaginquiry"].ToString() != "Y")
            {
                chkInquiry.Visible = false;
            }

            //if (dtgTmp != null)
            //{
            //    dtgTmp.DataSource = ds;
            //    dtgTmp.DataBind();
            //}
        }
    }

    public void initialDropDownList()
    {
        //ProfileData loginData = FormatStringApp.getLoginSession(Page, Session);
        MasterDataManager cm = new MasterDataManager();
        DataSet ds;

        ds = FormatStringApp.getAllDataStatus();
        ddlStatus.DataSource = ds;
        ddlStatus.DataTextField = "status_name";
        ddlStatus.DataValueField = "status_code";
        ddlStatus.DataBind();
        
    }
    public void docToUI(DataSet dsData)
    {
        DataSet dsMenu = new DataSet();
        DataSet dsTable = new DataSet();
        DataSet dsJob = new DataSet();
        MasterDataManager cm = new MasterDataManager();
        CheckBox chkTmp;
        CheckBox chkTable;
        CheckBox chkTmpFlagAdd;
        CheckBox chkTmpFlagEdit;
        CheckBox chkTmpFlagDelete;
        CheckBox chkTmpFlagInquiry;
        DataGrid dtgTable;
        string menuList = "";
        string menuFlagAdd = "";
        string menuFlagEdit = "";
        string menuFlagDelete = "";
        string menuFlagInquiry = "";

        if (dsData.Tables[0].Rows.Count > 0)
        {
            litJavaScript.Text += "<script>";
            txtRoleID.Text = dsData.Tables[0].Rows[0]["ROLE_ID"].ToString();
            txtRoleName.Text = dsData.Tables[0].Rows[0]["ROLE_NAME"].ToString();
            ddlStatus.SelectedValue = dsData.Tables[0].Rows[0]["ROLE_STATUS"].ToString();
            litJavaScript.Text += "</script>\n";

            dsMenu = cm.getMenuByRoleID(txtRoleID.Text);
            //dsTable = cm.getTableByRoleID(txtRoleID.Text);
            //dsJob = cm.getJobByRoleID(txtRoleID.Text);
            for (int j = 0; j <= dtgMenu.Items.Count - 1; j++)
            {
                //chkTmp = (CheckBox)dtgMenu.Items[j].Cells[0].FindControl("chkMenu");
                // Table
                dtgTable = (DataGrid)dtgMenu.Items[j].Cells[0].FindControl("dtgTable");
                //if (dsMenu.Tables[0].Select("menu_id = '" + chkTmp.Attributes["menu_id"] + "'").Length > 0)
                //{
                //    chkTmp.Checked = true;
                //}
                //else
                //{
                //    chkTmp.Checked = false;
                //}
                for (int k = 0; k <= dtgTable.Items.Count - 1; k++)
                {
                    chkTable = (CheckBox)dtgTable.Items[k].Cells[0].FindControl("chkTable");
                    chkTmpFlagAdd = (CheckBox)dtgTable.Items[k].Cells[0].FindControl("chkFlagAdd");
                    chkTmpFlagEdit = (CheckBox)dtgTable.Items[k].Cells[0].FindControl("chkFlagEdit");
                    chkTmpFlagDelete = (CheckBox)dtgTable.Items[k].Cells[0].FindControl("chkFlagDelete");
                    chkTmpFlagInquiry = (CheckBox)dtgTable.Items[k].Cells[0].FindControl("chkFlagInquiry");
                    if (chkTmpFlagAdd.Checked == true)
                    {
                        menuFlagAdd = chkTmpFlagAdd.Attributes["flag_id"];
                    }
                    else
                    {
                        menuFlagAdd = "N";
                    }
                    if (chkTmpFlagEdit.Checked == true)
                    {
                        menuFlagEdit = chkTmpFlagEdit.Attributes["flag_id"];
                    }
                    else
                    {
                        menuFlagEdit = "N";
                    }
                    if (chkTmpFlagDelete.Checked == true)
                    {
                        menuFlagDelete = chkTmpFlagDelete.Attributes["flag_id"];
                    }
                    else
                    {
                        menuFlagDelete = "N";
                    }
                    if (chkTmpFlagInquiry.Checked == true)
                    {
                        menuFlagInquiry = chkTmpFlagInquiry.Attributes["flag_id"];
                    }
                    else
                    {
                        menuFlagInquiry = "N";
                    }

                    if (chkTable.Checked)
                        menuList += chkTable.Attributes["table_id"] + "-" + menuFlagAdd + "_" + menuFlagEdit + "_" + menuFlagDelete + "_" + menuFlagInquiry + ",";
                    if (dsMenu.Tables[0].Select("menu_id = '" + chkTable.Attributes["table_id"] + "'" ).Length > 0)
                    {
                        chkTable.Checked = true;
                    }
                    else
                    {
                        chkTable.Checked = false;
                    }
                    if (dsMenu.Tables[0].Select("menu_id = '" + chkTable.Attributes["table_id"] + "' and role_menu_flagadd = '" + chkTmpFlagAdd.Attributes["flag_id"] + "'").Length > 0)
                    {
                        chkTmpFlagAdd.Checked = true;
                    }
                    else
                    {
                        chkTmpFlagAdd.Checked = false;
                    }
                    if (dsMenu.Tables[0].Select("menu_id = '" + chkTable.Attributes["table_id"] + "' and role_menu_flagedit = '" + chkTmpFlagEdit.Attributes["flag_id"] + "'").Length > 0)
                    {
                        chkTmpFlagEdit.Checked = true;
                    }
                    else
                    {
                        chkTmpFlagEdit.Checked = false;
                    }
                    if (dsMenu.Tables[0].Select("menu_id = '" + chkTable.Attributes["table_id"] + "' and role_menu_flagdelete = '" + chkTmpFlagDelete.Attributes["flag_id"] + "'").Length > 0)
                    {
                        chkTmpFlagDelete.Checked = true;
                    }
                    else
                    {
                        chkTmpFlagDelete.Checked = false;
                    }
                    if (dsMenu.Tables[0].Select("menu_id = '" + chkTable.Attributes["table_id"] + "' and role_menu_flaginquiry = '" + chkTmpFlagInquiry.Attributes["flag_id"] + "'").Length > 0)
                    {
                        chkTmpFlagInquiry.Checked = true;
                    }
                    else
                    {
                        chkTmpFlagInquiry.Checked = false;
                    }
                }
                // Job
                //dtgTable = (DataGrid)dtgMenu.Items[j].Cells[0].FindControl("dtgJob");
                //if (dsMenu.Tables[0].Select("menu_id = '" + chkTmp.Attributes["menu_id"] + "'").Length > 0)
                //{
                //    chkTmp.Checked = true;
                //}
                //else
                //{
                //    chkTmp.Checked = false;
                //}
                //for (int k = 0; k <= dtgTable.Items.Count - 1; k++)
                //{
                //    chkTable = (CheckBox)dtgTable.Items[k].Cells[0].FindControl("chkJob");
                //    if (dsJob.Tables[0].Select("rgj_id = '" + chkTable.Attributes["job_id"] + "'").Length > 0)
                //    {
                //        chkTable.Checked = true;
                //    }
                //    else
                //    {
                //        chkTable.Checked = false;
                //    }
                //}
            }
        }
    }
    public DataSet UIToDoc()
    {
        DataSet dsData = new DataSet();        
        DataTable dt = new DataTable();
        string[] ColumnType = {"string", "string", "string", "string","string", "string", "string", "string"};
        string[] ColumnName = {"ROLE_ID", "ROLE_NAME", "ROLE_STATUS","ROLE_FLAGBRANCH", "LOGINNAME", "MENU", "TABLE", "JOB"};
        ProfileData loginData = FormatStringApp.getLoginSession(Page, Session);
        string menuList = "";
        string tableList = "";
        string jobList = "";
        string menuFlagAdd = "";
        string menuFlagEdit = "";
        string menuFlagDelete = "";
        string menuFlagInquiry = "";
        DataGrid dtgTmp;
        CheckBox chkTmpMenu;
        CheckBox chkTmpTable;
        CheckBox chkTmpFlagAdd;
        CheckBox chkTmpFlagEdit;
        CheckBox chkTmpFlagDelete;
        CheckBox chkTmpFlagInquiry;
        dt = TDS.Utility.MasterUtil.AddColumnToDataTable(dt, ColumnType, ColumnName);
        dsData.Tables.Add(dt);
        dsData.Tables[0].Rows.Add(dsData.Tables[0].NewRow());
        dsData.Tables[0].Rows[0]["ROLE_ID"] = txtRoleID.Text;
        dsData.Tables[0].Rows[0]["ROLE_NAME"] = txtRoleName.Text;
        dsData.Tables[0].Rows[0]["ROLE_STATUS"] = ddlStatus.SelectedValue;
        dsData.Tables[0].Rows[0]["LOGINNAME"] = loginData.loginName;
        if (RadioA.Checked ==true )
        {
            dsData.Tables[0].Rows[0]["ROLE_FLAGBRANCH"] = "A";
        }
        else if (RadioB.Checked == true)
        {
            dsData.Tables[0].Rows[0]["ROLE_FLAGBRANCH"] = "B";
        }
        else {
            dsData.Tables[0].Rows[0]["ROLE_FLAGBRANCH"] = "S";
        }
        for (int i = 0; i <= dtgMenu.Items.Count - 1; i++)
        {
            //chkTmpMenu = (CheckBox)dtgMenu.Items[i].Cells[0].FindControl("chkMenu");
            // Table
            dtgTmp = (DataGrid)dtgMenu.Items[i].Cells[0].FindControl("dtgTable");
            //if (chkTmpMenu.Checked)
                //menuList += chkTmpMenu.Attributes["menu_id"] + ",";
            for (int j = 0; j <= dtgTmp.Items.Count - 1; j++)
            {
                chkTmpTable = (CheckBox)dtgTmp.Items[j].Cells[0].FindControl("chkTable");
                chkTmpFlagAdd = (CheckBox)dtgTmp.Items[j].Cells[0].FindControl("chkFlagAdd");
                chkTmpFlagEdit = (CheckBox)dtgTmp.Items[j].Cells[0].FindControl("chkFlagEdit");
                chkTmpFlagDelete = (CheckBox)dtgTmp.Items[j].Cells[0].FindControl("chkFlagDelete");
                chkTmpFlagInquiry = (CheckBox)dtgTmp.Items[j].Cells[0].FindControl("chkFlagInquiry");
                if (chkTmpFlagAdd.Checked == true )
                {
                    menuFlagAdd = chkTmpFlagAdd.Attributes["flag_id"];
                }else{
                    menuFlagAdd = "N";
                }
                if (chkTmpFlagEdit.Checked == true)
                {
                    menuFlagEdit = chkTmpFlagEdit.Attributes["flag_id"];
                }else{
                    menuFlagEdit = "N";
                }
                if (chkTmpFlagDelete.Checked == true)
                {
                    menuFlagDelete = chkTmpFlagDelete.Attributes["flag_id"];
                }else{
                    menuFlagDelete = "N";
                }
                if (chkTmpFlagInquiry.Checked == true)
                {
                    menuFlagInquiry  = chkTmpFlagInquiry.Attributes["flag_id"];
                }else{
                    menuFlagInquiry = "N";
                }

                if (chkTmpTable.Checked)
                    menuList += chkTmpTable.Attributes["table_id"] + "-" + menuFlagAdd + "_" + menuFlagEdit + "_" + menuFlagDelete + "_" + menuFlagInquiry + ",";
            }
            // Job
            //dtgTmp = (DataGrid)dtgMenu.Items[i].Cells[0].FindControl("dtgJob");
            //if (chkTmpMenu.Checked)
            //    menuList += chkTmpMenu.Attributes["menu_id"] + ",";
            //for (int j = 0; j <= dtgTmp.Items.Count - 1; j++)
            //{
            //    chkTmpTable = (CheckBox)dtgTmp.Items[j].Cells[0].FindControl("chkJob");
            //    if (chkTmpTable.Checked)
            //        jobList += chkTmpTable.Attributes["job_id"] + ",";
            //}
        }
        if (menuList.EndsWith(","))
            menuList = menuList.Substring(0, menuList.Length - 1);
        if (tableList.EndsWith(","))
            tableList = tableList.Substring(0, tableList.Length - 1);
        if (jobList.EndsWith(","))
            jobList = jobList.Substring(0, jobList.Length - 1);
        dsData.Tables[0].Rows[0]["MENU"] = menuList;
        dsData.Tables[0].Rows[0]["TABLE"] = tableList;
        dsData.Tables[0].Rows[0]["JOB"] = jobList;
        return dsData;
    }
    public void setMode()
    {
        if (txtMode.Text == "A")
        {
            btnAddNew.Visible = false;
        }
        else if (txtMode.Text == "E")
        {
            //txtRoleName.CssClass = "TEXTBOXDIS";
            //txtRoleName.ReadOnly = true;
            btnClear.Visible = false;
            btnAddNew.Visible = true;
        }
    }
    public void showData()
    {
        DataSet ds;
        MasterDataManager cm = new MasterDataManager();
        ProfileData loginData = new ProfileData();
        ds = cm.getAllModule();  
        dtgMenu.DataSource = ds;
        dtgMenu.DataBind();
    }

    public void showBranch()
    {
        DataSet ds=new DataSet() ;

        if (ViewState["DSMasterTable"] != null)
            ds = (DataSet)ViewState["DSMasterTable"];

        if (ds.Tables.Count > 0)
        {
            dv.Table = ds.Tables[0];
        }
        DtgBranch.DataSource = ds;
        DtgBranch.DataBind();
        GridViewUtil.ItemDataBound(DtgBranch.Items);
    }
    public void IntitialGrid()
    {
        DataSet ds = new DataSet();
        DataTable dt = new DataTable();
        MasterDataManager cm = new MasterDataManager();
        dt = TDS.Utility.MasterUtil.AddColumnToDataTable(dt, ColumnDSBranchType, ColumnDSBranchName);
        ds.Tables.Add(dt);
        ViewState.Add("DSMasterTable", ds);
        ds = cm.GetRoleBranch(CurrentRoleID);
        ViewState.Add("DSMasterTable", ds);
        showBranch();
        GridViewUtil.setGridStyle(DtgBranch , ColumnBranchWidth, ColumnBranchAlign, ColumnBranchTitle);
    }
    protected void RadioB_Click(object sender, EventArgs e)
    {
        DtgBranch.Visible = false;
        btnAddBranch.Visible = false;
    }
    protected void RadioA_Click(object sender, EventArgs e)
    {
        DtgBranch.Visible = false;
        btnAddBranch.Visible = false;
    }
    protected void RadioS_Click(object sender, EventArgs e)
    {
        DtgBranch.Visible = true;
        btnAddBranch.Visible = true;
    }
}
