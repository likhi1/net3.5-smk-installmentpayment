﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for IfxManager
/// </summary>
public class IfxManager : TDS.BL.BLWorker_MSSQL //TDS.BL.BLWorker_IFX_WS
{
	public IfxManager()
	{
		//
		// TODO: Add constructor logic here
		//
        strConnection = "strConInformixIns";
	}

    public DataSet getDataIfxVolun(string strDate)
    {
        DataSet ds = new DataSet();
        string strSQL = "";
        string strDBComp = "";
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();
        strSQL = "SELECT distinct 'V' as spol_type, pr.policy as spol_policy ,pr.pol_seq as spol_seq,mo.agt_cod as spol_agentcode, " + System.Environment.NewLine;
        strSQL += " pr.branch as spol_branch, mo.eff_fdte as spol_effectivefromdate, mo.eff_tdte as spol_effectivetodate, " + System.Environment.NewLine;
        strSQL += " mo.pre_grs as spol_grosspremium ,mo.pre_net as spol_netpremium, " + System.Environment.NewLine;
        strSQL += " cu.cust_pre as spol_custpre,cu.cust_fnam as spol_custfirstname,cu.cust_lnam as spol_custlastname, " +System.Environment.NewLine;
        strSQL += " cu.cust_addr as spol_custaddr1,cu.cust_addr2 as spol_custaddr2,cu.cust_zip as spol_custzip, " + System.Environment.NewLine;
        strSQL += " cu.cust_phone as spol_custphoneno, pr.receive_dte as spol_receivedate, " + System.Environment.NewLine;
        strSQL += " mo.license as spol_remark1, mo.chasis as spol_remark2, '' as spol_remark3, " + System.Environment.NewLine;
        strSQL += " '' as spol_remark4, '' spol_remark5, mo.sys_dte as spol_issuedate, " + System.Environment.NewLine;
        // พรบ.
        strSQL += " cpr.policy as spol_compolicy, cco.des as spol_compre, ccu.cust_nam as spol_comfirstname, ccu.cust_lnam as spol_comlastname, " + System.Environment.NewLine;
        strSQL += " cpr.license as spol_comlicense, cpr.chasis as spol_comchasis, cpr.pre_net as spol_comnetpremium, cpr.pre_grs as spol_comgrosspremium " + System.Environment.NewLine;
        strSQL += " FROM premium pr INNER JOIN motor mo ON mo.pol_yea = pr.pol_yea  AND  mo.license = pr.license " + System.Environment.NewLine;
        strSQL += "     AND  mo.chasis  = pr.chasis  AND  mo.period  = pr.period  AND  mo.running = '0'" + System.Environment.NewLine;
        strSQL += "     INNER JOIN customer cu ON cu.cust_cod = mo.cust_cod   AND    cu.cust_dte = mo.cust_dte " + System.Environment.NewLine;
        strSQL += "     INNER JOIN pol_agent ag ON pr.policy = ag.policy " + System.Environment.NewLine;
        strSQL += "     INNER JOIN subagent_type ty ON ag.agt_cod = ty.agt_cod  AND ag.sagt_cod  = ty.sagt_cod " + System.Environment.NewLine;        
        // รวม พรบ.
        strDBComp = getDBName("strConInformixMotor");
        strSQL += " LEFT JOIN " + strDBComp + ".dbo.policy cpr ON pr.chasis = cpr.chasis and pr.pol_yea = cpr.pol_yea and cpr.policy like '580010000%' " + System.Environment.NewLine;
        strSQL += " LEFT JOIN " + strDBComp + ".dbo.customer ccu ON cpr.cust_cod = ccu.cust_cod " + System.Environment.NewLine;
        strSQL += " LEFT JOIN " + strDBComp + ".dbo.code cco ON ccu.cust_pre = cco.code and cco.flag = 'TN' " + System.Environment.NewLine;


        strSQL += " WHERE SUBSTRING(pr.policy,1,3) =  '581' "; //pr.policy[1,3] 
        strSQL += "     AND ty.agent_typ = '005' " + System.Environment.NewLine;
        strSQL += "     AND mo.sys_dte >= '" + cmDate.convertDateStringForDB(strDate) + "' ";

        strConnection = "strConInformixIns";
        ds = executeQuery(strSQL);
        return ds;
    }

    public string addDataReceive(DataSet ds)
    {
        string strRet = "";
        // 1. chq_accept2

        // 2. cash2
        // 3. chq_cheque2
        // 4. credit_card2
        // 5. money_order2
        // 6. transfer2

        return strRet;
    }

    public string getDBName(string strKey)
    {
        string strConfig = System.Configuration.ConfigurationManager.AppSettings[strKey];
        string strDBName = "";
        int iStart = 0; 
        
        int iLen = 0;
        
        // SQL
        iStart = strConfig.IndexOf("Initial Catalog=");
        if (iStart > 0)
        {
            iStart = iStart + 16;
            iLen = strConfig.IndexOf(";", iStart) - iStart;
        }
        if (iStart == 0)
        {
            // IFX
            iStart = strConfig.IndexOf("Database=") + 9;
            iLen = strConfig.IndexOf(";", iStart) - iStart;
        }
        strDBName = strConfig.Substring(iStart, iLen);
        return strDBName;
    }
}
