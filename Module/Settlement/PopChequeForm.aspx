﻿<%@ Page Language="C#" MasterPageFile="~/MasterData/PopupMasterPage.master" AutoEventWireup="true" CodeFile="PopChequeForm.aspx.cs" Inherits="Module_Settlement_PopChequeForm" Title="Untitled Page" %>
<%@ Register src="../../UserControl/ucTxtDate.ascx" tagname="ucTxtDate" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script>
        function openPopupZoom() {
            openPopShadowBox("../../Module/Zoom/Zoom1.aspx", 500, 600, 'Zoom');
            return false;
        }
        function paymenttype_click() {
            document.getElementById("divCheque").style.display = "none";
            document.getElementById("divCashierCheque").style.display = "none";
            document.getElementById("divTransferCheque").style.display = "none";
            document.getElementById("divDraftCheque").style.display = "none";
            document.getElementById("divTransfer").style.display = "none";
            document.getElementById("divCash").style.display = "none";
            document.getElementById("divCreditCard").style.display = "none";
            document.getElementById("divAP").style.display = "none";
            if (document.getElementById("<%=rdoCheque.ClientID %>").checked == true) {
                document.getElementById("divCheque").style.display = "block";
            }
            else if (document.getElementById("<%=rdoCashierCheque.ClientID %>").checked == true) {
                document.getElementById("divCashierCheque").style.display = "block";
            }
            else if (document.getElementById("<%=rdoTransferCheque.ClientID %>").checked == true) {
                document.getElementById("divTransferCheque").style.display = "block";
            }
            else if (document.getElementById("<%=rdoDraftCheque.ClientID %>").checked == true) {
                document.getElementById("divDraftCheque").style.display = "block";
            }
            else if (document.getElementById("<%=rdoTransfer.ClientID %>").checked == true) {
                document.getElementById("divTransfer").style.display = "block";
            }
            else if (document.getElementById("<%=rdoCash.ClientID %>").checked == true) {
                document.getElementById("divCash").style.display = "block";
            }
            else if (document.getElementById("<%=rdoCreditCard.ClientID %>").checked == true) {
                document.getElementById("divCreditCard").style.display = "block";
            }
            else if (document.getElementById("<%=rdoAP.ClientID %>").checked == true) {
                document.getElementById("divAP").style.display = "block";
            }
        }
        function returnValue() {
             
            return true;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div id="page-inside">     
            <table width="100%">
                <tr>
                    <td>
                        <div class="heading1">:: รายละเอียดการรับชำระ::</div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td>
                                    <asp:Button ID="btnSave" runat="server" Text="ตกลง" cssClass="awesome"
                                        OnClientClick="return returnValue();" OnClick="btnSave_Click"/>
                                </td>
                                <td>
                                    <asp:Button ID="btnExit" runat="server" Text="ปิดหน้าจอ" CssClass="awesome" OnClientClick="parent.closeShadowBox(); return false;"/>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <fieldset>
                            <legend>ข้อมูลรับชำระ</legend>
                            <table width="100%">
                                <tr>
                                    <td align="right" width="20%">ประเภท :</td>
                                    <td align="left" width="50%">
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:RadioButton ID="rdoCheque" GroupName="PaymentType" runat="server" Text="เช็ค" Checked />
                                                </td>
                                                <td>
                                                    <asp:RadioButton ID="rdoCashierCheque" GroupName="PaymentType" runat="server" Text="Chashier cheque"/>
                                                </td>
                                                <td>
                                                    <asp:RadioButton ID="rdoTransferCheque" GroupName="PaymentType" runat="server" Text="เช็คใบโอน"/>
                                                </td>
                                                <td>
                                                    <asp:RadioButton ID="rdoDraftCheque" GroupName="PaymentType" runat="server" Text="Draft"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:RadioButton ID="rdoTransfer" GroupName="PaymentType" runat="server" Text="ใบโอน"/>
                                                </td>
                                                <td>
                                                    <asp:RadioButton ID="rdoCash" GroupName="PaymentType" runat="server" Text="เงินสด"/>
                                                </td>
                                                <td>
                                                    <asp:RadioButton ID="rdoCreditCard" GroupName="PaymentType" runat="server" Text="Credit card"/>
                                                </td>
                                                <td>
                                                    <asp:RadioButton ID="rdoAP" GroupName="PaymentType" runat="server" Text="เจ้าหนี้"/>
                                                </td>
                                            </tr>
                                        </table>                                        
                                    </td>
                                    <td align="right" width="20%"></td>
                                    <td align="left" width="10%"></td>
                                </tr>
                            </table>
                            <div id="divCheque" style="display:none">
                                <table>
                                    <tr>
                                        <td align="right" width="30%">เลขที่เช็ค :</td>
                                        <td align="left" width="70%">
                                            <asp:TextBox ID="txtChequeNo" CssClass="TEXTBOX" Width="50" MaxLength="5" runat="server"></asp:TextBox> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">วันที่ในเช็ค :</td>
                                        <td align="left">
                                            <uc1:ucTxtDate ID="txtChequeDate" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">ธนาคาร :</td>
                                        <td align="left">
                                            <asp:TextBox ID="txtTmpCode1" runat="server" CssClass="TEXTBOX" width="80"></asp:TextBox>
                                            <asp:ImageButton ID="imgPopup1" runat="server" 
                                                ImageUrl="../../Images/binoculars.gif" ToolTip="ค้นหาธนาคาร"
                                                style="width: 15px"  type="script" OnClientClick="return openPopupZoom();"/>
                                            <asp:TextBox ID="txtTmpName1" runat="server" CssClass="TEXTBOXDIS" width="200"></asp:TextBox>
                                        </td>                                        
                                    </tr>
                                    <tr>
                                        <td align="right">สาขา :</td>
                                        <td align="left">
                                            <asp:TextBox ID="txtTmpCode2" runat="server" CssClass="TEXTBOX" width="80"></asp:TextBox>
                                            <asp:ImageButton ID="imgPopup2" runat="server" 
                                                ImageUrl="../../Images/binoculars.gif" ToolTip="ค้นหาสาขา"
                                                style="width: 15px"  type="script" OnClientClick="return openPopupZoom();"/>
                                            <asp:TextBox ID="txtTmpName2" runat="server" CssClass="TEXTBOXDIS" width="200"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">เงินในเช็ค :</td>
                                        <td align="left">
                                            <asp:TextBox ID="txtChequeAmt" CssClass="TEXTBOXMONEY" MaxLength="18" runat="server"></asp:TextBox> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">Code บัญชี :</td>
                                        <td align="left">
                                            <asp:TextBox ID="txtTmpCode3" runat="server" CssClass="TEXTBOX" width="80"></asp:TextBox>
                                            <asp:ImageButton ID="imgPopup3" runat="server" 
                                                ImageUrl="../../Images/binoculars.gif" ToolTip="ค้นหาสาขา"
                                                style="width: 15px"  type="script" OnClientClick="return openPopupZoom();"/>
                                            <asp:TextBox ID="txtTmpName3" runat="server" CssClass="TEXTBOXDIS" width="200"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">สถานะ</td>
                                        <td align="left">      
                                            <asp:RadioButton ID="rdoStatus1" GroupName="Status" runat="server" Text="รอตัด" />  
                                            <asp:RadioButton ID="rdoStatus2" GroupName="Status" runat="server" Text="ฝากลอย" />  
                                            <asp:RadioButton ID="rdoStatus3" GroupName="Status" runat="server" Text="รับแทน สนญ." />  
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">หมายเหตุ :</td>
                                        <td align="left" colspan="3">
                                            <asp:TextBox ID="txtRemark" runat="server" CssClass="TEXTBOX" width="300" TextMode="MultiLine"></asp:TextBox>                                            
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div id="divCashierCheque" style="display:inline">
                                <table>
                                    <tr>
                                        <td align="right" width="30%">เลขที่เช็ค :</td>
                                        <td align="left" width="70%">
                                            <asp:TextBox ID="txtCashierChequeNo" CssClass="TEXTBOX" Width="50" MaxLength="5" runat="server"></asp:TextBox> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">วันที่ในเช็ค :</td>
                                        <td align="left">
                                            <uc1:ucTxtDate ID="txtCashierChequeDate" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">ธนาคาร :</td>
                                        <td align="left">
                                            <asp:TextBox ID="txtCashierChequeBankCode" runat="server" CssClass="TEXTBOX" width="80"></asp:TextBox>
                                            <asp:ImageButton ID="imgCashierChequeBank" runat="server" 
                                                ImageUrl="../../Images/binoculars.gif" ToolTip="ค้นหาธนาคาร"
                                                style="width: 15px"  type="script" OnClientClick="return openPopupZoom();"/>
                                            <asp:TextBox ID="txtCashierChequeBankName" runat="server" CssClass="TEXTBOXDIS" width="200"></asp:TextBox>
                                        </td>                                        
                                    </tr>
                                    <tr>
                                        <td align="right">สาขา :</td>
                                        <td align="left">
                                            <asp:TextBox ID="txtCashierChequeBranchCode" runat="server" CssClass="TEXTBOX" width="80"></asp:TextBox>
                                            <asp:ImageButton ID="imgCashierChequeBranch" runat="server" 
                                                ImageUrl="../../Images/binoculars.gif" ToolTip="ค้นหาสาขา"
                                                style="width: 15px"  type="script" OnClientClick="return openPopupZoom();"/>
                                            <asp:TextBox ID="txtCashierChequeBranchName" runat="server" CssClass="TEXTBOXDIS" width="200"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">เงินในเช็ค :</td>
                                        <td align="left">
                                            <asp:TextBox ID="txtCashierChequeAmt" CssClass="TEXTBOXMONEY" MaxLength="18" runat="server"></asp:TextBox> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">Code บัญชี :</td>
                                        <td align="left">
                                            <asp:TextBox ID="txtCashierChequeAcctCod" runat="server" CssClass="TEXTBOX" width="80"></asp:TextBox>
                                            <asp:ImageButton ID="imgCashierChequeAcct" runat="server" 
                                                ImageUrl="../../Images/binoculars.gif" ToolTip="ค้นหาสาขา"
                                                style="width: 15px"  type="script" OnClientClick="return openPopupZoom();"/>
                                            <asp:TextBox ID="txtCashierChequeAcctName" runat="server" CssClass="TEXTBOXDIS" width="200"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">สถานะ</td>
                                        <td align="left">      
                                            <asp:RadioButton ID="rdoCashierChequeStatus1" GroupName="Status" runat="server" Text="รอตัด" />  
                                            <asp:RadioButton ID="rdoCashierChequeStatus2" GroupName="Status" runat="server" Text="ฝากลอย" />  
                                            <asp:RadioButton ID="rdoCashierChequeStatus3" GroupName="Status" runat="server" Text="รับแทน สนญ." />  
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">หมายเหตุ :</td>
                                        <td align="left">
                                            <asp:TextBox ID="txtCashierChequeRemark" runat="server" CssClass="TEXTBOX" width="300" TextMode="MultiLine"></asp:TextBox>                                            
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div id="divTransferCheque" style="display:inline">
                                <table>
                                    <tr>
                                        <td align="right" width="30%">Payin เข้าบัญชี :</td>
                                        <td align="left" width="70%">
                                            <asp:TextBox ID="txtTransferChequePayInAcctCode" runat="server" CssClass="TEXTBOX" width="80"></asp:TextBox>
                                            <asp:ImageButton ID="imgTransferChequePayInAcct" runat="server" 
                                                ImageUrl="../../Images/binoculars.gif" ToolTip="ค้นหาบัญชี"
                                                style="width: 15px"  type="script" OnClientClick="return openPopupZoom();"/>
                                            <asp:TextBox ID="txtTransferChequePayInAcctName" runat="server" CssClass="TEXTBOXDIS" width="200"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">วันที่ Payin :</td>
                                        <td align="left">
                                            <uc1:ucTxtDate ID="txtTransferChequePayInDate" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">ธนาคาร :</td>
                                        <td align="left">
                                            <asp:TextBox ID="txtTransferChequePayInBankCode" runat="server" CssClass="TEXTBOXDIS" width="80"></asp:TextBox>
                                            <asp:TextBox ID="txtTransferChequePayInBankName" runat="server" CssClass="TEXTBOXDIS" width="200"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">สาขา :</td>
                                        <td align="left">
                                            <asp:TextBox ID="txtTransferChequePayInBranchCode" runat="server" CssClass="TEXTBOXDIS" width="80"></asp:TextBox>
                                            <asp:TextBox ID="txtTransferChequePayInBranchName" runat="server" CssClass="TEXTBOXDIS" width="200"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">ประเภทบัญชี :</td>
                                        <td align="left">
                                            <asp:TextBox ID="txtTransferChequePayInAcctTypeCode" runat="server" CssClass="TEXTBOXDIS" width="80"></asp:TextBox>
                                            <asp:TextBox ID="txtTransferChequePayInAcctTypeName" runat="server" CssClass="TEXTBOXDIS" width="200"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">ประเภทเช็ค :</td>
                                        <td align="left">
                                            <asp:RadioButton ID="rdoTransferChequeType1" GroupName="TransferChequeType" runat="server" Text="กรุงเทพ" />  
                                            <asp:RadioButton ID="rdoTransferChequeType2" GroupName="TransferChequeType" runat="server" Text="ต่างจังหวัด" />  
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                        <td align="right">เลขที่เช็ค :</td>
                                        <td align="left">
                                            <asp:TextBox ID="txtTransferChequeNo" CssClass="TEXTBOX" Width="50" MaxLength="5" runat="server"></asp:TextBox> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">วันที่ในเช็ค :</td>
                                        <td align="left">
                                            <uc1:ucTxtDate ID="txtTransferChequeDate" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">ธนาคาร :</td>
                                        <td align="left">
                                            <asp:TextBox ID="txtTransferChequeBankCode" runat="server" CssClass="TEXTBOX" width="80"></asp:TextBox>
                                            <asp:ImageButton ID="imgTransferChequeBank" runat="server" 
                                                ImageUrl="../../Images/binoculars.gif" ToolTip="ค้นหาธนาคาร"
                                                style="width: 15px"  type="script" OnClientClick="return openPopupZoom();"/>
                                            <asp:TextBox ID="txtTransferChequeBankName" runat="server" CssClass="TEXTBOXDIS" width="200"></asp:TextBox>
                                        </td>                                        
                                    </tr>
                                    <tr>
                                        <td align="right">สาขา :</td>
                                        <td align="left">
                                            <asp:TextBox ID="txtTransferChequeBranchCode" runat="server" CssClass="TEXTBOX" width="80"></asp:TextBox>
                                            <asp:ImageButton ID="imgTransferChequeBranch" runat="server" 
                                                ImageUrl="../../Images/binoculars.gif" ToolTip="ค้นหาสาขา"
                                                style="width: 15px"  type="script" OnClientClick="return openPopupZoom();"/>
                                            <asp:TextBox ID="txtTransferChequeBranchName" runat="server" CssClass="TEXTBOXDIS" width="200"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">เงินในเช็ค :</td>
                                        <td align="left">
                                            <asp:TextBox ID="txtTransferChequeAmt" CssClass="TEXTBOXMONEY" MaxLength="18" runat="server"></asp:TextBox> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">Code บัญชี :</td>
                                        <td align="left">
                                            <asp:TextBox ID="txtTransferChequeAcctCode" runat="server" CssClass="TEXTBOX" width="80"></asp:TextBox>
                                            <asp:ImageButton ID="imgTransferCheque" runat="server" 
                                                ImageUrl="../../Images/binoculars.gif" ToolTip="ค้นหาสาขา"
                                                style="width: 15px"  type="script" OnClientClick="return openPopupZoom();"/>
                                            <asp:TextBox ID="txtTransferChequeAcctName" runat="server" CssClass="TEXTBOXDIS" width="200"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">สถานะ</td>
                                        <td align="left">      
                                            <asp:RadioButton ID="rdoTransferChequeStatus1" GroupName="TransferChequeStatus" runat="server" Text="รอตัด" />  
                                            <asp:RadioButton ID="rdoTransferChequeStatus2" GroupName="TransferChequeStatus" runat="server" Text="ฝากลอย" />  
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">หมายเหตุ :</td>
                                        <td align="left">
                                            <asp:TextBox ID="txtTransferChequeRemark" runat="server" CssClass="TEXTBOX" width="300" TextMode="MultiLine"></asp:TextBox>                                            
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div id="divDraftCheque" style="display:inline">
                                <table>
                                    <tr>
                                        <td align="right" width="30%">เลขที่ draft :</td>
                                        <td align="left" width="70%">
                                            <asp:TextBox ID="txtDraftChequeNo" CssClass="TEXTBOX" Width="50" MaxLength="5" runat="server"></asp:TextBox> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">วันที่ในdraft :</td>
                                        <td align="left">
                                            <uc1:ucTxtDate ID="txtDraftChequeDate" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">ธนาคาร :</td>
                                        <td align="left">
                                            <asp:TextBox ID="txtDraftChequeBankCode" runat="server" CssClass="TEXTBOX" width="80"></asp:TextBox>
                                            <asp:ImageButton ID="imgDraftChequeBank" runat="server" 
                                                ImageUrl="../../Images/binoculars.gif" ToolTip="ค้นหาธนาคาร"
                                                style="width: 15px"  type="script" OnClientClick="return openPopupZoom();"/>
                                            <asp:TextBox ID="txtDraftChequeBankName" runat="server" CssClass="TEXTBOXDIS" width="200"></asp:TextBox>
                                        </td>                                        
                                    </tr>
                                    <tr>
                                        <td align="right">สาขา :</td>
                                        <td align="left">
                                            <asp:TextBox ID="txtDraftChequeBranchCode" runat="server" CssClass="TEXTBOX" width="80"></asp:TextBox>
                                            <asp:ImageButton ID="imgDraftChequeBranch" runat="server" 
                                                ImageUrl="../../Images/binoculars.gif" ToolTip="ค้นหาสาขา"
                                                style="width: 15px"  type="script" OnClientClick="return openPopupZoom();"/>
                                            <asp:TextBox ID="txtDraftChequeBranchName" runat="server" CssClass="TEXTBOXDIS" width="200"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">เงินในdraft :</td>
                                        <td align="left">
                                            <asp:TextBox ID="txtDraftChequeAmt" CssClass="TEXTBOXMONEY" MaxLength="18" runat="server"></asp:TextBox> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">Code บัญชี :</td>
                                        <td align="left">
                                            <asp:TextBox ID="txtDraftChequeAcctCode" runat="server" CssClass="TEXTBOX" width="80"></asp:TextBox>
                                            <asp:ImageButton ID="imgDraftChequeAcct" runat="server" 
                                                ImageUrl="../../Images/binoculars.gif" ToolTip="ค้นหาCodeบัญชี"
                                                style="width: 15px"  type="script" OnClientClick="return openPopupZoom();"/>
                                            <asp:TextBox ID="txtDraftChequeAcctName" runat="server" CssClass="TEXTBOXDIS" width="200"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">สถานะ</td>
                                        <td align="left">      
                                            <asp:RadioButton ID="rdoDraftChequeStatus1" GroupName="DraftChequeStatus" runat="server" Text="รอตัด" />  
                                            <asp:RadioButton ID="rdoDraftChequeStatus2" GroupName="DraftChequeStatus" runat="server" Text="ฝากลอย" />  
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">หมายเหตุ :</td>
                                        <td align="left">
                                            <asp:TextBox ID="txtDraftChequeRemark" runat="server" CssClass="TEXTBOX" width="300" TextMode="MultiLine"></asp:TextBox>                                            
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div id="divTransfer" style="display:inline">
                                <table>
                                    <tr>
                                        <td align="right" width="30%">รหัสโอนเงิน :</td>
                                        <td align="left" width="70%">
                                            <asp:TextBox ID="txtTransferNo" runat="server" CssClass="TEXTBOX" width="80"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">วันที่ในใบโอน :</td>
                                        <td align="left">
                                            <uc1:ucTxtDate ID="txtTransferDate" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" width="30%">โอนเข้าบัญชี :</td>
                                        <td align="left" width="70%">
                                            <asp:TextBox ID="txtTransferPayInAcctCode" runat="server" CssClass="TEXTBOX" width="80"></asp:TextBox>
                                            <asp:ImageButton ID="imgTransferPayInAcct" runat="server" 
                                                ImageUrl="../../Images/binoculars.gif" ToolTip="ค้นหาบัญชี"
                                                style="width: 15px"  type="script" OnClientClick="return openPopupZoom();"/>
                                            <asp:TextBox ID="txtTransferPayInAcctName" runat="server" CssClass="TEXTBOXDIS" width="200"></asp:TextBox>
                                        </td>
                                    </tr>                                    
                                    <tr>
                                        <td align="right">ธนาคาร :</td>
                                        <td align="left">
                                            <asp:TextBox ID="txtTransferPayInBankCode" runat="server" CssClass="TEXTBOXDIS" width="80"></asp:TextBox>
                                            <asp:TextBox ID="txtTransferPayInBankName" runat="server" CssClass="TEXTBOXDIS" width="200"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">สาขา :</td>
                                        <td align="left">
                                            <asp:TextBox ID="txtTransferPayInBranchCode" runat="server" CssClass="TEXTBOXDIS" width="80"></asp:TextBox>
                                            <asp:TextBox ID="txtTransferPayInBranchName" runat="server" CssClass="TEXTBOXDIS" width="200"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">ประเภทบัญชี :</td>
                                        <td align="left">
                                            <asp:TextBox ID="txtTransferPayInAcctTypeCode" runat="server" CssClass="TEXTBOXDIS" width="80"></asp:TextBox>
                                            <asp:TextBox ID="txtTransferPayInAcctTypeName" runat="server" CssClass="TEXTBOXDIS" width="200"></asp:TextBox>
                                        </td>
                                    </tr>                                    
                                    <tr>
                                        <td align="right">จำนวนเงิน :</td>
                                        <td align="left">
                                            <asp:TextBox ID="txtTransferAmt" CssClass="TEXTBOXMONEY" MaxLength="18" runat="server"></asp:TextBox> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">Code บัญชี :</td>
                                        <td align="left">
                                            <asp:TextBox ID="txtTransferAcctCode" runat="server" CssClass="TEXTBOX" width="80"></asp:TextBox>
                                            <asp:ImageButton ID="imgTransferAcct" runat="server" 
                                                ImageUrl="../../Images/binoculars.gif" ToolTip="ค้นหาสาขา"
                                                style="width: 15px"  type="script" OnClientClick="return openPopupZoom();"/>
                                            <asp:TextBox ID="txtTransferAcctName" runat="server" CssClass="TEXTBOXDIS" width="200"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">สถานะ</td>
                                        <td align="left">      
                                            <asp:RadioButton ID="rdoTransferStatus1" GroupName="TransferStatus" runat="server" Text="รอตัด" />  
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">หมายเหตุ :</td>
                                        <td align="left">
                                            <asp:TextBox ID="txtTransferRemark" runat="server" CssClass="TEXTBOX" width="300" TextMode="MultiLine"></asp:TextBox>                                            
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div id="divCash" style="display:inline">
                                <table>
                                    <tr>
                                        <td align="right" width="30%">วันที่รับเงิน :</td>
                                        <td align="left" width="70%">
                                            <uc1:ucTxtDate ID="txtCashDate" runat="server" />
                                        </td>
                                    </tr>                                                                      
                                    <tr>
                                        <td align="right">จำนวนเงิน :</td>
                                        <td align="left">
                                            <asp:TextBox ID="txtCashAmt" CssClass="TEXTBOXMONEY" MaxLength="18" runat="server"></asp:TextBox> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">Code บัญชี :</td>
                                        <td align="left">
                                            <asp:TextBox ID="txtCashAcctCode" runat="server" CssClass="TEXTBOX" width="80"></asp:TextBox>
                                            <asp:ImageButton ID="imgCashAcct" runat="server" 
                                                ImageUrl="../../Images/binoculars.gif" ToolTip="ค้นหาสาขา"
                                                style="width: 15px"  type="script" OnClientClick="return openPopupZoom();"/>
                                            <asp:TextBox ID="txtCashAcctName" runat="server" CssClass="TEXTBOXDIS" width="200"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">สถานะ</td>
                                        <td align="left">      
                                            <asp:RadioButton ID="rdoCashStatus1" GroupName="CashStatus" runat="server" Text="รอตัด" />  
                                            <asp:RadioButton ID="rdoCashStatus3" GroupName="CashStatus" runat="server" Text="รับแทน สนญ." />  
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">หมายเหตุ :</td>
                                        <td align="left">
                                            <asp:TextBox ID="txtCashRemark" runat="server" CssClass="TEXTBOX" width="300" TextMode="MultiLine"></asp:TextBox>                                            
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div id="divCreditCard" style="display:inline">
                                <table>
                                    <tr>
                                        <td align="right" width="30%">Payin เข้าบัญชี :</td>
                                        <td align="left" width="70%">
                                            <asp:TextBox ID="txtCreditCardPayInAcctCode" runat="server" CssClass="TEXTBOX" width="80"></asp:TextBox>
                                            <asp:ImageButton ID="imgCreditCardPayInAcct" runat="server" 
                                                ImageUrl="../../Images/binoculars.gif" ToolTip="ค้นหาบัญชี"
                                                style="width: 15px"  type="script" OnClientClick="return openPopupZoom();"/>
                                            <asp:TextBox ID="txtCreditCardPayInAcctName" runat="server" CssClass="TEXTBOXDIS" width="200"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">ธนาคาร :</td>
                                        <td align="left">
                                            <asp:TextBox ID="txtCreditCardPayInBankCode" runat="server" CssClass="TEXTBOXDIS" width="80"></asp:TextBox>
                                            <asp:TextBox ID="txtCreditCardPayInBankName" runat="server" CssClass="TEXTBOXDIS" width="200"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">สาขา :</td>
                                        <td align="left">
                                            <asp:TextBox ID="txtCreditCardPayInBranchCode" runat="server" CssClass="TEXTBOXDIS" width="80"></asp:TextBox>
                                            <asp:TextBox ID="txtCreditCardPayInBranchName" runat="server" CssClass="TEXTBOXDIS" width="200"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">ประเภทบัญชี :</td>
                                        <td align="left">
                                            <asp:TextBox ID="txtCreditCardPayInAcctTypeCode" runat="server" CssClass="TEXTBOXDIS" width="80"></asp:TextBox>
                                            <asp:TextBox ID="txtCreditCardPayInAcctTypeName" runat="server" CssClass="TEXTBOXDIS" width="200"></asp:TextBox>
                                        </td>
                                    </tr>                                    
                                    <tr>
                                        <td align="right">เลขที่บัตรเครดิต :</td>
                                        <td align="left">
                                            <asp:TextBox ID="txtCreditCardNo" CssClass="TEXTBOX" Width="50" MaxLength="5" runat="server"></asp:TextBox> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">เลขที่ Sales Slip :</td>
                                        <td align="left">
                                            <asp:TextBox ID="txtCreditCardSaleSlip" CssClass="TEXTBOX" Width="50" MaxLength="5" runat="server"></asp:TextBox> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">วันที่รูดบัตร :</td>
                                        <td align="left">
                                            <uc1:ucTxtDate ID="txtCreditCardDate" runat="server" />
                                        </td>
                                    </tr>                                    
                                    <tr>
                                        <td align="right">จำนวนเงิน :</td>
                                        <td align="left">
                                            <asp:TextBox ID="txtCreditCardAmt" CssClass="TEXTBOXMONEY" MaxLength="18" runat="server"></asp:TextBox> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">App. Code :</td>
                                        <td align="left">
                                            <asp:TextBox ID="txtCreditCardAppCode" CssClass="TEXTBOX" MaxLength="18" runat="server"></asp:TextBox> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">ค่าธรรมเนียม :</td>
                                        <td align="left">
                                            <asp:TextBox ID="txtCreditCardFeeRate" CssClass="TEXTBOXMONEY" MaxLength="5" runat="server"></asp:TextBox> %
                                            <asp:TextBox ID="txtCreditCardFeeBath" CssClass="TEXTBOXMONEY" MaxLength="18" runat="server"></asp:TextBox> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">ภาษี :</td>
                                        <td align="left">
                                            <asp:TextBox ID="txtCreditCardTax" CssClass="TEXTBOXMONEY" MaxLength="18" runat="server"></asp:TextBox> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">เงินเข้าบัญชีสุทธิ :</td>
                                        <td align="left">
                                            <asp:TextBox ID="txtCreditCardNet" CssClass="TEXTBOXMONEY" MaxLength="18" runat="server"></asp:TextBox> 
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                        <td align="right">Code บัญชี :</td>
                                        <td align="left">
                                            <asp:TextBox ID="txtCreditCardAcctCode" runat="server" CssClass="TEXTBOX" width="80"></asp:TextBox>
                                            <asp:ImageButton ID="imgCreditCardAcct" runat="server" 
                                                ImageUrl="../../Images/binoculars.gif" ToolTip="ค้นหาสาขา"
                                                style="width: 15px"  type="script" OnClientClick="return openPopupZoom();"/>
                                            <asp:TextBox ID="txtCreditCardAcctName" runat="server" CssClass="TEXTBOXDIS" width="200"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">สถานะ</td>
                                        <td align="left">      
                                            <asp:RadioButton ID="rdoCreditCardStatus1" GroupName="CreditCardStatus" runat="server" Text="รอตัด" />  
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">หมายเหตุ :</td>
                                        <td align="left">
                                            <asp:TextBox ID="txtCreditCardRemark" runat="server" CssClass="TEXTBOX" width="300" TextMode="MultiLine"></asp:TextBox>                                            
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div id="divAP" style="display:inline">
                                <table>
                                    <tr>
                                        <td align="right" width="30%">เลข AP :</td>
                                        <td align="left" width="70%">
                                            <asp:TextBox ID="txtAPNo" CssClass="TEXTBOX" runat="server"></asp:TextBox> 
                                        </td>
                                    </tr> 
                                    <tr>
                                        <td align="right">วันที่รับเงิน :</td>
                                        <td align="left">
                                            <uc1:ucTxtDate ID="txtAPDate" runat="server" />
                                        </td>
                                    </tr>                                                                      
                                    <tr>
                                        <td align="right">จำนวนเงิน :</td>
                                        <td align="left">
                                            <asp:TextBox ID="txtAPAmt" CssClass="TEXTBOXMONEY" MaxLength="18" runat="server"></asp:TextBox> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">Code บัญชี :</td>
                                        <td align="left">
                                            <asp:TextBox ID="txtAPAcctCode" runat="server" CssClass="TEXTBOX" width="80"></asp:TextBox>
                                            <asp:ImageButton ID="imgAPAcct" runat="server" 
                                                ImageUrl="../../Images/binoculars.gif" ToolTip="ค้นหาสาขา"
                                                style="width: 15px"  type="script" OnClientClick="return openPopupZoom();"/>
                                            <asp:TextBox ID="txtAPAcctName" runat="server" CssClass="TEXTBOXDIS" width="200"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">สถานะ</td>
                                        <td align="left">      
                                            <asp:RadioButton ID="rdoAPStatus1" GroupName="APStatus" runat="server" Text="รอตัด" />  
                                            <asp:RadioButton ID="rdoAPStatus2" GroupName="APStatus" runat="server" Text="โอนลอย" />  
                                            <asp:RadioButton ID="rdoAPStatus3" GroupName="APStatus" runat="server" Text="รับแทน สนญ." />  
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">หมายเหตุ :</td>
                                        <td align="left">
                                            <asp:TextBox ID="txtAPRemark" runat="server" CssClass="TEXTBOX" width="300" TextMode="MultiLine"></asp:TextBox>                                            
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </fieldset>            
                    </td>
                </tr>                        
                <tr>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Literal ID="litJavaScript" runat="server"></asp:Literal>
                    </td>
                </tr>
            </table>
        </div>
</asp:Content>

