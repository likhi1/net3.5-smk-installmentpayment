﻿<%@ Page Language="C#" MasterPageFile="~/MasterData/MainMasterPage.master" AutoEventWireup="true" CodeFile="SettlementForm.aspx.cs" Inherits="Module_Settlement_SettlementForm" Title="Untitled Page" %>
<%@ Register src="../../UserControl/ucTxtDate.ascx" tagname="ucTxtDate" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" Runat="Server">
<script>
        function goPage(strPage)
        {
            window.location= strPage;
            return false;
        }
        function validateSearchForm() {
            var isRet = true;
            var strMsg = "";
            if (document.getElementById("<%=rdoMotor.ClientID %>").checked == true) {
                if (document.getElementById("<%=txtVolunNo.ClientID %>").value == "") {
                    isRet = false;
                    strMsg += "\t-  เลขกรมธรรม์สมัครใจ\n";
                }
                if (document.getElementById("<%=chkCompul.ClientID %>").checked == true) {
                    if (document.getElementById("<%=txtCompulNo.ClientID %>").value == "") {
                        isRet = false;
                        strMsg += "\t-  เลขกรมธรรม์พรบ.\n";
                    }
                }
            }
            if (document.getElementById("<%=rdoNonMotor.ClientID %>").checked == true) {
                if (document.getElementById("<%=txtNonMotorNo.ClientID %>").value == "") {
                    isRet = false;
                    strMsg += "\t-  เลขกรมธรรม์\n";
                }
            }

            if (strMsg != "") {
                strMsg = "! กรุณาระบุเงื่อนไขการดึงข้อมูล\n" + strMsg;
            }

            if (isRet == false)
                alert(strMsg);
            return isRet;
        }
        function validateForm()
        {
            var isRet = true;
            var strMsg = "";
            var dSumInstNet = 0;
                        
            // policy no   
            if (document.getElementById("<%=txtPolicy.ClientID %>").value == "")
            {
                isRet = false;
                strMsg += "  - เลขกรมธรรม์\n";
            }                
            // txtBalance = sum of inst_net (dtgInstallment)
            dSumInstNet = parseFloat(document.getElementById("divSumNet").innerHTML.replace(/,/g,"").replace("&nbsp;",""));
            if (parseFloat(document.getElementById("<%=txtBalance.ClientID %>").value.replace(/,/g,"")) != dSumInstNet)
            {
                isRet = false;
                strMsg += "  - รวมเงินที่ผ่อนชำระ ไม่เท่ากับ ยอดผ่อนชำระ\n";
            }
            // total paid > 0
            if (parseFloat(document.getElementById("<%=txtTotalPaid.ClientID %>").value.replace(/,/g,"")) <= 0)
            {
                isRet = false;
                strMsg += "  - จำนวนเงินรับครั้งนี้ ต้องมากกว่า 0\n";
            }
            // rows of installment
            if (parseFloat("<%=dtgInstallments.Items.Count%>") < 1)
            {
                isRet = false;
                strMsg += "  - ไม่มีรายการงวดผ่อนชำระ\n";
            }
            // rows of accept
            if (parseFloat("<%=dtgRV.Items.Count%>") < 1)
            {
                isRet = false;
                strMsg += "  - ไม่มีรายการรับชำระ\n";
            }
            
 
            if (strMsg != "")
            {                
                strMsg = "! กรุณาตรวจสอบ\n" + strMsg;
            }            
                                    
            if (isRet == false)
                alert(strMsg);
            return isRet;
        }
        function calDiscount() {
            var dPremium = 0;
            var dNet = 0;
            var dRate = 0;
            var dDiscount = 0;
            var dTaxStamp = 0;
            if (document.getElementById("<%=lblPolicyPremium.ClientID %>").innerText != "")
                dPremium = parseFloat(document.getElementById("<%=lblPolicyPremium.ClientID %>").innerText.replace(",", "").replace(",", ""));
            if (document.getElementById("<%=lblPolicyNet.ClientID%>").innerText != "")
                dNet = parseFloat(document.getElementById("<%=lblPolicyNet.ClientID %>").innerText.replace(",", "".replace(",", "")));
            dTaxStamp = dNet - dPremium;
            if (document.getElementById("<%=txtDiscountRate.ClientID %>").value != "")
                dRate = parseFloat(document.getElementById("<%=txtDiscountRate.ClientID %>").value.replace(",", "".replace(",", "")));
            dDiscount = dPremium * dRate / 100;
            document.getElementById("<%=txtDiscount.ClientID %>").value = CommaFormatted2(dDiscount);
            calBalance();
        }
        function calBalance() {
            var dPolicy = 0;
            var dDiscount = 0;
            if (document.getElementById("<%=txtPolicyBalance.ClientID %>").value != "")
                dPolicy = parseFloat(document.getElementById("<%=txtPolicyBalance.ClientID %>").value.replace(",", "".replace(",", "")));
            if (document.getElementById("<%=txtDiscount.ClientID %>").value != "")
                dDiscount = parseFloat(document.getElementById("<%=txtDiscount.ClientID %>").value.replace(",", "".replace(",", "")));
            dPolicy = dPolicy - dDiscount;
            document.getElementById("<%=txtBalance.ClientID %>").value = CommaFormatted2(dPolicy);
        }
        function validateInstallmentForm() {
            var isRet = true;
            var strMsg = "";
            if (document.getElementById("<%=txtBalance.ClientID %>").value == "") {
                isRet = false;
                strMsg += "\t-  ยอดผ่อนชำระ\n";                
            }
            if (document.getElementById("<%=txtInstallments.ClientID %>").value == "") {
                isRet = false;
                strMsg += "\t-  จำนวนงวด\n";
            }
            if (document.getElementById("<%=txtMonths.ClientID %>").value == "") {
                isRet = false;
                strMsg += "\t-  ราย(เดือน)\n";
            }
            if (document.getElementById("<%=txtStartDate.txtID %>").value == "" ||
                document.getElementById("<%=txtStartDate.txtID %>").value == "__/__/____") {
                isRet = false;
                strMsg += "\t-  วันที่เริ่มจ่าย\n";
            }

            if (strMsg != "") {
                strMsg = "! กรุณาระบุการคำนวณงวดผ่อนชำระ\n" + strMsg;
            }

            if (isRet == false)
                alert(strMsg);
            return isRet;
        }
        function calNetInstallment(objRate) {
            var dNet = 0;
            var dBalance = 0;
            var dDiscount = 0;
            var dRate = 0;
            var dInstallment = 0;
            var strID = objRate.getAttribute("ID");
            
            
            if (document.getElementById("<%=lblPolicyNet.ClientID%>").innerText != "")
                dNet = parseFloat(document.getElementById("<%=lblPolicyNet.ClientID %>").innerText.replace(",", "".replace(",", "")));            
            if (document.getElementById("<%=txtBalance.ClientID%>").value != "")
                dBalance = parseFloat(document.getElementById("<%=txtBalance.ClientID %>").value.replace(",", "".replace(",", "")));
            if (document.getElementById("<%=txtDiscount.ClientID%>").value != "")
                dDiscount = parseFloat(document.getElementById("<%=txtDiscount.ClientID %>").value.replace(",", "".replace(",", "")));
            if (objRate.value != "")
                dRate = parseFloat(objRate.value.replace(",", "".replace(",", "")));

            dInstallment = (dNet - dDiscount) * dRate / 100;
            dInstallment = Math.round(dInstallment / 10) * 10;
            if (objRate.getAttribute("inst") == "1" &&
                document.getElementById("<%=lblCompulNet.ClientID%>").innerText != "") {
                dInstallment = dInstallment + parseFloat(document.getElementById("<%=lblCompulNet.ClientID %>").innerText.replace(",", "".replace(",", "")));
            }
            // กรณีงวดสุดท้าย
            if (objRate.getAttribute("inst") == document.getElementById("<%=txtInstallments.ClientID %>").value) {
                var tableObj = document.getElementById("ctl00_ContentPlaceHolder1_dtgInstallments");
                var dTmpNet = 0;
                var dTmpTotal = 0;
                var strTmpID = "";
                for (var i = 1; i <= (tableObj.rows.length - 3); i++) {
                    if (i <= 8)
                        strTmpID = "ctl00_ContentPlaceHolder1_dtgInstallments_ctl0" + ((i + 1).toString());
                    else
                        strTmpID = "ctl00_ContentPlaceHolder1_dtgInstallments_ctl" + ((i + 1).toString());

                    dTmpNet = parseFloat(dTmpNet) + parseFloat((parseFloat(document.getElementById(strTmpID + "_txtNet").value.replace(",", "").replace(",", ""))));
                    dTmpNet = parseFloat(dTmpNet.toFixed(2));
                    dTmpTotal = dTmpTotal + dTmpNet;
                }
                dInstallment = dBalance - dTmpNet;
                dInstallment = parseFloat(dInstallment.toFixed(2));
            }
            strID = strID.replace("txtRate", "txtNet");
            document.getElementById(strID).value = CommaFormatted2(dInstallment);
            calLastInst();
            calSumNet();
        }
        function calLastInst(){
            var dNet = 0;
            var dBalance = 0;
            var dDiscount = 0;
            var dRate = 0;
            var dInstallment = 0;
            var strID = "";
            var strNetID = "";
            var tableObj = document.getElementById("ctl00_ContentPlaceHolder1_dtgInstallments");
            var dTmpNet = 0;
            var dTmpTotal = 0;
            var strTmpID = "";
            
            if (document.getElementById("<%=lblPolicyNet.ClientID%>").innerText != "")
                dNet = parseFloat(document.getElementById("<%=lblPolicyNet.ClientID %>").innerText.replace(",", "".replace(",", "")));            
            if (document.getElementById("<%=txtBalance.ClientID%>").value != "")
                dBalance = parseFloat(document.getElementById("<%=txtBalance.ClientID %>").value.replace(",", "".replace(",", "")));
            if (document.getElementById("<%=txtDiscount.ClientID%>").value != "")
                dDiscount = parseFloat(document.getElementById("<%=txtDiscount.ClientID %>").value.replace(",", "".replace(",", "")));
            
            //
            for (var i = 1; i <= (tableObj.rows.length - 3); i++) {
                if (i <= 8)
                {
                    strTmpID = "ctl00_ContentPlaceHolder1_dtgInstallments_ctl0" + ((i + 1).toString());
                    strNetID = "ctl00_ContentPlaceHolder1_dtgInstallments_ctl0" ;
                }
                else
                {
                    strTmpID = "ctl00_ContentPlaceHolder1_dtgInstallments_ctl" + ((i + 1).toString());
                    strNetID = "ctl00_ContentPlaceHolder1_dtgInstallments_ctl0" ;
                }

                dTmpNet = parseFloat(dTmpNet) + parseFloat((parseFloat(document.getElementById(strTmpID + "_txtNet").value.replace(",", "").replace(",", ""))));
                dTmpNet = parseFloat(dTmpNet.toFixed(2));
                dTmpTotal = dTmpTotal + dTmpNet;
            }
            dInstallment = dBalance - dTmpNet;
            dInstallment = parseFloat(dInstallment.toFixed(2));
            strNetID = strNetID + ((tableObj.rows.length - 1).toString()) + "_txtNet";            
            document.getElementById(strNetID).value = CommaFormatted2(dInstallment);
        }
        function calSumNet() {
            var tableObj = document.getElementById("ctl00_ContentPlaceHolder1_dtgInstallments");
            var dNet = 0;
            var strID = "";
            for (var i = 1; i <= (tableObj.rows.length - 2); i++) {
                if (i <= 8)
                    strID = "ctl00_ContentPlaceHolder1_dtgInstallments_ctl0" + ((i + 1).toString());
                else
                    strID = "ctl00_ContentPlaceHolder1_dtgInstallments_ctl" + ((i + 1).toString());

                dNet = parseFloat(dNet) + parseFloat((parseFloat(document.getElementById(strID + "_txtNet").value.replace(",", "").replace(",", ""))));
                dNet = parseFloat(dNet.toFixed(2));
            }
            document.getElementById("divSumNet").innerHTML = CommaFormatted2(dNet) + "&nbsp;";
        }
        function calSumPaid() {
            var tableObj = document.getElementById("ctl00_ContentPlaceHolder1_dtgInstallments");
            var dNet = 0;
            var strID = "";
            for (var i = 1; i <= (tableObj.rows.length - 2); i++) {
                if (i <= 8)
                    strID = "ctl00_ContentPlaceHolder1_dtgInstallments_ctl0" + ((i + 1).toString());
                else
                    strID = "ctl00_ContentPlaceHolder1_dtgInstallments_ctl" + ((i + 1).toString());

                dNet = parseFloat(dNet) + parseFloat((parseFloat(document.getElementById(strID + "_txtPaid").value.replace(",", "").replace(",", ""))));
                dNet = parseFloat(dNet.toFixed(2));
            }
            document.getElementById("divSumPaid").innerHTML = CommaFormatted2(dNet) + "&nbsp;";
            document.getElementById("<%=txtTotalPaid.ClientID %>").value = CommaFormatted2(dNet);
            calSumBalance();
        }
        function calSumBalance() {
            var dNet = 0;
            var dPaid = 0;
            var dBalance = 0;
            dNet = parseFloat((parseFloat(document.getElementById("divSumNet").innerHTML.replace("&nbsp;","").replace(",", "").replace(",", ""))));
            dNet = parseFloat(dNet.toFixed(2));
            dPaid = parseFloat((parseFloat(document.getElementById("divSumPaid").innerHTML.replace("&nbsp;","").replace(",", "").replace(",", ""))));
            dPaid = parseFloat(dPaid.toFixed(2));
            dBalance = dNet - dPaid
            document.getElementById("divSumBalance").innerHTML = CommaFormatted2(dBalance) + "&nbsp;";
        }
        function openZoom(){
            openShadowBox("../../Module/Zoom/Zoom1.aspx", 500, 600, 'Zoom');
            return false;
        }
        function openPopChequeForm() {
            openShadowBox("../../Module/Settlement/PopChequeForm.aspx", 750, 700, 'PopCheque');
        }
        function addRVSubmit() {
            <%=Page.ClientScript.GetPostBackEventReference(btnRVSave, string.Empty) %>
            //__doPostBack('<%=btnRVSave.ClientID %>', '');
        }
</script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div id="page-inside">     
<table width="100%">
    <tr>
        <td>
            <div class="heading1">:: บันทึกผ่อนชำระ ::</div>
            <asp:ScriptManager ID="ScriptManager1" runat="server">
            </asp:ScriptManager>
        </td>
    </tr>
    <tr>
         <td colspan="1">
            <asp:Button ID="btnback" runat="server" Text="กลับ" CssClass="awesome" OnClientClick="return goPage('../../Module/Settlement/SettlementList.aspx');"/>
            <asp:Button ID="btnSave" runat="server" Text="บันทึก" CssClass="awesome" onclick="btnSave_Click" OnClientClick="return validateForm();"/>            
            <asp:Button ID="btnClear" runat="server" Text="เริ่มใหม่" CssClass="awesome" OnClientClick="return goPage('../../Module/Settlement/SettlementForm.aspx');"/>            
            <asp:Button ID="btnExit" runat="server" Text="ออก" CssClass="awesome"  OnClientClick="return goPage('../../Module/MasterData/MainPage.aspx');"/>
        </td>
    </tr>
    <tr>
        <td>
            <div id="divSearchPolicy">
            <fieldset >
                <legend>ค้นหากรมธรรม์</legend>
                <table width="75%">
                    <tr>
                        <td align="right" width="20%">ประเภท :</td>
                        <td align="left" width="20%">
                            <asp:RadioButton ID="rdoMotor" GroupName="rdoPolType" runat="server" Checked/>ระบบ Motor
                        </td>
                        <td width="18%">
                            เลขกรมธรรม์สมัครใจ :
                        </td>
                        <td width="20%">
                            <asp:TextBox ID="txtVolunNo" runat="server" CssClass="TEXTBOX" Width="100" MaxLength="15" ></asp:TextBox>
                        </td>                        
                        <td width="22%"></td>
                    </tr>                    
                    <tr>
                        <td></td>
                        <td align="right"></td>
                        <td>
                            <asp:CheckBox ID="chkCompul" runat="server" Text=" "/>เลขกรมธรรม์ พรบ. :
                        </td>
                        <td>
                            <asp:TextBox ID="txtCompulNo" runat="server" CssClass="TEXTBOX" Width="100" MaxLength="15" ></asp:TextBox>
                        </td>
                        <td>
                        </td>
                    </tr>                                        
                    <tr>
                        <td></td>
                        <td align="left">
                            <asp:RadioButton ID="rdoNonMotor" GroupName="rdoPolType" runat="server" />ระบบ Non-Motor
                        </td>
                        <td>
                            เลขกรมธรรม์ :
                        </td>
                        <td>
                            <asp:TextBox ID="txtNonMotorNo" runat="server" CssClass="TEXTBOX" Width="100" MaxLength="15" ></asp:TextBox>
                        </td>
                        <td>
                        </td>
                    </tr> 
                    <tr>
                        <td></td>
                        <td align="right"></td>
                        <td>
                            
                        </td>  
                        <td align="right"></td>
                        <td>
                            <asp:Button ID="btnSearch" runat="server" Text="ค้นหากรมธรรม์" CssClass="awesome" onclick="btnSearch_Click" OnClientClick="return validateSearchForm();"/>
                            <asp:Button ID="btnClearSearch" runat="server" Text="เคลียร์" CssClass="awesome" OnClientClick="goPage('../../Module/Settlement/SettlementForm.aspx');"/>
                        </td>
                    </tr>                                             
                </table>
            </fieldset>
            </div>
        </td>
    </tr> 
    <tr>
        <td>
            <div id="divPolicy" style="display:none" >
            <fieldset >
                <legend>ข้อมูลกรมธรรม์</legend>
                <table width="100%">
                    <tr>
                        <td align="right" width="20%">เลขกรมธรรม์ :</td>

                        <td width="30%">
                            <asp:Label ID="lblPolicyNo" runat="server" CssClass="textBlue" ></asp:Label>
                        </td>

                        <td align="right" width="20%">วันที่รับกรมธรรม์ :</td>                        
                        <td width="30%">
                            <asp:Label ID="lblIssueDate" runat="server" CssClass="textBlue" ></asp:Label>
                        </td>
                    </tr>
                    <tr>

                        <td align="right">สาขา :</td>
                        <td>
                            <asp:Label ID="lblBranchName" runat="server" CssClass="textBlue" ></asp:Label>
                        </td>
                        <td align="right">รหัสตัวแทน :</td>
                        <td>
                            <asp:Label ID="lblAgentName" runat="server" CssClass="textBlue" ></asp:Label>
                        </td>

                    </tr>
                    <tr>
                        <td align="right">
                            <div id="divPolicyRemark1" style="display:inline">ทะเบียนรถ :</div>
                        </td>
                        <td>
                            <asp:Label ID="lblRemark1" runat="server" CssClass="textBlue" ></asp:Label>
                        </td>
                        <td align="right">
                            <div id="divPolicyRemark2" style="display:inline">chasis :</div>
                        </td>
                        <td>
                            <asp:Label ID="lblRemark2" runat="server" CssClass="textBlue" ></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">วันคุ้มครอง :</td>
                        <td>
                            <asp:Label ID="lblPeriodFromTo" runat="server" CssClass="textBlue" ></asp:Label>
                        </td>
                    </tr> 
                    <tr>
                        <td align="right">ชื่อ - นามสกุลผู้เอาประกันภัย :</td>
                        <td>
                            <asp:Label ID="lblInsurerName" runat="server" CssClass="textBlue" ></asp:Label>
                        </td>
                        <td align="right">เบอร์โทรศัพท์  :</td>
                        <td>
                            <asp:Label ID="lblPhoneNo" runat="server" CssClass="textBlue" ></asp:Label>
                        </td>
                    </tr>                                                            
                    <tr>
                        <td align="right">เบี้ยสุทธิ :</td>
                        <td>
                            <asp:Label ID="lblPolicyPremium" runat="server" CssClass="textBlue" ></asp:Label>
                        </td>  
                        <td align="right">เบี้ยรวม :</td>
                        <td>
                            <asp:Label ID="lblPolicyNet" runat="server" CssClass="textBlue" ></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <div id="divCom" style="display:none">
                            <table width="100%">
                                <tr>
                                    <td colspan="4">
                                        <hr />
                                    </td>
                                </tr>      
                                <tr>
                                    <td align="right">เลขกรมธรรม์(พรบ.) :</td>
                                    <td>
                                        <asp:Label ID="lblCompulNo" runat="server" CssClass="textBlue" ></asp:Label>
                                    </td>  
                                    <td align="right">ทะเบียนรถ(พรบ.) :</td>
                                    <td>
                                        <asp:Label ID="lblCompulLicense" runat="server" CssClass="textBlue" ></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">ชื่อ-นามสกุลผู้เอาประกันภัย(พรบ.) :</td>
                                    <td>
                                        <asp:Label ID="lblCompulName" runat="server" CssClass="textBlue" ></asp:Label>
                                    </td>  
                                    <td align="right">chasis(พรบ.) :</td>
                                    <td>
                                        <asp:Label ID="lblCompulChasis" runat="server" CssClass="textBlue" ></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" width="20%">เบี้ยสุทธิ (พรบ.) :</td>
                                    <td  width="30%">
                                        <asp:Label ID="lblCompulPremium" runat="server" CssClass="textBlue" ></asp:Label>
                                    </td>  
                                    <td align="right" width="20%">เบี้ยรวม (พรบ.) :</td>
                                    <td  width="30%">
                                        <asp:Label ID="lblCompulNet" runat="server" CssClass="textBlue" ></asp:Label>
                                    </td>
                                </tr>     
                            </table>
                            </div>
                        </td>
                    </tr>                              
                </table>
            </fieldset>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <div id="divSendDoc" style="display:none">
            <fieldset >
                <legend>ที่อยู่จัดส่งเอกสาร</legend>
                <table width="100%">
                    <tr>
                        <td align="right" width="20%">ชื่อ - นามสกุล :</td>
                        <td width="30%">
                            <asp:TextBox ID="txtContactName" runat="server" CssClass="TEXTBOX" MaxLength="200" width="300"></asp:TextBox>
                        </td>
                        <td align="right" width="20%">เบอร์โทรศัพท์ :</td>                        
                        <td width="30%">
                            <asp:TextBox ID="txtContactPhone" runat="server" CssClass="TEXTBOX" MaxLength="200" width="300"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">ที่อยู่ 1 :</td>
                        <td colspan="3">
                            <asp:TextBox ID="txtContactAddr1" runat="server" CssClass="TEXTBOX" width="400" MaxLength="200"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">ที่อยู่ 2 :</td>
                        <td colspan="3">
                            <asp:TextBox ID="txtContactAddr2" runat="server" CssClass="TEXTBOX" width="400" MaxLength="200"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">รหัสไปรษณีย์ :</td>
                        <td>
                            <asp:TextBox ID="txtContactPostCode" runat="server" CssClass="TEXTBOX" Width="50" MaxLength="5"></asp:TextBox>
                        </td>
                    </tr>                                
                </table>
            </fieldset>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <div id="divInstallments" style="display:none">
            <fieldset >
                <legend>ข้อมูลผ่อนชำระ</legend>
                <table width="100%">
                    <tr>
                        <td align="right" width="20%">เลขที่ผ่อนชำระ :</td>
                        <td width="30%">
                            <asp:TextBox ID="txtSetttlementID" runat="server" CssClass="TEXTBOXDIS"></asp:TextBox>
                        </td>
                        <td width="20%" align="right">วันที่บันทึกผ่อนชำระ :</td>                        
                        <td width="30%">
                            <uc1:ucTxtDate ID="txtSettlementDate" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td align="right">เบี้ยกรมธรรม์หลัก :</td>
                        <td>
                            <asp:TextBox ID="txtPremium1" runat="server" CssClass="TEXTBOXMONEYDIS"></asp:TextBox>
                        </td>
                        <td align="right">เบี้ยพรบ. :</td>
                        <td>
                            <asp:TextBox ID="txtPremium2" runat="server" CssClass="TEXTBOXMONEYDIS"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">รวมเบี้ยกรมธรรม์ :</td>
                        <td>
                            <asp:TextBox ID="txtPolicyTotal" runat="server" CssClass="TEXTBOXMONEYDIS"></asp:TextBox>
                        </td>
                        <td align="right">รวมเบี้ยสลักหลัง :</td>
                        <td>
                            <asp:TextBox ID="txtEndorseTotal" runat="server" CssClass="TEXTBOXMONEYDIS"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">รวมยอดเบี้ย :</td>
                        <td>
                            <asp:TextBox ID="txtPolicyBalance" runat="server" CssClass="TEXTBOXMONEYDIS"></asp:TextBox>
                        </td>
                        <td align="right">ส่วนลด :</td>
                        <td>
                            <asp:TextBox ID="txtDiscountRate" runat="server" CssClass="TEXTBOXMONEY" Width="50" MaxLength="5" onblur="calDiscount();"></asp:TextBox>%
                            <asp:TextBox ID="txtDiscount" runat="server" CssClass="TEXTBOXMONEYDIS"></asp:TextBox>บาท
                        </td>
                    </tr> 
                    <tr>
                        <td align="right">ยอดผ่อนชำระ :</td>
                        <td>
                            <asp:TextBox ID="txtBalance" runat="server" CssClass="TEXTBOXMONEYDIS"></asp:TextBox>บาท
                        </td>
                        <td align="right">จำนวนงวด :</td>
                        <td>
                            <asp:TextBox ID="txtInstallments" runat="server" CssClass="TEXTBOXMONEY" Width="50" MaxLength="2"></asp:TextBox>
                            ราย
                            <asp:TextBox ID="txtMonths" runat="server" CssClass="TEXTBOXMONEY" Width="50" MaxLength="2"></asp:TextBox>เดือน
                        </td>
                    </tr>    
                    <tr>
                        <td align="right">วันที่เริ่มจ่าย :</td>                        
                        <td>
                            <uc1:ucTxtDate ID="txtStartDate" runat="server" />
                        </td>
                        <td align="right">เลข AR :</td>
                        <td>
                            <asp:TextBox ID="txtARNo" runat="server" CssClass="TEXTBOXDIS"></asp:TextBox>
                        </td>
                    </tr>  
                    <tr>
                        <td align="right">ยอดที่จ่ายมาแล้ว :</td>
                        <td>
                            <asp:TextBox ID="txtPaid" runat="server" CssClass="TEXTBOXMONEYDIS"></asp:TextBox>บาท
                        </td>
                        <td align="right">ยอดค้างชำระ :</td>
                        <td>
                            <asp:TextBox ID="txtArrear" runat="server" CssClass="TEXTBOXMONEYDIS"></asp:TextBox>
                        </td>
                    </tr>    
                    <tr>
                        <td align="right">หมายเหตุ :</td>
                        <td>
                            <asp:TextBox ID="txtRemark" runat="server" TextMode="MultiLine" Rows="4" width="300"></asp:TextBox>
                        </td>
                    </tr>                      
                    <tr>
                        <td colspan="4">
                            <asp:UpdatePanel ID="uPanelIns" runat="server">
                                <ContentTemplate>                                
                                <table width="100%">
                                <tr>
                                    <td>
                                        <asp:Button ID="btnInstallment" runat="server" Text="คำนวณ" CssClass="awesome" onclick="btnInstallments_Click" OnClientClick="document.getElementById('divInsProgress').style.display='inline';return validateInstallmentForm();"/>
                                        <asp:Button ID="btnAddInst" runat="server" Text="เพิ่มงวดการชำระ" CssClass="awesome" onclick="btnAddInst_Click"/>                            
                                        <div id="divInsProgress" style="display:none">
                                            <img alt="progress" src="../../images/wait.gif"/>
                                        </div>
                                    </td>                                              
                                </tr>
                                </table>
                                <asp:DataGrid ID="dtgInstallments" runat="server" AutoGenerateColumns="false" Width="100%"
                                    HeaderStyle-CssClass="gridhead" ItemStyle-CssClass="gridrow0" AlternatingItemStyle-CssClass="gridrow1"
                                    AllowPaging="false" PagerStyle-Visible="false" OnItemDataBound="dtgInstallments_ItemDataBound"
                                    ShowFooter="true">
                                    <Columns>
                                        <asp:TemplateColumn>
					                        <HeaderTemplate>
                                                <%#ColumnTitle[0]%>					                
					                        </HeaderTemplate>
					                        <ItemTemplate>
					                            <%#Eval(ColumnName[0])%>
					                        </ItemTemplate>
				                        </asp:TemplateColumn>
                                        <asp:TemplateColumn>
					                        <HeaderTemplate>
						                        <%#ColumnTitle[1]%>
					                        </HeaderTemplate>
					                        <ItemTemplate>
					                            <uc1:ucTxtDate ID="txtDueDate" runat="server" Text="<%#FormatStringApp.FormatDate(Eval(ColumnName[1]))%>" />
					                        </ItemTemplate>
				                        </asp:TemplateColumn>
				                        <asp:TemplateColumn>
					                        <HeaderTemplate>
						                        <%#ColumnTitle[2]%>
					                        </HeaderTemplate>
					                        <ItemTemplate>
					                           <%#ShowLateDays(Eval(ColumnName[1]))%>
					                        </ItemTemplate>
				                        </asp:TemplateColumn>
				                        <asp:TemplateColumn>
					                        <HeaderTemplate>
						                        <%#ColumnTitle[3]%>
					                        </HeaderTemplate>
					                        <ItemTemplate>
					                            <asp:TextBox ID="txtRate" runat="server" CssClass="TEXTBOXMONEY" Width="50" MaxLength="5" Text="<%#Eval(ColumnName[3])%>" inst="<%#Eval(ColumnName[0])%>" onblur="calNetInstallment(this)"></asp:TextBox>
					                        </ItemTemplate>
				                        </asp:TemplateColumn>	
				                        <asp:TemplateColumn>
					                        <HeaderTemplate>
						                        <%#ColumnTitle[4]%>
					                        </HeaderTemplate>
					                        <ItemTemplate>
					                            <asp:TextBox ID="txtNet" runat="server" CssClass="TEXTBOXMONEY" MaxLength="18" Text="<%#FormatStringApp.FormatMoney(Eval(ColumnName[4]))%>" onblur="calLastInst();calSumNet();"></asp:TextBox>
					                        </ItemTemplate>
				                        </asp:TemplateColumn>
				                        <asp:TemplateColumn>
					                        <HeaderTemplate>
						                        <%#ColumnTitle[5]%>
					                        </HeaderTemplate>
					                        <ItemTemplate>							                        
					                            <asp:TextBox ID="txtPaid" runat="server" CssClass="TEXTBOXMONEYDIS" MaxLength="18" Text="<%#Eval(ColumnName[5])%>" onblur="calSumPaid();"></asp:TextBox>
					                        </ItemTemplate>
				                        </asp:TemplateColumn>
				                        <asp:TemplateColumn>
					                        <HeaderTemplate>
						                        <%#ColumnTitle[6]%>
					                        </HeaderTemplate>
					                        <ItemTemplate>
					                            <%#Eval(ColumnName[6])%>
					                        </ItemTemplate>
				                        </asp:TemplateColumn>	
				                        <asp:TemplateColumn>
					                        <HeaderTemplate>
						                        <%#ColumnTitle[7]%>
					                        </HeaderTemplate>
					                        <ItemTemplate>							                        
					                            <asp:TextBox ID="txtInsRemark" runat="server" CssClass="TEXTBOX" Text="<%#Eval(ColumnName[7])%>" Width="200"></asp:TextBox>
					                        </ItemTemplate>
				                        </asp:TemplateColumn>
				                        <asp:TemplateColumn>
					                        <HeaderTemplate>						                    
					                        </HeaderTemplate>
					                        <ItemTemplate>
                                                <asp:ImageButton ID="imgDelete" runat="server" ImageUrl="../../images/btnDelete.GIF" ToolTip="ลบรายการ" onclick="imgDeleteInst_Click" dsindex="<%# Container.DataSetIndex%>"/>                                            
					                        </ItemTemplate>
				                        </asp:TemplateColumn>		                    
				                    </Columns>
                                </asp:DataGrid>
                                <asp:Literal ID="litScriptIns" runat="server"></asp:Literal>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="btnInstallment" EventName="Click" />
                                    <asp:AsyncPostBackTrigger ControlID="btnAddInst" EventName="Click" />
                                </Triggers>
                            </asp:UpdatePanel>
                            
                        </td>
                    </tr>
                </table>
            </fieldset>
            </div>
        </td>
    </tr> 
    <tr>
        <td>
            <div id="divRV" style="display:none">
            <fieldset>
                <legend>ข้อมูลรับชำระ</legend>
                <table width="100%">
                    <tr>
                        <td align="right" width="15%">เลขที่นำส่ง :</td>
                        <td width="35%">
                            <asp:TextBox ID="txtRVNO" runat="server" CssClass="TEXTBOXDIS"></asp:TextBox>
                            <div style="display:none">
                                <asp:TextBox ID="txtPayID" runat="server" CssClass="TEXTBOXDIS"></asp:TextBox>
                            </div>
                        </td>
                        <td align="right" width="20%">วันที่รับเงิน :</td>                        
                        <td width="30%">
                            <uc1:ucTxtDate ID="txtRVDate" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td align="right">จากฝ่าย :</td>
                        <td>
                            <asp:TextBox ID="txtDeptCode" runat="server" CssClass="TEXTBOX" width="80"></asp:TextBox>
                            <asp:ImageButton ID="imgDept" runat="server" 
                                ImageUrl="../../Images/binoculars.gif" ToolTip="ค้นหาฝ่าย"
                                style="width: 15px"  type="script" OnClientClick="return openZoom();"/>
                            <asp:TextBox ID="txtDeptName" runat="server" CssClass="TEXTBOXDIS" width="200"></asp:TextBox>
                        </td>
                        <td align="right">เล่ม - เลขที่ :</td>
                        <td>
                            <asp:TextBox ID="txtBookNO" runat="server" CssClass="TEXTBOXDIS" Width="150"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">สาขารับเงิน :</td>
                        <td>
                            <asp:TextBox ID="txtBrAcceptCode" runat="server" CssClass="TEXTBOX" width="80"></asp:TextBox>
                            <asp:ImageButton ID="imgBrAccept" runat="server" 
                                ImageUrl="../../Images/binoculars.gif" ToolTip="ค้นหาสาขา"
                                style="width: 15px"  type="script"/>
                            <asp:TextBox ID="txtBrAcceptName" runat="server" CssClass="TEXTBOXDIS" width="200"></asp:TextBox>
                        </td>
                        <td align="right">หลักฐานการรับเงิน :</td>
                        <td>
                            <asp:TextBox ID="txtDocument" runat="server" CssClass="TEXTBOXDIS" width="200"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">รหัสตัวแทน :</td>
                        <td>
                            <asp:TextBox ID="txtAgentCode" runat="server" CssClass="TEXTBOXDIS" width="80"></asp:TextBox>
                            <asp:TextBox ID="txtAgentName" runat="server" CssClass="TEXTBOXDIS" width="200"></asp:TextBox>
                        </td>
                        <td align="right">หัวหน้าสาย :</td>
                        <td>
                            <asp:TextBox ID="txtHead" runat="server" CssClass="TEXTBOXDIS" width="200"></asp:TextBox>
                        </td>
                    </tr>    
                    <tr>
                        <td align="right">รับเงินเพื่อ :</td>
                        <td>
                            <asp:TextBox ID="txtAcceptForCode" runat="server" CssClass="TEXTBOX" width="80"></asp:TextBox>
                            <asp:ImageButton ID="imgAcceptFor" runat="server" 
                                ImageUrl="../../Images/binoculars.gif" ToolTip="ค้นหารับเงินเพื่อ"
                                style="width: 15px"  type="script"/>
                            <asp:TextBox ID="txtAcceptForName" runat="server" CssClass="TEXTBOXDIS" width="200"></asp:TextBox>
                        </td>
                        <td align="right">จำนวนเงินรับครั้งนี้</td>
                        <td>
                            <asp:TextBox ID="txtTotalPaid" runat="server" CssClass="TEXTBOXMONEYDIS" width="80"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">ชื่อผู้นำส่ง :</td>
                        <td>
                            <asp:TextBox ID="txtSenderCode" runat="server" CssClass="TEXTBOX" width="80"></asp:TextBox>
                            <asp:ImageButton ID="imgSender" runat="server" 
                                ImageUrl="../../Images/binoculars.gif" ToolTip="ค้นหาผู้นำส่ง"
                                style="width: 15px"  type="script"/>
                            <asp:TextBox ID="txtSenderName" runat="server" CssClass="TEXTBOXDIS" width="200"></asp:TextBox>
                        </td>
                        <td align="right"></td>
                        <td>
                        </td>
                    </tr>  
                    <tr>
                        <td>
                            <asp:Button ID="btnAddRV" runat="server" Text="เพิ่มรายการรับชำระ" CssClass="awesome" OnClientClick="openPopChequeForm();return false;"/>
                        </td>                      
                    </tr> 
                    <tr>
                        <td colspan="4">
                            <asp:DataGrid ID="dtgRV" runat="server" AutoGenerateColumns="false" Width="100%"
                                HeaderStyle-CssClass="gridhead" ItemStyle-CssClass="gridrow0" AlternatingItemStyle-CssClass="gridrow1"
                                AllowPaging="false" PagerStyle-Visible="false">
                                <Columns>
                                    <asp:TemplateColumn>
					                    <HeaderTemplate>
                                            <%#ColumnTitleRV[0]%>					                
					                    </HeaderTemplate>
					                    <ItemTemplate>
					                        <%# Container.DataSetIndex +1%>
					                    </ItemTemplate>
				                    </asp:TemplateColumn>
                                    <asp:TemplateColumn>
					                    <HeaderTemplate>
						                    <%#ColumnTitleRV[1]%>
					                    </HeaderTemplate>
					                    <ItemTemplate>
					                        <%#(Eval(ColumnNameRV[1]))%>
					                    </ItemTemplate>
				                    </asp:TemplateColumn>
				                    <asp:TemplateColumn>
					                    <HeaderTemplate>
						                    <%#ColumnTitleRV[2]%>
					                    </HeaderTemplate>
					                    <ItemTemplate>
					                        <%#Eval(ColumnNameRV[2])%>
					                    </ItemTemplate>
				                    </asp:TemplateColumn>	
				                    <asp:TemplateColumn>
					                    <HeaderTemplate>
						                    <%#ColumnTitleRV[3]%>
					                    </HeaderTemplate>
					                    <ItemTemplate>
					                        <%#Eval(ColumnNameRV[3])%>
					                    </ItemTemplate>
				                    </asp:TemplateColumn>
				                    <asp:TemplateColumn>
					                    <HeaderTemplate>						                    
					                    </HeaderTemplate>
					                    <ItemTemplate>
					                        <asp:ImageButton ID="imgEdit" runat="server" ImageUrl="../../images/btnEDIT.GIF" ToolTip="แก้ไขเงินนำส่ง" OnClientClick="openPopChequeForm(); return false;" onclick="imgEditRV_Click" dsindex="<%# Container.DataSetIndex%>"/>
                                            <asp:ImageButton ID="imgDelete" runat="server" ImageUrl="../../images/btnDelete.GIF" ToolTip="ลบรายการ" onclick="imgDeleteRV_Click" dsindex="<%# Container.DataSetIndex%>"/>                                            
					                    </ItemTemplate>
				                    </asp:TemplateColumn>		                    
				                </Columns>
                            </asp:DataGrid>
                        </td>
                    </tr>                           
                </table>
            </fieldset>
            </div>
        </td>
    </tr>
    <tr>
        <td>
        <div style="display:none">
            <asp:TextBox ID="txtMode" runat="server"></asp:TextBox>
            <asp:TextBox ID="txtPolicy" runat="server"></asp:TextBox>
            <asp:Button ID="btnRVSave" runat="server" Text="เพิ่มรายการรับชำระ" CssClass="awesome" onclick="btnRVSave_Click"/>
            
        </div>
        </td>
    </tr>          
    <tr>
        <td>            
        </td>
    </tr>
    <tr>
        <td>
            <asp:Literal ID="litJavaScript" runat="server"></asp:Literal>
        </td>
    </tr> 
</table>    
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
</asp:Content>

