<%@ Page Language="C#" MasterPageFile="~/MasterData/MainMasterPage.master" AutoEventWireup="true" CodeFile="RoleForm.aspx.cs" Inherits="Module_MasterData_RoleForm"%>
<%@ Register src="../../UserControl/ucPageNavigator.ascx" tagname="ucPageNavigator" tagprefix="uc1" %>
<%@ Register Src="../../UserControl/ucTxtDate.ascx" TagName="ucTxtDate" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" Runat="Server">
<script>
        function goPage(strPage)
        {
            window.location= strPage;
            return false;
        }
        function validateForm()
        {
            var isRet = true;
            var strMsg = "";
            if (document.getElementById("<%=txtRoleName.ClientID %>").value == ""){
                isRet = false;
                strMsg += "\t-  Role name\n";
            }            
            if (document.getElementById("<%=ddlStatus.ClientID %>").value == ""){
                isRet = false;
                strMsg += "\t-  Status\n";
            }
            if (document.getElementById("<%=RadioA.ClientID %>").Checked == false &&
                document.getElementById("<%=RadioB.ClientID %>").Checked == false &&
                document.getElementById("<%=RadioS.ClientID %>").Checked == false) {
                isRet = false;
                strMsg += "\t-  Branch\n";
            }
            if (strMsg != "")
            {
                strMsg = "! Please check\n" + strMsg;
            }            
                                    
            if (isRet == false)
                alert(strMsg);
            return isRet;
        }                        
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div id="page-inside">     
<table width="100%">
    <tr>
        <td>
            <div class="heading1">:: ข้อมูลกลุ่มหน้าที่ผู้ใช้งาน ::</div>
        </td>
    </tr>
    <tr>
         <td colspan="1">
            <asp:Button ID="btnback" runat="server" Text="ย้อนกลับ" CssClass="awesome" OnClientClick="return goPage('../../Module/MasterData/RoleList.aspx');"/>
            <asp:Button ID="btnAddBranch" runat="server" Text="เพิ่มสาขา" CssClass="awesome" onclick="btnAddBranch_Click"/>            
            <asp:Button ID="btnSave" runat="server" Text="บันทึก" CssClass="awesome" onclick="btnSave_Click" OnClientClick="return validateForm();"/>            
            <asp:Button ID="btnClear" runat="server" Text="ล้างข้อมูล" CssClass="awesome" OnClientClick="return goPage('../../Module/MasterData/RoleForm.aspx');"/>            
            <asp:Button ID="btnAddNew" runat="server" Text="เพิ่มข้อมูล" CssClass="awesome" OnClientClick="return goPage('../../Module/MasterData/RoleForm.aspx');"/>
            <asp:Button ID="btnExit" runat="server" Text="ออกจากระบบ" CssClass="awesome"  OnClientClick="return goPage('../../Module/MasterData/MainPage.aspx');"/>
        </td>
    </tr>
    <tr>
        <td>
            <fieldset >
                <legend>รายละเอียดกลุ่มผู้ใช้งาน</legend>
                <table width="100%" border="0">
                    <tr>
                        <td align="right" width="20%"><div class="labelRequireField">ชื่อกลุ่มหน้าที่ผู้ใช้งาน :</div></td>
                        <td width="30%">
                            <asp:TextBox ID="txtRoleName" runat="server" CssClass="TEXTBOX" Width="100" MaxLength="15" ></asp:TextBox>
                        </td>
                        <td width="20%"></td>                        
                        <td width="30%"></td>
                    </tr>                                  
                    <tr>                        
                        <td align="right"><div class="labelRequireField">สถานะ :</div></td>
                        <td>
                            <asp:DropDownList ID="ddlStatus" runat="server">                            
                            </asp:DropDownList>
                        </td>
                    </tr>    
                    <tr>
                        <td align="right" valign="top">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>สิทธิ์เมนู :</td>
                                </tr>
                            </table>                        
                        </td>
                        <td colspan="3">
                            <asp:DataGrid ID="dtgMenu" runat="server" AutoGenerateColumns="false" Width="100%" ShowHeader="false" BorderWidth="0" OnItemDataBound="dtg_ItemDataBound">
                                <Columns>
                                    <asp:TemplateColumn>
                                        <ItemTemplate>
                                            <asp:CheckBox runat="server" id="chkMenu" Visible="false" menu_id="<%#Eval(ColumnNameMenu[0])%>" Text="<%#Eval(ColumnNameMenu[1])%>"></asp:CheckBox>                                            
                                            <asp:TextBox ID="txtModule" runat="server" Visible="false" text="<%#Eval(ColumnNameMenu[0])%>"></asp:TextBox>
                                            <%#Eval(ColumnNameMenu[1])%>
                                            <asp:DataGrid ID="dtgTable" runat="server" AutoGenerateColumns="false" Width="100%" ShowHeader="false" BorderWidth="0" OnItemDataBound="dtgTable_ItemDataBound">
                                                <Columns>
                                                    <asp:TemplateColumn>
                                                        <ItemTemplate>                                                     
                                                            <asp:CheckBox runat="server" id="chkTable" table_id="<%#Eval(ColumnNameTable[0])%>" Text="<%#Eval(ColumnNameTable[1])%>"></asp:CheckBox>                                                                                                                        
                                                        </ItemTemplate>  
                                                        <ItemStyle VerticalAlign="top" Width="200" CssClass="gridrow2"/>                                                      
                                                    </asp:TemplateColumn>
                                                     <asp:TemplateColumn>
                                                        <ItemTemplate>           
                                                            <asp:CheckBox runat="server" id="chkFlagAdd" flag_id="Y" Text="บันทึก"></asp:CheckBox>  
                                                            <asp:CheckBox runat="server" id="chkFlagEdit" flag_id="Y"  Text="แก้ไข"></asp:CheckBox>             
                                                            <asp:CheckBox runat="server" id="chkFlagDelete" flag_id="Y" Text="ยกเลิก"></asp:CheckBox> 
                                                            <asp:CheckBox runat="server" id="chkFlagInquiry" flag_id="Y" Text="สอบถาม"></asp:CheckBox>                                                         
                                                        </ItemTemplate> 
                                                        <ItemStyle VerticalAlign="top" CssClass="gridrow2"/>                                          
                                                    </asp:TemplateColumn>
                                                </Columns>
                                            </asp:DataGrid>                                    
                                        </ItemTemplate>
                                        <ItemStyle VerticalAlign="top" CssClass="gridrow1"/>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" valign="top">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>สาขา :</td>
                                </tr>
                            </table>                        
                        </td>
                        <td colspan="3">
                            <asp:RadioButton ID="RadioB" Checked="true" GroupName="SetBranch" runat="server" Text=" เฉพาะสาขาที่สังกัด" OnCheckedChanged="RadioB_Click" AutoPostBack="true"  />
                            <asp:RadioButton ID="RadioA" GroupName="SetBranch" runat="server" Text=" สาขาทั้งหมด" OnCheckedChanged="RadioA_Click" AutoPostBack="true" />
                            <asp:RadioButton ID="RadioS" GroupName="SetBranch" runat="server" Text=" สาขาที่สังกัดและสาขาที่ระบุ" OnCheckedChanged="RadioS_Click" AutoPostBack="true" />
                              <br /><br />
                            <asp:DataGrid ID="DtgBranch" runat="server" AutoGenerateColumns="false" Width="50%"
                                HeaderStyle-CssClass="gridhead" ItemStyle-CssClass="gridrow0" AlternatingItemStyle-CssClass="gridrow1"
                                AllowPaging="false" PagerStyle-Visible="false" 
                                onitemdatabound="DtgBranch_ItemDataBound">
                                <Columns>
                                    <asp:TemplateColumn>
					                    <HeaderTemplate>
                                            <%#ColumnBranchTitle[0]%>					                
					                    </HeaderTemplate>
					                    <ItemTemplate>
					                        <%# Container.DataSetIndex +1%>
					                    </ItemTemplate>
				                    </asp:TemplateColumn>
                                    <asp:TemplateColumn>
					                    <HeaderTemplate>
						                    <%#ColumnBranchTitle[1]%>
					                    </HeaderTemplate>
					                    <ItemTemplate>
					                        <%#Eval(ColumnBranchName[1])%>
					                    </ItemTemplate>
					                    <EditItemTemplate>
                                            <asp:DropDownList ID="ddlBranchCode" runat="server" >                            
                                            </asp:DropDownList>
					                    </EditItemTemplate>
				                    </asp:TemplateColumn>		                    
				                    <asp:TemplateColumn>
					                    <HeaderTemplate>						                    
					                    </HeaderTemplate>
					                    <ItemTemplate>
                                            <asp:ImageButton ID="imgEdit" runat="server" ImageUrl="../../images/btnEDIT.GIF" ToolTip="Edit" onclick="imgEdit_Click" dsindex="<%# Container.DataSetIndex%>"/>
                                            <asp:ImageButton ID="imgDelete" runat="server" ImageUrl="../../images/btnDelete.GIF" ToolTip="Delete" onclick="imgDelete_Click" dsindex="<%# Container.DataSetIndex%>"/>
                                            
					                    </ItemTemplate>
					                    <EditItemTemplate>
					                        <asp:ImageButton ID="imgSave" runat="server" ImageUrl="../../images/img_save.gif" ToolTip="Save" onclick="imgSave_Click" dsindex="<%# Container.DataSetIndex%>"/>
					                        <asp:ImageButton ID="imgCancel" runat="server" ImageUrl="../../images/btnDelete.GIF" ToolTip="Cancel" onclick="imgCancel_Click" dsindex="<%# Container.DataSetIndex%>"/>                                            
					                    </EditItemTemplate>
				                    </asp:TemplateColumn>				                    
				                </Columns>
                            </asp:DataGrid>
                        </td>
                    </tr>                                                           
                </table>
            </fieldset>
        </td>
    </tr>     
    <tr>
       <td>
            <div style="display:none">
                <asp:TextBox ID="txtMode" runat="server"></asp:TextBox>                
                <asp:TextBox ID="txtRoleID" runat="server"></asp:TextBox>                
            </div>
        </td>
    </tr>  
    <tr>
        <td>
            
        </td>
    </tr>
    <tr>
        <td>
            <asp:Literal ID="litJavaScript" runat="server"></asp:Literal>
        </td>
    </tr> 
</table>    
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
</asp:Content>

