﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Zoom1.aspx.cs" Inherits="Module_Zoom_Zoom1" %>
<%@ Register src="../../UserControl/ucPageNavigator.ascx" tagname="ucPageNavigator" tagprefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ค้นหาฝ่าย</title>
    <link rel="stylesheet" type="text/css" href="../../css/globalUse.css" />
    <link rel="stylesheet" type="text/css" href="../../css/weberNew.css" />
    <link rel="stylesheet" type="text/css" href="../../Script/jquery-ui_demo/css/redmond/jquery-ui-1.10.3.custom.css" /> 
    <script language="javascript">
        function goPage(strPage)
        {
            window.location= strPage;
            return false;
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div id="page-inside">     
            <table width="100%">
                <tr>
                    <td>
                        <div class="heading1">:: ค้นหาฝ่าย ::</div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td>
                                    <asp:Button ID="btnSearch" runat="server" Text="Search" cssClass="awesome"
                                        onclick="btnSearch_Click"/>
                                </td>
                                <td>
                                    <asp:Button ID="btnClear" runat="server" Text="Clear" CssClass="awesome" OnClientClick="return goPage('../../Module/MasterData/UserList.aspx');"/>
                                </td>
                                <td>
                                    <asp:Button ID="btnExit" runat="server" Text="Exit" CssClass="awesome" OnClientClick="return goPage('../../Module/MasterData/MainPage.aspx');"/>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <fieldset>
                            <legend>เงื่อนไขการค้นหา</legend>
                            <table width="100%">
                                <tr>
                                    <td align="right" width="20%">รหัสฝ่าย :</td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtCode" runat="server" CssClass="TEXTBOX" Width="100"></asp:TextBox>
                                    </td>
                                    <td align="right" width="20%">ชื่อฝ่าย :</td>
                                    <td align="left" width="30%">
                                        <asp:TextBox ID="txtName" runat="server" CssClass="TEXTBOX" Width="100"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>            
                    </td>
                </tr>
                <tr>
                    <td>            
                        <fieldset>
                            <legend>ผลการค้นหา</legend>
                            <table width="100%">
                                <tr>
                                    <td>                            
                                        <uc1:ucPageNavigator ID="ucPageNavigator1" runat="server" OnPageIndexChanged="PageIndexChanged"/>                            
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:DataGrid ID="dtgData" runat="server" AutoGenerateColumns="false" Width="100%"
                                            HeaderStyle-CssClass="gridhead" ItemStyle-CssClass="gridrow0" AlternatingItemStyle-CssClass="gridrow1"
                                            AllowPaging="true" PagerStyle-Visible="false">
                                            <Columns>
                                                <asp:TemplateColumn>
						                            <HeaderTemplate>
                                                        <%#ColumnTitle[0]%>					                
						                            </HeaderTemplate>
						                            <ItemTemplate>
						                                <%# Container.DataSetIndex +1%>
						                            </ItemTemplate>
					                            </asp:TemplateColumn>
                                                <asp:TemplateColumn>
						                            <HeaderTemplate>
							                            <asp:LinkButton ID="lnkID1" Runat="server" OnClick="sortColumn" text="<%#ColumnTitle[1]%>">
							                            </asp:LinkButton>
							                            <asp:Image ID="imgSortCol1" Runat="server" ImageUrl="<%#getPicUrl(1)%>">
							                            </asp:Image>
						                            </HeaderTemplate>
						                            <ItemTemplate>
						                                <a href="#" onclick="returnValue();"><%#Eval(ColumnName[1])%></a>
						                            </ItemTemplate>
					                            </asp:TemplateColumn>
					                            <asp:TemplateColumn>
						                            <HeaderTemplate>
							                            <asp:LinkButton ID="lnkID2" Runat="server" OnClick="sortColumn" text="<%#ColumnTitle[2]%>">
							                            </asp:LinkButton>
							                            <asp:Image ID="imgSortCol2" Runat="server" ImageUrl="<%#getPicUrl(2)%>">
							                            </asp:Image>
						                            </HeaderTemplate>
						                            <ItemTemplate>
						                                <%#Eval(ColumnName[2])%>
						                            </ItemTemplate>
					                            </asp:TemplateColumn>					                            
					                        </Columns>
                                        </asp:DataGrid>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>        
                    </td>
                </tr>        
                <tr>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Literal ID="litJavaScript" runat="server"></asp:Literal>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
