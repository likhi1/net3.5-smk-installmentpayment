<%@ Page Language="C#" MasterPageFile="~/MasterData/MainMasterPage.master" AutoEventWireup="true" CodeFile="RoleList.aspx.cs" Inherits="Module_MasterData_RoleList"%>
<%@ Register src="../../UserControl/ucPageNavigator.ascx" tagname="ucPageNavigator" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" Runat="Server">
    <script language="javascript">
        function goPage(strPage)
        {
            window.location= strPage;
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div id="page-inside">     
    <table width="100%">
        <tr>
            <td>
                <div class="heading1">:: ข้อมูลกลุ่มหน้าที่ผู้ใช้งาน ::</div>
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                            <asp:Button ID="btnSearch" runat="server" Text="ค้นหา" cssClass="awesome"
                                onclick="btnSearch_Click"/>
                        </td>
                        <td>
                            <asp:Button ID="btnClear" runat="server" Text="ล้างข้อมูล" CssClass="awesome" OnClientClick="return goPage('../../Module/MasterData/RoleList.aspx');"/>
                        </td>
                        <td>
                            <asp:Button ID="btnAdd" runat="server" Text="เพิ่มข้อมูล" CssClass="awesome" OnClientClick="return goPage('../../Module/MasterData/RoleForm.aspx');"/>
                        </td>
                        <!--<td>
                            <asp:Button ID="Button1" runat="server" Text="จัดการสิทธิ์สาขา" CssClass="awesome" OnClientClick="return goPage('../../Module/MasterData/RoleBranchList.aspx');"/>
                        </td> ! -->
                        <td>
                            <asp:Button ID="btnExit" runat="server" Text="ออกจากระบบ" CssClass="awesome" OnClientClick="return goPage('../../Module/MasterData/MainPage.aspx');"/>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <fieldset>
                    <legend>การค้นหา</legend>
                    <table width="100%">
                        <tr>
                            <td align="right" width="20%">กลุ่มหน้าที่ผู้ใช้งาน :</td>
                            <td align="left" width="30%">
                                <asp:TextBox ID="txtName" runat="server" CssClass="TEXTBOX" Width="100"></asp:TextBox>
                            </td>
                            <td align="right" width="20%">สถานะ :</td>
                            <td align="left" width="30%"> 
                                <asp:DropDownList ID="ddlStatus" runat="server">
                                </asp:DropDownList>                               
                            </td>
                        </tr>
                    </table>
                </fieldset>            
            </td>
        </tr>
        <tr>
            <td>            
                <fieldset>
                    <legend>ผลลัพธ์การค้นหา</legend>
                    <table width="100%">
                        <tr>
                            <td>                            
                                <uc1:ucPageNavigator ID="ucPageNavigator1" runat="server" OnPageIndexChanged="PageIndexChanged"/>                            
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:DataGrid ID="dtgData" runat="server" AutoGenerateColumns="false" Width="100%"
                                    HeaderStyle-CssClass="gridhead" ItemStyle-CssClass="gridrow0" AlternatingItemStyle-CssClass="gridrow1"
                                    AllowPaging="true" PagerStyle-Visible="false">
                                    <Columns>
                                        <asp:TemplateColumn>
						                    <HeaderTemplate>
                                                <%#ColumnTitle[0]%>					                
						                    </HeaderTemplate>
						                    <ItemTemplate>
						                        <%# Container.DataSetIndex +1%>
						                    </ItemTemplate>
					                    </asp:TemplateColumn>
                                        <asp:TemplateColumn>
						                    <HeaderTemplate>
							                    <asp:LinkButton ID="lnkID1" Runat="server" OnClick="sortColumn" text="<%#ColumnTitle[1]%>">
							                    </asp:LinkButton>
							                    <asp:Image ID="imgSortCol1" Runat="server" ImageUrl="<%#getPicUrl(1)%>">
							                    </asp:Image>
						                    </HeaderTemplate>
						                    <ItemTemplate>
						                        <a href="#" onclick="goPage('../../Module/MasterData/RoleForm.aspx?roleid=<%#Eval(ColumnName[0])%>');"><%#Eval(ColumnName[1])%></a>
						                    </ItemTemplate>
					                    </asp:TemplateColumn>					                    
					                    <asp:TemplateColumn>
						                    <HeaderTemplate>
							                    <asp:LinkButton ID="lnkID2" Runat="server" OnClick="sortColumn" text="<%#ColumnTitle[2]%>">
							                    </asp:LinkButton>
							                    <asp:Image ID="imgSortCol2" Runat="server" ImageUrl="<%#getPicUrl(2)%>">
							                    </asp:Image>
						                    </HeaderTemplate>
						                    <ItemTemplate>
						                        <%#FormatStringApp.getStatusName(Eval(ColumnName[2]))%>
						                    </ItemTemplate>
					                    </asp:TemplateColumn>
					                     <asp:TemplateColumn>
						                    <HeaderTemplate>
							                    <asp:LinkButton ID="lnkID3" Runat="server" OnClick="sortColumn" text="<%#ColumnTitle[3]%>">
							                    </asp:LinkButton>
							                    <asp:Image ID="imgSortCol3" Runat="server" ImageUrl="<%#getPicUrl(3)%>">
							                    </asp:Image>
						                    </HeaderTemplate>
						                    <ItemTemplate>
						                        <%#getAllMenuByRoleID(Eval(ColumnName[0]))%>
						                    </ItemTemplate>
					                    </asp:TemplateColumn>
					                    <asp:TemplateColumn>
						                    <HeaderTemplate>
							                    <%#ColumnTitle[4]%>
						                    </HeaderTemplate>
						                    <ItemTemplate>
						                        <%#GetBranchConfig(Eval(ColumnName[4]))%><%#GetAllBranchSpecific(Eval(ColumnName[0]))%>
						                    </ItemTemplate>
					                    </asp:TemplateColumn>
					                </Columns>
                                </asp:DataGrid>
                            </td>
                        </tr>
                    </table>
                </fieldset>        
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Literal ID="litJavaScript" runat="server"></asp:Literal>
            </td>
        </tr>
    </table>
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
</asp:Content>

