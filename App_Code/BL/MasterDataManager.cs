using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Web.Security;
using System.Xml.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Security.Cryptography;
using System.Text;
using System.IO; 
using System.Linq;
//////////////////////////////////////// 
using System.Collections.Generic;

 

/// <summary>
/// Summary description for MasterDataManager
/// </summary>
public class MasterDataManager : TDS.BL.BLWorker_MSSQL
{
	public MasterDataManager()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    #region for login method
    public DataSet getDataUserByLogin(string strLogin)
    {
        string strSQL = "";
        DataSet ds;
        strSQL = "SELECT A.*, ISNULL(A.user_fname,'') + ' ' + ISNULL(A.user_lname,'') as user_fullname,";
        strSQL += "     B.role_name,  T.team_name, ";
        strSQL += "     UI.user_fname + ' ' + UI.user_lname as insert_name, ";
        strSQL += "     UU.user_fname + ' ' + UU.user_lname as update_name ";
        strSQL += " FROM aut_user  A INNER JOIN aut_role B ON A.role_id = B.role_id ";
        strSQL += "     LEFT JOIN aut_user UI ON A.insert_login = UI.user_login ";
        strSQL += "     LEFT JOIN aut_user UU ON A.update_login = UU.user_login ";
        strSQL += "     LEFT JOIN mst_team T ON A.team_id = T.team_id ";
        strSQL += " WHERE A.user_login = '" + strLogin + "' ";
        ds = executeQuery(strSQL);
        return ds;
    }
    public string doLogin(string strUserLogin, string strPassword)
    {
        string strSQL = "";
        string strRet = "";
        DataSet ds;

        //1. ตรวจสอบ รหัสผู้ใช้งาน
        strSQL = "SELECT * ";
        strSQL += " FROM aut_user ";
        strSQL += " WHERE user_login = '" + TDS.Utility.MasterUtil.replaceSpecialSQL(strUserLogin) + "' AND ";
        strSQL += "     user_pwd = '" + encode(strPassword, strUserLogin) + "' ";
        ds = executeQuery(strSQL);
        if (ds.Tables[0].Rows.Count > 0)
        {
            if (ds.Tables[0].Rows[0]["user_status"].ToString() == "A")
            {
                if (DateTime.Compare((DateTime)ds.Tables[0].Rows[0]["start_date"], DateTime.Now) > 0)
                {
                    strRet = "1 : รหัสผู้ใช้ยังไม่เปิดให้เข้าสู่ระบบได้ ท่านสามารถเข้าระบบได้ตั้งแต่วันที่ " + TDS.Utility.MasterUtil.getDateString((DateTime)ds.Tables[0].Rows[0]["start_date"], "dd/MM/yyyy", "th-TH");
                }
                else if ((!Convert.IsDBNull(ds.Tables[0].Rows[0]["stop_date"])) && (DateTime.Compare((DateTime)ds.Tables[0].Rows[0]["stop_date"], DateTime.Now.AddDays(-1)) < 0))
                {
                    strRet = "1 : รหัสผู้ใช้หมดอายุแล้ว ตั้งแต่วันที่ " + TDS.Utility.MasterUtil.getDateString((DateTime)ds.Tables[0].Rows[0]["stop_date"], "dd/MM/yyyy", "th-TH");
                }

                //if (strRet == "" &&
                //        (!Convert.IsDBNull(ds.Tables[0].Rows[0]["user_lastlogindate"])) &&
                //        (DateTime.Compare(((DateTime)ds.Tables[0].Rows[0]["user_lastlogindate"]).AddDays(Convert.ToDouble(System.Configuration.ConfigurationManager.AppSettings["loginDays"])), DateTime.Now.AddDays(-1)) < 0))
                //{
                //    strRet = "1 : ท่านไม่ได้เข้าใช้งานเกินกำหนด";
                //    strRet += "\\nกรุณาติดต่อผู้ดูแลระบบเพื่อทำการเคลียร์ค่าวันใช้งานล่าสุด";
                //}
            }
            else
            {
                strRet = "1 : รหัสผู้ใช้ของท่านได้ถูกยกเลิกไปแล้ว ";
            }
        }
        else
        {
            strSQL = "SELECT * ";
            strSQL += " FROM aut_user ";
            strSQL += " WHERE user_login = '" + TDS.Utility.MasterUtil.replaceSpecialSQL(strUserLogin) + "' ";
            ds = executeQuery(strSQL);
            if (ds.Tables[0].Rows.Count > 0)
            {
                strRet = "2 : รหัสผ่านไม่ถูกต้อง";
            }
            else
            {
                strRet = "3 : ไม่พบรหัสผู้ใช้งานที่ระบุ";
            }
        }
        //if (strRet == "")
        //{
        //    // 2. ตรวจสอบว่าเคยมีการเข้าใช้งานหรือยัง
        //    if (Convert.IsDBNull(ds.Tables[0].Rows[0]["user_lastlogindate"]))
        //    {
        //        strRet = "0 : คุณเข้าใช้งานระบบเป็นครั้งแรก จึงต้องทำการเปลี่ยนรหัสผ่านก่อน";
        //    }
        //}
        if (strRet == "")
        {
            // 3. กรณีสามารถเข้าใช้งานได้ ทำการ update ค่า user_lastlogindate ด้วย
            strSQL = "UPDATE aut_user SET";
            strSQL += " user_lastlogindate = getDate(),";
            strSQL += " update_date = getDate(), ";
            strSQL += " update_login = '" + strUserLogin + "' ";
            strSQL += " WHERE user_login = '" + TDS.Utility.MasterUtil.replaceSpecialSQL(strUserLogin) + "' ";
            executeUpdate(strSQL);
        }
        return strRet;
    }
    public DataSet getDataMenuByRoleID(string strRoleID)
    {
        string strSQL = "";
        DataSet ds;
        strSQL = "SELECT * ";
        strSQL += ",case when A.role_menu_flagadd='Y' then 'บันทึก' else '' end flagadd  ,";
        strSQL += "case when A.role_menu_flagedit='Y' then ',แก้ไข' else '' end flagedit   , ";
        strSQL += "case when A.role_menu_flagdelete='Y' then ',ยกเลิก' else '' end flagdelete , ";
        strSQL += "case when A.role_menu_flaginquiry='Y' then ',สอบถาม' else '' end flaginquiry ";
        strSQL += " FROM aut_role_menu A INNER JOIN aut_menu B ON A.menu_id = B.menu_id ";
        strSQL += " WHERE A.role_id = '" + strRoleID + "' AND ";
        strSQL += "     role_menu_status = 'A' ";

        ds = executeQuery(strSQL);
        return ds;
    }
    public DataSet getDataMenuByuserID(string strUserID)
    {
        string strSQL = "";
        DataSet ds;
        strSQL = "SELECT * ";
        strSQL += ",case when A.urmenu_flagadd='Y' then 'บันทึก' else '' end flagadd  ,";
        strSQL += "case when A.urmenu_flagedit='Y' then ',แก้ไข' else '' end flagedit   , ";
        strSQL += "case when A.urmenu_flagdelete='Y' then ',ยกเลิก' else '' end flagdelete , ";
        strSQL += "case when A.urmenu_flaginquiry='Y' then ',สอบถาม' else '' end flaginquiry ";
        strSQL += " FROM aut_user_menu A INNER JOIN aut_menu B ON A.menu_id = B.menu_id ";
        strSQL += " WHERE A.user_login = '" + strUserID + "' AND ";
        strSQL += "     urmenu_status = 'A' ";

        ds = executeQuery(strSQL);
        return ds;
    }
    #endregion
    public DataSet getModuleByRoleID(string strRoleID)
    {
        DataSet ds = new DataSet();
        string strSQL = "";

        strSQL = "SELECT distinct C.modu_id, C.modu_name, B.role_id ";
        strSQL += " FROM aut_menu A INNER JOIN aut_role_menu B ON A.menu_id = B.menu_id AND ";
        strSQL += "     B.role_menu_status = 'A' ";
        strSQL += "     INNER JOIN aut_module C ON A.modu_id = C.modu_id AND C.modu_status = 'A' ";
        strSQL += " WHERE  B.role_id = '" + strRoleID + "' ";
        strSQL += " ORDER BY C.modu_id ";
        ds = executeQuery(strSQL);
        return ds;
    }
    public DataSet getModuleByID(string strMenuID)
    {
        DataSet ds = new DataSet();
        string strSQL = "";

        strSQL = "SELECT A.* ";
        strSQL += " FROM aut_menu A  ";
        strSQL += " WHERE  A.menu_id = '" + strMenuID + "' ";
        strSQL += " ORDER BY A.menu_id ";
        ds = executeQuery(strSQL);
        return ds;
    }

    public DataSet getMenuByRoleModuleUser(string strRoleID, string strModule, string strUser)
    {
        DataSet ds = new DataSet();
        string strSQL = "";

        strSQL = " SELECT * FROM (";
        strSQL += "SELECT A.* ";
        strSQL += " FROM aut_menu A INNER JOIN aut_role_menu B ON A.menu_id = B.menu_id AND ";
        strSQL += "     B.role_menu_status = 'A'";
        strSQL += "     ";
        strSQL += " WHERE  B.role_id = '" + strRoleID + "' AND";
        strSQL += "     modu_id = '" + strModule + "' ";
        strSQL += " UNION ";
        strSQL += "SELECT A.* ";
        strSQL += " FROM aut_menu A INNER JOIN aut_user_menu B ON A.menu_id = B.menu_id AND ";
        strSQL += "     B.urmenu_status = 'A'";
        strSQL += "     ";
        strSQL += " WHERE  B.user_login = '" + strUser + "' AND";
        strSQL += "     modu_id = '" + strModule + "' ";
        strSQL += " ) TMP ";
        strSQL += " ORDER BY menu_no ";
        ds = executeQuery(strSQL);
        return ds;
    }
    public DataSet getMenuByRoleID(string strRoleID)
    {
        DataSet ds = new DataSet();
        string strSQL = "";

        strSQL = "SELECT distinct A.*,B.role_menu_flagadd,B.role_menu_flagedit,B.role_menu_flagdelete,B.role_menu_flaginquiry ";
        strSQL += " FROM aut_menu A INNER JOIN aut_role_menu B ON A.menu_id = B.menu_id AND ";
        strSQL += "     B.role_menu_status = 'A' AND ";
        strSQL += "     B.role_id = '" + strRoleID + "' ";
        strSQL += " WHERE menu_status = 'A' ";
        strSQL += " ORDER BY menu_no ";
        ds = executeQuery(strSQL);
        return ds;
    }
    public DataSet getTableByRoleID(string strRoleID)
    {
        DataSet ds = new DataSet();
        string strSQL = "";

        strSQL = "SELECT A.* ";
        strSQL += " FROM mis_table A INNER JOIN aut_role_table B ON A.rgt_id = B.rgt_id AND ";
        strSQL += "     B.role_table_status = 'A' AND ";
        strSQL += "     B.role_id = '" + strRoleID + "' ";
        //strSQL += " WHERE menu_status = 'A' ";
        strSQL += " ORDER BY rgt_desc ";
        ds = executeQuery(strSQL);
        return ds;
    }
    public DataSet getJobByRoleID(string strRoleID)
    {
        DataSet ds = new DataSet();
        string strSQL = "";

        strSQL = "SELECT A.* ";
        strSQL += " FROM mis_job A INNER JOIN aut_role_job B ON A.rgj_id = B.rgj_id AND ";
        strSQL += "     B.role_job_status = 'A' AND ";
        strSQL += "     B.role_id = '" + strRoleID + "' ";
        //strSQL += " WHERE menu_status = 'A' ";
        strSQL += " ORDER BY rgj_desc ";
        ds = executeQuery(strSQL);
        return ds;
    }
    public DataSet getAllModule()
    {
        DataSet ds = new DataSet();
        string strSQL = "";

        strSQL = "SELECT A.* ";
        strSQL += " FROM aut_module A ";
        strSQL += " WHERE A.modu_status = 'A' ";
        strSQL += " ORDER BY A.modu_id ";
        ds = executeQuery(strSQL);
        return ds;
    }
    public DataSet getAllMenu()
    {
        DataSet ds = new DataSet();
        string strSQL = "";

        strSQL = "SELECT A.* ";
        strSQL += " FROM aut_menu A ";
        strSQL += " WHERE A.menu_status = 'A' ";
        strSQL += " ORDER BY A.menu_no ";
        ds = executeQuery(strSQL);
        return ds;
    }
    public DataSet getMenuByModule(string strModule)
    {
        DataSet ds = new DataSet();
        string strSQL = "";

        strSQL = "SELECT A.* ";
        strSQL += " FROM aut_menu A ";
        strSQL += " WHERE A.menu_status = 'A' AND ";
        strSQL += "     A.modu_id = '" + strModule + "' ";
        strSQL += " ORDER BY A.menu_no ";
        ds = executeQuery(strSQL);
        return ds;
    }
    public DataSet getAllDataRole()
    {
        DataSet ds = new DataSet();
        string strSQL = "";
        strSQL = "SELECT * ";
        strSQL += " FROM aut_role ";
        strSQL += " ORDER BY role_name ";
        ds = executeQuery(strSQL);
        return ds;
    }
    public DataSet getAllDataBranch()
    {
        DataSet ds = new DataSet();
        string strSQL = "";
        strSQL = "SELECT * ";
        strSQL += " FROM mst_branch ";
        strSQL += " ORDER BY branch_name ";
        ds = executeQuery(strSQL);
        return ds;
    }
    public DataSet getAllDatateam()
    {
        DataSet ds = new DataSet();
        string strSQL = "";
        strSQL = "SELECT * ";
        strSQL += " FROM mst_team ";
        strSQL += " ORDER BY team_name ";
        ds = executeQuery(strSQL);
        return ds;
    }
    public DataSet searchRole(string strRoleName, string strStatus)
    {
        DataSet ds = new DataSet();
        string strSQL = "";
        string strWhere = "";
        string strMenu = "";
        SqlConnection conn;
        SqlTransaction trans;
        conn = getDbConnection();
        trans = conn.BeginTransaction();

        //strMenu = genMenu(strRoleName, strStatus);
        strSQL = "SELECT A.* ";
        //strSQL += ",m.menu_name,case when r.role_menu_flagadd='Y' then 'บันทึก' else '' end flagadd  ,";
        //strSQL += "case when role_menu_flagedit='Y' then ',แก้ไข' else '' end flagedit   , ";
        //strSQL += "case when role_menu_flagdelete='Y' then ',ยกเลิก' else '' end flagdel , ";
        //strSQL += "case when role_menu_flaginquiry='Y' then ',สอบถาม' else '' end flaginq ";
        strSQL += " FROM aut_role A ";
        //strSQL += " inner join aut_menu m on r.menu_id=m.menu_id ";
        if (strRoleName != "")
            strWhere += " A.role_name like '%" + strRoleName + "%' AND ";
        if (strStatus != "")
            strWhere += " A.role_status = '" + strStatus + "' AND ";

        if (strWhere != "")
            strSQL = strSQL + " WHERE " + strWhere.Substring(0, strWhere.Length - 4);

        strSQL = strSQL + " ORDER BY A.role_name ";
        ds = executeQuery(strSQL);
        return ds;
    }
    public string genMenu(string strRoleName, string strStatus)
    {
        string strMenu = "";
        string strSQL = "";
        string strWhere = "";
        DataSet ds;
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();
        strSQL = "SELECT A.* ,m.menu_name , r.role_menu_flagadd  ,r.role_menu_flagedit   , r.role_menu_flagdelete , r.role_menu_flaginquiry";
        strSQL += " FROM aut_role A inner join aut_role_menu r on a.role_id=r.role_id ";
        strSQL += " inner join aut_menu m on r.menu_id=m.menu_id ";
        if (strRoleName != "")
            strWhere += " A.role_name like '%" + strRoleName + "%' AND ";
        if (strStatus != "")
            strWhere += " A.role_status = '" + strStatus + "' AND  ";

        if (strWhere != "")
            strSQL = strSQL + " WHERE " + strWhere.Substring(0, strWhere.Length - 4);

        strSQL = strSQL + " ORDER BY A.role_name ";
        ds = executeQuery(strSQL);

        strMenu = ds.Tables[0].Rows[0]["menu_name"].ToString()+"(";
        if (ds.Tables[0].Rows[0]["role_menu_flagadd"].ToString() == "Y")
        {
            strMenu += "Add";
        }
        if (ds.Tables[0].Rows[0]["role_menu_flagedit"].ToString() == "Y")
        {
            strMenu += ",Edit";
        }
        if (ds.Tables[0].Rows[0]["role_menu_flagdelete"].ToString() == "Y")
        {
            strMenu += ",Delete";
        }
        if (ds.Tables[0].Rows[0]["role_menu_flaginquiry"].ToString() == "Y")
        {
            strMenu += ",Inquiry";
        }
            strMenu += ")";

        return strMenu;
    }
    public DataSet getDataRoleByID(string strRoleID)
    {
        DataSet ds ;
        string strSQL = "";
        strSQL = "SELECT * ";
        strSQL += " FROM aut_role ";
        strSQL += " WHERE ROLE_ID = '" + strRoleID + "'";
        strSQL += " ORDER BY role_name ";
        ds = executeQuery(strSQL);
        return ds;
    }
    public string addDataRole(DataSet ds, DataSet dsBranch)
    {
        bool isRet = true;
        string strRet = "";
        string strSQL = "";
        string[] arrMenu ;
        string[] arrMenuFlag;
        string[] arrFlag;
        string[] arrTable;
        string strMenu = "";
        string strFlagAdd = "";
        string strFlagEdit = "";
        string strFlagDelete = "";
        string strFlagInquiry = "";
        SqlConnection conn;
        SqlTransaction trans;

        conn = getDbConnection();
        trans = conn.BeginTransaction();
        try
        {
            // 1. insert into aut_role
            // Add new Column[role_flagbranch][with allow null because table not emtry] on 6/2/15
            strRet = getMaxID("role_id", "aut_role","",3,true,true);
            strSQL = "INSERT INTO aut_role(role_id, role_name, role_status,role_flagbranch, role_canceldate, role_cancellogin, ";
            strSQL += " insert_date, insert_login) ";
            strSQL += " VALUES( ";
            strSQL += " '" + strRet + "', ";
            strSQL += " '" + ds.Tables[0].Rows[0]["role_name"].ToString() + "', ";
            strSQL += " '" + ds.Tables[0].Rows[0]["role_status"].ToString() + "', ";
            strSQL += " '" + ds.Tables[0].Rows[0]["role_flagbranch"].ToString() + "', ";
            if (ds.Tables[0].Rows[0]["role_status"].ToString() == "C")
            {
                strSQL += " getDate(), ";
                strSQL += " '" + ds.Tables[0].Rows[0]["LOGINNAME"].ToString() + "', ";
            }
            else
            {
                strSQL += " null, ";
                strSQL += " null, ";
            }
            strSQL += " getDate(), ";
            strSQL += " '" + ds.Tables[0].Rows[0]["LOGINNAME"].ToString() + "' ";
            strSQL += " ) ";
            executeUpdate(strSQL, conn, trans);

            // 2. insert into aut_role_menu
            if (ds.Tables[0].Rows[0]["MENU"].ToString() != "")
            {
                arrMenu = ds.Tables[0].Rows[0]["MENU"].ToString().Split(',');
                
                for (int i = 0; i <= arrMenu.Length - 1; i++)
                {
                    arrMenuFlag = arrMenu[i].ToString().Split('-');
                    strMenu = arrMenuFlag[0];
                    arrFlag = arrMenuFlag[1].ToString().Split('_');
                    strFlagAdd = arrFlag[0];
                    strFlagEdit = arrFlag[1];
                    strFlagDelete = arrFlag[2];
                    strFlagInquiry = arrFlag[3];
                    strSQL = "INSERT INTO aut_role_menu(role_menu_id, role_id, menu_id, role_menu_status, ";
                    strSQL += " insert_date, insert_login,role_menu_flagadd,role_menu_flagedit,role_menu_flagdelete,role_menu_flaginquiry ) ";
                    strSQL += " VALUES( ";
                    strSQL += " '" + getMaxID("role_menu_id", "aut_role_menu", "", 3, true, true, conn, trans) + "', ";
                    strSQL += " '" + strRet + "', ";
                    strSQL += " '" + strMenu + "', ";
                    strSQL += " 'A', ";                    
                    strSQL += " getDate(), ";
                    strSQL += " '" + ds.Tables[0].Rows[0]["LOGINNAME"].ToString() + "', ";
                    strSQL += " '" + strFlagAdd + "', '" + strFlagEdit + "', '" + strFlagDelete + "', '" + strFlagInquiry + "' ) ";
                    executeUpdate(strSQL, conn, trans);
                }
            }
            //New Insert into aut_role_branch
            //Insert Code by Myponn
            //Concept Add Role_id + Branch_code and robr_status,insert_date,insert_login
            //Update info will be equal to insert fields
            //Dataset table[1] From RoleForm be contained Branch info
            //Add reference Dataset from UI[role_id,branch_code,robr_status,insert_date,insert_login,update_date,update_login]
            if (ds.Tables[0].Rows[0]["role_flagbranch"] == "S")
            {
                for (int i = 0; i < dsBranch.Tables[0].Rows.Count; i++)
                {
                    strSQL = "insert into aut_role_branch(role_id,branch_code,robr_status,insert_date,insert_login,update_date,update_login) values(";
                    strSQL += " '" + dsBranch.Tables[0].Rows[i]["role_id"] + "',";
                    strSQL += " '" + dsBranch.Tables[0].Rows[i]["branch_code"] + "',";
                    strSQL += " '" + dsBranch.Tables[0].Rows[i]["robr_status"] + "',";
                    strSQL += "getDate(),"; //" " + dsBranch.Tables[0].Rows[i]["insert_date"] + ",";
                    strSQL += " '" + dsBranch.Tables[0].Rows[i]["insert_login"] + "',";
                    strSQL += "getDate(),"; //" " + dsBranch.Tables[0].Rows[i]["update_date"] + ",";
                    strSQL += " '" + dsBranch.Tables[0].Rows[i]["update_login"] + "')";
                    executeUpdate(strSQL, conn, trans);
                }
            }
            else 
            {
                executeUpdate("delete from aut_role_branch where role_id='" + ds.Tables[0].Rows[0]["role_id"].ToString() + "'");
            }
            // 3. insert into aut_role_table
        //if (ds.Tables[0].Rows[0]["TABLE"].ToString() != "")
        //{
        //    arrTable = ds.Tables[0].Rows[0]["TABLE"].ToString().Split(',');
        //    for (int i = 0; i <= arrTable.Length - 1; i++)
        //    {
        //        strSQL = "INSERT INTO aut_role_table(role_table_id, role_id, rgt_id, role_table_status, ";
        //        strSQL += " insert_date, insert_login) ";
        //        strSQL += " VALUES( ";
        //        strSQL += " '" + getMaxID("role_table_id", "aut_role_table", "", 3, true, true, conn, trans) + "', ";
        //        strSQL += " '" + strRet + "', ";
        //        strSQL += " '" + arrTable[i] + "', ";
        //        strSQL += " 'A', ";
        //        strSQL += " getDate(), ";
        //        strSQL += " '" + ds.Tables[0].Rows[0]["LOGINNAME"].ToString() + "' ";
        //        strSQL += " ) ";
        //        executeUpdate(strSQL, conn, trans);
        //    }
        //}
            // 4. insert into aut_role_job
            //if (ds.Tables[0].Rows[0]["JOB"].ToString() != "")
            //{
            //    arrTable = ds.Tables[0].Rows[0]["JOB"].ToString().Split(',');
            //    for (int i = 0; i <= arrTable.Length - 1; i++)
            //    {
            //        strSQL = "INSERT INTO aut_role_job(role_job_id, role_id, rgj_id, role_job_status, ";
            //        strSQL += " insert_date, insert_login) ";
            //        strSQL += " VALUES( ";
            //        strSQL += " '" + getMaxID("role_job_id", "aut_role_job", "", 3, true, true, conn, trans) + "', ";
            //        strSQL += " '" + strRet + "', ";
            //        strSQL += " '" + arrTable[i] + "', ";
            //        strSQL += " 'A', ";
            //        strSQL += " getDate(), ";
            //        strSQL += " '" + ds.Tables[0].Rows[0]["LOGINNAME"].ToString() + "' ";
            //        strSQL += " ) ";
            //        executeUpdate(strSQL, conn, trans);
            //    }
            //}

            trans.Commit();
        }
        catch (Exception e)
        {
            trans.Rollback();
            strRet = "ERROR";
            TDS.Utility.MasterUtil.writeError("ERROR ON MasterData.addDataRole()", e.ToString());
            TDS.Utility.MasterUtil.writeError("ERROR ON MasterData.addDataRole()", strSQL);
        }
        finally
        {
            trans.Dispose();
            conn.Close();
            conn.Dispose();
        }
        return strRet;
    }
    public bool editDataRole(DataSet ds,DataSet dsBranch)
    {
        bool isRet = true;
        string strSQL = "";
        string[] arrMenu;
        string[] arrTable;
        string[] arrMenuFlag;
        string[] arrFlag;
        string strMenu = "";
        string strFlagAdd = "";
        string strFlagEdit = "";
        string strFlagDelete = "";
        string strFlagInquiry = "";
        SqlConnection conn;
        SqlTransaction trans;
        DataSet dsTmp;

        conn = getDbConnection();
        trans = conn.BeginTransaction();
        try
        {
            // 1. UPDATE aut_role
            strSQL = "UPDATE aut_role SET ";
            strSQL += " role_name = '" + ds.Tables[0].Rows[0]["role_name"].ToString() + "', ";
            strSQL += " role_status = '" + ds.Tables[0].Rows[0]["role_status"].ToString() + "', ";
            strSQL += "     role_flagbranch = '" + ds.Tables[0].Rows[0]["role_flagbranch"].ToString() +"',";
            if (ds.Tables[0].Rows[0]["role_status"].ToString() == "C")
            {
                strSQL += " role_canceldate = getDate(), ";
                strSQL += " role_cancellogin = '" + ds.Tables[0].Rows[0]["LOGINNAME"].ToString() + "', ";
            }
            else
            {
                strSQL += " role_canceldate = null, ";
                strSQL += " role_cancellogin = null, ";
            }
            strSQL += " update_date = getDate(), ";
            strSQL += " update_login = '" + ds.Tables[0].Rows[0]["LOGINNAME"].ToString() + "' ";
            strSQL += " WHERE role_id = '" + ds.Tables[0].Rows[0]["role_id"].ToString() + "' ";
            executeUpdate(strSQL, conn, trans);

            // 2. update aut_role_menu.role_menu_status = 'C'
            strSQL = "UPDATE aut_role_menu ";
            strSQL += " SET  role_menu_status = 'C', ";
            strSQL += "     update_date = getDate(), ";
            strSQL += "     update_login = '" + ds.Tables[0].Rows[0]["LOGINNAME"].ToString() + "' ";
            strSQL += " WHERE role_id = '" + ds.Tables[0].Rows[0]["role_id"].ToString() + "' "; 
            executeUpdate(strSQL, conn, trans);
            // 3. insert or update aut_role_menu.role_menu_status = 'A'
            arrMenu = ds.Tables[0].Rows[0]["MENU"].ToString().Split(',');
            for (int i = 0; i <= arrMenu.Length - 1; i++)
            {
                arrMenuFlag = arrMenu[i].ToString().Split('-');
                strMenu = arrMenuFlag[0];
                arrFlag = arrMenuFlag[1].ToString().Split('_');
                strFlagAdd = arrFlag[0];
                strFlagEdit = arrFlag[1];
                strFlagDelete = arrFlag[2];
                strFlagInquiry = arrFlag[3];
                strSQL = "SELECT * FROM aut_role_menu ";
                strSQL += " WHERE role_id = '" + ds.Tables[0].Rows[0]["role_id"].ToString() + "' AND ";
                strSQL += "     menu_id = '" + strMenu + "' ";
                dsTmp = executeQuery(strSQL, conn, trans);
                if (dsTmp.Tables[0].Rows.Count > 0)
                {
                    // UPDATE
                    strSQL = "UPDATE aut_role_menu ";
                    strSQL += " SET  role_menu_status = 'A', ";
                    strSQL += "     update_date = getDate(), ";
                    strSQL += "     update_login = '" + ds.Tables[0].Rows[0]["LOGINNAME"].ToString() + "', ";
                    strSQL += "     role_menu_flagadd = '" + strFlagAdd + "',role_menu_flagedit = '" + strFlagEdit + "', ";
                    strSQL += "     role_menu_flagdelete = '" + strFlagDelete + "',role_menu_flaginquiry = '" + strFlagInquiry + "' ";
                    strSQL += " WHERE role_id = '" + ds.Tables[0].Rows[0]["role_id"].ToString() + "' AND ";
                    strSQL += "     menu_id = '" + strMenu + "' ";
                    executeUpdate(strSQL, conn, trans);
                }
                else
                {
                    strSQL = "INSERT INTO aut_role_menu(role_menu_id, role_id, menu_id, role_menu_status, ";
                    strSQL += " insert_date, insert_login,role_menu_flagadd,role_menu_flagedit,role_menu_flagdelete,role_menu_flaginquiry) ";
                    strSQL += " VALUES( ";
                    strSQL += " '" + getMaxID("role_menu_id", "aut_role_menu", "", 3, true, true, conn, trans) + "', ";
                    strSQL += " '" + ds.Tables[0].Rows[0]["role_id"].ToString() + "', ";
                    strSQL += " '" + strMenu + "', ";
                    strSQL += " 'A', ";
                    strSQL += " getDate(), ";
                    strSQL += " '" + ds.Tables[0].Rows[0]["LOGINNAME"].ToString() + "', ";
                    strSQL += " '" + strFlagAdd + "', '" + strFlagEdit + "', '" + strFlagDelete + "', '" + strFlagInquiry + "' ";
                    strSQL += " ) ";
                    executeUpdate(strSQL, conn, trans);
                }
            }
            //Begin Added by  Myponn
            //Save Branch Info to DB
            if (ds.Tables[0].Rows[0]["role_flagbranch"] == "S")
            {
                strSQL = "delete from aut_role_branch where role_id='" + dsBranch.Tables[0].Rows[0]["role_id"] + "'";
                executeQuery(strSQL);
                for (int i = 0; i < dsBranch.Tables[0].Rows.Count; i++)
                {
                    strSQL = "insert into aut_role_branch(role_id,branch_code,robr_status,insert_date,insert_login,update_date,update_login) values(";
                    strSQL += " '" + dsBranch.Tables[0].Rows[i]["role_id"] + "',";
                    strSQL += " '" + dsBranch.Tables[0].Rows[i]["branch_code"] + "',";
                    strSQL += " '" + dsBranch.Tables[0].Rows[i]["robr_status"] + "',";
                    strSQL += " getDate(),";//" " + dsBranch.Tables[0].Rows[i]["insert_date"] + ",";  //
                    strSQL += " '" + dsBranch.Tables[0].Rows[i]["insert_login"] + "',";
                    strSQL += " getDate(),";//" " + dsBranch.Tables[0].Rows[i]["update_date"] + ",";  //
                    strSQL += " '" + dsBranch.Tables[0].Rows[i]["update_login"] + "')";
                    executeUpdate(strSQL, conn, trans);
                }
            }
            else
            {
                executeUpdate("delete from aut_role_branch where role_id='" + ds.Tables[0].Rows[0]["role_id"].ToString() + "'");
            }
            //// 4. update aut_role_table.role_table_status = 'C'
            //strSQL = "UPDATE aut_role_table ";
            //strSQL += " SET  role_table_status = 'C', ";
            //strSQL += "     update_date = getDate(), ";
            //strSQL += "     update_login = '" + ds.Tables[0].Rows[0]["LOGINNAME"].ToString() + "' ";
            //strSQL += " WHERE role_id = '" + ds.Tables[0].Rows[0]["role_id"].ToString() + "' ";
            //executeUpdate(strSQL, conn, trans);
            //// 5. insert or update aut_role_table.role_table_status = 'A'
            //arrTable = ds.Tables[0].Rows[0]["TABLE"].ToString().Split(',');
            //for (int i = 0; i <= arrTable.Length - 1; i++)
            //{
            //    strSQL = "SELECT * FROM aut_role_table ";
            //    strSQL += " WHERE role_id = '" + ds.Tables[0].Rows[0]["role_id"].ToString() + "' AND ";
            //    strSQL += "     rgt_id = '" + arrTable[i] + "' ";
            //    dsTmp = executeQuery(strSQL, conn, trans);
            //    if (dsTmp.Tables[0].Rows.Count > 0)
            //    {
            //        // UPDATE
            //        strSQL = "UPDATE aut_role_table ";
            //        strSQL += " SET  role_table_status = 'A', ";
            //        strSQL += "     update_date = getDate(), ";
            //        strSQL += "     update_login = '" + ds.Tables[0].Rows[0]["LOGINNAME"].ToString() + "' ";
            //        strSQL += " WHERE role_id = '" + ds.Tables[0].Rows[0]["role_id"].ToString() + "' AND ";
            //        strSQL += "     rgt_id = '" + arrTable[i] + "' ";
            //        executeUpdate(strSQL, conn, trans);
            //    }
            //    else
            //    {
            //        strSQL = "INSERT INTO aut_role_table(role_table_id, role_id, rgt_id, role_table_status, ";
            //        strSQL += " insert_date, insert_login) ";
            //        strSQL += " VALUES( ";
            //        strSQL += " '" + getMaxID("role_table_id", "aut_role_table", "", 3, true, true, conn, trans) + "', ";
            //        strSQL += " '" + ds.Tables[0].Rows[0]["role_id"].ToString() + "', ";
            //        strSQL += " '" + arrTable[i] + "', ";
            //        strSQL += " 'A', ";
            //        strSQL += " getDate(), ";
            //        strSQL += " '" + ds.Tables[0].Rows[0]["LOGINNAME"].ToString() + "' ";
            //        strSQL += " ) ";
            //        executeUpdate(strSQL, conn, trans);
            //    }
            //}
            //// 6. update aut_role_table.role_table_status = 'C'
            //strSQL = "UPDATE aut_role_job ";
            //strSQL += " SET  role_job_status = 'C', ";
            //strSQL += "     update_date = getDate(), ";
            //strSQL += "     update_login = '" + ds.Tables[0].Rows[0]["LOGINNAME"].ToString() + "' ";
            //strSQL += " WHERE role_id = '" + ds.Tables[0].Rows[0]["role_id"].ToString() + "' ";
            //executeUpdate(strSQL, conn, trans);
            //// 7. insert or update aut_role_table.role_table_status = 'A'
            //arrTable = ds.Tables[0].Rows[0]["JOB"].ToString().Split(',');
            //for (int i = 0; i <= arrTable.Length - 1; i++)
            //{
            //    strSQL = "SELECT * FROM aut_role_job ";
            //    strSQL += " WHERE role_id = '" + ds.Tables[0].Rows[0]["role_id"].ToString() + "' AND ";
            //    strSQL += "     rgj_id = '" + arrTable[i] + "' ";
            //    dsTmp = executeQuery(strSQL, conn, trans);
            //    if (dsTmp.Tables[0].Rows.Count > 0)
            //    {
            //        // UPDATE
            //        strSQL = "UPDATE aut_role_job ";
            //        strSQL += " SET  role_job_status = 'A', ";
            //        strSQL += "     update_date = getDate(), ";
            //        strSQL += "     update_login = '" + ds.Tables[0].Rows[0]["LOGINNAME"].ToString() + "' ";
            //        strSQL += " WHERE role_id = '" + ds.Tables[0].Rows[0]["role_id"].ToString() + "' AND ";
            //        strSQL += "     rgj_id = '" + arrTable[i] + "' ";
            //        executeUpdate(strSQL, conn, trans);
            //    }
            //    else
            //    {
            //        strSQL = "INSERT INTO aut_role_job(role_job_id, role_id, rgj_id, role_job_status, ";
            //        strSQL += " insert_date, insert_login) ";
            //        strSQL += " VALUES( ";
            //        strSQL += " '" + getMaxID("role_job_id", "aut_role_job", "", 3, true, true, conn, trans) + "', ";
            //        strSQL += " '" + ds.Tables[0].Rows[0]["role_id"].ToString() + "', ";
            //        strSQL += " '" + arrTable[i] + "', ";
            //        strSQL += " 'A', ";
            //        strSQL += " getDate(), ";
            //        strSQL += " '" + ds.Tables[0].Rows[0]["LOGINNAME"].ToString() + "' ";
            //        strSQL += " ) ";
            //        executeUpdate(strSQL, conn, trans);
            //    }
            //}

            trans.Commit();
        }
        catch (Exception e)
        {
            trans.Rollback();
            isRet = false;
            TDS.Utility.MasterUtil.writeError("ERROR ON MasterData.editDataRole()", e.ToString());
            TDS.Utility.MasterUtil.writeError("ERROR ON MasterData.editDataRole()", strSQL);
        }
        finally
        {
            trans.Dispose();
            conn.Close();
            conn.Dispose();
        }
        return isRet;
    }

    public DataSet searchDataUser(string strUserName, string strRoleID,
                                    string strName, string strStatus)
    {
        DataSet ds = new DataSet();
        string strSQL = "";
        string strWhere = "";

        strSQL = "SELECT A.*, A.user_fname + ' ' + A.user_lname as user_name, ";
        strSQL += " B.role_name ";
        strSQL += " FROM aut_user A INNER JOIN aut_role B ON A.role_id = B.role_id ";
        if (strUserName != "")
            strWhere += " A.user_login like '%" + strUserName + "%' AND ";
        if (strRoleID != "")
            strWhere += " A.role_id = '" + strRoleID + "' AND ";
        if (strName != "")
            strWhere += " A.user_fname + ' ' + A.user_lname like '%" + strName + "%' AND ";
        if (strStatus != "")
            strWhere += " A.user_status = '" + strStatus + "' AND ";

        if (strWhere != "")
            strSQL = strSQL + " WHERE " + strWhere.Substring(0, strWhere.Length - 4);

        strSQL = strSQL + " ORDER BY user_login ";
        ds = executeQuery(strSQL);
        return ds;
    }
    public DataSet getDataUserByUserName(string strUserName)
    {
        DataSet ds ;
        string strSQL = "";

        strSQL = "SELECT A.*, A.user_fname + ' ' + A.user_lname as user_name, ";
        strSQL += " B.role_name ,";
        strSQL += "     UI.user_fname + ' ' + UI.user_lname as insert_name, ";
        strSQL += "     UU.user_fname + ' ' + UU.user_lname as update_name ";
        strSQL += " FROM aut_user  A INNER JOIN aut_role B ON A.role_id = B.role_id ";
        strSQL += "     LEFT JOIN aut_user UI ON A.insert_login = UI.user_login ";
        strSQL += "     LEFT JOIN aut_user UU ON A.update_login = UU.user_login ";
        //strSQL += " FROM aut_user A LEFT JOIN aut_role B ON A.role_id = B.role_id ";
        strSQL += " WHERE A.user_login = '" + strUserName + "' AND B.role_status = 'A' ";
        ds = executeQuery(strSQL);
        return ds;
    }
    public DataSet getDataUserAuthByUserName(string strUserName)
    {
        DataSet ds;
        string strSQL = "";

        strSQL = "SELECT B.*,C.branch_name ";
        strSQL += " FROM aut_user A INNER JOIN aut_user_branch B ON A.user_login = B.user_login inner join mst_branch C on B.branch_code=C.branch_code ";
        strSQL += " WHERE A.user_login = '" + strUserName + "' ";
        ds = executeQuery(strSQL);
        return ds;
    }
    public DataSet getDataUserMenuByUserName(string strUserName)
    {
        DataSet ds;
        string strSQL = "";

        strSQL = "SELECT B.*, C.menu_name ";
        strSQL += " FROM aut_user A INNER JOIN aut_user_menu B ON A.user_login = B.user_login ";
        strSQL += "     INNER JOIN aut_menu C ON B.menu_id = C.menu_id ";
        strSQL += " WHERE A.user_login = '" + strUserName + "' ";
        ds = executeQuery(strSQL);
        return ds;
    }
    public bool addDataUser(DataSet ds, DataSet dsAuth, DataSet dsMenu)
    {
        bool isRet = true;
        string strSQL = "";
        SqlConnection conn;
        SqlTransaction trans;
        conn = getDbConnection();
        trans = conn.BeginTransaction();
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();

        try
        {
            strSQL = "INSERT INTO aut_user(user_login, user_pwd, user_fname, user_lname, user_email, role_id, user_status,";
            strSQL += " user_canceldate, user_cancellogin, insert_date, insert_login,start_date, stop_date,user_level,team_id) ";
            strSQL += " VALUES( ";
            strSQL += " '" + ds.Tables[0].Rows[0]["user_login"].ToString() + "', ";
            strSQL += " '" + encode(ds.Tables[0].Rows[0]["user_pwd"].ToString(), ds.Tables[0].Rows[0]["user_login"].ToString()) + "', ";
            strSQL += " '" + ds.Tables[0].Rows[0]["user_fname"].ToString() + "', ";
            strSQL += " '" + ds.Tables[0].Rows[0]["user_lname"].ToString() + "', ";
            strSQL += " '" + ds.Tables[0].Rows[0]["user_email"].ToString() + "', ";
            strSQL += " '" + ds.Tables[0].Rows[0]["role_id"].ToString() + "', ";
            strSQL += " '" + ds.Tables[0].Rows[0]["user_status"].ToString() + "', ";
            if (ds.Tables[0].Rows[0]["user_status"].ToString() == "C")
            {
                strSQL += " getDate(), ";
                strSQL += " '" + ds.Tables[0].Rows[0]["LOGINNAME"].ToString() + "', ";
            }
            else
            {
                strSQL += " null, ";
                strSQL += " null, ";
            }
            strSQL += " getDate(), ";
            strSQL += " '" + ds.Tables[0].Rows[0]["LOGINNAME"].ToString() + "', ";
            if (ds.Tables[0].Rows[0]["start_date"].ToString() != "__/__/____")
                strSQL += " '" + cmDate.convertDateStringForDBTH(ds.Tables[0].Rows[0]["start_date"].ToString()) + "', ";
            else
                strSQL += " null, ";
            if (ds.Tables[0].Rows[0]["stop_date"].ToString() != "__/__/____")
                strSQL += " '" + cmDate.convertDateStringForDBTH(ds.Tables[0].Rows[0]["stop_date"].ToString()) + "', ";
            else
                strSQL += " null, ";
           // strSQL += " '" + ds.Tables[0].Rows[0]["user_branch"].ToString() + "' ";
            strSQL += " '" + ds.Tables[0].Rows[0]["user_level"].ToString() + "', ";
            strSQL += " '" + ds.Tables[0].Rows[0]["team_id"].ToString() + "' ";
            strSQL += " ) ";
            executeUpdate(strSQL, conn, trans);

            for (int i = 0; i <= dsAuth.Tables[0].Rows.Count - 1; i++)
            {
              var c =  dsAuth.Tables[0].Rows[i]["branch_code"].ToString();

                strSQL = "INSERT INTO aut_user_branch(user_login, branch_code, usbr_status,insert_date,insert_login) ";
                strSQL += " VALUES( ";
                strSQL += " '" + ds.Tables[0].Rows[0]["user_login"].ToString() + "', ";
                strSQL += " '" + dsAuth.Tables[0].Rows[i]["branch_code"].ToString() + "', ";
                strSQL += " 'A', ";
                strSQL += " getDate(), ";
                strSQL += " '" + ds.Tables[0].Rows[0]["LOGINNAME"].ToString() + "' ";
                strSQL += " ) ";
                executeUpdate(strSQL, conn, trans);

                
            }
            for (int j = 0; j <= dsMenu.Tables[0].Rows.Count - 1; j++)
            {
                strSQL = "INSERT INTO aut_user_menu(urmenu_id,user_login, menu_id, urmenu_flagadd,urmenu_flagedit,urmenu_flagdelete,urmenu_flaginquiry, urmenu_status,insert_date,insert_login) ";
                strSQL += " VALUES( ";
                strSQL += " '" + ds.Tables[0].Rows[0]["user_login"].ToString() +  dsMenu.Tables[0].Rows[j]["menu_id"].ToString() + "', ";
                strSQL += " '" + ds.Tables[0].Rows[0]["user_login"].ToString() + "', ";
                strSQL += " '" + dsMenu.Tables[0].Rows[j]["menu_id"].ToString() + "', ";
                strSQL += " '" + dsMenu.Tables[0].Rows[j]["urmenu_flagadd"].ToString() + "', ";
                strSQL += " '" + dsMenu.Tables[0].Rows[j]["urmenu_flagedit"].ToString() + "', ";
                strSQL += " '" + dsMenu.Tables[0].Rows[j]["urmenu_flagdelete"].ToString() + "', ";
                strSQL += " '" + dsMenu.Tables[0].Rows[j]["urmenu_flaginquiry"].ToString() + "', ";
                strSQL += " 'A', ";
                strSQL += " getDate(), ";
                strSQL += " '" + ds.Tables[0].Rows[0]["LOGINNAME"].ToString() + "' ";
                strSQL += " ) ";
                executeUpdate(strSQL, conn, trans);
            }
            trans.Commit();
        }
        catch (Exception e)
        {
            isRet = false;
            trans.Rollback();
            TDS.Utility.MasterUtil.writeError("ERROR ON MasterData.addDataUser()", e.ToString());
            TDS.Utility.MasterUtil.writeError("ERROR ON MasterData.addDataUser()", strSQL);
        }
        finally
        {
            conn.Close();
            trans.Dispose();
            conn.Dispose();
        }
        return isRet;
    }
    public bool editDataUser(DataSet ds, DataSet dsAuth, DataSet dsMenu)
    {
        bool isRet = true;
        string strSQL = "";
        SqlConnection conn;
        SqlTransaction trans;
        conn = getDbConnection();
        trans = conn.BeginTransaction();
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();

        try
        {
            strSQL = "UPDATE aut_user SET ";
            strSQL += " user_fname = '" + ds.Tables[0].Rows[0]["user_fname"].ToString() + "', ";
            strSQL += " user_lname = '" + ds.Tables[0].Rows[0]["user_lname"].ToString() + "', ";
            strSQL += " user_email = '" + ds.Tables[0].Rows[0]["user_email"].ToString() + "', ";
            strSQL += " role_id = '" + ds.Tables[0].Rows[0]["role_id"].ToString() + "', ";
            strSQL += " user_status = '" + ds.Tables[0].Rows[0]["user_status"].ToString() + "', ";
            if (ds.Tables[0].Rows[0]["user_status"].ToString() == "C")
            {
                strSQL += " user_canceldate = getDate(), ";
                strSQL += " user_cancellogin = '" + ds.Tables[0].Rows[0]["LOGINNAME"].ToString() + "', ";
            }
            else
            {
                strSQL += " user_canceldate = null, ";
                strSQL += " user_cancellogin = null, ";
            }
            if (ds.Tables[0].Rows[0]["start_date"].ToString() != "__/__/____")
                strSQL += " start_date = '" + cmDate.convertDateStringForDBTH(ds.Tables[0].Rows[0]["start_date"].ToString()) + "', ";
            else
                strSQL += " start_date = null, ";
            if (ds.Tables[0].Rows[0]["stop_date"].ToString() != "__/__/____")
                strSQL += " stop_date = '" + cmDate.convertDateStringForDBTH(ds.Tables[0].Rows[0]["stop_date"].ToString()) + "', ";
            else
                strSQL += " stop_date = null, ";
            strSQL += " update_date = getDate(), ";
            strSQL += " update_login = '" + ds.Tables[0].Rows[0]["LOGINNAME"].ToString() + "', ";
            strSQL += " user_level = '" + ds.Tables[0].Rows[0]["user_level"].ToString() + "', ";
            strSQL += " team_id = '" + ds.Tables[0].Rows[0]["team_id"].ToString() + "' ";
          //  strSQL += " user_branch = '" + ds.Tables[0].Rows[0]["user_branch"].ToString() + "' ";
            strSQL += " WHERE user_login = '" + ds.Tables[0].Rows[0]["user_login"].ToString() + "' ";
            executeUpdate(strSQL, conn, trans);

            strSQL = "DELETE FROM aut_user_branch WHERE user_login = '" + ds.Tables[0].Rows[0]["user_login"].ToString() + "' ";
            executeUpdate(strSQL, conn, trans);
            for (int i = 0; i <= dsAuth.Tables[0].Rows.Count - 1; i++)
            {
                strSQL = "INSERT INTO aut_user_branch(user_login, branch_code, usbr_status,insert_date,insert_login,update_date,update_login) ";
                strSQL += " VALUES( ";
                strSQL += " '" + ds.Tables[0].Rows[0]["user_login"].ToString() + "', ";
                strSQL += " '" + dsAuth.Tables[0].Rows[i]["branch_code"].ToString() + "', ";
                strSQL += " 'A', ";
                strSQL += " getDate(), ";
                strSQL += " '" + ds.Tables[0].Rows[0]["LOGINNAME"].ToString() + "', ";
                strSQL += " getDate(), ";
                strSQL += " '" + ds.Tables[0].Rows[0]["LOGINNAME"].ToString() + "' ";
                strSQL += " ) ";
                executeUpdate(strSQL, conn, trans);
            }
            strSQL = "DELETE FROM aut_user_menu WHERE user_login = '" + ds.Tables[0].Rows[0]["user_login"].ToString() + "' ";
            executeUpdate(strSQL, conn, trans);
            for (int j = 0; j <= dsMenu.Tables[0].Rows.Count - 1; j++)
            {
                strSQL = "INSERT INTO aut_user_menu(urmenu_id,user_login, menu_id, urmenu_flagadd,urmenu_flagedit,urmenu_flagdelete,urmenu_flaginquiry, urmenu_status,insert_date,insert_login,update_date,update_login) ";
                strSQL += " VALUES( ";
                strSQL += " '" + ds.Tables[0].Rows[0]["user_login"].ToString() + dsMenu.Tables[0].Rows[j]["menu_id"].ToString() + "', ";
                strSQL += " '" + ds.Tables[0].Rows[0]["user_login"].ToString() + "', ";
                strSQL += " '" + dsMenu.Tables[0].Rows[j]["menu_id"].ToString() + "', ";
                strSQL += " '" + dsMenu.Tables[0].Rows[j]["urmenu_flagadd"].ToString() + "', ";
                strSQL += " '" + dsMenu.Tables[0].Rows[j]["urmenu_flagedit"].ToString() + "', ";
                strSQL += " '" + dsMenu.Tables[0].Rows[j]["urmenu_flagdelete"].ToString() + "', ";
                strSQL += " '" + dsMenu.Tables[0].Rows[j]["urmenu_flaginquiry"].ToString() + "', ";
                strSQL += " 'A', ";
                strSQL += " getDate(), ";
                strSQL += " '" + ds.Tables[0].Rows[0]["LOGINNAME"].ToString() + "', ";
                strSQL += " getDate(), ";
                strSQL += " '" + ds.Tables[0].Rows[0]["LOGINNAME"].ToString() + "' ";
                strSQL += " ) ";
                executeUpdate(strSQL, conn, trans);
            }
            trans.Commit();
        }
        catch (Exception e)
        {
            isRet = false;
            trans.Rollback();
            TDS.Utility.MasterUtil.writeError("ERROR ON MasterData.editDataUser()", e.ToString());
            TDS.Utility.MasterUtil.writeError("ERROR ON MasterData.editDataUser()", strSQL);
        }
        finally
        {
            conn.Close();
            trans.Dispose();
            conn.Dispose();
        }
        return isRet;
    }

    public DataSet getDataCompanyDivisionByUserName(string strUserName)
    {
        DataSet ds;
        string strSQL = "";

        strSQL = "SELECT *, ";
        strSQL += " cast(Company as varchar) + ':' + Division as value, ";
        strSQL += " 'Company : ' + cast(Company as varchar) + ' / Division : ' + Division  as text ";
        strSQL += " FROM UserInfo ";
        strSQL += " WHERE  RTRIM(UserID) = '" + strUserName + "' ";
        strSQL += " ORDER BY Company, Division ";
        ds = executeQuery(strSQL);
        return ds;
    }
    public string getDataErrorByPageNameNo(string strPage, string strNo)
    {
        DataSet ds;
        string strSQL = "";
        string strRet = "";

        strSQL = "SELECT * ";
        strSQL += " FROM mst_errortext ";
        strSQL += " WHERE err_page = '" + strPage + "' AND ";
        strSQL += "     err_no = '" + strNo + "' ";
        ds = executeQuery(strSQL);
        if (ds.Tables[0].Rows.Count > 0)
        {
            strRet = ds.Tables[0].Rows[0]["err_desc"].ToString();
        }
        else
        {
            strRet = strPage + " >> " + strNo;
        }
        return strRet;
    }
    public string editDataPassword(string strLogin, string strNewPassword,
                               string strUserLogin, string strFlagLoginDate)
    {
        string strRet = "";
        string strSQL = "";
        DataSet ds;
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();

        strSQL = "SELECT * ";
        strSQL += " FROM aut_user ";
        strSQL += " WHERE user_login = '" + strLogin + "' AND ";
        strSQL += "     user_pwd = '" + encode(strNewPassword, strLogin) + "' ";
        ds = executeQuery(strSQL);
        if (ds.Tables[0].Rows.Count > 0)
        {
            strRet = "1 : รหัสผ่านซ้ำกับรหัสผ่านเดิม กรุณาเปลี่ยนรหัสผ่านอื่น";
        }
        else
        {
            strSQL = "UPDATE aut_user ";
            strSQL += " SET ";
            strSQL += " user_pwd = '" + encode(strNewPassword, strLogin) + "', ";
            if (strFlagLoginDate == "Y")
                strSQL += " user_lastlogindate = getDate(), ";
            strSQL += " user_status = 'A', ";
            strSQL += " user_changepwddate = getDate(), ";
            strSQL += " update_date = getDate(), ";
            strSQL += " update_login = '" + strUserLogin + "' ";
            strSQL += " WHERE  user_login = '" + strLogin + "' ";
            executeUpdate(strSQL);
        }
        return strRet;
    }
    //Begin Added by Myponn
    public DataSet SearchBranch(string strBranch) 
    {
        DataSet ds;
        string strSQL;
        strSQL = "select a.branch_code,a.branch_name,b.role_id,b.robr_status,b.inser_tdate,b.insert_login, b.update_date,b.update_login from mst_branch a ";
        strSQL += " inner join aut_role_branch b on a.branch_code=b.branch_code ";
        strSQL += " where a.branch_name like '%" + strBranch  + "%'";
        ds = executeQuery(strSQL);
        return ds;
    }

    public DataSet GetRoleBranch(string iRole)
    {
        DataSet ds;
        string strSQL;
        strSQL = "select a.branch_code,a.branch_name,b.role_id,b.robr_status,b.insert_date,b.insert_login, b.update_date,b.update_login from mst_branch a ";
        strSQL += " inner join aut_role_branch b on a.branch_code=b.branch_code ";
        strSQL += " where b.role_id='" + iRole + "'";
        ds = executeQuery(strSQL);
        return ds;
    }

    /*public bool AddRoleBranch(string iRole, string iBranch, string iLogin)
    {
        bool isRet = true;
        string strSQL = "";
        SqlConnection conn;
        SqlTransaction trans;
        conn = getDbConnection();
        trans = conn.BeginTransaction();
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();

        try
        {
                strSQL = "INSERT INTO aut_role_branch(role_id, branch_code, robr_status,insert_date,insert_login) ";
                strSQL += " VALUES( ";
                strSQL += " '" + iRole  + "', ";
                strSQL += " '" + iBranch  + "', ";
                strSQL += " 'A', ";
                strSQL += " getdate(), ";
                strSQL += " '" + iLogin  + "', ";
                strSQL += " ) ";
                executeUpdate(strSQL, conn, trans);
            trans.Commit();
        }
        catch (Exception e)
        {
            isRet = false;
            trans.Rollback();
            TDS.Utility.MasterUtil.writeError("ERROR ON MasterData.AddRoleBranch()", e.ToString());
            TDS.Utility.MasterUtil.writeError("ERROR ON MasterData.AddRoleBranch()", strSQL);
        }
        finally
        {
            conn.Close();
            trans.Dispose();
            conn.Dispose();
        }
        return isRet;
    }*/
    /*
    public bool EditRoleBranch(string iRole, String iBranch, String iLogin)
    {
        bool isRet = true;
        string strSQL = "";
        SqlConnection conn;
        SqlTransaction trans;
        conn = getDbConnection();
        trans = conn.BeginTransaction();
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();

        try
        {
            strSQL = "UPDATE aut_role_branch SET ";
            strSQL += " role_id = '" + iRole + "', ";
            strSQL += " branch_code = '" + iBranch + "', ";
            strSQL += " update_date = getdate(), ";
            strSQL += " update_login = '" + iLogin + "'";
            strSQL += " where role_id = '' and branch_code = ''";
            executeUpdate(strSQL, conn, trans);
            trans.Commit();
        }
        catch (Exception e)
        {
            isRet = false;
            trans.Rollback();
            TDS.Utility.MasterUtil.writeError("ERROR ON MasterData.AddRoleBranch()", e.ToString());
            TDS.Utility.MasterUtil.writeError("ERROR ON MasterData.AddRoleBranch()", strSQL);
        }
        finally
        {
            conn.Close();
            trans.Dispose();
            conn.Dispose();
        }
        return isRet;
    }*/

    public bool DeleteRoleBranch(string iRole, string iBranch)
    {
        bool isRet=true ;
        string strSQL="";
        strSQL = "delete from aut_role_branch where role_id='" + iRole  + "' and branch_code='" + iBranch + "'";
        //try
        //{
        //    isRet = executeQuery(strSQL);
        //}

        return isRet;
    }

    /*public bool CheckRoleBranch(string iRole, string iBranch)
    {
        bool isRet = true;
        string strSQL = "";

        return isRet;

    }*/

    //public DataSet getDataUserAuthByUserName(string strUserName)
    //{
    //    DataSet ds;
    //    string strSQL = "";

    //    strSQL = "SELECT B.* ";
    //    strSQL += " FROM aut_user A INNER JOIN UserInfo B ON A.user_login = B.UserID ";
    //    strSQL += " WHERE A.user_login = '" + strUserName + "' ";
    //    ds = executeQuery(strSQL);
    //    return ds;
    //}
    #region encode - decode
    public string encode(string strData, string strKey)
    {
        // Encode the string �message�
        // ScrambleKey and ScambleIV are randomly generated

        byte[] ScrambleIV = new byte[] { 0, 1, 2, 3, 4, 5, 6, 7 };
        byte[] ScrambleKey = Encoding.Default.GetBytes(strKey);
        UTF8Encoding textConverter = new UTF8Encoding();
        RC2CryptoServiceProvider rc2CSP =
                         new RC2CryptoServiceProvider();

        //Convert the data to a byte array.
        byte[] toEncrypt = textConverter.GetBytes(strData);

        //Get an encryptor.
        ICryptoTransform encryptor =
           rc2CSP.CreateEncryptor(ScrambleKey, ScrambleIV);

        //Encrypt the data.
        MemoryStream msEncrypt = new MemoryStream();
        CryptoStream csEncrypt = new CryptoStream(msEncrypt,
                                 encryptor, CryptoStreamMode.Write);

        //Write all data to the crypto stream and flush it.
        // Encode length as first 4 bytes
        byte[] length = new byte[4];
        length[0] = (byte)(strData.Length & 0xFF);
        length[1] = (byte)((strData.Length >> 8) & 0xFF);
        length[2] = (byte)((strData.Length >> 16) & 0xFF);
        length[3] = (byte)((strData.Length >> 24) & 0xFF);
        csEncrypt.Write(length, 0, 4);
        csEncrypt.Write(toEncrypt, 0, toEncrypt.Length);
        csEncrypt.FlushFinalBlock();

        //Get encrypted array of bytes.
        byte[] encrypted = msEncrypt.ToArray();
        return Convert.ToBase64String(encrypted);
    }
    public string decode(string strData, string strKey)
    {
        // Decode the �encrypted� byte[]
        UTF8Encoding textConverter = new UTF8Encoding();
        RC2CryptoServiceProvider rc2CSP = new RC2CryptoServiceProvider();
        //Get a decryptor that uses the same key and IV as the encryptor.
        byte[] ScrambleIV = new byte[] { 0, 1, 2, 3, 4, 5, 6, 7 };
        byte[] ScrambleKey = Encoding.Default.GetBytes(strKey);
        ICryptoTransform decryptor =
               rc2CSP.CreateDecryptor(ScrambleKey, ScrambleIV);

        //Now decrypt the previously encrypted message using the decryptor
        // obtained in the above step.
        MemoryStream msDecrypt = new MemoryStream(Convert.FromBase64String(strData));
        CryptoStream csDecrypt = new CryptoStream(msDecrypt,
                                 decryptor, CryptoStreamMode.Read);

        byte[] fromEncrypt = new byte[strData.Length - 4];

        //Read the data out of the crypto stream.
        byte[] length = new byte[4];
        csDecrypt.Read(length, 0, 4);
        csDecrypt.Read(fromEncrypt, 0, fromEncrypt.Length);
        int len = (int)length[0] | (length[1] << 8) |
                  (length[2] << 16) | (length[3] << 24);

        //Convert the byte array back into a string.
        return textConverter.GetString(fromEncrypt).Substring(0, len);
    }
    #endregion

 
    //=============  Manage Role Menu  =========================================================//

    public List<string> CheckRoleMenu(  params object[]  Objects )
    {
        string menu_id = ((RoleMenu)(Objects[0])).menu_id;
        string role_id = ((RoleMenu)(Objects[0])).roleID;
        string user_login = ((RoleMenu)(Objects[0])).loginName;
         
        DataSet ds1;
        DataSet ds2; 
        string strSQL1 = ""; 
        string strSQL2 = "";
        List<string> result ;

        string flagadd1;
        string flagedit1;
        string flagdelete1;
        string flaginquiry1;
        string flagadd2;
        string flagedit2;
        string flagdelete2;
        string flaginquiry2;

        if ( menu_id != null  &&  role_id != null && user_login == null )
        {
            strSQL1 = "SELECT *   ";
            strSQL1 += " FROM  aut_role_menu   ";
            strSQL1 += " WHERE  role_id = '" + role_id + "' AND   ";
            strSQL1 += " menu_id = '" + menu_id + "'  ";
            ds1 = executeQuery(strSQL1);
            flagadd1 = ds1.Tables[0].Rows[0]["role_menu_flagadd"].ToString();
            flagedit1 = ds1.Tables[0].Rows[0]["role_menu_flagedit"].ToString();
            flagdelete1 = ds1.Tables[0].Rows[0]["role_menu_flagdelete"].ToString();
            flaginquiry1 = ds1.Tables[0].Rows[0]["role_menu_flaginquiry"].ToString();
            result = CheckRoleFlag(flagadd1, flagedit1, flagdelete1, flaginquiry1);
        }
        else if (menu_id != null && role_id == null && user_login != null)
        {
            strSQL2 = "SELECT *   ";
            strSQL2 += " FROM  aut_user_menu   ";
            strSQL2 += " WHERE  user_login = '" + user_login + "' AND   ";
            strSQL2 += "   menu_id = '" + menu_id + "'  ";
            ds2 = executeQuery(strSQL2);
            flagadd2 = ds2.Tables[0].Rows[0]["urmenu_flagadd"].ToString();
            flagedit2 = ds2.Tables[0].Rows[0]["urmenu_flagedit"].ToString();
            flagdelete2 = ds2.Tables[0].Rows[0]["urmenu_flagdelete"].ToString();
            flaginquiry2 = ds2.Tables[0].Rows[0]["urmenu_flaginquiry"].ToString();
            result = CheckRoleFlag(flagadd2, flagedit2, flagdelete2, flaginquiry2);
        }
        else if (menu_id != null && role_id != null && user_login != null)
        {
            strSQL1 = "SELECT *   ";
            strSQL1 += " FROM  aut_role_menu   ";
            strSQL1 += " WHERE  role_id = '" + role_id + "' AND   ";
            strSQL1 += " menu_id = '" + menu_id + "'  ";
            ds1 = executeQuery(strSQL1);

            strSQL2 = "SELECT *   ";
            strSQL2 += " FROM  aut_user_menu   ";
            strSQL2 += " WHERE  user_login = '" + user_login + "' AND   ";
            strSQL2 += "   menu_id = '" + menu_id + "'  ";
            ds2 = executeQuery(strSQL2);
            flagadd1 = ds1.Tables[0].Rows[0]["role_menu_flagadd"].ToString();
            flagedit1 = ds1.Tables[0].Rows[0]["role_menu_flagedit"].ToString();
            flagdelete1 = ds1.Tables[0].Rows[0]["role_menu_flagdelete"].ToString();
            flaginquiry1 = ds1.Tables[0].Rows[0]["role_menu_flaginquiry"].ToString();
            flagadd2 = ds2.Tables[0].Rows[0]["urmenu_flagadd"].ToString();
            flagedit2 = ds2.Tables[0].Rows[0]["urmenu_flagedit"].ToString();
            flagdelete2 = ds2.Tables[0].Rows[0]["urmenu_flagdelete"].ToString();
            flaginquiry2 = ds2.Tables[0].Rows[0]["urmenu_flaginquiry"].ToString();

            List<string> list1 = CheckRoleFlag(flagadd1, flagedit1, flagdelete1, flaginquiry1);
            List<string> list2 = CheckRoleFlag(flagadd2, flagedit2, flagdelete2, flaginquiry2);
            result = list1.Concat(list2).ToList();
        }
        else
        {
            result  = null ;
        }
         
           
        return result ;    
    }

    public List<string> CheckRoleFlag(string flagadd, string flagedit, string flagdelete, string flaginquiry)
    {
      //  string[] strArr = new string[4]; 

        List<string> list = new List<string>();

          if(flagadd == "Y"){
              list.Add("A") ;
          }
          if (flagedit == "Y")
          {
              list.Add("E");
          }
          if (flagdelete == "Y")
          {
              list.Add("D");
          }
          if (flaginquiry == "Y")
          {
              list.Add("Q");
          }

          return list;
    }

}
