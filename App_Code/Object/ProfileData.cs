﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

/// <summary>
/// Summary description for ProfileData
/// </summary>
public class ProfileData
{
	public ProfileData()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public string loginName;
    public string userName;
    public string email;
    public string roleID;
    public string roleName;
    public string teamCode;
    public string teamName;
    public string level;
    public string[] arrBranch;
}
