﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Module_Settlement_EndorsementForm : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        litJavaScript.Text += "<script>\n";
        litJavaScript.Text += "     \tdocument.getElementById('divSearchPolicy').style.display = 'none';\n";
        litJavaScript.Text += "     \tdocument.getElementById('divPolicy').style.display = 'inline';\n";
        litJavaScript.Text += "</script>\n";

        //set Data
        // policy data
        if (rdoMotor.Checked)
        {
            lblPolicyNo.Text = "1234-222-239389";
            litJavaScript.Text += "<script>document.getElementById('divPolicyRemark1').innerHTML = 'ทะเบียนรถ :';</script>\n";
            lblRemark1.Text = "กก 1234 กท";
        }
        else
        {
            lblPolicyNo.Text = "1234-290-239389";
            litJavaScript.Text += "<script>document.getElementById('divPolicyRemark1').innerHTML = 'หมายเหตุ1 :';</script>\n";
            lblRemark1.Text = "xxxxxxxxxx";
        }
        lblIssueDate.Text = "10/09/2014";
        lblBranchName.Text = "100 : สำนักงานใหญ่";
        lblAgentName.Text = "010005 : xxxxxxxxxx";
        lblPeriodFromTo.Text = "01/10/2014 - 01/10/2015";
        lblInsurerName.Text = "xxxxx xxxxxxxxxxxx";
        lblPhoneNo.Text = "02-222-2222, 02-333-3333, 02-444-4444";
        lblPolicyPremium.Text = "19,383.20";
        lblPolicyNet.Text = "20,492.00";

        lblCompulNo.Text = "1234-111-8764444";
        lblCompulPremium.Text = "601.25";
        lblCompulNet.Text = "645.21";
        // cmi data
        lblCompulNo.Text = "121-3933-39999";
        lblCompulPremium.Text = "640.00";
        lblCompulNet.Text = "670.00";
        // endorse data
        txtEndorseNo.Text = "283930-393-00";
        txtEndorseDate.Text = "30/11/2557";
        txtEndorsePremium.Text = "-200.00";
        txtEndorseNet.Text = "-214.00";

        //ข้อมูลผ่อนชำระ
        double dPolicyNet = 0;
        double dCompulNet = 0;
        double dEndorseNet = 0;
        if (lblPolicyNet.Text != "")
            dPolicyNet = Convert.ToDouble(lblPolicyNet.Text);
        if (lblCompulNet.Text != "")
            dCompulNet = Convert.ToDouble(lblCompulNet.Text);
        //txtPolicyTotal.Text = FormatStringApp.FormatMoney(dPolicyNet);
        //txtPolicyBalance.Text = FormatStringApp.FormatMoney(dPolicyNet + dCompulNet);
        //txtEndorseTotal.Text = "-214";
        dEndorseNet = -214;
        //lblBalance.Text = FormatStringApp.FormatMoney(dPolicyNet + dCompulNet + dEndorseNet);
        //lblStartDate.Text = "15/11/2014";
        //txtARNo.Text = "1239-3988";
        //txtPaid.Text = "690.00";
        //txtArrear.Text = "1290.00";
        //lblSettlementID.Text = "57110001";
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {

    }
}
