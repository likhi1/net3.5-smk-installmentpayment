﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Module_Settlement_SettlementForm : System.Web.UI.Page
{
    public string[] ColumnName = { "inst_no", "inst_duedate", "", "inst_rate", "inst_net", "inst_paid", "inst_rvno", "inst_remark", "inst_duedatedesc" };
    public string[] ColumnType = { "string", "DateTime", "string", "double", "double", "double", "string", "string", "string" };
    public string[] ColumnTitle = { "งวดที่", "วันครบกำหนด", "จำนวนวันล่าช้า", "%", "ยอดผ่อน", "ยอดที่ชำระ", "เลขที่ใบนำส่ง", "หมายเหตุ", "ลบ" };
    string[] ColumnWidth = { "5%", "10%", "5%", "5%", "20%", "20%", "10%", "20%", "5%" };
    string[] ColumnAlign = { "center", "center", "center", "right", "right", "right", "center", "left", "center" };

    public string[] ColumnNameRV = { "rv_no", "rv_type", "rv_desc", "rv_net", "" };
    public string[] ColumnTypeRV = { "string", "string", "string", "string", "string" };
    public string[] ColumnTitleRV = { "ลำดับที่", "ประเภท", "รายละเอียด", "จำนวนเงิน", "แก้ไข/ลบ" };
    string[] ColumnWidthRV = { "5%", "15%", "60%", "10%", "10%" };
    string[] ColumnAlignRV = { "center", "left", "left", "right", "center" };

    DataView dv = new DataView();

    protected void Page_Load(object sender, EventArgs e)
    {
        ProfileData loginData = FormatStringApp.getLoginSession(Page, Session);
        SettlementManager cm = new SettlementManager();
        SyncManager cmSync = new SyncManager();
        DataSet ds = new DataSet();
        DataSet dsAuth = new DataSet();
        double dTotalPremiumm = 0;
        double dDiscountRate = 0;
        double dDiscount = 0;
        if (!IsPostBack)
        {
            initialDropDownList();
            initialDataGrid();
            txtMode.Text = "A";
            txtSettlementDate.Text = FormatStringApp.FormatDate(DateTime.Today);
            txtInstallments.Text = ConfigurationManager.AppSettings["defaultInstallment"];
            txtMonths.Text = ConfigurationManager.AppSettings["defaultMonths"];
            txtDiscountRate.Text = "0";
            if (Request.QueryString["id"] != null)
            {
                // มาจากหน้ารายการงานค้าง
                ds = cmSync.getDataSyncPolicyByID(Request.QueryString["id"]);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    // 
                    ds.Tables[0].Columns.Add("send_name");
                    ds.Tables[0].Columns.Add("send_address1");
                    ds.Tables[0].Columns.Add("send_address2");
                    ds.Tables[0].Columns.Add("send_province");
                    ds.Tables[0].Columns.Add("send_postcode");
                    ds.Tables[0].Columns["send_name"].Expression = "spol_custname";
                    docToUIPolicy(ds);
                    txtStartDate.Text = FormatStringApp.FormatDate(ds.Tables[0].Rows[0]["spol_effectivefromdate"]);
                    txtPremium1.Text = FormatStringApp.FormatMoney(ds.Tables[0].Rows[0]["spol_grosspremium"]);
                    txtPremium2.Text = FormatStringApp.FormatMoney(ds.Tables[0].Rows[0]["spol_comgrosspremium"]);
                    if (txtPremium1.Text != "")
                        dTotalPremiumm = dTotalPremiumm + (Convert.ToDouble(txtPremium1.Text));
                    if (txtPremium2.Text != "")
                        dTotalPremiumm = dTotalPremiumm + (Convert.ToDouble(txtPremium2.Text));
                    txtPolicyTotal.Text = FormatStringApp.FormatMoney(dTotalPremiumm);
                    txtPolicyBalance.Text = FormatStringApp.FormatMoney(dTotalPremiumm);
                    //ส่วนลด
                    if (txtDiscountRate.Text != "")
                        dDiscountRate = (Convert.ToDouble(txtDiscountRate.Text));
                    else
                        dDiscountRate = 0;
                    dDiscount = dTotalPremiumm*dDiscountRate/100;
                    txtDiscount.Text = FormatStringApp.FormatMoney(dDiscount);
                    txtBalance.Text = FormatStringApp.FormatMoney(dTotalPremiumm - dDiscount);
                    btnInstallments_Click(null, null);

                    litJavaScript.Text += "<script>\n";
                    litJavaScript.Text += "     \tdocument.getElementById('divSearchPolicy').style.display = 'none';\n";
                    litJavaScript.Text += "     \tdocument.getElementById('divPolicy').style.display = 'inline';\n";
                    litJavaScript.Text += "     \tdocument.getElementById('divSendDoc').style.display = 'inline';\n";
                    litJavaScript.Text += "     \tdocument.getElementById('divInstallments').style.display = 'inline';\n";
                    litJavaScript.Text += "     \tdocument.getElementById('divRV').style.display = 'inline';";
                    litJavaScript.Text += "</script>\n";
                }
                else
                {
                    litJavaScript.Text = "<script>alert('ไม่พบข้อมูล กรุณาระบุเลขกรมธรรม์ใหม่');</script>";
                }
            }
            else if (Request.QueryString["polno"] != null)
            {
                txtMode.Text = "E";
                lblPolicyNo.Text = Request.QueryString["polno"];
                txtPolicy.Text = Request.QueryString["polno"];
                ds = cm.getDataSettlementByPolNo(lblPolicyNo.Text);
                docToUIPolicy(ds);
                btnSearch_Click(null, null);
            }
            setMode();
        }
        else
        {
            litJavaScript.Text = "";
            litJavaScript.Text += "<script>\n";
            litJavaScript.Text += "     \tdocument.getElementById('divSearchPolicy').style.display = 'none';\n";
            litJavaScript.Text += "     \tdocument.getElementById('divPolicy').style.display = 'inline';\n";
            litJavaScript.Text += "     \tdocument.getElementById('divSendDoc').style.display = 'inline';\n";
            litJavaScript.Text += "     \tdocument.getElementById('divInstallments').style.display = 'inline';\n";
            litJavaScript.Text += "     \tdocument.getElementById('divRV').style.display = 'inline';";
            litJavaScript.Text += "</script>\n";
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        string strSyncID = "";
        string strMsg = "";
        double dTotalPremiumm = 0;
        double dDiscountRate = 0;
        double dDiscount = 0;
        bool isValidate = true;
        DataSet ds = new DataSet();
        DataSet dsSync = new DataSet();
        DataSet dsCheck = new DataSet();
        SQLManager cmSQL = new SQLManager();
        SyncManager cmSync = new SyncManager();
        SettlementManager cmSettle = new SettlementManager();        
        ProfileData loginData = FormatStringApp.getLoginSession(Page, Session);
        litJavaScript.Text += "<script>\n";
        litJavaScript.Text += "     \tdocument.getElementById('divSearchPolicy').style.display = 'none';\n";
        litJavaScript.Text += "     \tdocument.getElementById('divPolicy').style.display = 'inline';\n";
        litJavaScript.Text += "     \tdocument.getElementById('divSendDoc').style.display = 'inline';\n";
        litJavaScript.Text += "     \tdocument.getElementById('divInstallments').style.display = 'inline';\n";
        litJavaScript.Text += "     \tdocument.getElementById('divRV').style.display = 'inline';";
        litJavaScript.Text += "</script>\n";

        //set Data
        // policy data
        if (rdoMotor.Checked)
        {
            lblPolicyNo.Text = txtVolunNo.Text;
            txtPolicy.Text = txtVolunNo.Text;
            litJavaScript.Text += "<script>document.getElementById('divPolicyRemark1').innerHTML = 'ทะเบียนรถ :';</script>\n";
            dsSync = cmSQL.getDataSQLNonMotorByPolicy(txtPolicy.Text);
        }
        else
        {
            lblPolicyNo.Text = txtNonMotorNo.Text;
            txtPolicy.Text = txtNonMotorNo.Text;
            litJavaScript.Text += "<script>document.getElementById('divPolicyRemark1').innerHTML = 'หมายเหตุ1 :';</script>\n";
            dsSync = cmSQL.getDataSQLNonMotorByPolicy(txtPolicy.Text);
        }
        if (dsSync.Tables[0].Rows.Count > 0)
        {
            // ทำการตรวจสอบเลขกรมธรรม์ ว่ามีการบันทึกตั้งงวดในระบบแล้ว
            dsCheck = cmSettle.getDataSettlementByPolNo(txtPolicy.Text);
            if (dsCheck.Tables[0].Rows.Count > 0)
            {
                isValidate = false;
                strMsg = "เลขกรมธรรม์ที่ระบุมีการบันทึกผ่อนชำระแล้ว";
            }

            if (isValidate)
            {
                // ถ้ามี กรมธรรม์ใน sync_policy ให้เอารายการใน sync_policy
                dsCheck = cmSync.getDataSyncPolicyByPolNo(txtPolicy.Text);
                if (dsCheck.Tables[0].Rows.Count > 0)
                    strSyncID = dsCheck.Tables[0].Rows[0]["spol_id"].ToString();
                else
                    strSyncID = cmSync.addDataSyncPolicyEndorse(dsSync, dsSync, loginData.loginName, "N");
                ds = cmSync.getDataSyncPolicyByID(strSyncID);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    // 
                    ds.Tables[0].Columns.Add("send_name");
                    ds.Tables[0].Columns.Add("send_address1");
                    ds.Tables[0].Columns.Add("send_address2");
                    ds.Tables[0].Columns.Add("send_province");
                    ds.Tables[0].Columns.Add("send_postcode");
                    ds.Tables[0].Columns["send_name"].Expression = "spol_custname";
                    docToUIPolicy(ds);
                    txtStartDate.Text = FormatStringApp.FormatDate(ds.Tables[0].Rows[0]["spol_effectivefromdate"]);
                    txtPremium1.Text = FormatStringApp.FormatMoney(ds.Tables[0].Rows[0]["spol_grosspremium"]);
                    txtPremium2.Text = FormatStringApp.FormatMoney(ds.Tables[0].Rows[0]["spol_comgrosspremium"]);
                    if (txtPremium1.Text != "")
                        dTotalPremiumm = dTotalPremiumm + (Convert.ToDouble(txtPremium1.Text));
                    if (txtPremium2.Text != "")
                        dTotalPremiumm = dTotalPremiumm + (Convert.ToDouble(txtPremium2.Text));
                    txtPolicyTotal.Text = FormatStringApp.FormatMoney(dTotalPremiumm);
                    txtPolicyBalance.Text = FormatStringApp.FormatMoney(dTotalPremiumm);
                    //ส่วนลด
                    if (txtDiscountRate.Text != "")
                        dDiscountRate = (Convert.ToDouble(txtDiscountRate.Text));
                    else
                        dDiscountRate = 0;
                    dDiscount = dTotalPremiumm * dDiscountRate / 100;
                    txtDiscount.Text = FormatStringApp.FormatMoney(dDiscount);
                    txtBalance.Text = FormatStringApp.FormatMoney(dTotalPremiumm - dDiscount);
                    btnInstallments_Click(null, null);

                    litJavaScript.Text += "<script>\n";
                    litJavaScript.Text += "     \tdocument.getElementById('divSearchPolicy').style.display = 'none';\n";
                    litJavaScript.Text += "     \tdocument.getElementById('divPolicy').style.display = 'inline';\n";
                    litJavaScript.Text += "     \tdocument.getElementById('divSendDoc').style.display = 'inline';\n";
                    litJavaScript.Text += "     \tdocument.getElementById('divInstallments').style.display = 'inline';\n";
                    litJavaScript.Text += "     \tdocument.getElementById('divRV').style.display = 'inline';";
                    litJavaScript.Text += "</script>\n";
                }
                else
                {
                    litJavaScript.Text = "<script>alert('ไม่พบข้อมูล กรุณาระบุเลขกรมธรรม์ใหม่');</script>";
                }
            }
            else
            {
                litJavaScript.Text = "<script>alert('" + strMsg + "');</script>";
            }
        }
        else
        {
            litJavaScript.Text = "<script>alert('ไม่พบข้อมูล กรุณาระบุเลขกรมธรรม์ใหม่');</script>";
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        string strRet = "";
        bool isRet = true;
        DataSet ds;
        ProfileData loginData = FormatStringApp.getLoginSession(Page, Session);
        SettlementManager cmSettle = new SettlementManager();
        MasterDataManager cm = new MasterDataManager();
        litJavaScript.Text += "<script>";
        ds = UIToDoc();
        if (txtMode.Text == "A")
        {
            strRet = cmSettle.addDataSettlement(ref ds);
            if (strRet.StartsWith("ERROR"))
            {
                litJavaScript.Text += "alert('" + cm.getDataErrorByPageNameNo("", "002") + "');";
            }
            else
            {
                litJavaScript.Text += "alert('" + cm.getDataErrorByPageNameNo("", "001") + "');";
                txtMode.Text = "V";
            }
        }
        litJavaScript.Text += "</script>";
        docToUI(ds);
        setMode();
    }

    protected void btnInstallments_Click(object sender, EventArgs e)
    {
        DataSet ds = new DataSet();
        int iInst = Convert.ToInt32(txtInstallments.Text);
        int iMonths = Convert.ToInt32(txtMonths.Text);
        double dRate = Math.Round(Convert.ToDouble(100/iInst),2);
        double dNet = Convert.ToDouble(lblPolicyNet.Text);
        double dBalance = Convert.ToDouble(txtBalance.Text);
        double dDiscount = 0;
        double dInstallment = 0;
        double dTotal = 0;
        double dTotalRate = 0;
        DateTime dtDueDate = new DateTime(Convert.ToInt32(txtStartDate.Text.Substring(6, 4)), 
                                            Convert.ToInt32(txtStartDate.Text.Substring(3, 2)), 
                                            Convert.ToInt32(txtStartDate.Text.Substring(0, 2)));
        if (ViewState["DSMasterTableInstallments"] != null)
            ds = (DataSet)ViewState["DSMasterTableInstallments"];

        ds.Tables[0].Rows.Clear();
        if (txtDiscount.Text != "")
            dDiscount = Convert.ToDouble(txtDiscount.Text);
        for (int i = 0; i <= iInst - 1; i++)
        {
            ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
            //"inst_no", "inst_duedate", "inst_rate", "inst_net", "inst_paid", "inst_rvno"
            ds.Tables[0].Rows[i]["inst_no"] = Convert.ToString(i+1);
            ds.Tables[0].Rows[i]["inst_duedate"] = dtDueDate;
            ds.Tables[0].Rows[i]["inst_duedatedesc"] = FormatStringApp.FormatDate(dtDueDate);            
            dInstallment = (dNet - dDiscount) * dRate / 100;
            dInstallment = Math.Round(dInstallment / 10) * 10;
            if (i == 0)
            {
                // งวดแรก >>(เบี้ยรวมกรมธรรม์ - ส่วนลด)/rate  + เบี้ยรวมพรบ.                
                if (lblCompulNet.Text != "")
                    dInstallment = dInstallment + Convert.ToDouble(txtPremium2.Text);
            }
            else if (i == iInst - 1)
            {
                //งวดสุดท้าย >> 
                dInstallment = dBalance - dTotal;
                dRate = 100 - dTotalRate;
            }
            dInstallment = Math.Round(dInstallment, 2);
            dTotal = dTotal + dInstallment;
            dTotalRate = dTotalRate + dRate;
            ds.Tables[0].Rows[i]["inst_rate"] = Convert.ToString(dRate);
            ds.Tables[0].Rows[i]["inst_net"] = dInstallment;
            ds.Tables[0].Rows[i]["inst_paid"] = "0.00";
            ds.Tables[0].Rows[i]["inst_rvno"] = "";
            dtDueDate = dtDueDate.AddMonths(iMonths);
        }
        ViewState["DSMasterTableInstallments"] = ds;
        ShowDataInstallments();
        //litScriptIns.Text = "<script>document.getElementById('divInsProgress').style.display='inline';</script>";
    }
    protected void btnAddInst_Click(object sender, EventArgs e)
    {
        DataSet ds = new DataSet();
        int i = 0;
        int iMonths = Convert.ToInt32(txtMonths.Text);
        DateTime dtDueDate;

        //เก็บค่าที่แก้ไข
        saveTmpInstallment();
        if (ViewState["DSMasterTableInstallments"] != null)
            ds = (DataSet)ViewState["DSMasterTableInstallments"];        

        ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
        i = ds.Tables[0].Rows.Count-1;


        dtDueDate = Convert.ToDateTime(ds.Tables[0].Rows[i - 1]["inst_duedate"]);
        dtDueDate = dtDueDate.AddMonths(iMonths);
        ds.Tables[0].Rows[i]["inst_no"] = Convert.ToString(i+1);
        ds.Tables[0].Rows[i]["inst_duedate"] = dtDueDate;
        ds.Tables[0].Rows[i]["inst_duedatedesc"] = FormatStringApp.FormatDate(dtDueDate);
        ds.Tables[0].Rows[i]["inst_rate"] = "0";
        ds.Tables[0].Rows[i]["inst_net"] = "0.00";
        ds.Tables[0].Rows[i]["inst_paid"] = "0.00";
        ds.Tables[0].Rows[i]["inst_rvno"] = "";

        ViewState["DSMasterTableInstallments"] = ds;
        ShowDataInstallments();
        litJavaScript.Text += "<script>\n";
        litJavaScript.Text += "     \tdocument.getElementById('divSearchPolicy').style.display = 'none';\n";
        litJavaScript.Text += "     \tdocument.getElementById('divPolicy').style.display = 'inline';\n";
        litJavaScript.Text += "     \tdocument.getElementById('divSendDoc').style.display = 'inline';\n";
        litJavaScript.Text += "     \tdocument.getElementById('divInstallments').style.display = 'inline';\n";
        litJavaScript.Text += "     \tdocument.getElementById('divRV').style.display = 'inline';";
        litJavaScript.Text += "</script>\n";
    }    
    protected void imgDeleteInst_Click(object sender, ImageClickEventArgs e)
    {
        DataSet ds = new DataSet();
        int i = 0;
        ImageButton imgDelete = (ImageButton)sender;
        UserControl_ucTxtDate txtDtgDueDate ;
        //เก็บค่าที่แก้ไข
        saveTmpInstallment();
        if (ViewState["DSMasterTableInstallments"] != null)
            ds = (DataSet)ViewState["DSMasterTableInstallments"];        

        i = Convert.ToInt32(imgDelete.Attributes["dsindex"]);
        ds.Tables[0].Rows.RemoveAt(i);
        // update งวดที่
        for (int j = i; j <= ds.Tables[0].Rows.Count - 1; j++)
        {
            ds.Tables[0].Rows[j]["inst_no"] = Convert.ToString(j + 1);
        }
        ViewState["DSMasterTableInstallments"] = ds;
        ShowDataInstallments();
        litJavaScript.Text += "<script>\n";
        litJavaScript.Text += "     \tdocument.getElementById('divSearchPolicy').style.display = 'none';\n";
        litJavaScript.Text += "     \tdocument.getElementById('divPolicy').style.display = 'inline';\n";
        litJavaScript.Text += "     \tdocument.getElementById('divSendDoc').style.display = 'inline';\n";
        litJavaScript.Text += "     \tdocument.getElementById('divInstallments').style.display = 'inline';\n";
        litJavaScript.Text += "     \tdocument.getElementById('divRV').style.display = 'inline';";
        litJavaScript.Text += "</script>\n";
        
    }
    protected void dtgInstallments_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        TextBox txtDtgInstallmentsPaid;
        DataSet ds = new DataSet();
        object dNet = 0;
        object dPaid = 0;
        double dBalance = 0;
        
        if (e.Item.ItemType == ListItemType.Item ||
            e.Item.ItemType == ListItemType.AlternatingItem)
        {
            txtDtgInstallmentsPaid = (TextBox)e.Item.FindControl("txtPaid");
            if (e.Item.ItemIndex == 0)
            {
                txtDtgInstallmentsPaid.CssClass = "TEXTBOXMONEY";
                txtDtgInstallmentsPaid.ReadOnly = false;
            }
            else
            {
                txtDtgInstallmentsPaid.CssClass = "TEXTBOXMONEYDIS";
                txtDtgInstallmentsPaid.ReadOnly = true;
            }
        }
        else if (e.Item.ItemType == ListItemType.Footer)
        {
            if (ViewState["DSMasterTableInstallments"] != null)
            {
                ds = (DataSet)ViewState["DSMasterTableInstallments"];
                e.Item.Cells[4].Text = "<div id=\"divSumNet\" class=\"textBlue\">" + FormatStringApp.FormatMoney(ds.Tables[0].Compute("sum(inst_net)", "").ToString()) + "&nbsp;</div>";
                e.Item.Cells[4].HorizontalAlign = HorizontalAlign.Right;
                e.Item.Cells[4].VerticalAlign = VerticalAlign.Middle;
                e.Item.Cells[5].Text = "<div id=\"divSumPaid\" class=\"textBlue\">" + FormatStringApp.FormatMoney(ds.Tables[0].Compute("sum(inst_paid)", "").ToString()) + "&nbsp;</div>";
                e.Item.Cells[5].HorizontalAlign = HorizontalAlign.Right;
                e.Item.Cells[5].VerticalAlign = VerticalAlign.Middle;
                dNet = ds.Tables[0].Compute("sum(inst_net)", "");
                dPaid = ds.Tables[0].Compute("sum(inst_paid)", "");
                dBalance = (Convert.IsDBNull(dNet) ? 0 : Convert.ToDouble(dNet)) - (Convert.IsDBNull(dPaid) ? 0 : Convert.ToDouble(dPaid));
                e.Item.Cells[6].Text = "<div id=\"divSumBalance\" class=\"textBlue\">" + FormatStringApp.FormatMoney(dBalance) + "&nbsp;</div>";
                e.Item.Cells[6].HorizontalAlign = HorizontalAlign.Right;
                e.Item.Cells[6].VerticalAlign = VerticalAlign.Middle;

                txtTotalPaid.Text = FormatStringApp.FormatMoney(ds.Tables[0].Compute("sum(inst_paid)", "").ToString());
            }

        }
    }

    protected void btnAddRV_Click(object sender, EventArgs e)
    {
        
    }    
    protected void imgEditRV_Click(object sender, ImageClickEventArgs e)
    {

    }
    protected void imgDeleteRV_Click(object sender, ImageClickEventArgs e)
    {

    }       
    protected void btnRVSave_Click(object sender, EventArgs e)
    {
        //DataSet dsRV = new DataSet();
        //if (ViewState["DSMasterTableRV"] != null)
        //    dsRV = (DataSet)ViewState["DSMasterTableRV"];

        //dsRV.Tables[0].Rows.Add(dsRV.Tables[0].NewRow());
        //dsRV.Tables[0].Rows[dsRV.Tables[0].Rows.Count - 1]["rv_type"] = "เช็ค";
        //dsRV.Tables[0].Rows[dsRV.Tables[0].Rows.Count - 1]["rv_desc"] = "เลขที่เช็ค:12345  วันที่ในเช็ค:15/11/2014  ธนาคาร:กรุงศรีอยุธยา จำกัด(มหาชน)  สาขา:สีลม";
        //dsRV.Tables[0].Rows[dsRV.Tables[0].Rows.Count - 1]["rv_net"] = "6,310.00";

        //ViewState.Add("DSMasterTableRV", dsRV);
        ShowDataRV();
    }


    public void initialDropDownList()
    {
        MasterDataManager cm = new MasterDataManager();
        DataSet ds;

    }
    public void initialDataGrid()
    {
        DataSet ds = new DataSet();
        DataTable dt = new DataTable();
        dt = TDS.Utility.MasterUtil.AddColumnToDataTable(dt, ColumnType, ColumnName);
        ds.Tables.Add(dt);
        ViewState.Add("DSMasterTableInstallments", ds);
        ShowDataInstallments();
        GridViewUtil.setGridStyle(dtgInstallments, ColumnWidth, ColumnAlign, ColumnTitle);

        ds = new DataSet();
        dt = new DataTable();
        dt = TDS.Utility.MasterUtil.AddColumnToDataTable(dt, ColumnTitleRV, ColumnNameRV);
        ds.Tables.Add(dt);
        ViewState.Add("DSMasterTableRV", ds);
        ShowDataRV();
        GridViewUtil.setGridStyle(dtgRV, ColumnWidthRV, ColumnAlignRV, ColumnTitleRV);
    }
    public void docToUIPolicy(DataSet dsData)
    {
        if (dsData.Tables[0].Rows.Count > 0)
        {
            litJavaScript.Text += "<script>";
            lblPolicyNo.Text = dsData.Tables[0].Rows[0]["spol_policy"].ToString();
            txtPolicy.Text = dsData.Tables[0].Rows[0]["spol_policy"].ToString();
            lblRemark1.Text = dsData.Tables[0].Rows[0]["spol_remark1"].ToString();
            lblRemark2.Text = dsData.Tables[0].Rows[0]["spol_remark2"].ToString();
            if (dsData.Tables[0].Rows[0]["spol_type"].ToString() == "V")
            {                
                litJavaScript.Text += "<script>document.getElementById('divPolicyRemark1').innerHTML = 'ทะเบียนรถ :';\n";
                litJavaScript.Text += "document.getElementById('divPolicyRemark2').innerHTML = 'chasis :';</script>\n";
            }
            else
            {
                litJavaScript.Text += "<script>document.getElementById('divPolicyRemark1').innerHTML = 'หมายเหตุ1 :';\n";
                litJavaScript.Text += "document.getElementById('divPolicyRemark2').innerHTML = '';</script>\n";
            } 

            lblIssueDate.Text = FormatStringApp.FormatDate(dsData.Tables[0].Rows[0]["spol_receivedate"].ToString());
            lblBranchName.Text = dsData.Tables[0].Rows[0]["spol_branch"].ToString();
            lblAgentName.Text = dsData.Tables[0].Rows[0]["spol_agentcode"].ToString();
            lblPeriodFromTo.Text = FormatStringApp.showRangeDate(dsData.Tables[0].Rows[0]["spol_effectivefromdate"], dsData.Tables[0].Rows[0]["spol_effectivetodate"]);
            lblInsurerName.Text = dsData.Tables[0].Rows[0]["spol_custname"].ToString();
            lblPhoneNo.Text = dsData.Tables[0].Rows[0]["spol_custphoneno"].ToString();
            lblPolicyPremium.Text = dsData.Tables[0].Rows[0]["spol_grosspremium"].ToString();
            lblPolicyNet.Text = dsData.Tables[0].Rows[0]["spol_netpremium"].ToString();
            if (dsData.Tables[0].Rows[0]["spol_compolicy"].ToString() != "")
            {
                lblCompulNo.Text = dsData.Tables[0].Rows[0]["spol_compolicy"].ToString();
                lblCompulLicense.Text = dsData.Tables[0].Rows[0]["spol_comlicense"].ToString();
                lblCompulName.Text = dsData.Tables[0].Rows[0]["spol_comname"].ToString();
                lblCompulChasis.Text = dsData.Tables[0].Rows[0]["spol_comchasis"].ToString();
                lblCompulPremium.Text = dsData.Tables[0].Rows[0]["spol_comgrosspremium"].ToString();
                lblCompulNet.Text = dsData.Tables[0].Rows[0]["spol_comnetpremium"].ToString();
                litJavaScript.Text += "<script>document.getElementById('divCom').style.display = 'inline';\n</script>\n";                
            }
            // senddoc data
            txtContactName.Text = dsData.Tables[0].Rows[0]["send_name"].ToString();
            txtContactPhone.Text = dsData.Tables[0].Rows[0]["spol_custphoneno"].ToString();
            txtContactAddr1.Text = dsData.Tables[0].Rows[0]["spol_custaddr1"].ToString();
            txtContactAddr2.Text = dsData.Tables[0].Rows[0]["spol_custaddr2"].ToString();
            txtContactPostCode.Text = dsData.Tables[0].Rows[0]["spol_custzip"].ToString();
            litJavaScript.Text += "</script>\n";

            // chq_accept2
            txtAgentCode.Text = dsData.Tables[0].Rows[0]["spol_agentcode"].ToString();

            //ViewState.Add("DSMasterTable", dsAuth);
            //ShowData();

        }
    }
    public void docToUI(DataSet dsData)
    {
        if (dsData.Tables[0].Rows.Count > 0)
        {
            litJavaScript.Text += "<script>";
            //txtUserName.Text = dsData.Tables[0].Rows[0]["user_login"].ToString();
            //ddlRole.SelectedValue = dsData.Tables[0].Rows[0]["ROLE_ID"].ToString();
            //txtFName.Text = dsData.Tables[0].Rows[0]["user_fname"].ToString();
            //txtLName.Text = dsData.Tables[0].Rows[0]["user_lname"].ToString();
            //txtEmail.Text = dsData.Tables[0].Rows[0]["user_email"].ToString();
            //ddlStatus.SelectedValue = dsData.Tables[0].Rows[0]["user_status"].ToString();
            //if (!Convert.IsDBNull(dsData.Tables[0].Rows[0]["user_lastlogindate"]))
            //    txtLastLogin.Text = TDS.Utility.MasterUtil.getDateString((DateTime)dsData.Tables[0].Rows[0]["user_lastlogindate"], "dd/MM/yyyy");
            //if (!Convert.IsDBNull(dsData.Tables[0].Rows[0]["start_date"]))
            //    txtStartDate.Text = TDS.Utility.MasterUtil.getDateString((DateTime)dsData.Tables[0].Rows[0]["start_date"], "dd/MM/yyyy");
            //if (!Convert.IsDBNull(dsData.Tables[0].Rows[0]["stop_date"]))
            //    txtEndDate.Text = TDS.Utility.MasterUtil.getDateString((DateTime)dsData.Tables[0].Rows[0]["stop_date"], "dd/MM/yyyy");
            //txtInsertLogin.Text = dsData.Tables[0].Rows[0]["insert_name"].ToString();
            //txtInsertDate.Text = FormatStringApp.FormatDateTime(dsData.Tables[0].Rows[0]["insert_date"]);
            //if (!Convert.IsDBNull(dsData.Tables[0].Rows[0]["update_name"]))
            //{
            //    txtUpdateLogin.Text = dsData.Tables[0].Rows[0]["update_name"].ToString();
            //    txtUpdateDate.Text = FormatStringApp.FormatDateTime(dsData.Tables[0].Rows[0]["update_date"]);
            //}
            //ddlBranch.SelectedValue = dsData.Tables[0].Rows[0]["user_branch"].ToString();
            litJavaScript.Text += "</script>\n";

            //ViewState.Add("DSMasterTable", dsAuth);
            //ShowData();
        }
    }
    public DataSet UIToDoc()
    {
        DataSet dsData = new DataSet();
        DataTable dt = new DataTable();
        string[] ColumnTypeSettle = {"string", "string", "string", "string", "string", 
                                "string", "string", "string", "string", "string", 
                                "string", "string", "string", "string", "string", 
                                "string", "string"};
        string[] ColumnNameSettle = {"settle_id", "settle_date", "policy_no", "settle_premium", "settle_compremium",
                                "settle_endorsepremium", "settle_totalpremium", "settle_discountrate", "settle_discount", "settle_balance", 
                                "settle_paid", "settle_arrear", "settle_installments", "settle_startdate", "settle_months", 
                                "settle_arno", "LOGINNAME"};
        string[] ColumnTypeSend = {"string", "string", "string", "string", "string", 
                                "string", "string"};
        string[] ColumnNameSend = {"policy_no", "send_name", "send_address1", "send_address2", "send_province",
                                "send_postcode", "LOGINNAME"};
        string[] ColumnTypePay = {"string", "string", "string", "string", "string", "string"};
        string[] ColumnNamePay = {"pay_id", "pay_docno", "policy_no", "pay_amt", "pay_date", "LOGINNAME"};
        string[] ColumnTypePayDetail = { "string", "string", "string", "string", "string" };
        string[] ColumnNamePayDetail = { "pay_id", "pay_docno", "policy_no", "pay_amt", "LOGINNAME" };
        string[] ColumnTypeInstall = { "string", "string", "string", "string", "string",
                                         "string", "string", "string", "string", "string",
                                        "string", "string", "string"};
        string[] ColumnNameInstall = { "inst_id", "settle_id", "policy_no", "inst_no", "inst_duedate", 
                                        "inst_rate", "inst_net", "inst_rvno", "inst_paiddate", "inst_paid", 
                                        "inst_remark", "pay_id", "LOGINNAME"};
        ProfileData loginData = FormatStringApp.getLoginSession(Page, Session);
        dt = TDS.Utility.MasterUtil.AddColumnToDataTable(dt, ColumnTypeSettle, ColumnNameSettle);
        dsData.Tables.Add(dt);
        dsData.Tables[0].TableName = "settlement";
        dsData.Tables[0].Rows.Add(dsData.Tables[0].NewRow());
        dsData.Tables[0].Rows[0]["settle_id"] = txtSetttlementID.Text;
        dsData.Tables[0].Rows[0]["settle_date"] = txtSettlementDate.Text;
        dsData.Tables[0].Rows[0]["policy_no"] = lblPolicyNo.Text;
        dsData.Tables[0].Rows[0]["settle_premium"] = txtPremium1.Text;
        dsData.Tables[0].Rows[0]["settle_compremium"] = (txtPremium2.Text == "" ? "0" : txtPremium2.Text);
        dsData.Tables[0].Rows[0]["settle_endorsepremium"] = (txtEndorseTotal.Text == "" ? "0" : txtEndorseTotal.Text);
        dsData.Tables[0].Rows[0]["settle_totalpremium"] = txtPolicyBalance.Text;
        dsData.Tables[0].Rows[0]["settle_discountrate"] = (txtDiscountRate.Text == "" ? "0" : txtDiscountRate.Text);
        dsData.Tables[0].Rows[0]["settle_discount"] = (txtDiscount.Text == "" ? "0" : txtDiscount.Text);
        dsData.Tables[0].Rows[0]["settle_balance"] = txtBalance.Text;
        dsData.Tables[0].Rows[0]["settle_paid"] = "0";
        dsData.Tables[0].Rows[0]["settle_arrear"] = txtBalance.Text;
        dsData.Tables[0].Rows[0]["settle_installments"] = txtInstallments.Text;
        dsData.Tables[0].Rows[0]["settle_startdate"] = txtStartDate.Text;
        dsData.Tables[0].Rows[0]["settle_months"] = txtMonths.Text;
        dsData.Tables[0].Rows[0]["settle_arno"] = txtARNo.Text;
        dsData.Tables[0].Rows[0]["LOGINNAME"] = loginData.loginName;

        dt = new DataTable();
        dt = TDS.Utility.MasterUtil.AddColumnToDataTable(dt, ColumnTypeSend, ColumnNameSend);
        dsData.Tables.Add(dt);
        dsData.Tables[dsData.Tables.Count - 1].TableName = "senddoc";
        dsData.Tables["senddoc"].Rows.Add(dsData.Tables["senddoc"].NewRow());
        dsData.Tables["senddoc"].Rows[0]["policy_no"] = lblPolicyNo.Text;
        dsData.Tables["senddoc"].Rows[0]["send_name"] = txtContactName.Text;
        dsData.Tables["senddoc"].Rows[0]["send_address1"] = txtContactAddr1.Text;
        dsData.Tables["senddoc"].Rows[0]["send_address2"] = txtContactAddr2.Text;        
        dsData.Tables["senddoc"].Rows[0]["send_postcode"] = txtContactPostCode.Text;
        dsData.Tables["senddoc"].Rows[0]["LOGINNAME"] = loginData.loginName;

        dt = new DataTable();
        dt = TDS.Utility.MasterUtil.AddColumnToDataTable(dt, ColumnTypePay, ColumnNamePay);
        dsData.Tables.Add(dt);
        dsData.Tables[dsData.Tables.Count-1].TableName = "payin";
        dsData.Tables["payin"].Rows.Add(dsData.Tables["payin"].NewRow());
        dsData.Tables["payin"].Rows[0]["pay_id"] = "";
        dsData.Tables["payin"].Rows[0]["pay_docno"] = txtRVNO.Text;
        dsData.Tables["payin"].Rows[0]["policy_no"] = lblPolicyNo.Text;
        dsData.Tables["payin"].Rows[0]["pay_amt"] = txtTotalPaid.Text;
        dsData.Tables["payin"].Rows[0]["pay_date"] = txtRVDate.Text;
        dsData.Tables["payin"].Rows[0]["LOGINNAME"] = loginData.loginName;

        dt = new DataTable();
        dt = TDS.Utility.MasterUtil.AddColumnToDataTable(dt, ColumnTypeInstall, ColumnNameInstall);
        dsData.Tables.Add(dt);
        dsData.Tables[dsData.Tables.Count - 1].TableName = "installment";

        UserControl_ucTxtDate txtDtgInstDueDate;
        TextBox txtDtgInstRate;
        TextBox txtDtgInstNet;
        UserControl_ucTxtDate txtDtgInstPaidDate;
        TextBox txtDtgInstPaid;
        TextBox txtDtgInstRemark;
        for (int iInst = 0; iInst <= dtgInstallments.Items.Count - 1; iInst++)
        {
            txtDtgInstDueDate = (UserControl_ucTxtDate)dtgInstallments.Items[iInst].FindControl("txtDueDate");
            txtDtgInstRate = (TextBox)dtgInstallments.Items[iInst].FindControl("txtRate");
            txtDtgInstNet = (TextBox)dtgInstallments.Items[iInst].FindControl("txtNet");
            txtDtgInstPaidDate = (UserControl_ucTxtDate)dtgInstallments.Items[iInst].FindControl("");
            txtDtgInstPaid = (TextBox)dtgInstallments.Items[iInst].FindControl("txtPaid");
            txtDtgInstRemark = (TextBox)dtgInstallments.Items[iInst].FindControl("txtInsRemark");
            dsData.Tables["installment"].Rows.Add(dsData.Tables["installment"].NewRow());
            dsData.Tables["installment"].Rows[iInst]["inst_id"] = "";
            dsData.Tables["installment"].Rows[iInst]["settle_id"] = txtSetttlementID.Text;
            dsData.Tables["installment"].Rows[iInst]["policy_no"] = lblPolicyNo.Text;
            dsData.Tables["installment"].Rows[iInst]["inst_no"] = Convert.ToString(iInst+1);
            dsData.Tables["installment"].Rows[iInst]["inst_duedate"] = txtDtgInstDueDate.Text;
            dsData.Tables["installment"].Rows[iInst]["inst_rate"] = txtDtgInstRate.Text;
            dsData.Tables["installment"].Rows[iInst]["inst_net"] = txtDtgInstNet.Text;
            if (Convert.ToDouble(txtDtgInstPaid.Text) > 0)
            {
                dsData.Tables["installment"].Rows[iInst]["inst_paiddate"] = txtRVDate.Text;
                dsData.Tables["installment"].Rows[iInst]["inst_rvno"] = txtRVNO.Text;
            }
            else
            {
                dsData.Tables["installment"].Rows[iInst]["inst_paiddate"] = "__/__/____";
                dsData.Tables["installment"].Rows[iInst]["inst_rvno"] = "";
            }
            dsData.Tables["installment"].Rows[iInst]["inst_paid"] = txtDtgInstPaid.Text;
            dsData.Tables["installment"].Rows[iInst]["inst_remark"] = txtDtgInstRemark.Text;
            dsData.Tables["installment"].Rows[iInst]["pay_id"] = txtPayID.Text;
            dsData.Tables["installment"].Rows[iInst]["LOGINNAME"] = loginData.loginName;
        }
        return dsData;
    }
    public void setMode()
    {
        txtAgentCode.CssClass = "TEXTBOXDIS";
        txtAgentCode.ReadOnly = true;
        txtAgentName.CssClass = "TEXTBOXDIS";
        txtAgentName.ReadOnly = true;
        if (txtMode.Text == "A")
        {
            //txtUserName.CssClass = "TEXTBOX";
            //txtUserName.ReadOnly = false;
            //btnAddNew.Visible = false;
        }
        else if (txtMode.Text == "E")
        {
            btnInstallment.Visible = false;
            btnAddRV.Visible = false;
            txtDeptCode.CssClass = "TEXTBOX";
            txtDeptCode.ReadOnly = false;
            txtDeptCode.Visible = false;
            imgDept.Visible = false;
            txtBrAcceptCode.CssClass = "TEXTBOX";
            txtBrAcceptCode.ReadOnly = false;
            txtBrAcceptCode.Visible = false;
            imgBrAccept.Visible = false;
            txtAcceptForCode.CssClass = "TEXTBOX";
            txtAcceptForCode.ReadOnly = false;
            txtAcceptForCode.Visible = false;
            imgAcceptFor.Visible = false;
            txtSenderCode.CssClass = "TEXTBOX";
            txtSenderCode.ReadOnly = false;
            txtSenderCode.Visible = false;
            imgSender.Visible = false;
            txtRVDate.Mode = "view";
        }
        else if (txtMode.Text == "V")
        {
            btnInstallment.Visible = false;
            btnAddRV.Visible = false;
            btnSave.Visible = false;
            btnSearch.Visible = false;
        }
    }
    public void ShowDataInstallments()
    {
        DataSet ds = new DataSet();
        if (ViewState["DSMasterTableInstallments"] != null)
            ds = (DataSet)ViewState["DSMasterTableInstallments"];

        if (ds.Tables.Count > 0)
        {
            dv.Table = ds.Tables[0];
        }
        dtgInstallments.DataSource = dv;
        dtgInstallments.DataBind();

        GridViewUtil.ItemDataBound(dtgInstallments.Items);        
    }
    public void ShowDataRV()
    {
        DataSet ds = new DataSet();
        if (Session["DSMasterTableRV"] != null)
            ds = (DataSet)Session["DSMasterTableRV"];

        if (ds.Tables.Count > 0)
        {
            dv.Table = ds.Tables[0];
        }
        dtgRV.DataSource = dv;
        dtgRV.DataBind();

        GridViewUtil.ItemDataBound(dtgRV.Items);
    }
    public string ShowLateDays(object objDueDate)
    {
        string strRet = "";
        int iDays = 0;
        DateTime dtDueDate = Convert.ToDateTime(objDueDate);

        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();
        iDays = cmDate.daysDiff(FormatStringApp.FormatDate(objDueDate), FormatStringApp.FormatDate(DateTime.Now));
        strRet = iDays.ToString();
        return strRet;
    }
    private void saveTmpInstallment()
    {
        DataSet ds = new DataSet();
        int i = 0;        
        UserControl_ucTxtDate txtDtgDueDate;
        TextBox txtDtgRate;
        TextBox txtDtgNet;
        TextBox txtDtgPaid;
        TextBox txtDtgRemark;
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();
        if (ViewState["DSMasterTableInstallments"] != null)
            ds = (DataSet)ViewState["DSMasterTableInstallments"];

        for (int j = i; j <= ds.Tables[0].Rows.Count - 1; j++)
        {
            txtDtgDueDate = (UserControl_ucTxtDate)dtgInstallments.Items[j].FindControl("txtDueDate");
            txtDtgRate = (TextBox)dtgInstallments.Items[j].FindControl("txtRate");
            txtDtgNet = (TextBox)dtgInstallments.Items[j].FindControl("txtNet");
            txtDtgPaid = (TextBox)dtgInstallments.Items[j].FindControl("txtPaid");
            txtDtgRemark = (TextBox)dtgInstallments.Items[j].FindControl("txtInsRemark");
            ds.Tables[0].Rows[j]["inst_duedate"] = Convert.ToDateTime(cmDate.convertDateStringForDB(txtDtgDueDate.Text));
            ds.Tables[0].Rows[j]["inst_rate"] = txtDtgRate.Text;
            ds.Tables[0].Rows[j]["inst_net"] = txtDtgNet.Text;
            ds.Tables[0].Rows[j]["inst_paid"] = txtDtgPaid.Text;
            ds.Tables[0].Rows[j]["inst_remark"] = txtDtgRemark.Text;
        }
        ViewState["DSMasterTableInstallments"] = ds;
    }
}
