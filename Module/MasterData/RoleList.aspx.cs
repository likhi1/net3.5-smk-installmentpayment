﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Module_MasterData_RoleList : System.Web.UI.Page
{
    public string[] ColumnName = { "ROLE_ID", "ROLE_NAME", "ROLE_STATUS", "menu_name", "role_flagbranch", };
    public string[] ColumnType = { "string", "string", "string", "string","string" };
    public string[] ColumnTitle = { "ลำดับที่", "ชื่อ", "สถานะ", "เมนู","สาขา" };
    string[] ColumnWidth = { "10%", "20%", "10%","30%","30%"};
    string[] ColumnAlign = { "center", "left", "center", "left","left" };

    public string tranPicUrl = "";
    DataView dv = new DataView();
    protected void Page_Load(object sender, EventArgs e)
    {
        ProfileData loginData = FormatStringApp.getLoginSession(Page, Session);
        MasterDataManager cm = new MasterDataManager();
        DataSet ds = new DataSet();
        if (!IsPostBack)
        {
            initialDropDownList();
            initialDataGrid();
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        ProfileData loginData = FormatStringApp.getLoginSession(Page, Session);
        MasterDataManager cm = new MasterDataManager();
        DataSet ds = new DataSet();
        

        ds = cm.searchRole(txtName.Text, ddlStatus.SelectedValue);

        ViewState.Remove("SortDirection");
        ViewState.Remove("SortString");
        ViewState.Remove("SortIndex");
        ViewState.Add("DSMasterTable", ds);
        ShowData();
    }
    protected void PageIndexChanged(Object source, DataGridPageChangedEventArgs e)
    {
        DataView dv = new DataView();
        DataSet ds = new DataSet();
        if (!(ViewState["DSMasterTable"] == null))
        {
            ds = (DataSet)ViewState["DSMasterTable"];
            if (e.NewPageIndex > dtgData.PageCount - 1)
                dtgData.CurrentPageIndex = dtgData.PageCount - 1;
            else
                dtgData.CurrentPageIndex = e.NewPageIndex;

            String sortBy = "";
            if (!(ViewState["DSMasterTable"] == null))
            {
                sortBy = (string)(ViewState["SortString"]);
            }
            dv.Table = ds.Tables[0];
            dv.Sort = sortBy;
            ShowData();

        }
    }

    public void initialDropDownList()
    {
        DataSet ds;
        MasterDataManager cmMaster = new MasterDataManager();        

        ds = new DataSet();
        ds = FormatStringApp.getAllDataStatus();
        ddlStatus.DataSource = ds;
        ddlStatus.DataValueField = "status_code";
        ddlStatus.DataTextField = "status_name";
        ddlStatus.DataBind();
        ddlStatus.Items.Insert(0, new ListItem("-- All --", ""));

    }
    public void initialDataGrid()
    {
        DataSet ds = new DataSet();
        DataTable dt = new DataTable();
        int iPageSize = 0;
        dt = TDS.Utility.MasterUtil.AddColumnToDataTable(dt, ColumnType, ColumnName);
        ds.Tables.Add(dt);
        ViewState.Remove("SortDirection");
        ViewState.Remove("SortString");
        ViewState.Remove("SortIndex");
        ViewState.Add("DSMasterTable", ds);
        ShowData();
        GridViewUtil.setGridStyle(dtgData, ColumnWidth, ColumnAlign, ColumnTitle);
        try
        {
            iPageSize = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["pageSize"].ToString());
        }
        catch (Exception e)
        {
            iPageSize = 10;
        }
        dtgData.PageSize = iPageSize;
    }
    public void ShowData()
    {
        DataSet ds = new DataSet();
        if (ViewState["DSMasterTable"] != null)
            ds = (DataSet)ViewState["DSMasterTable"];

        string sortBy = "";
        if (ViewState["SortString"] != null)
            sortBy = (String)ViewState["SortString"];

        if (ds.Tables.Count > 0)
        {
            dv.Table = ds.Tables[0];
            dv.Sort = sortBy;

            int newPageCount = (Int32)(Math.Ceiling((double)dv.Table.Rows.Count / (double)dtgData.PageSize));
            if (dtgData.CurrentPageIndex >= newPageCount && newPageCount > 0)
                dtgData.CurrentPageIndex = newPageCount - 1;
        }
        dtgData.DataSource = dv;
        dtgData.DataBind();

        GridViewUtil.ItemDataBound(dtgData.Items);

        ucPageNavigator1.dvData = ds.Tables[0].DefaultView;
        ucPageNavigator1.dtgData = dtgData;
    }
    public void sortColumn(object Sender, EventArgs e)
    {
        string[] SortDirection = new string[ColumnTitle.Length];
        for (int i = 0; i < SortDirection.Length; i++)
        {
            SortDirection[i] = "";
        }
        if (ViewState["SortDirection"] != null)
            SortDirection = (string[])ViewState["SortDirection"];

        LinkButton lb = (LinkButton)Sender;
        string sortBy = "";
        int index = 0;
        while ((index < ColumnName.Length) && !((lb.Text).Equals(ColumnTitle[index])))
        {
            index++;
        }
        sortBy = ("" + ColumnName[index] + " " + SortDirection[index]);
        SortDirection[index] = (SortDirection[index].Equals("") ? "DESC" : "");
        ViewState.Add("SortDirection", SortDirection);
        ViewState.Add("SortString", sortBy);
        ViewState.Add("SortIndex", index);
        ShowData();
    }
    public string getPicUrl(int colNo)
    {
        string returnValue = Request.ApplicationPath + "/Images/tran.gif";
        if (ViewState["SortIndex"] != null)
        {
            DataGridItem dgi = (DataGridItem)(dtgData.Controls[0].Controls[1]);

            string[] SortDirection = new string[Title.Length];
            SortDirection = (string[])ViewState["SortDirection"];

            if ((int)(ViewState["SortIndex"]) == colNo)
            {
                returnValue = Request.ApplicationPath + "/Images/" + (SortDirection[colNo].Equals("") ? "sortDown.gif" : "sortUp.gif");
            }
        }
        return returnValue;
    }
    public string getAllMenuByRoleID(object objRoleID)
    {
        string strRet = "";
        ProfileData loginData = FormatStringApp.getLoginSession(Page, Session);
        MasterDataManager cm = new MasterDataManager();
        DataSet ds;
        ds = cm.getDataMenuByRoleID(objRoleID.ToString());
        for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
        {
            strRet += ds.Tables[0].Rows[i]["menu_name"].ToString() + "(" + ds.Tables[0].Rows[i]["flagadd"].ToString() + ds.Tables[0].Rows[i]["flagedit"].ToString() + ds.Tables[0].Rows[i]["flagdelete"].ToString() + ds.Tables[0].Rows[i]["flaginquiry"].ToString() + "), <br>";
        }

        if (strRet.EndsWith(", <br>"))
            strRet = strRet.Substring(0, strRet.Length - 6);

        return strRet;
    }
    public string GetAllBranchSpecific(object objRoleID) 
    {
        string strRet = "";
        ProfileData loginData = FormatStringApp.getLoginSession(Page, Session);
        MasterDataManager cm = new MasterDataManager();
        DataSet ds;
        ds = cm.GetRoleBranch(objRoleID.ToString());
        for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
        {
            strRet += ds.Tables[0].Rows[i]["branch_name"].ToString() + ", <br>";
        }

        if (strRet.EndsWith(", <br>"))
            strRet = strRet.Substring(0, strRet.Length - 6);

        return strRet; 
    }
    public string GetBranchConfig(object objBranchSet)
    {
        string StrRet = "";
        if (objBranchSet.ToString() == "S")
        {
            StrRet = "สาขาที่สังกัดและสาขาที่ระบุ<br />";
        }
        else if (objBranchSet.ToString() == "A")
        {
            StrRet = "สาขาทั้งหมด<br />";
        }
        else if (objBranchSet.ToString() == "B")
        {
            StrRet = "เฉพาะสาขาที่สังกัด<br />";
        }
        return StrRet;
    }
}
