﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

/// <summary>
/// Summary description for SyncManager
/// </summary>
public class SyncManager : TDS.BL.BLWorker_MSSQL
{
	public SyncManager()
	{
		//
		// TODO: Add constructor logic here
		//
    }
    #region Sync Data
    public DataSet searchDataSyncHistory(string strFromDate, string strToDate)
    {
        DataSet ds = new DataSet();
        string strSQL = "";
        string strWhere = "";
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();

        strSQL = "SELECT A.*, B.user_fname + ' ' + B.user_lname as sync_name ";
        strSQL += " FROM sync_history A LEFT JOIN aut_user B ON A.insert_login = B.user_login ";
        if (strFromDate != "__/__/____" && strFromDate != "")
            strWhere += " sync_datetime >= '" + cmDate.convertDateStringForDB(strFromDate) + " 00:00:00' AND ";
        if (strToDate != "__/__/____" && strToDate != "")
            strWhere += " sync_datetime <= '" + cmDate.convertDateStringForDB(strToDate) + " 23:59:59' AND ";
        if (strWhere.EndsWith("AND ") == true)
            strSQL = strSQL + " WHERE " + strWhere.Substring(0, strWhere.Length - 4);
        strSQL += " ORDER BY sync_datetime asc ";
        ds = executeQuery(strSQL);
        return ds;
    }
    public DataSet searchDataSyncPolicy(string strFromDate, string strToDate)
    {
        DataSet ds = new DataSet();
        string strSQL = "";
        string strWhere = "";
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();

        strSQL = "SELECT * ";
        strSQL += " FROM sync_policy ";
        strSQL += " WHERE spol_status = '' ";
        if (strFromDate != "__/__/____" && strFromDate != "")
            strWhere += " spol_issuedate >= '" + cmDate.convertDateStringForDB(strFromDate) + "' AND ";
        if (strToDate != "__/__/____" && strToDate != "")
            strWhere += " spol_issuedate <= '" + cmDate.convertDateStringForDB(strToDate) + "' AND ";
        if (strWhere.EndsWith("AND ") == true)
            strSQL = strSQL + " AND " + strWhere.Substring(0, strWhere.Length - 4);
        strSQL += " ORDER BY spol_issuedate asc ";
        ds = executeQuery(strSQL);
        return ds;
    }

    public string addDataSyncPolicyEndorse(DataSet dsData, DataSet dsEndorse, string strLogin, string strFlagHistory)
    {
        int iRetPolicy = 0;
        string strSQL = "";
        string strID = "";
        string strSyncID = "";
        string strRet = "";
        DateTime dtSyncDateTime =  DateTime.Now;
        SqlConnection conn ;
        SqlTransaction trans;
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();

        conn = getDbConnection();
        trans = conn.BeginTransaction();

        try
        {
            strSyncID = getMaxID("sync_id", "sync_history", "", 5, true, true, conn, trans);
            dtSyncDateTime = DateTime.Now;
            
            for (int iPol = 0; iPol <= dsData.Tables[0].Rows.Count - 1; iPol++)
            {
                strID = getMaxID("spol_id", "sync_policy", "", 9, true, true, conn, trans);
                strSQL = "";
                strSQL = "INSERT INTO sync_policy (";
                strSQL += " spol_id, spol_type, spol_policy, spol_agentcode, spol_branch, spol_effectivefromdate ";
                strSQL += "     ,spol_effectivetodate, spol_grosspremium, spol_netpremium, spol_custpre, spol_custfirstname ";
                strSQL += "     ,spol_custlastname, spol_custaddr1, spol_custaddr2, spol_custzip, spol_custphoneno ";
                strSQL += "     ,spol_receivedate, spol_remark1, spol_remark2, spol_remark3, spol_remark4 ";
                strSQL += "     ,spol_remark5, spol_compolicy, spol_comfirstname, spol_comlastname, spol_comlicense ";
                strSQL += "     ,spol_comchasis, spol_comgrosspremium, spol_comnetpremium, spol_issuedate, spol_settlementdate ";
                strSQL += "     ,spol_status, sync_id, sync_datetime, insert_date, insert_login) ";
                strSQL += " VALUES ( ";
                strSQL += " '" + strID + "', ";
                strSQL += " '" + dsData.Tables[0].Rows[iPol]["spol_type"].ToString().TrimEnd() + "', ";
                strSQL += " '" + dsData.Tables[0].Rows[iPol]["spol_policy"].ToString().TrimEnd() + "', ";
                strSQL += " '" + dsData.Tables[0].Rows[iPol]["spol_agentcode"].ToString().TrimEnd() + "', ";
                strSQL += " '" + dsData.Tables[0].Rows[iPol]["spol_branch"].ToString().TrimEnd() + "', ";
                strSQL += " '" + cmDate.convertDateStringForDB(FormatStringApp.FormatDate(dsData.Tables[0].Rows[iPol]["spol_effectivefromdate"].ToString())) + "', ";
                if (dsData.Tables[0].Rows[iPol]["spol_effectivetodate"].ToString() != "")
                    strSQL += " '" + cmDate.convertDateStringForDB(FormatStringApp.FormatDate(dsData.Tables[0].Rows[iPol]["spol_effectivetodate"].ToString())) + "', ";
                else
                    strSQL += " null, ";
                strSQL += " '" + dsData.Tables[0].Rows[iPol]["spol_grosspremium"].ToString().Replace(",","") + "', ";
                strSQL += " '" + dsData.Tables[0].Rows[iPol]["spol_netpremium"].ToString().Replace(",","") + "', ";
                strSQL += " '" + dsData.Tables[0].Rows[iPol]["spol_custpre"].ToString().TrimEnd() + "', ";
                strSQL += " '" + dsData.Tables[0].Rows[iPol]["spol_custfirstname"].ToString().TrimEnd() + "', ";
                strSQL += " '" + dsData.Tables[0].Rows[iPol]["spol_custlastname"].ToString().TrimEnd() + "', ";
                strSQL += " '" + dsData.Tables[0].Rows[iPol]["spol_custaddr1"].ToString().TrimEnd() + "', ";
                strSQL += " '" + dsData.Tables[0].Rows[iPol]["spol_custaddr2"].ToString().TrimEnd() + "', ";
                strSQL += " '" + dsData.Tables[0].Rows[iPol]["spol_custzip"].ToString().TrimEnd() + "', ";
                strSQL += " '" + dsData.Tables[0].Rows[iPol]["spol_custphoneno"].ToString().TrimEnd() + "', ";
                if (dsData.Tables[0].Rows[iPol]["spol_receivedate"].ToString() != "")
                    strSQL += " '" + cmDate.convertDateStringForDB(FormatStringApp.FormatDate(dsData.Tables[0].Rows[iPol]["spol_receivedate"].ToString())) + "', ";
                else
                    strSQL += " null, ";
                strSQL += " '" + dsData.Tables[0].Rows[iPol]["spol_remark1"].ToString().TrimEnd() + "', ";
                strSQL += " '" + dsData.Tables[0].Rows[iPol]["spol_remark2"].ToString().TrimEnd() + "', ";
                strSQL += " '" + dsData.Tables[0].Rows[iPol]["spol_remark3"].ToString().TrimEnd() + "', ";
                strSQL += " '" + dsData.Tables[0].Rows[iPol]["spol_remark4"].ToString().TrimEnd() + "', ";
                strSQL += " '" + dsData.Tables[0].Rows[iPol]["spol_remark5"].ToString().TrimEnd() + "', ";
                strSQL += " '" + dsData.Tables[0].Rows[iPol]["spol_compolicy"].ToString().TrimEnd() + "', ";
                strSQL += " '" + dsData.Tables[0].Rows[iPol]["spol_comfirstname"].ToString().TrimEnd() + "', ";
                strSQL += " '" + dsData.Tables[0].Rows[iPol]["spol_comlastname"].ToString().TrimEnd() + "', ";
                strSQL += " '" + dsData.Tables[0].Rows[iPol]["spol_comlicense"].ToString().TrimEnd() + "', ";
                strSQL += " '" + dsData.Tables[0].Rows[iPol]["spol_comchasis"].ToString().TrimEnd() + "', ";
                if (dsData.Tables[0].Rows[iPol]["spol_comgrosspremium"].ToString() != "")
                    strSQL += " '" + dsData.Tables[0].Rows[iPol]["spol_comgrosspremium"].ToString().Replace(",","") + "', ";
                else
                    strSQL += " '0', ";
                if (dsData.Tables[0].Rows[iPol]["spol_comnetpremium"].ToString() != "")
                    strSQL += " '" + dsData.Tables[0].Rows[iPol]["spol_comnetpremium"].ToString().Replace(",","") + "', ";
                else
                    strSQL += " '0', ";
                strSQL += " '" + cmDate.convertDateStringForDB(FormatStringApp.FormatDate(dsData.Tables[0].Rows[iPol]["spol_issuedate"].ToString())) + "', ";
                strSQL += " null, ";                
                strSQL += " '', ";
                strSQL += " '" + strSyncID + "', ";
                strSQL += " '" + cmDate.convertDateStringForDB(FormatStringApp.FormatDate(dtSyncDateTime)) + " " + FormatStringApp.FormatDateTime(dtSyncDateTime).Substring(11) + "', ";
                strSQL += " getdate(), ";
                strSQL += " '" + strLogin + "' ";
                strSQL += " ) ";

                executeUpdate(strSQL, conn, trans);
                iRetPolicy++;
            }

            if (strFlagHistory == "Y")
            {
                strSQL = "INSERT INTO sync_history( ";
                strSQL += " sync_id, sync_datetime, sync_policy, sync_endorse, insert_date, insert_login ) ";
                strSQL += " VALUES ( ";
                strSQL += " '" + strSyncID + "', ";
                strSQL += " '" + cmDate.convertDateStringForDB(FormatStringApp.FormatDate(dtSyncDateTime)) + " " + FormatStringApp.FormatDateTime(dtSyncDateTime).Substring(11) + "', ";
                strSQL += " '" + iRetPolicy + "', ";
                strSQL += " '0', ";
                strSQL += " getdate(), ";
                strSQL += " '" + strLogin + "' ";
                strSQL += " ) ";
                executeUpdate(strSQL, conn, trans);
            }
            trans.Commit();
        }
        catch (Exception e)
        {
            trans.Rollback();
            iRetPolicy = 0;

            TDS.Utility.MasterUtil.writeError("SyncManager.addDataSyncPolicy - ERROR", e.ToString());
            TDS.Utility.MasterUtil.writeError("SyncManager.addDataSyncPolicy - SQL", strSQL);
        }
        finally
        {
            trans.Dispose();
            conn.Close();
            conn.Dispose();
        }


        if (strFlagHistory == "Y")
            strRet = iRetPolicy.ToString();
        else
            strRet = strSyncID;
        return strRet;
    }
    #endregion

    #region To Do Policy
    public DataSet searchDataToDoPolicy(string strPolicyNo, string strAgentCode, string strPolType)
    {
        DataSet ds = new DataSet();
        string strSQL = "";
        string strWhere = "";
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();

        strSQL = "SELECT *,spol_custpre + spol_custfirstname + ' ' + spol_custlastname as spol_custname ";
        strSQL += " FROM sync_policy ";
        strSQL += " WHERE spol_settlementdate is null ";
        if (strPolicyNo != "")
            strWhere += " spol_policy = '" + strPolicyNo + "' AND ";
        if (strAgentCode != "")
            strWhere += " sync_agentcode = '" + strAgentCode + "' AND ";
        if (strPolType != "")
            strWhere += " spol_type = '" + strPolType + "' AND ";

        if (strWhere.EndsWith("AND ") == true)
            strSQL = strSQL + " AND " + strWhere.Substring(0, strWhere.Length - 4);
        strSQL += " ORDER BY spol_policy asc ";
        ds = executeQuery(strSQL);
        return ds;
    }
    public DataSet getDataSyncPolicyByID(string strID)
    {
        DataSet ds = new DataSet();
        string strSQL = "";
        string strWhere = "";
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();

        strSQL = "SELECT *,spol_custpre + spol_custfirstname + ' ' + spol_custlastname as spol_custname ";
        strSQL += " ,spol_compre + spol_comfirstname + ' ' + spol_comlastname as spol_comname ";
        strSQL += " FROM sync_policy ";
        strSQL += " WHERE spol_id = '" + strID + "' ";
        strSQL += "     AND spol_status = '' ";
        ds = executeQuery(strSQL);
        return ds;
    }
    public DataSet getDataSyncPolicyByPolNo(string strPolNo)
    {
        DataSet ds = new DataSet();
        string strSQL = "";
        string strWhere = "";
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();

        strSQL = "SELECT *,spol_custpre + spol_custfirstname + ' ' + spol_custlastname as spol_custname ";
        strSQL += " ,spol_compre + spol_comfirstname + ' ' + spol_comlastname as spol_comname ";
        strSQL += " FROM sync_policy ";
        strSQL += " WHERE spol_policy = '" + strPolNo + "' ";
        strSQL += "     AND spol_status = '' ";
        ds = executeQuery(strSQL);
        return ds;
    }
    #endregion
}
