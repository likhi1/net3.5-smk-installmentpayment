﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Module_Settlement_ToDoPolicyList : System.Web.UI.Page
{
    public string[] ColumnName = { "spol_id", "spol_policy", "spol_agentcode", "spol_branch", "spol_effectivefromdate", "spol_custname", "spol_netpremium", "spol_grosspremium", "spol_compolicy", "spol_effectivetodate" };
    public string[] ColumnType = { "string", "string", "string", "string", "string", "string", "string", "string", "string" };
    public string[] ColumnTitle = { "ลำดับที่", "เลขกรมธรรม์", "รหัสตัวแทน", "สาขา", "วันที่คุ้มครอง", "ชื่อ-นามสกุลผู้เอาประกันภัย", "เบี้ยรวม", "เบี้ยสุทธิ", "หมายเหตุ" };
    string[] ColumnWidth = { "5%", "10%", "10%", "10%", "15%", "20%" , "10%", "10%", "10%"};
    string[] ColumnAlign = { "center", "left", "left", "left", "center", "left", "right", "right", "center" };

    public string tranPicUrl = "";
    DataView dv = new DataView();

    protected void Page_Load(object sender, EventArgs e)
    {
        ProfileData loginData = FormatStringApp.getLoginSession(Page, Session);
        MasterDataManager cm = new MasterDataManager();
        DataSet ds = new DataSet();
        if (!IsPostBack)
        {
            initialDropDownList();
            initialDataGrid();
        }
        else
        {
            litJavaScript.Text = "";
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        ProfileData loginData = FormatStringApp.getLoginSession(Page, Session);
        SyncManager cmSync = new SyncManager();
        DataSet ds = new DataSet();

        ds = cmSync.searchDataToDoPolicy(txtPolicy.Text, txtAgentCode.Text, ddlPolType.SelectedValue);        
        ViewState.Remove("SortDirection");
        ViewState.Remove("SortString");
        ViewState.Remove("SortIndex");
        ViewState.Add("DSMasterTable", ds);
        ShowData();
    }

    protected void PageIndexChanged(Object source, DataGridPageChangedEventArgs e)
    {
        DataView dv = new DataView();
        DataSet ds = new DataSet();
        if (!(ViewState["DSMasterTable"] == null))
        {
            ds = (DataSet)ViewState["DSMasterTable"];
            if (e.NewPageIndex > dtgData.PageCount - 1)
                dtgData.CurrentPageIndex = dtgData.PageCount - 1;
            else
                dtgData.CurrentPageIndex = e.NewPageIndex;

            String sortBy = "";
            if (!(ViewState["DSMasterTable"] == null))
            {
                sortBy = (string)(ViewState["SortString"]);
            }
            dv.Table = ds.Tables[0];
            dv.Sort = sortBy;
            ShowData();

        }
    }

    public void initialDropDownList()
    {
        DataSet ds;
        MasterTableManager cmMaster = new MasterTableManager();

        ds = cmMaster.getAllDataPolType();
        ddlPolType.DataSource = ds;
        ddlPolType.DataTextField = "ptype_name";
        ddlPolType.DataValueField = "ptype_code";
        ddlPolType.DataBind();
        ddlPolType.Items.Insert(0, new ListItem(" -- ทั้งหมด -- ", ""));
    }
    public void initialDataGrid()
    {
        DataSet ds = new DataSet();
        DataTable dt = new DataTable();
        int iPageSize = 0;
        dt = TDS.Utility.MasterUtil.AddColumnToDataTable(dt, ColumnType, ColumnName);
        ds.Tables.Add(dt);
        ViewState.Remove("SortDirection");
        ViewState.Remove("SortString");
        ViewState.Remove("SortIndex");
        ViewState.Add("DSMasterTable", ds);
        ShowData();
        GridViewUtil.setGridStyle(dtgData, ColumnWidth, ColumnAlign, ColumnTitle);
        try
        {
            iPageSize = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["pageSize"].ToString());
        }
        catch (Exception e)
        {
            iPageSize = 10;
        }
        dtgData.PageSize = iPageSize;
    }
    public void ShowData()
    {
        DataSet ds = new DataSet();
        if (ViewState["DSMasterTable"] != null)
            ds = (DataSet)ViewState["DSMasterTable"];

        string sortBy = "";
        if (ViewState["SortString"] != null)
            sortBy = (String)ViewState["SortString"];

        if (ds.Tables.Count > 0)
        {
            dv.Table = ds.Tables[0];
            dv.Sort = sortBy;

            int newPageCount = (Int32)(Math.Ceiling((double)dv.Table.Rows.Count / (double)dtgData.PageSize));
            if (dtgData.CurrentPageIndex >= newPageCount && newPageCount > 0)
                dtgData.CurrentPageIndex = newPageCount - 1;
        }
        dtgData.DataSource = dv;
        dtgData.DataBind();

        GridViewUtil.ItemDataBound(dtgData.Items);

        ucPageNavigator1.dvData = ds.Tables[0].DefaultView;
        ucPageNavigator1.dtgData = dtgData;
    }
    public void sortColumn(object Sender, EventArgs e)
    {
        string[] SortDirection = new string[ColumnTitle.Length];
        for (int i = 0; i < SortDirection.Length; i++)
        {
            SortDirection[i] = "";
        }
        if (ViewState["SortDirection"] != null)
            SortDirection = (string[])ViewState["SortDirection"];

        LinkButton lb = (LinkButton)Sender;
        string sortBy = "";
        int index = 0;
        while ((index < ColumnName.Length) && !((lb.Text).Equals(ColumnTitle[index])))
        {
            index++;
        }
        sortBy = ("" + ColumnName[index] + " " + SortDirection[index]);
        SortDirection[index] = (SortDirection[index].Equals("") ? "DESC" : "");
        ViewState.Add("SortDirection", SortDirection);
        ViewState.Add("SortString", sortBy);
        ViewState.Add("SortIndex", index);
        ShowData();
    }
    public string getPicUrl(int colNo)
    {
        string returnValue = Request.ApplicationPath + "/Images/tran.gif";
        if (ViewState["SortIndex"] != null)
        {
            DataGridItem dgi = (DataGridItem)(dtgData.Controls[0].Controls[1]);

            string[] SortDirection = new string[Title.Length];
            SortDirection = (string[])ViewState["SortDirection"];

            if ((int)(ViewState["SortIndex"]) == colNo)
            {
                returnValue = Request.ApplicationPath + "/Images/" + (SortDirection[colNo].Equals("") ? "sortDown.gif" : "sortUp.gif");
            }
        }
        return returnValue;
    }
    public string showRemark(object objComNo, object objSpolID)
    {
        string strRet = "";
        if (objComNo.ToString() != "")
            strRet = "รวมพรบ.เลขที่" + objComNo.ToString() + "";

        // หาข้อมูลสลักหลัง
        return strRet;
    }
}
