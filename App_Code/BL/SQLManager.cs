﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for SQLManager
/// </summary>
public class SQLManager : TDS.BL.BLWorker_MSSQL
{
	public SQLManager()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public DataSet getDataSQLNonMotor(string strDate)
    {
        DataSet ds = new DataSet();
        string strSQL = "";
        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();
        strSQL = "SELECT distinct 'N' as spol_type, pr.policy as spol_policy ,1 as spol_seq,mo.agt_cod as spol_agentcode, " + System.Environment.NewLine;
        strSQL += " mo.branch as spol_branch, mo.eff_fdte as spol_effectivefromdate, mo.eff_tdte as spol_effectivetodate, " + System.Environment.NewLine;
        strSQL += " mo.nmt_pre_grs as spol_grosspremium ,mo.nmt_pre_net as spol_netpremium, " + System.Environment.NewLine;
        strSQL += " tt.desc_tha as spol_custpre,mo.hol_name  as spol_custfirstname,mo.hol_name as spol_custlastname, " + System.Environment.NewLine;
        strSQL += " mo.hol_add as spol_custaddr1,'อำเภอ' + am.desc_tha + ' จังหวัด' + pv.desc_tha as spol_custaddr2, mo.hol_zip as spol_custzip, " + System.Environment.NewLine;
        strSQL += " mo.hol_tel as spol_custphoneno, pr.pol_dte as spol_receivedate, " + System.Environment.NewLine;
        strSQL += " p.plan_desc as spol_remark1, '' as spol_remark2, '' as spol_remark3, " + System.Environment.NewLine;
        strSQL += " '' as spol_remark4, '' spol_remark5,  mo.input_date as spol_issuedate, " + System.Environment.NewLine;
        strSQL += " '' as spol_compolicy, '' as spol_compre, '' as spol_comfirstname, '' as spol_comlastname, " + System.Environment.NewLine;
        strSQL += " '' as spol_comlicense, '' as spol_comchasis, 0 as spol_comnetpremium, 0 as spol_comgrosspremium " + System.Environment.NewLine;
        strSQL += " FROM nmt_policy pr INNER JOIN nmt_proposal mo ON pr.nmt_prop_id = mo.nmt_prop_id " + System.Environment.NewLine;
        strSQL += "     LEFT JOIN desc1 tt on mo.hol_pre = tt.desc_cod and tt.desc_typ = 'PF' " + System.Environment.NewLine;
        strSQL += "     LEFT JOIN desc1 am on mo.hol_amp = tt.desc_cod and tt.desc_typ = 'PF' " + System.Environment.NewLine;
        strSQL += "     LEFT JOIN desc1 pv on mo.hol_prv = tt.desc_cod and tt.desc_typ = 'PV' " + System.Environment.NewLine;
        strSQL += "     LEFT JOIN nmt_plan_premium pp on pp.plan_prem_code = mo.plan_prem_code " + System.Environment.NewLine;
        strSQL += "     LEFT JOIN nmt_plan p  on p.plan_code = pp.plan_code " + System.Environment.NewLine;
        strSQL += "     INNER JOIN subagent_type ty ON mo.agt_cod = ty.agt_cod  and mo.sagt_cod  = ty.sagt_cod " + System.Environment.NewLine;
        strSQL += " WHERE ";
        strSQL += "     ty.agent_typ = '005' " + System.Environment.NewLine;
        strSQL += "     AND mo.input_date >= '" + cmDate.convertDateStringForDB(strDate) + "' ";

        strConnection = "strConSQLNonMotor";
        ds = executeQuery(strSQL);
        return ds;
    }
    public DataSet getDataSQLNonMotorByPolicy(string sttPolicy)
    {
        DataSet ds = new DataSet();
        string strSQL = "";
        strSQL = "SELECT distinct 'N' as spol_type, pr.policy as spol_policy ,1 as spol_seq,mo.agt_cod as spol_agentcode, " + System.Environment.NewLine;
        strSQL += " mo.branch as spol_branch, mo.eff_fdte as spol_effectivefromdate, mo.eff_tdte as spol_effectivetodate, " + System.Environment.NewLine;
        strSQL += " mo.nmt_pre_grs as spol_grosspremium ,mo.nmt_pre_net as spol_netpremium, " + System.Environment.NewLine;
        strSQL += " tt.desc_tha as spol_custpre,mo.hol_name  as spol_custfirstname,mo.hol_name as spol_custlastname, " + System.Environment.NewLine;
        strSQL += " mo.hol_add as spol_custaddr1,'อำเภอ' + am.desc_tha + ' จังหวัด' + pv.desc_tha as spol_custaddr2, mo.hol_zip as spol_custzip, " + System.Environment.NewLine;
        strSQL += " mo.hol_tel as spol_custphoneno, pr.pol_dte as spol_receivedate, " + System.Environment.NewLine;
        strSQL += " p.plan_desc as spol_remark1, '' as spol_remark2, '' as spol_remark3, " + System.Environment.NewLine;
        strSQL += " '' as spol_remark4, '' spol_remark5,  mo.input_date as spol_issuedate, " + System.Environment.NewLine;
        strSQL += " '' as spol_compolicy, '' as spol_compre, '' as spol_comfirstname, '' as spol_comlastname, " + System.Environment.NewLine;
        strSQL += " '' as spol_comlicense, '' as spol_comchasis, 0 as spol_comnetpremium, 0 as spol_comgrosspremium " + System.Environment.NewLine;
        strSQL += " FROM nmt_policy pr INNER JOIN nmt_proposal mo ON pr.nmt_prop_id = mo.nmt_prop_id " + System.Environment.NewLine;
        strSQL += "     LEFT JOIN desc1 tt on mo.hol_pre = tt.desc_cod and tt.desc_typ = 'PF' " + System.Environment.NewLine;
        strSQL += "     LEFT JOIN desc1 am on mo.hol_amp = tt.desc_cod and tt.desc_typ = 'PF' " + System.Environment.NewLine;
        strSQL += "     LEFT JOIN desc1 pv on mo.hol_prv = tt.desc_cod and tt.desc_typ = 'PV' " + System.Environment.NewLine;
        strSQL += "     LEFT JOIN nmt_plan_premium pp on pp.plan_prem_code = mo.plan_prem_code " + System.Environment.NewLine;
        strSQL += "     LEFT JOIN nmt_plan p  on p.plan_code = pp.plan_code " + System.Environment.NewLine;
        strSQL += "     INNER JOIN subagent_type ty ON mo.agt_cod = ty.agt_cod  and mo.sagt_cod  = ty.sagt_cod " + System.Environment.NewLine;
        strSQL += " WHERE ";
        strSQL += "     ty.agent_typ = '005' " + System.Environment.NewLine;
        strSQL += "     AND pr.policy = '" + sttPolicy + "' ";
        TDS.Utility.MasterUtil.writeLog("SQLManager.getDataSQLNonMotorByPolicy() - SQL ", strSQL);
        strConnection = "strConSQLNonMotor";
        ds = executeQuery(strSQL);
        return ds;
    }
}
