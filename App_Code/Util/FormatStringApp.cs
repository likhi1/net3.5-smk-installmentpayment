using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Globalization;
using System.Text.RegularExpressions;

/// <summary>
/// Summary description for FormatStringApp
/// </summary>
public class FormatStringApp
{
	public FormatStringApp()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    private static string dateCulture = "en-US";
    

    // Modify By : Somza
    // Modiry Date : 06/10/1007
    public static string FormatMoney(string dataMoney)
    {
        double dTmp = 0;
        if (dataMoney == null)
            dataMoney = "0";
        dataMoney = dataMoney.Trim();
        if (dataMoney != "")
            dTmp = Math.Round(Convert.ToDouble(dataMoney),4);
        else
            dTmp = 0;
        return dTmp.ToString("###,###,###,##0.00");
    }
    public static string FormatMoney(double dataMoney)
    {
        dataMoney = Math.Round(Convert.ToDouble(dataMoney), 4);
        return dataMoney.ToString("###,###,###,##0.00");
    }
    public static string FormatMoney(object dataMoney)
    {
        double dTmp = 0;
        if (dataMoney != null && !Convert.IsDBNull(dataMoney))
            dTmp = Math.Round(Convert.ToDouble(dataMoney), 4);
        else
            dTmp = 0;
        return dTmp.ToString("###,###,###,##0.00");
    }
    public static string FormatInt(string dataInt)
    {
        double dTmp = 0;
        if (dataInt == null)
            dataInt = "0";
        dataInt = dataInt.Trim();
        if (dataInt != "")
            dTmp = Convert.ToDouble(dataInt);
        else
            dTmp = 0;
        return FormatInt(dTmp);
    }
    public static string FormatInt(double dataInt)
    {
        return dataInt.ToString("###########0");
    }
    public static string FormatInt(object dataInt)
    {
        double dTmp = 0;
        if (dataInt != null)
            dTmp = Convert.ToDouble(Convert.IsDBNull(dataInt)?0:dataInt);
        else
            dTmp = 0;
        return FormatInt(dTmp);
    }
    public static string Format3Digit(string dataMoney)
    {
        double dTmp = 0;
        if (dataMoney == null)
            dataMoney = "0";
        dataMoney = dataMoney.Trim();
        if (dataMoney != "")
            dTmp = Convert.ToDouble(dataMoney);
        else
            dTmp = 0;
        return dTmp.ToString("###,###,###,##0.000");
    }
    public static string Format3Digit(double dataMoney)
    {
        return dataMoney.ToString("###,###,###,##0.000");
    }
    public static string Format3Digit(object dataMoney)
    {
        double dTmp = 0;
        if (dataMoney != null)
            dTmp = Convert.ToDouble(dataMoney);
        else
            dTmp = 0;
        return dTmp.ToString("###,###,###,##0.000");
    }
    public static string Format4Digit(string dataMoney)
    {
        double dTmp = 0;
        if (dataMoney == null)
            dataMoney = "0";
        dataMoney = dataMoney.Trim();
        if (dataMoney != "")
            dTmp = Convert.ToDouble(dataMoney);
        else
            dTmp = 0;
        return dTmp.ToString("###,###,###,##0.0000");
    }
    public static string Format4Digit(double dataMoney)
    {
        return dataMoney.ToString("###,###,###,##0.0000");
    }
    public static string Format4Digit(object dataMoney)
    {
        double dTmp = 0;
        if (dataMoney != null)
            dTmp = Convert.ToDouble(dataMoney);
        else
            dTmp = 0;
        return dTmp.ToString("###,###,###,##0.0000");
    }    

    public static string replaceQuote(string strData)
    {
        string strRet = "";
        strRet = strData.Replace("\"", "&quot;");
        return strRet;
    }
    /// <summary>
    /// Create by : somza
    /// create date : 01/06/2551
    /// ใช้ กำหนดไม่ให้โชว์ปุ่ม delete เมื่อสถานะในกริดวิวเท่ากับไม่ใช้งาน
    /// </summary>
    /// <param name="dataStatus">คำอธิบายสถานะ</param>
    /// <param name="dataID"></param>
    /// <returns></returns>
    public static string showDelete(object dataStatus, object dataID)
    {
        string strImg = "";
        if (dataStatus.ToString() == "ใช้งาน")
        {
            strImg = "<img title=\"ลบรายการ\"  id=\"imgDelete\" style=\"cursor: hand;\" border=\"0\" src=\"../../Theme/Images/delete_icon.gif\" onclick=\"deleteData('" + dataID.ToString() + "');\" >";
        }
        return strImg;
    }

    public static string FormatDateNumber(object dataDate)
    {
        string strRet = "";
        if ((!Convert.IsDBNull(dataDate)) && (dataDate.ToString() != "") && (dataDate.ToString() != "0"))
        {
            strRet = TDS.Utility.MasterUtil.getDateString(Convert.ToDateTime(dataDate.ToString().Insert(4, "/").Insert(7, "/")), "dd/MM/yyyy", "en-US");
        }
        return strRet;
    }
    public static string FormatDate(object dataDate)
    {
        string strRet = "";
        if ((!Convert.IsDBNull(dataDate)) && (dataDate.ToString() != ""))
        {
            strRet = TDS.Utility.MasterUtil.getDateString(Convert.ToDateTime(dataDate), "dd/MM/yyyy", dateCulture);
        }
        return strRet;
    }
    public static string FormatShortDate(object dataDate)
    {
        string strRet = "";
        if ((!Convert.IsDBNull(dataDate)) && (dataDate.ToString() != ""))
        {
            strRet = TDS.Utility.MasterUtil.getDateString(Convert.ToDateTime(dataDate), "dd/MM/yy", dateCulture);
        }
        return strRet;
    }
    public static string showDate(object objDate)
    {
        string strRet = "";
        if ( (!Convert.IsDBNull(objDate)) && (objDate.ToString() != ""))
            strRet = TDS.Utility.MasterUtil.getDateString(Convert.ToDateTime(objDate), "dd/MM/yyyy", dateCulture);
        return strRet;
    }
    public static string showRangeDate(object objFromDate, object objToDate)
    {
        string strRet = "";
        if (!Convert.IsDBNull(objFromDate))
            strRet = TDS.Utility.MasterUtil.getDateString(Convert.ToDateTime(objFromDate), "dd/MM/yyyy", dateCulture);
        if (!Convert.IsDBNull(objToDate))
        {
            if (strRet != "")
                strRet += " - ";
            strRet += TDS.Utility.MasterUtil.getDateString(Convert.ToDateTime(objToDate), "dd/MM/yyyy", dateCulture);
        }
        else
        {
            strRet = "ตั้งแต่ " + strRet + " เป็นต้นไป";
        }

        return strRet;
    }
    public static string showShortRangeDate(object objFromDate, object objToDate)
    {
        string strRet = "";
        if (!Convert.IsDBNull(objFromDate))
            strRet = TDS.Utility.MasterUtil.getDateString(Convert.ToDateTime(objFromDate), "dd/MM/yy", dateCulture);
        if (!Convert.IsDBNull(objToDate))
        {
            if (strRet != "")
                strRet += " - ";
            strRet += TDS.Utility.MasterUtil.getDateString(Convert.ToDateTime(objToDate), "dd/MM/yy", dateCulture);
        }
        else
        {
            strRet = "ตั้งแต่ " + strRet + " เป็นต้นไป";
        }

        return strRet;
    }
    public static string showShortRangeDateAndToDesc(object objFromDate, object objToDate, object objToDesc)
    {
        string strRet = "";
        if (!Convert.IsDBNull(objFromDate))
            strRet = TDS.Utility.MasterUtil.getDateString(Convert.ToDateTime(objFromDate), "dd/MM/yy", dateCulture);
        if (!Convert.IsDBNull(objToDate))
        {
            if (strRet != "")
                strRet += " - ";
            strRet += TDS.Utility.MasterUtil.getDateString(Convert.ToDateTime(objToDate), "dd/MM/yy", dateCulture);
        }
        else
        {
            if (objToDesc.ToString() != "")
                strRet += " - " + objToDesc.ToString();
            else
                strRet = "ตั้งแต่ " + strRet + " เป็นต้นไป" + objToDesc.ToString();
        }

        return strRet;
    }
    public static string FormatDateTime(object dataDate)
    {
        string strRet = "";              
        if (!Convert.IsDBNull(dataDate))
        {
            strRet = TDS.Utility.MasterUtil.getDateString(Convert.ToDateTime(dataDate), "dd/MM/yyyy HH:mm:ss", dateCulture);
        }
        return strRet;
    }
    public static string FormatDateTime2(object dataDate)
    {
        string strRet = "";
        if (!Convert.IsDBNull(dataDate))
        {
            strRet = TDS.Utility.MasterUtil.getDateString(Convert.ToDateTime(dataDate), "dd/MM/yyyy HH:mm:ss:fff", dateCulture);
        }
        return strRet;
    }
    public static string showIndexNo(int iIndex, object objStatus)
    {
        string strRet = "";
        strRet = iIndex.ToString();
        if (!Convert.IsDBNull(objStatus) && objStatus.ToString() == "C")
        {
            strRet = strRet + "C";
        }
        else if (!Convert.IsDBNull(objStatus) && objStatus.ToString() == "CC")
        {
            strRet = strRet + "C";
        }
        return strRet;
    }
    public static string FormatFlagCommission(object objFlagCommission)
    {
        string strRet = "ไม่คำนวณ";
        if (Convert.IsDBNull(objFlagCommission))
            strRet = "ไม่คำนวณ";
        else if (objFlagCommission.ToString() == "Y")
            strRet = "คำนวณ";

        return strRet;
    }
    public static string ShowBooleanValue(object objBooleanValue)
    {
        string strRet = "Yes";
        if (Convert.IsDBNull(objBooleanValue))
            strRet = "No";
        else if (objBooleanValue.ToString() == "N")
            strRet = "No";


        return strRet;
    }
    public static string ShowColumnType(object objColumnTypeValue)
    {
        DataSet ds = getAllDataType();
        DataRow[] drs;
        string strSelect = "";
        string strRet = "";
        strSelect = "DTY_ID = '" + objColumnTypeValue.ToString() + "' ";
        drs = ds.Tables[0].Select(strSelect, "");
        if (drs.Length > 0)
        {
            strRet = drs[0]["DTY_NAME"].ToString();
        }
        return strRet;
    }
    

    public static string getStatusName(string strStatus)
    {
        string strRet = "Active";
        if (strStatus == "C")
            strRet = "Inactive";
        
        return strRet;
    }
    public static string getStatusName(object strStatus)
    {       
        return getStatusName(strStatus.ToString());
    }
    public static string getStatusName2(object strStatus)
    {
        string strRet = "ใช้งาน";
        if (strStatus.ToString() == "C")
            strRet = "ยกเลิก";

        return strRet;
    }
    public static string getPaymentName(object strStatus)
    {
        string strRet = "เงินสด";
        strRet = getPaymentName(strStatus.ToString());
        return strRet;
    }
    public static string getPaymentName(string strStatus)
    {
        string strRet = "เงินสด";
        if (strStatus == "CA")
            strRet = "เงินสด";
        else if (strStatus == "CH")
            strRet = "เช็ค";
        else if (strStatus == "TR")
            strRet = "เงินโอน";
        else if (strStatus.ToString() == "CR")
            strRet = "บัตรเครดิต";
        else if (strStatus.ToString() == "SO")
            strRet = "เบี้ยจ่ายเกิน";

        return strRet;
    }
    public static string getPercentTypeName(object strType)
    {
        string strRet = "";
        if (strType.ToString() == "1")
            strRet = "ส่วนลดลูกค้า";
        else if (strType.ToString() == "2")
            strRet = "ค่านายหน้ารับ";
        else if (strType.ToString() == "3")
            strRet = "ค่านายหน้าจ่าย";

        return strRet;
    }
    public static string getSaleComTypeName(object strType)
    {
        string strRet = "";
        if (strType.ToString() == "1")
            strRet = "ค่านายหน้าจ่าย";
        else if (strType.ToString() == "2")
            strRet = "ค่าส่งเสริมการขาย";

        return strRet;
    }
    public static DataSet getMonthName()
    {
        DataSet dsRet = new DataSet();
        dsRet.Tables.Add("");
        dsRet.Tables[0].Columns.Add("month_name");
        dsRet.Tables[0].Columns.Add("month_id");

        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();        
        dsRet.Tables[0].Rows.Add(dsRet.Tables[0].NewRow());
        dsRet.Tables[0].Rows[dsRet.Tables[0].Rows.Count - 1]["month_name"] = "มกราคม";
        dsRet.Tables[0].Rows[dsRet.Tables[0].Rows.Count - 1]["month_id"] = "1";

        dsRet.Tables[0].Rows.Add(dsRet.Tables[0].NewRow());
        dsRet.Tables[0].Rows[dsRet.Tables[0].Rows.Count - 1]["month_name"] = "กุมภาพันธ์";
        dsRet.Tables[0].Rows[dsRet.Tables[0].Rows.Count - 1]["month_id"] = "2";

        dsRet.Tables[0].Rows.Add(dsRet.Tables[0].NewRow());
        dsRet.Tables[0].Rows[dsRet.Tables[0].Rows.Count - 1]["month_name"] = "มีนาคม";
        dsRet.Tables[0].Rows[dsRet.Tables[0].Rows.Count - 1]["month_id"] = "3";

        dsRet.Tables[0].Rows.Add(dsRet.Tables[0].NewRow());
        dsRet.Tables[0].Rows[dsRet.Tables[0].Rows.Count - 1]["month_name"] = "เมษายน";
        dsRet.Tables[0].Rows[dsRet.Tables[0].Rows.Count - 1]["month_id"] = "4";

        dsRet.Tables[0].Rows.Add(dsRet.Tables[0].NewRow());
        dsRet.Tables[0].Rows[dsRet.Tables[0].Rows.Count - 1]["month_name"] = "พฤษภาคม";
        dsRet.Tables[0].Rows[dsRet.Tables[0].Rows.Count - 1]["month_id"] = "5";

        dsRet.Tables[0].Rows.Add(dsRet.Tables[0].NewRow());
        dsRet.Tables[0].Rows[dsRet.Tables[0].Rows.Count - 1]["month_name"] = "มิถุนายน";
        dsRet.Tables[0].Rows[dsRet.Tables[0].Rows.Count - 1]["month_id"] = "6";

        dsRet.Tables[0].Rows.Add(dsRet.Tables[0].NewRow());
        dsRet.Tables[0].Rows[dsRet.Tables[0].Rows.Count - 1]["month_name"] = "กรกฎาคม";
        dsRet.Tables[0].Rows[dsRet.Tables[0].Rows.Count - 1]["month_id"] = "7";

        dsRet.Tables[0].Rows.Add(dsRet.Tables[0].NewRow());
        dsRet.Tables[0].Rows[dsRet.Tables[0].Rows.Count - 1]["month_name"] = "สิงหาคม";
        dsRet.Tables[0].Rows[dsRet.Tables[0].Rows.Count - 1]["month_id"] = "8";

        dsRet.Tables[0].Rows.Add(dsRet.Tables[0].NewRow());
        dsRet.Tables[0].Rows[dsRet.Tables[0].Rows.Count - 1]["month_name"] = "กันยายน";
        dsRet.Tables[0].Rows[dsRet.Tables[0].Rows.Count - 1]["month_id"] = "9";

        dsRet.Tables[0].Rows.Add(dsRet.Tables[0].NewRow());
        dsRet.Tables[0].Rows[dsRet.Tables[0].Rows.Count - 1]["month_name"] = "ตุลาคม";
        dsRet.Tables[0].Rows[dsRet.Tables[0].Rows.Count - 1]["month_id"] = "10";

        dsRet.Tables[0].Rows.Add(dsRet.Tables[0].NewRow());
        dsRet.Tables[0].Rows[dsRet.Tables[0].Rows.Count - 1]["month_name"] = "พฤศจิกายน";
        dsRet.Tables[0].Rows[dsRet.Tables[0].Rows.Count - 1]["month_id"] = "11";

        dsRet.Tables[0].Rows.Add(dsRet.Tables[0].NewRow());
        dsRet.Tables[0].Rows[dsRet.Tables[0].Rows.Count - 1]["month_name"] = "ธันวาคม";
        dsRet.Tables[0].Rows[dsRet.Tables[0].Rows.Count - 1]["month_id"] = "12";

        return dsRet;
    }
    public static DataSet getAllDataStatus()
    {
        DataSet dsRet = new DataSet();
        dsRet.Tables.Add("");
        dsRet.Tables[0].Columns.Add("status_name");
        dsRet.Tables[0].Columns.Add("status_code");

        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();
        dsRet.Tables[0].Rows.Add(dsRet.Tables[0].NewRow());
        dsRet.Tables[0].Rows[dsRet.Tables[0].Rows.Count - 1]["status_name"] = "Active";
        dsRet.Tables[0].Rows[dsRet.Tables[0].Rows.Count - 1]["status_code"] = "A";

        dsRet.Tables[0].Rows.Add(dsRet.Tables[0].NewRow());
        dsRet.Tables[0].Rows[dsRet.Tables[0].Rows.Count - 1]["status_name"] = "Inactive";
        dsRet.Tables[0].Rows[dsRet.Tables[0].Rows.Count - 1]["status_code"] = "C";

        return dsRet;
    }
    public static DataSet getAllDataType()
    {
        DataSet dsRet = new DataSet();
        dsRet.Tables.Add("");
        dsRet.Tables[0].Columns.Add("DTY_ID");
        dsRet.Tables[0].Columns.Add("DTY_NAME");

        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();
        dsRet.Tables[0].Rows.Add(dsRet.Tables[0].NewRow());
        dsRet.Tables[0].Rows[dsRet.Tables[0].Rows.Count - 1]["DTY_ID"] = "001";
        dsRet.Tables[0].Rows[dsRet.Tables[0].Rows.Count - 1]["DTY_NAME"] = "Varchar";

        dsRet.Tables[0].Rows.Add(dsRet.Tables[0].NewRow());
        dsRet.Tables[0].Rows[dsRet.Tables[0].Rows.Count - 1]["DTY_ID"] = "002";
        dsRet.Tables[0].Rows[dsRet.Tables[0].Rows.Count - 1]["DTY_NAME"] = "Char";

        dsRet.Tables[0].Rows.Add(dsRet.Tables[0].NewRow());
        dsRet.Tables[0].Rows[dsRet.Tables[0].Rows.Count - 1]["DTY_ID"] = "003";
        dsRet.Tables[0].Rows[dsRet.Tables[0].Rows.Count - 1]["DTY_NAME"] = "Integer";

        dsRet.Tables[0].Rows.Add(dsRet.Tables[0].NewRow());
        dsRet.Tables[0].Rows[dsRet.Tables[0].Rows.Count - 1]["DTY_ID"] = "004";
        dsRet.Tables[0].Rows[dsRet.Tables[0].Rows.Count - 1]["DTY_NAME"] = "Float";

        return dsRet;
    }
    public static string getDbTypeFromCode(string strCode)
    {
        DataSet ds = new DataSet();
        DataRow[] drs;
        string strSelect = "";
        string strRet = "";
        ds = getAllDataType();
        strSelect = "DTY_ID = '" + strCode + "' ";
        drs = ds.Tables[0].Select(strSelect);
        if (drs.Length > 0)
            strRet = drs[0]["DTY_NAME"].ToString();
        return strRet;
    }
    public static DataSet getAllDataLevel()
    {
        DataSet dsRet = new DataSet();
        dsRet.Tables.Add("");
        dsRet.Tables[0].Columns.Add("level_name");
        dsRet.Tables[0].Columns.Add("level_code");

        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();
        dsRet.Tables[0].Rows.Add(dsRet.Tables[0].NewRow());
        dsRet.Tables[0].Rows[dsRet.Tables[0].Rows.Count - 1]["level_name"] = "พนักงาน";
        dsRet.Tables[0].Rows[dsRet.Tables[0].Rows.Count - 1]["level_code"] = "01";

        dsRet.Tables[0].Rows.Add(dsRet.Tables[0].NewRow());
        dsRet.Tables[0].Rows[dsRet.Tables[0].Rows.Count - 1]["level_name"] = "หัวหน้า";
        dsRet.Tables[0].Rows[dsRet.Tables[0].Rows.Count - 1]["level_code"] = "02";

        return dsRet;
    }

    public static bool isNumeric(string val, System.Globalization.NumberStyles NumberStyle)
    {
        Double result;
        return Double.TryParse(val, NumberStyle,
            System.Globalization.CultureInfo.CurrentCulture, out result);
    }
    public static string replaceWhereValue(string strWhere, string strCompanyCode, string strDivisionCode)
    {
        strWhere = strWhere.ToLower().Replace("$company_code", "'" + strCompanyCode + "'");
        strWhere = strWhere.ToLower().Replace("$division_code", "'" + strDivisionCode + "'");
        while (strWhere.IndexOf("#") > 0)
        {
            int i = strWhere.IndexOf("#");
            string strPrev = strWhere.Substring(0,i);
            string strNext = strWhere.Substring(i);
            if (strPrev.ToUpper().LastIndexOf("AND ") > 0)
                strWhere = strPrev.Substring(0, strPrev.ToUpper().LastIndexOf("AND "));
            else
                strWhere = "";
            if (strNext.ToUpper().IndexOf("AND ", 0) > 0)
                strWhere += " " + strNext.Substring(strNext.ToUpper().IndexOf("AND ", 0));

        }
        return strWhere;
    }
    public static string getFileVariable(string strWhere)
    {
        string strRet = "";
        int iStart = 0;
        int iEnd = 0;
        bool isFound = false;
        for (int i = 0; i <= strWhere.Length - 3; i++)
        {
            if (strWhere.Substring(i, 1) == "#")
            {
                iStart = i;
                isFound = true;
            }
            else if (strWhere.Substring(i, 3).ToUpper() == "AND")            
                iEnd = i;

            if ( (iEnd > iStart) && isFound )
            {
                strRet += ", " + strWhere.Substring(iStart + 1, iEnd - iStart - 2);
                isFound = false;
                iStart = 0;
                iEnd = 0;
            }
            else if (isFound && i == strWhere.Length-3)
            {
                strRet += ", " + strWhere.Substring(iStart + 1, strWhere.Length-iStart-1) ;
            }
        }
        return strRet;
    }

    // write file
    public static string FormatFileMoney(object dataMoney)
    {
        //double dTmp = 0;
        //if (dataMoney != null && !Convert.IsDBNull(dataMoney))
        //    dTmp = Convert.ToDouble(dataMoney);
        //else
        //    dTmp = 0;
        //return dTmp.ToString("00000000.00");
        return FormatMoney(dataMoney);
    }
    public static ProfileData getLoginSession(System.Web.UI.Page objPage, System.Web.SessionState.HttpSessionState session)
    {
        ProfileData loginData;
        string strAppName = System.Configuration.ConfigurationManager.AppSettings["appName"];
        if (session["LOGINDATA"] == null)
        {
            //objPage.Response.Redirect("\\" + strAppName + "/Login.aspx");
            //loginData = new ProfileData();

            // For Test
            DataSet ds;
            DataSet dsMenu;
            DataSet dsTmp;
            DataSet dsBranch;
            string[] arrBranch;
            string menuList = "";
            string strRet = "";
            MasterDataManager cm = new MasterDataManager();

            loginData = new ProfileData();
            ds = cm.getDataUserByLogin("somza");
            loginData = new ProfileData();
            if (ds.Tables[0].Rows.Count > 0)
            {
                loginData.loginName = ds.Tables[0].Rows[0]["user_login"].ToString();
                loginData.userName = ds.Tables[0].Rows[0]["user_fullname"].ToString();
                loginData.roleID = ds.Tables[0].Rows[0]["role_id"].ToString();
                loginData.roleName = ds.Tables[0].Rows[0]["role_name"].ToString();
                loginData.teamCode = ds.Tables[0].Rows[0]["team_id"].ToString();
                loginData.teamName = ds.Tables[0].Rows[0]["team_name"].ToString();
                loginData.level = ds.Tables[0].Rows[0]["user_level"].ToString();

                // branch


                //loginData.userBranch = ds.Tables[0].Rows[0]["user_branch"].ToString();
                //dsTmp = cm.getDataLevelByID(loginData.levelID);
                //if (dsTmp.Tables[0].Rows.Count > 0)
                //    loginData.levelName = dsTmp.Tables[0].Rows[0]["level_name"].ToString();

                // role_menu
                dsMenu = cm.getDataMenuByRoleID(loginData.roleID);
                for (int i = 0; i <= dsMenu.Tables[0].Rows.Count - 1; i++)
                {
                    menuList += dsMenu.Tables[0].Rows[i]["menu_id"].ToString() + ", ";
                }
                // user_menu
                dsMenu = cm.getDataMenuByuserID(loginData.loginName);
                for (int i = 0; i <= dsMenu.Tables[0].Rows.Count - 1; i++)
                {
                    if (menuList.IndexOf(dsMenu.Tables[0].Rows[i]["menu_id"].ToString()) == -1)
                        menuList += dsMenu.Tables[0].Rows[i]["menu_id"].ToString() + ", ";
                }

                if (strRet.EndsWith(", "))
                    menuList = strRet.Substring(0, strRet.Length - 2);
                // loginData.menuList = menuList;
                //Session.Add("LoginData", loginData);
                session["LOGINDATA"] = loginData;

                //Response.Redirect("../" + strAppName + "/module/MasterData/MainPage.aspx");

            }
        }
        else
        {
            loginData = (ProfileData)session["LOGINDATA"];
        }
        return loginData;
    }

    public static DataSet getAllDataDropdownListType()
    {
        DataSet dsRet = new DataSet();
        dsRet.Tables.Add("");
        dsRet.Tables[0].Columns.Add("code");
        dsRet.Tables[0].Columns.Add("text");

        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();
        dsRet.Tables[0].Rows.Add(dsRet.Tables[0].NewRow());
        dsRet.Tables[0].Rows[dsRet.Tables[0].Rows.Count - 1]["code"] = "potype";
        dsRet.Tables[0].Rows[dsRet.Tables[0].Rows.Count - 1]["text"] = "POType";

        dsRet.Tables[0].Rows.Add(dsRet.Tables[0].NewRow());
        dsRet.Tables[0].Rows[dsRet.Tables[0].Rows.Count - 1]["code"] = "warehouse";
        dsRet.Tables[0].Rows[dsRet.Tables[0].Rows.Count - 1]["text"] = "Warehouse";

        dsRet.Tables[0].Rows.Add(dsRet.Tables[0].NewRow());
        dsRet.Tables[0].Rows[dsRet.Tables[0].Rows.Count - 1]["code"] = "costcenter";
        dsRet.Tables[0].Rows[dsRet.Tables[0].Rows.Count - 1]["text"] = "CostCenter";

        dsRet.Tables[0].Rows.Add(dsRet.Tables[0].NewRow());
        dsRet.Tables[0].Rows[dsRet.Tables[0].Rows.Count - 1]["code"] = "cotype";
        dsRet.Tables[0].Rows[dsRet.Tables[0].Rows.Count - 1]["text"] = "COType";

        dsRet.Tables[0].Rows.Add(dsRet.Tables[0].NewRow());
        dsRet.Tables[0].Rows[dsRet.Tables[0].Rows.Count - 1]["code"] = "budgettype";
        dsRet.Tables[0].Rows[dsRet.Tables[0].Rows.Count - 1]["text"] = "BudgetType";

        dsRet.Tables[0].Rows.Add(dsRet.Tables[0].NewRow());
        dsRet.Tables[0].Rows[dsRet.Tables[0].Rows.Count - 1]["code"] = "adjusttype";
        dsRet.Tables[0].Rows[dsRet.Tables[0].Rows.Count - 1]["text"] = "AdjustType";

        return dsRet;
    }
    public static DataSet getAllDataIsActive()
    {
        DataSet dsRet = new DataSet();
        dsRet.Tables.Add("");
        dsRet.Tables[0].Columns.Add("IsActive_name");
        dsRet.Tables[0].Columns.Add("IsActive_code");

        TDS.Utility.DateUtil cmDate = new TDS.Utility.DateUtil();
        dsRet.Tables[0].Rows.Add(dsRet.Tables[0].NewRow());
        dsRet.Tables[0].Rows[dsRet.Tables[0].Rows.Count - 1]["IsActive_name"] = "True";
        dsRet.Tables[0].Rows[dsRet.Tables[0].Rows.Count - 1]["IsActive_code"] = "True";

        dsRet.Tables[0].Rows.Add(dsRet.Tables[0].NewRow());
        dsRet.Tables[0].Rows[dsRet.Tables[0].Rows.Count - 1]["IsActive_name"] = "False";
        dsRet.Tables[0].Rows[dsRet.Tables[0].Rows.Count - 1]["IsActive_code"] = "False";

        return dsRet;
    }
}
