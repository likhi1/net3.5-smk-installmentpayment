﻿<%@ Page Language="C#" MasterPageFile="~/MasterData/MainMasterPage.master" AutoEventWireup="true" CodeFile="SendDocList.aspx.cs" Inherits="Module_Settlement_SendDocList" Title="Untitled Page" %>

<%@ Register Src="../../UserControl/ucPageNavigator.ascx" TagName="ucPageNavigator" TagPrefix="uc1" %>
<%@ Register Src="../../UserControl/ucTxtDate.ascx" TagName="ucTxtDate" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
    <script language="javascript">
        function goPage(strPage) {
            window.location = strPage;
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-inside">
        <table width="100%">
            <tr>
                <td>
                    <div class="heading1">:: ข้อมูลจัดส่งเอกสาร ::</div>
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                <asp:Button ID="btnSearch" runat="server" Text="ค้นหา" CssClass="awesome"
                                    OnClick="btnSearch_Click" />
                            </td>
                            <td>
                                <asp:Button ID="btnClear" runat="server" Text="เคลียร์" CssClass="awesome" OnClientClick="return goPage('../../Module/Settlement/SendDocList.aspx');" />
                            </td>
                            <td>
                                <asp:Button ID="btnExit" runat="server" Text="ออก" CssClass="awesome" OnClientClick="return goPage('../../Module/MasterData/MainPage.aspx');" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <fieldset>
                        <legend>เงื่อนไขการค้นหา</legend>
                        <table width="100%">
                            <tr>
                                <td align="right">เลขกรมธรรม์ :</td>
                                <td align="left">
                                    <asp:TextBox ID="txtPolicyNo" runat="server" CssClass="TEXTBOX" Width="100"></asp:TextBox>
                                </td>
                                <td align="right">ชื่อผู้เอาประกัน :</td>
                                <td align="left">
                                    <asp:TextBox ID="txtInsurerName" runat="server" CssClass="TEXTBOX" Width="100"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">สาขา :</td>
                                <td align="left">
                                    <asp:DropDownList ID="ddlBranch" runat="server">
                                    </asp:DropDownList>
                                </td>
                                <td align="right">รหัสตัวแทน :</td>
                                <td align="left">
                                    <asp:TextBox ID="txtAgentCode" runat="server" CssClass="TEXTBOX" Width="100"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">วันเริ่มคุ้มครองตั้งแต่ :</td>
                                <td align="left">
                                    <uc1:ucTxtDate ID="txtPeriodFromDate1" runat="server" />
                                </td>
                                <td align="right">ถึงวันที่ :</td>
                                <td align="left">
                                    <uc1:ucTxtDate ID="txtPeriodFromDate2" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
            <tr>
                <td>
                    <fieldset>
                        <legend>ผลการค้นหา</legend>
                        <table width="100%">
                            <tr>
                                <td>
                                    <uc1:ucPageNavigator ID="ucPageNavigator1" runat="server" OnPageIndexChanged="PageIndexChanged" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:DataGrid ID="dtgData" runat="server" AutoGenerateColumns="false" Width="100%"
                                        HeaderStyle-CssClass="gridhead" ItemStyle-CssClass="gridrow0" AlternatingItemStyle-CssClass="gridrow1"
                                        AllowPaging="true" PagerStyle-Visible="false">
                                        <Columns>
                                            <asp:TemplateColumn>
                                                <HeaderTemplate>
                                                    <%#ColumnTitle[0]%>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <%# Container.DataSetIndex +1%>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>

                                            <asp:TemplateColumn>
                                                <HeaderTemplate>
                                                    <asp:LinkButton ID="lnkID1" runat="server" OnClick="sortColumn" Text="<%#ColumnTitle[1]%>">
                                                    </asp:LinkButton>
                                                    <asp:Image ID="imgSortCol1" runat="server" ImageUrl="<%#getPicUrl(1)%>"></asp:Image>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                          <a href="#" onclick="goPage('../../Module/Settlement/SendDocForm.aspx?id=<%#(Eval(ColumnName[1]))%>');">
						                            <%#(Eval(ColumnName[1]))%>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>

                                            <asp:TemplateColumn>
                                                <HeaderTemplate>
                                                    <asp:LinkButton ID="lnkID2" runat="server" OnClick="sortColumn" Text="<%#ColumnTitle[2]%>">
                                                    </asp:LinkButton>
                                                    <asp:Image ID="imgSortCol2" runat="server" ImageUrl="<%#getPicUrl(2)%>"></asp:Image>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <%#Eval(ColumnName[2])%>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>


                                            <asp:TemplateColumn>
                                                <HeaderTemplate>
                                     <asp:LinkButton ID="lnkID3" runat="server" OnClick="sortColumn" Text="<%#ColumnTitle[3]%>">
                                                    </asp:LinkButton>
                                                    <asp:Image ID="imgSortCol3" runat="server" ImageUrl="<%#getPicUrl(3)%>"></asp:Image>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                           <%#FormatStringApp.showRangeDate(Eval(ColumnName[3]), Eval(ColumnName[8]))%>
                                                     
                                                </ItemTemplate>
                                            </asp:TemplateColumn>

                                             
                                            <asp:TemplateColumn>
                                                <HeaderTemplate>
                                                    <asp:LinkButton ID="lnkID4" runat="server" OnClick="sortColumn" Text="<%#ColumnTitle[4]%>">
                                                    </asp:LinkButton>
                                                    <asp:Image ID="imgSortCol4" runat="server" ImageUrl="<%#getPicUrl(4)%>"></asp:Image>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                           <%#FormatStringApp.FormatDate(Eval(ColumnName[4])) %> 

                                                </ItemTemplate>
                                            </asp:TemplateColumn>

                                            <asp:TemplateColumn>
                                                <HeaderTemplate>
                                                    <asp:LinkButton ID="lnkID5" runat="server" OnClick="sortColumn" Text="<%#ColumnTitle[5]%>">
                                                    </asp:LinkButton>
                                                    <asp:Image ID="imgSortCol5" runat="server" ImageUrl="<%#getPicUrl(5)%>"></asp:Image>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                              <%--      <%#FormatStringApp.FormatMoney(Eval(ColumnName[5]))%>--%>
                                                           <%#Eval(ColumnName[5])%>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>


                                            <asp:TemplateColumn>
                                                <HeaderTemplate>
                                                    <asp:LinkButton ID="lnkID6" runat="server" OnClick="sortColumn" Text="<%#ColumnTitle[6]%>">
                                                    </asp:LinkButton>
                                                    <asp:Image ID="imgSortCol6" runat="server" ImageUrl="<%#getPicUrl(6)%>"></asp:Image>
                                                </HeaderTemplate>
                                                <ItemTemplate> 
                                                 

                                                        <%--        <%#FormatStringApp.FormatMoney(Eval(ColumnName[4]))%>--%>
                                                           <%#Eval(ColumnName[6])%>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>



                                            <asp:TemplateColumn>
                                                <HeaderTemplate>
                                                    <asp:LinkButton ID="lnkID7" runat="server" OnClick="sortColumn" Text="<%#ColumnTitle[7]%>">
                                                    </asp:LinkButton>
                                                    <asp:Image ID="imgSortCol7" runat="server" ImageUrl="<%#getPicUrl(7)%>"></asp:Image>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                 
						                             <%#Eval(ColumnName[7])%>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                        </Columns>
                                    </asp:DataGrid>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
            <tr>
                <td></td>
            </tr>
            <tr>
                <td>
                    <asp:Literal ID="litJavaScript" runat="server"></asp:Literal>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>

