﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
//////////////////////////////////////// 
using System.Collections.Generic;

public partial class Module_Settlement_SendDocForm : System.Web.UI.Page
{
      
    public string[] ColumnName = { "inst_no", "inst_duedate", "inst_rate", "inst_net", "inst_paid", "inst_rvno", "inst_duedatedesc" };
    public string[] ColumnType = { "string", "DateTime", "double", "double", "double", "string", "string" };
    public string[] ColumnTitle = { "งวดที่", "วันครบกำหนด", "%", "ยอดผ่อน", "ยอดที่ชำระ", "เลขที่ใบนำส่ง", "ลบ" };
    string[] ColumnWidth = { "10%", "15%", "15%", "20%", "20%", "10%", "10%" };
    string[] ColumnAlign = { "center", "center", "right", "right", "right", "center", "center" };

    public string[] ColumnNameRV = { "rv_no", "rv_type", "rv_desc", "rv_net", "" };
    public string[] ColumnTypeRV = { "string", "string", "string", "string", "string" };
    public string[] ColumnTitleRV = { "ลำดับที่", "ประเภท", "รายละเอียด", "จำนวนเงิน", "แก้ไข/ลบ" };
    string[] ColumnWidthRV = { "5%", "15%", "60%", "10%", "10%" };
    string[] ColumnAlignRV = { "center", "left", "left", "right", "center" };

    DataView dv = new DataView();

    protected void Page_Load(object sender, EventArgs e)  {
        ProfileData loginData = FormatStringApp.getLoginSession(Page, Session);
         
        SendDocManager cm = new SendDocManager();
        DataSet ds = new DataSet();
        DataSet dsAuth = new DataSet();
        if (!IsPostBack)
        {
           // initialDropDownList();
            initialDataGrid();

            //\\\\\\\\\ Check user role from menu_id  
            MasterDataManager masterDataManager = new MasterDataManager();
            RoleMenu rm = new RoleMenu
            {
                menu_id = "006", // Set my menu ID  
                roleID = loginData.roleID , // Set Null for don't send paramiter
                loginName =   loginData.loginName // Set Null for don't send paramiter
                
            };
            //List<string> roleMenu = masterDataManager.CheckRoleMenu(loginData.roleID, loginData.loginName , menu_id );
            List<string> roleMenu = masterDataManager.CheckRoleMenu(rm);
            if (roleMenu.Contains("E") )
            { // Set Enable Edit Data
                txtMode.Text = "E";
            } else { // Set Enable Just Query 
                txtMode.Text = "";
            }
             

            if (Request.QueryString["id"] != null)
            {
                txtMode.Text = "E";
                lblPolicyNo.Text = Request.QueryString["id"];
                ds = cm.getDataSendDocByPolNo(lblPolicyNo.Text);
                docToUI(ds);
                setViewStage(ds); // Keep data set to ViewStage
               // btnSearch_Click(null, null);
            }
            setMode();
        }
        else
        {
            //setValueDiabledControl();
            litJavaScript.Text = "";
        }
    }
     
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        
        //set Data
        // policy data

        /////////////////////////////
        //lblPolicyNo.Text = "1234-222-394850";
        //litJavaScript.Text += "<script>document.getElementById('divPolicyRemark1').innerHTML = 'ทะเบียนรถ :';</script>\n";
        //lblRemark1.Text = "กก 1234 กท";
        //lblIssueDate.Text = "10/09/2014";
        //lblBranchName.Text = "100 : สำนักงานใหญ่";
        //lblAgentName.Text = "010005 : xxxxxxxxxx";
        //lblPeriodFromTo.Text = "01/10/2014 - 01/10/2015";
        //lblInsurerName.Text = "xxxxx xxxxxxxxxxxx";
        //lblPhoneNo.Text = "02-222-2222, 02-333-3333, 02-444-4444";
        //lblPolicyPremium.Text = "19,383.20";
        //lblPolicyNet.Text = "20,492.00";
        //lblCompulNo.Text = "1234-111-394490";
        //lblCompulPremium.Text = "601.25";
        //lblCompulNet.Text = "645.21";
        //lblChasis.Text = ""; 
        //// senddoc data
        //txtContactName.Text = lblInsurerName.Text;
        //txtContactPhone.Text = lblPhoneNo.Text;
        //txtContactAddr1.Text = "88 อาคารปาโซ่ ทาวเวอร์ ถนนสีลม";
        //txtContactAddr2.Text = "แขวงสุริยวงษ์ เขตบางรัก ";
        //txtContactPostCode.Text = "10500";

    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string strRet = "";
        bool isRet = true;
        DataSet ds;
        ProfileData loginData = FormatStringApp.getLoginSession(Page, Session);
        SendDocManager cmSettle = new SendDocManager();
        MasterDataManager cm = new MasterDataManager();
        litJavaScript.Text += "<script>";
        ds = UIToDoc();
        if (txtMode.Text == "A")
        {
            strRet = cmSettle.addDataSendDoc(ref ds);
            if (strRet.StartsWith("ERROR"))
            {
                litJavaScript.Text += "alert('" + cm.getDataErrorByPageNameNo("", "002") + "');";
            }
            else
            {
                litJavaScript.Text += "alert('" + cm.getDataErrorByPageNameNo("", "001") + "');";
                txtMode.Text = "V";
            }
        }
        litJavaScript.Text += "</script>";
        //  docToUI(ds);
        setMode();
    }

    public void initialDropDownList()
    {
        MasterDataManager cm = new MasterDataManager();
        DataSet ds;

        //ds = cm.getAllDataBranch();
        //ddlContactProvince.DataSource = ds;
        //ddlContactProvince.DataTextField = "branch_name";
        //ddlContactProvince.DataValueField = "branch_code";
        //ddlContactProvince.DataBind();
        //ddlContactProvince.Items.Insert(0, new ListItem("-- เลือก -- ", ""));

    }
    public void initialDataGrid()
    {

    }
   
    protected void setViewStage(DataSet ds)
    {
        ViewState["sync_policy"] = ds;

    }

    //================= For Convert  Dataset to Control
    public void docToUI(DataSet dsData)
    {
        if (dsData.Tables[0].Rows.Count > 0)
        {
            litJavaScript.Text += "<script>"; 

            lblIssueDate.Text = FormatStringApp.FormatDate(dsData.Tables[0].Rows[0]["spol_receivedate"].ToString());
            lblBranchName.Text = dsData.Tables[0].Rows[0]["spol_branch"].ToString();
            lblAgentName.Text = dsData.Tables[0].Rows[0]["spol_agentcode"].ToString();
            lblRemark1.Text = dsData.Tables[0].Rows[0]["spol_remark1"].ToString();
            lblRemark2.Text = dsData.Tables[0].Rows[0]["Spol_remark2"].ToString();
            lblPeriodFromTo.Text = FormatStringApp.showRangeDate(dsData.Tables[0].Rows[0]["spol_effectivefromdate"], dsData.Tables[0].Rows[0]["spol_effectivetodate"]);

            lblInsurerName.Text = dsData.Tables[0].Rows[0]["spol_custname"].ToString();
            lblPhoneNo.Text = dsData.Tables[0].Rows[0]["spol_custphoneno"].ToString();
            lblPolicyPremium.Text = dsData.Tables[0].Rows[0]["spol_grosspremium"].ToString();
            lblPolicyNet.Text = dsData.Tables[0].Rows[0]["spol_netpremium"].ToString();
            lblCompulNo.Text = dsData.Tables[0].Rows[0]["spol_compolicy"].ToString();
            lblCompulNet.Text = dsData.Tables[0].Rows[0]["spol_comnetpremium"].ToString();
            lblCompulPremium.Text = dsData.Tables[0].Rows[0]["spol_comgrosspremium"].ToString();
            lblCompulRegister.Text = dsData.Tables[0].Rows[0]["spol_comlicense"].ToString();
            lblCompulName.Text = dsData.Tables[0].Rows[0]["spol_comname"].ToString();
            lblCompulChasis.Text = dsData.Tables[0].Rows[0]["spol_comchasis"].ToString(); ///

            txtContactName.Text = dsData.Tables[0].Rows[0]["send_name"].ToString();
            txtContactPhone.Text = dsData.Tables[0].Rows[0]["send_phoneno"].ToString();
            txtContactAddr1.Text = dsData.Tables[0].Rows[0]["send_address1"].ToString();
            txtContactAddr2.Text = dsData.Tables[0].Rows[0]["send_address2"].ToString();
            txtContactPostCode.Text = dsData.Tables[0].Rows[0]["send_postcode"].ToString();

#region
            //txtUserName.Text = dsData.Tables[0].Rows[0]["user_login"].ToString();
            //ddlRole.SelectedValue = dsData.Tables[0].Rows[0]["ROLE_ID"].ToString();
            //txtFName.Text = dsData.Tables[0].Rows[0]["user_fname"].ToString();
            //txtLName.Text = dsData.Tables[0].Rows[0]["user_lname"].ToString();
            //txtEmail.Text = dsData.Tables[0].Rows[0]["user_email"].ToString();
            //ddlStatus.SelectedValue = dsData.Tables[0].Rows[0]["user_status"].ToString();
            //if (!Convert.IsDBNull(dsData.Tables[0].Rows[0]["user_lastlogindate"]))
            //    txtLastLogin.Text = TDS.Utility.MasterUtil.getDateString((DateTime)dsData.Tables[0].Rows[0]["user_lastlogindate"], "dd/MM/yyyy");
            //if (!Convert.IsDBNull(dsData.Tables[0].Rows[0]["start_date"]))
            //    txtStartDate.Text = TDS.Utility.MasterUtil.getDateString((DateTime)dsData.Tables[0].Rows[0]["start_date"], "dd/MM/yyyy");
            //if (!Convert.IsDBNull(dsData.Tables[0].Rows[0]["stop_date"]))
            //    txtEndDate.Text = TDS.Utility.MasterUtil.getDateString((DateTime)dsData.Tables[0].Rows[0]["stop_date"], "dd/MM/yyyy");
            //txtInsertLogin.Text = dsData.Tables[0].Rows[0]["insert_name"].ToString();
            //txtInsertDate.Text = FormatStringApp.FormatDateTime(dsData.Tables[0].Rows[0]["insert_date"]);
            //if (!Convert.IsDBNull(dsData.Tables[0].Rows[0]["update_name"]))
            //{
            //    txtUpdateLogin.Text = dsData.Tables[0].Rows[0]["update_name"].ToString();
            //    txtUpdateDate.Text = FormatStringApp.FormatDateTime(dsData.Tables[0].Rows[0]["update_date"]);
            //}
            //ddlBranch.SelectedValue = dsData.Tables[0].Rows[0]["user_branch"].ToString();
#endregion
            litJavaScript.Text += "</script>\n";

            //ViewState.Add("DSMasterTable", dsAuth);
            //ShowData();
        }
    }
   
    
    //================= For Convert  Control to Dataset
    public DataSet UIToDoc()
    {
        DataSet dsData = new DataSet();
        DataTable dt = new DataTable();
        string[] ColumnType = {"string", "string", "string", "string", "string" };
        string[] ColumnName = { "send_name", "send_phoneno", "send_address1", "send_address2", "send_postcode" };
        ProfileData loginData = FormatStringApp.getLoginSession(Page, Session);
        dt = TDS.Utility.MasterUtil.AddColumnToDataTable(dt, ColumnType, ColumnName);
        dsData.Tables.Add(dt);
        dsData.Tables[0].Rows.Add(dsData.Tables[0].NewRow());

        dsData.Tables[0].Rows[0]["send_name"] = txtContactName.Text;
        dsData.Tables[0].Rows[0]["send_phoneno"] = txtContactPhone.Text;
        dsData.Tables[0].Rows[0]["send_address1"] = txtContactAddr1.Text;
        dsData.Tables[0].Rows[0]["send_address2"] = txtContactAddr2.Text;
        dsData.Tables[0].Rows[0]["send_postcode"] = txtContactPostCode.Text; 

  


        #region
        // string[] ColumnName = {"user_login", "user_fname", "user_lname", "user_email", "role_id", "user_status", "LOGINNAME","user_pwd","start_date", "stop_date", "user_branch"};
        //dsData.Tables[0].Rows[0]["user_login"] = txtUserName.Text;
        //dsData.Tables[0].Rows[0]["user_fname"] = txtFName.Text;
        //dsData.Tables[0].Rows[0]["user_lname"] = txtLName.Text;
        //dsData.Tables[0].Rows[0]["user_email"] = txtEmail.Text;
        //dsData.Tables[0].Rows[0]["role_id"] = ddlRole.SelectedValue;
        //dsData.Tables[0].Rows[0]["user_status"] = ddlStatus.SelectedValue;
        //dsData.Tables[0].Rows[0]["LOGINNAME"] = loginData.loginName;
        //if (txtMode.Text == "E")
        //    dsData.Tables[0].Rows[0]["user_pwd"] = hddPassword.Value;
        //else
        //    dsData.Tables[0].Rows[0]["user_pwd"] = txtPassword.Text;
        //dsData.Tables[0].Rows[0]["start_date"] = txtStartDate.Text;
        //dsData.Tables[0].Rows[0]["stop_date"] = txtEndDate.Text;
        //dsData.Tables[0].Rows[0]["user_branch"] = ddlBranch.SelectedValue;
        #endregion


        return dsData;
    }
    public void setMode()  {
        if (txtMode.Text == "A") {
            //txtUserName.CssClass = "TEXTBOX";
           //txtUserName.ReadOnly = false;
          // btnAddNew.Visible = false;
        } else if (txtMode.Text == "E")  { 
             btnSave.Visible = false;
             txtContactName.ReadOnly = true ;
             txtContactPhone.ReadOnly = true;
             txtContactAddr1.ReadOnly = true;
             txtContactAddr2.ReadOnly = true;
             txtContactPostCode.ReadOnly = true;
        }


    }
}
