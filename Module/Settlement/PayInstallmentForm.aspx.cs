﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Module_Settlement_PayInstallmentForm : System.Web.UI.Page
{
    public string[] ColumnName = { "inst_no", "inst_duedate", "inst_rate", "inst_net", "inst_paid", "inst_rvno", "inst_duedatedesc" };
    public string[] ColumnType = { "string", "DateTime", "double", "double", "double", "string", "string" };
    public string[] ColumnTitle = { "งวดที่", "วันครบกำหนด", "%", "ยอดผ่อน", "ยอดที่ชำระ", "เลขที่ใบนำส่ง", "ลบ" };
    string[] ColumnWidth = { "10%", "15%", "15%", "20%", "20%", "10%", "10%" };
    string[] ColumnAlign = { "center", "center", "right", "right", "right", "center", "center" };

    public string[] ColumnNameRV = { "rv_no", "rv_type", "rv_desc", "rv_net", "" };
    public string[] ColumnTypeRV = { "string", "string", "string", "double", "string" };
    public string[] ColumnTitleRV = { "ลำดับที่", "ประเภท", "รายละเอียด", "จำนวนเงิน", "แก้ไข/ลบ" };
    string[] ColumnWidthRV = { "5%", "15%", "60%", "10%", "10%" };
    string[] ColumnAlignRV = { "center", "left", "left", "right", "center" };

    DataView dv = new DataView();
    protected void Page_Load(object sender, EventArgs e)
    {
        ProfileData loginData = FormatStringApp.getLoginSession(Page, Session);
        SettlementManager cm = new SettlementManager();
        DataSet ds = new DataSet();
        DataSet dsAuth = new DataSet();
        if (!IsPostBack)
        {
            initialDropDownList();
            initialDataGrid();
            txtMode.Text = "A";
            lblSettlementDate.Text = FormatStringApp.FormatDate(DateTime.Today);
            txtInstallments.Text = ConfigurationManager.AppSettings["defaultInstallment"];
            lblMonths.Text = ConfigurationManager.AppSettings["defaultMonths"];
            lblDiscountRate.Text = "0";
            lblDiscount.Text = "0.00";
            if (Request.QueryString["payid"] != null)
            {
                txtMode.Text = "V";
                txtPayID.Text = Request.QueryString["payid"];
                ds = cm.getDataPayInstallmentByPayID(lblPolicyNo.Text);
                docToUI(ds);
            }
            setMode();
        }
        else
        {
            //setValueDiabledControl();
            litJavaScript.Text = "";
            litJavaScript.Text += "<script>\n";
            litJavaScript.Text += "     \tdocument.getElementById('divSearchPolicy').style.display = 'none';\n";
            litJavaScript.Text += "     \tdocument.getElementById('divPolicy').style.display = 'inline';\n";
            litJavaScript.Text += "     \tdocument.getElementById('divSendDoc').style.display = 'inline';\n";
            litJavaScript.Text += "     \tdocument.getElementById('divInstallments').style.display = 'inline';\n";
            litJavaScript.Text += "     \tdocument.getElementById('divRV').style.display = 'inline';";
            litJavaScript.Text += "</script>\n";
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        litJavaScript.Text += "<script>\n";
        litJavaScript.Text += "     \tdocument.getElementById('divSearchPolicy').style.display = 'none';\n";
        litJavaScript.Text += "     \tdocument.getElementById('divPolicy').style.display = 'inline';\n";
        litJavaScript.Text += "     \tdocument.getElementById('divSendDoc').style.display = 'inline';\n";
        litJavaScript.Text += "     \tdocument.getElementById('divInstallments').style.display = 'inline';\n";
        litJavaScript.Text += "     \tdocument.getElementById('divRV').style.display = 'inline';";
        litJavaScript.Text += "</script>\n";

        //set Data
        // policy data
        if (rdoMotor.Checked)
        {
            lblPolicyNo.Text = txtVolunNo.Text;
            litJavaScript.Text += "<script>document.getElementById('divPolicyRemark1').innerHTML = 'ทะเบียนรถ :';</script>\n";
            
        }
        else
        {
            lblPolicyNo.Text = txtNonMotorNo.Text;
            litJavaScript.Text += "<script>document.getElementById('divPolicyRemark1').innerHTML = 'หมายเหตุ1 :';</script>\n";
            
        }
        

        
    }    
    protected void btnSave_Click(object sender, EventArgs e)
    {

    }
    protected void btnAddInst_Click(object sender, EventArgs e)
    {
        DataSet ds = new DataSet();
        int i = 0;
        int iMonths = Convert.ToInt32(lblMonths.Text);
        DateTime dtDueDate;

        if (ViewState["DSMasterTableInstallments"] != null)
            ds = (DataSet)ViewState["DSMasterTableInstallments"];

        ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
        i = ds.Tables[0].Rows.Count - 1;


        dtDueDate = Convert.ToDateTime(ds.Tables[0].Rows[i - 1]["inst_duedate"]);
        dtDueDate = dtDueDate.AddMonths(iMonths);
        ds.Tables[0].Rows[i]["inst_no"] = Convert.ToString(i + 1);
        ds.Tables[0].Rows[i]["inst_duedate"] = dtDueDate;
        ds.Tables[0].Rows[i]["inst_duedatedesc"] = FormatStringApp.FormatDate(dtDueDate);
        ds.Tables[0].Rows[i]["inst_rate"] = "0";
        ds.Tables[0].Rows[i]["inst_net"] = "0.00";
        ds.Tables[0].Rows[i]["inst_paid"] = "0.00";
        ds.Tables[0].Rows[i]["inst_rvno"] = "";

        ViewState["DSMasterTableInstallments"] = ds;
        ShowDataInstallments();
        litJavaScript.Text += "<script>\n";
        litJavaScript.Text += "     \tdocument.getElementById('divSearchPolicy').style.display = 'none';\n";
        litJavaScript.Text += "     \tdocument.getElementById('divPolicy').style.display = 'inline';\n";
        litJavaScript.Text += "     \tdocument.getElementById('divSendDoc').style.display = 'inline';\n";
        litJavaScript.Text += "     \tdocument.getElementById('divInstallments').style.display = 'inline';\n";
        litJavaScript.Text += "     \tdocument.getElementById('divRV').style.display = 'inline';";
        litJavaScript.Text += "</script>\n";
    }
    protected void imgDeleteInst_Click(object sender, ImageClickEventArgs e)
    {
        DataSet ds = new DataSet();
        int i = 0;
        ImageButton imgDelete = (ImageButton)sender;
        if (ViewState["DSMasterTableInstallments"] != null)
            ds = (DataSet)ViewState["DSMasterTableInstallments"];

        i = Convert.ToInt32(imgDelete.Attributes["dsindex"]);
        ds.Tables[0].Rows.RemoveAt(i);
        for (int j = i; j <= ds.Tables[0].Rows.Count - 1; j++)
        {
            ds.Tables[0].Rows[j]["inst_no"] = Convert.ToString(j + 1);
        }
        ViewState["DSMasterTableInstallments"] = ds;
        ShowDataInstallments();
        litJavaScript.Text += "<script>\n";
        litJavaScript.Text += "     \tdocument.getElementById('divSearchPolicy').style.display = 'none';\n";
        litJavaScript.Text += "     \tdocument.getElementById('divPolicy').style.display = 'inline';\n";
        litJavaScript.Text += "     \tdocument.getElementById('divSendDoc').style.display = 'inline';\n";
        litJavaScript.Text += "     \tdocument.getElementById('divInstallments').style.display = 'inline';\n";
        litJavaScript.Text += "     \tdocument.getElementById('divRV').style.display = 'inline';";
        litJavaScript.Text += "</script>\n";

    }
    protected void btnAddRV_Click(object sender, EventArgs e)
    {

    }
    protected void imgEditRV_Click(object sender, ImageClickEventArgs e)
    {

    }
    protected void imgDeleteRV_Click(object sender, ImageClickEventArgs e)
    {

    }
    protected void dtgInstallments_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        TextBox txtDtgInstallmentsPaid;
        DataSet ds = new DataSet();

        if (e.Item.ItemType == ListItemType.Item ||
            e.Item.ItemType == ListItemType.AlternatingItem)
        {
            txtDtgInstallmentsPaid = (TextBox)e.Item.FindControl("txtPaid");
            if (e.Item.ItemIndex == 0)
            {
                txtDtgInstallmentsPaid.CssClass = "TEXTBOXMONEY";
                txtDtgInstallmentsPaid.ReadOnly = false;
            }
            else
            {
                txtDtgInstallmentsPaid.CssClass = "TEXTBOXMONEYDIS";
                txtDtgInstallmentsPaid.ReadOnly = true;
            }
        }
        else if (e.Item.ItemType == ListItemType.Footer)
        {
            if (ViewState["DSMasterTableInstallments"] != null)
            {
                ds = (DataSet)ViewState["DSMasterTableInstallments"];
                e.Item.Cells[3].Text = "<div id=\"divSumNet\" class=\"textBlue\">" + FormatStringApp.FormatMoney(ds.Tables[0].Compute("sum(inst_net)", "").ToString()) + "&nbsp;</div>";
                e.Item.Cells[3].HorizontalAlign = HorizontalAlign.Right;
                e.Item.Cells[3].VerticalAlign = VerticalAlign.Middle;
                e.Item.Cells[4].Text = "<div id=\"divSumPaid\" class=\"textBlue\">" + FormatStringApp.FormatMoney(ds.Tables[0].Compute("sum(inst_paid)", "").ToString()) + "&nbsp;</div>";
                e.Item.Cells[4].HorizontalAlign = HorizontalAlign.Right;
                e.Item.Cells[4].VerticalAlign = VerticalAlign.Middle;
            }

        }
    }
    protected void btnRVSave_Click(object sender, EventArgs e)
    {
        DataSet dsRV = new DataSet();
        if (ViewState["DSMasterTableRV"] != null)
            dsRV = (DataSet)ViewState["DSMasterTableRV"];

        dsRV.Tables[0].Rows.Add(dsRV.Tables[0].NewRow());
        dsRV.Tables[0].Rows[dsRV.Tables[0].Rows.Count - 1]["rv_type"] = "เช็ค";
        dsRV.Tables[0].Rows[dsRV.Tables[0].Rows.Count - 1]["rv_desc"] = "เลขที่เช็ค:12345  วันที่ในเช็ค:15/11/2014  ธนาคาร:กรุงศรีอยุธยา จำกัด(มหาชน)  สาขา:สีลม";
        dsRV.Tables[0].Rows[dsRV.Tables[0].Rows.Count - 1]["rv_net"] = "6,310.00";

        ViewState.Add("DSMasterTableRV", dsRV);
        ShowDataRV();
    }
    protected void dtgRV_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        TextBox txtDtgInstallmentsPaid;
        DataSet ds = new DataSet();

        if (e.Item.ItemType == ListItemType.Footer)
        {
            if (ViewState["DSMasterTableRV"] != null)
            {
                ds = (DataSet)ViewState["DSMasterTableRV"];
                //FormatStringApp.FormatMoney(ds.Tables[0].Compute("sum(rv_net)", "").ToString()) 
                e.Item.Cells[3].Text = "<div id=\"divSumRV\" class=\"textBlue\">6,310.00&nbsp;</div>";
                e.Item.Cells[3].HorizontalAlign = HorizontalAlign.Right;
                e.Item.Cells[3].VerticalAlign = VerticalAlign.Middle;
            }

        }
    }

    public void initialDropDownList()
    {
        MasterDataManager cm = new MasterDataManager();
        DataSet ds;

    }
    public void initialDataGrid()
    {
        DataSet ds = new DataSet();
        DataTable dt = new DataTable();
        dt = TDS.Utility.MasterUtil.AddColumnToDataTable(dt, ColumnType, ColumnName);
        ds.Tables.Add(dt);
        ViewState.Add("DSMasterTableInstallments", ds);
        ShowDataInstallments();
        GridViewUtil.setGridStyle(dtgInstallments, ColumnWidth, ColumnAlign, ColumnTitle);

        ds = new DataSet();
        dt = new DataTable();
        dt = TDS.Utility.MasterUtil.AddColumnToDataTable(dt, ColumnTitleRV, ColumnNameRV);
        ds.Tables.Add(dt);
        ViewState.Add("DSMasterTableRV", ds);
        ShowDataRV();
        GridViewUtil.setGridStyle(dtgRV, ColumnWidthRV, ColumnAlignRV, ColumnTitleRV);
    }
    public void docToUI(DataSet dsData)
    {
        //if (dsData.Tables[0].Rows.Count > 0)
        //{
        //    litJavaScript.Text += "<script>";
        //    lblPolicyNo.Text = dsData.Tables[0].Rows[0]["spol_policy"].ToString();
        //    txtPolicy.Text = dsData.Tables[0].Rows[0]["spol_policy"].ToString();
        //    lblRemark1.Text = dsData.Tables[0].Rows[0]["spol_remark1"].ToString();
        //    lblRemark2.Text = dsData.Tables[0].Rows[0]["spol_remark2"].ToString();
        //    if (dsData.Tables[0].Rows[0]["spol_type"].ToString() == "V")
        //    {
        //        litJavaScript.Text += "<script>document.getElementById('divPolicyRemark1').innerHTML = 'ทะเบียนรถ :';\n";
        //        litJavaScript.Text += "document.getElementById('divPolicyRemark2').innerHTML = 'chasis :';</script>\n";
        //    }
        //    else
        //    {
        //        litJavaScript.Text += "<script>document.getElementById('divPolicyRemark1').innerHTML = 'หมายเหตุ1 :';\n";
        //        litJavaScript.Text += "document.getElementById('divPolicyRemark2').innerHTML = '';</script>\n";
        //    }
        //    lblIssueDate.Text = FormatStringApp.FormatDate(dsData.Tables[0].Rows[0]["spol_receivedate"].ToString());
        //    lblBranchName.Text = dsData.Tables[0].Rows[0]["spol_branch"].ToString();
        //    lblAgentName.Text = dsData.Tables[0].Rows[0]["spol_agentcode"].ToString();
        //    lblPeriodFromTo.Text = FormatStringApp.showRangeDate(dsData.Tables[0].Rows[0]["spol_effectivefromdate"], dsData.Tables[0].Rows[0]["spol_effectivetodate"]);
        //    lblInsurerName.Text = dsData.Tables[0].Rows[0]["spol_custname"].ToString();
        //    lblPhoneNo.Text = dsData.Tables[0].Rows[0]["spol_custphoneno"].ToString();
        //    lblPolicyPremium.Text = dsData.Tables[0].Rows[0]["spol_grosspremium"].ToString();
        //    lblPolicyNet.Text = dsData.Tables[0].Rows[0]["spol_netpremium"].ToString();
        //    if (dsData.Tables[0].Rows[0]["spol_compolicy"].ToString() != "")
        //    {
        //        lblCompulNo.Text = dsData.Tables[0].Rows[0]["spol_compolicy"].ToString();
        //        lblCompulLicense.Text = dsData.Tables[0].Rows[0]["spol_comlicense"].ToString();
        //        lblCompulName.Text = dsData.Tables[0].Rows[0]["spol_comname"].ToString();
        //        lblCompulChasis.Text = dsData.Tables[0].Rows[0]["spol_comchasis"].ToString();
        //        lblCompulPremium.Text = dsData.Tables[0].Rows[0]["spol_comgrosspremium"].ToString();
        //        lblCompulNet.Text = dsData.Tables[0].Rows[0]["spol_comnetpremium"].ToString();
        //        litJavaScript.Text += "<script>document.getElementById('divCom').style.display = 'inline';\n</script>\n";
        //    }
        //    // senddoc data
        //    txtContactName.Text = dsData.Tables[0].Rows[0]["send_name"].ToString();
        //    txtContactPhone.Text = dsData.Tables[0].Rows[0]["spol_custphoneno"].ToString();
        //    txtContactAddr1.Text = dsData.Tables[0].Rows[0]["spol_custaddr1"].ToString();
        //    txtContactAddr2.Text = dsData.Tables[0].Rows[0]["spol_custaddr2"].ToString();
        //    txtContactPostCode.Text = dsData.Tables[0].Rows[0]["spol_custzip"].ToString();
        //    litJavaScript.Text += "</script>\n";

        //    // chq_accept2
        //    txtAgentCode.Text = dsData.Tables[0].Rows[0]["spol_agentcode"].ToString();

        //    //ViewState.Add("DSMasterTable", dsAuth);
        //    //ShowData();

        //}
    }
    public DataSet UIToDoc()
    {
        DataSet dsData = new DataSet();
        DataTable dt = new DataTable();
        string[] ColumnType = {"string", "string", "string", "string", "string", 
                                "string", "string", "string", "string", "string", 
                                "string"};
        string[] ColumnName = {"user_login", "user_fname", "user_lname", "user_email", "role_id",
                                "user_status", "LOGINNAME","user_pwd","start_date", "stop_date",
                                "user_branch"};
        ProfileData loginData = FormatStringApp.getLoginSession(Page, Session);
        dt = TDS.Utility.MasterUtil.AddColumnToDataTable(dt, ColumnType, ColumnName);
        dsData.Tables.Add(dt);
        dsData.Tables[0].Rows.Add(dsData.Tables[0].NewRow());
        //dsData.Tables[0].Rows[0]["user_login"] = txtUserName.Text;
        //dsData.Tables[0].Rows[0]["user_fname"] = txtFName.Text;
        //dsData.Tables[0].Rows[0]["user_lname"] = txtLName.Text;
        //dsData.Tables[0].Rows[0]["user_email"] = txtEmail.Text;
        //dsData.Tables[0].Rows[0]["role_id"] = ddlRole.SelectedValue;
        //dsData.Tables[0].Rows[0]["user_status"] = ddlStatus.SelectedValue;
        //dsData.Tables[0].Rows[0]["LOGINNAME"] = loginData.loginName;
        //if (txtMode.Text == "E")
        //    dsData.Tables[0].Rows[0]["user_pwd"] = hddPassword.Value;
        //else
        //    dsData.Tables[0].Rows[0]["user_pwd"] = txtPassword.Text;
        //dsData.Tables[0].Rows[0]["start_date"] = txtStartDate.Text;
        //dsData.Tables[0].Rows[0]["stop_date"] = txtEndDate.Text;
        //dsData.Tables[0].Rows[0]["user_branch"] = ddlBranch.SelectedValue;
        return dsData;
    }
    public void setMode()
    {
        if (txtMode.Text == "A")
        {
            //txtUserName.CssClass = "TEXTBOX";
            //txtUserName.ReadOnly = false;
            //btnAddNew.Visible = false;
        }
        else if (txtMode.Text == "E")
        {
            //txtUserName.CssClass = "TEXTBOXDIS";
            //txtUserName.ReadOnly = true;
            //btnClear.Visible = false;
            //btnAddNew.Visible = true;
            //divLablePassword.Visible = false;
            //divLblRePassword.Visible = false;
            //divtxtPassword.Visible = false;
            //divtxtRePassword.Visible = false;
        }
        else if (txtMode.Text == "V")
        {
        }
    }
    public void ShowDataInstallments()
    {
        DataSet ds = new DataSet();
        if (ViewState["DSMasterTableInstallments"] != null)
            ds = (DataSet)ViewState["DSMasterTableInstallments"];

        if (ds.Tables.Count > 0)
        {
            dv.Table = ds.Tables[0];
        }
        dtgInstallments.DataSource = dv;
        dtgInstallments.DataBind();

        GridViewUtil.ItemDataBound(dtgInstallments.Items);
    }
    public void ShowDataRV()
    {
        DataSet ds = new DataSet();
        if (ViewState["DSMasterTableRV"] != null)
            ds = (DataSet)ViewState["DSMasterTableRV"];

        if (ds.Tables.Count > 0)
        {
            dv.Table = ds.Tables[0];
        }
        dtgRV.DataSource = dv;
        dtgRV.DataBind();

        GridViewUtil.ItemDataBound(dtgRV.Items);
    }
}
