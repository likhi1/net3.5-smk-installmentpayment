﻿<%@ Page Language="C#" MasterPageFile="~/MasterData/MainMasterPage.master" AutoEventWireup="true" CodeFile="SendDocForm.aspx.cs" Inherits="Module_Settlement_SendDocForm" Title="Untitled Page" %>
<%@ Register src="../../UserControl/ucTxtDate.ascx" tagname="ucTxtDate" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" Runat="Server">
    <style type="text/css">
        .auto-style1 {
            height: 17px;
        }
        .auto-style2 {
            height: 33px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div id="page-inside">     
<table width="100%">
    <tr>
        <td>
            <div class="heading1">:: ข้อมูลที่อยู่จัดส่งเอกสาร ::</div>
        </td>
    </tr>
    <tr>
         <td colspan="1">
            <asp:Button ID="btnback" runat="server" Text="กลับ" CssClass="awesome" OnClientClick="return goPage('../../Module/Settlement/SendDocList.aspx');"/>
            <asp:Button ID="btnSave" runat="server" Text="บันทึก" CssClass="awesome" onclick="btnSave_Click" OnClientClick="return validateForm();"/>            
            <asp:Button ID="btnClear" runat="server" Text="เริ่มใหม่" CssClass="awesome" OnClientClick="return goPage('../../Module/Settlement/SendDocForm.aspx');"/>   
            <asp:Button ID="btnExit" runat="server" Text="ออก" CssClass="awesome"  OnClientClick="return goPage('../../Module/MasterData/MainPage.aspx');"/>

        </td>
    </tr>
    <tr>
        <td>
            <div id="divPolicy" style="display:inline">
            <fieldset >
                <legend>ข้อมูลกรมธรรม์</legend>
                <table width="100%">
                    <tr>
                        <td align="right" width="20%">เลขกรมธรรม์ :</td>
                        <td width="30%">
                            <asp:Label ID="lblPolicyNo" runat="server" CssClass="textBlue" ></asp:Label>
                        </td>
                        <td align="right" width="20%">วันที่รับกรมธรรม์ :</td>                        
                        <td width="30%">
                            <asp:Label ID="lblIssueDate" runat="server" CssClass="textBlue" ></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">สาขา :</td>
                        <td>
                            <asp:Label ID="lblBranchName" runat="server" CssClass="textBlue" ></asp:Label>
                        </td>
                        <td align="right">รหัสตัวแทน :</td>
                        <td>
                            <asp:Label ID="lblAgentName" runat="server" CssClass="textBlue" ></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" class="auto-style1">
                            <div id="divPolicyRemark1" style="display:inline">ทะเบียนรถ :</div>
                        </td>
                        <td class="auto-style1">
                            <asp:Label ID="lblRemark1" runat="server" CssClass="textBlue" ></asp:Label>
                        </td>
                        <td align="right" class="auto-style1">chasis :</td>
                        <td class="auto-style1">
                            <asp:Label ID="lblRemark2" runat="server" CssClass="textBlue" ></asp:Label></td>
                    </tr> 
                    <tr>
                        <td align="right" class="auto-style1">วันคุ้มครอง : </td>
                        <td class="auto-style1">
                            <asp:Label ID="lblPeriodFromTo" runat="server" CssClass="textBlue" ></asp:Label>
                        </td>
                        <td align="right" class="auto-style1">&nbsp;</td>
                        <td class="auto-style1">
                            &nbsp;</td>
                    </tr> 
                    <tr>
                        <td align="right" class="auto-style2">ชื่อ - นามสกุลผู้เอาประกันภัย :</td>
                        <td class="auto-style2">
                            <asp:Label ID="lblInsurerName" runat="server" CssClass="textBlue" ></asp:Label>
                        </td>
                        <td align="right" class="auto-style2">เบอร์โทรศัพท์  :</td>
                        <td class="auto-style2">
                            <asp:Label ID="lblPhoneNo" runat="server" CssClass="textBlue" ></asp:Label>
                        </td>
                    </tr>                                                            
                    <tr>
                        <td align="right">เบี้ยสุทธิ :</td>
                        <td>
                            <asp:Label ID="lblPolicyPremium" runat="server" CssClass="textBlue" ></asp:Label>
                        </td>  
                        <td align="right">เบี้ยรวม :</td>
                        <td>
                            <asp:Label ID="lblPolicyNet" runat="server" CssClass="textBlue" ></asp:Label>
                        </td>
                    </tr>
                    
                    <tr>
                        <td colspan="4">
                            <hr />
                        </td>
                    </tr>      
                    <tr>
                        <td align="right">เลขกรมธรรม์ (พรบ.) :</td>
                        <td>
                            <asp:Label ID="lblCompulNo" runat="server" CssClass="textBlue" ></asp:Label>
                        </td>  
                        <td align="right">เบี้ยรวม (พรบ.) :</td>
                        <td>
                            <asp:Label ID="lblCompulNet" runat="server" CssClass="textBlue" ></asp:Label>
                        
                        </td>
                    </tr>
                    <tr>
                        <td align="right">เบี้ยสุทธิ (พรบ.) :</td>
                        <td>
                            <asp:Label ID="lblCompulPremium" runat="server" CssClass="textBlue" ></asp:Label>
                        </td>  
                        <td align="right">ทะเบียนรถ (พรบ.) :</td>
                        <td>
                            <asp:Label ID="lblCompulRegister" runat="server" CssClass="textBlue" ></asp:Label></td>
                    </tr>            
                    <tr>
                        

                        <td align="right">ชื่อ-นามสกุลผู้เอาประกัน (พรบ.) :</td>
                        <td>
<asp:Label ID="lblCompulName" runat="server" CssClass="textBlue" ></asp:Label>
                          </td>   
                        <td align="right">chasis (พรบ.) :</td>
                        <td>
                        <asp:Label ID="lblCompulChasis" runat="server" CssClass="textBlue" ></asp:Label></td>  
                    </tr>            
                              
                </table>
            </fieldset>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <div id="divSendDoc" style="display:inline">
            <fieldset >
                <legend>ที่อยู่จัดส่งเอกสาร</legend>
                <table width="100%" style="margin-bottom: 0px">
                    <tr>
                        <td align="right" width="20%">ชื่อ - นามสกุล :</td>
                        <td width="30%">
                            <asp:TextBox ID="txtContactName" runat="server" CssClass="TEXTBOX" MaxLength="200" width="200"></asp:TextBox>
                        </td>
                        <td align="right" width="20%">เบอร์โทรศัพท์ :</td>                        
                        <td width="30%">
                            <asp:TextBox ID="txtContactPhone" runat="server" CssClass="TEXTBOX" MaxLength="200" width="300"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">ที่อยู่ 1 :</td>
                        <td colspan="3">
                            <asp:TextBox ID="txtContactAddr1" runat="server" CssClass="TEXTBOX" width="400" MaxLength="200"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">ที่อยู่ 2 :</td>
                        <td colspan="3">
                            <asp:TextBox ID="txtContactAddr2" runat="server" CssClass="TEXTBOX" width="400" MaxLength="200"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                      


                        <td align="right">รหัสไปรษณีย์ :</td>
                        <td>
                            <asp:TextBox ID="txtContactPostCode" runat="server" CssClass="TEXTBOX" Width="50" MaxLength="5"></asp:TextBox>
                        </td>

  <td align="right"><%--จังหวัด :--%></td>
                        <td>
                          <%--  <asp:DropDownList ID="ddlContactProvince" runat="server">
                            </asp:DropDownList>--%>
                        </td>
                    </tr>                                
                </table>
            </fieldset>
            </div>
        </td>
    </tr>
    <tr>
        <td>
        <div style="display:block"> 
            <asp:TextBox ID="txtMode" runat="server"></asp:TextBox>            
        </div>
        </td>
    </tr>          
    <tr>
        <td>            
        </td>
    </tr>
    <tr>
        <td>
            <asp:Literal ID="litJavaScript" runat="server"></asp:Literal>
        </td>
    </tr> 
</table>    
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
</asp:Content>

