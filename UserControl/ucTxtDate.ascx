<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucTxtDate.ascx.cs" Inherits="UserControl_ucTxtDate" %>
<%@ Register Src="ucCalendar.ascx" TagName="ucCalendar" TagPrefix="uc1" %>
<script>
    jQuery(function($) {
        //$("#<%=txtDate.ClientID %>").mask("99/99/9999", { placeholder: "_" });
    $("#<%=txtDate.ClientID %>").inputmask("99/99/9999", {
            placeholder: "__/__/____",
            clearMaskOnLostFocus: false
        });
    });
</script>
<table cellpadding="0" cellspacing="0">
    <tr valign="middle">
        <td><asp:TextBox ID="txtDate" runat="server" Width="80" CssClass="TEXTBOX"
                    mindate="" maxdate=""
                    maxlength="10" 
                    onpaste="dateOnPaste(this)" onblur="if(isDate(this) == false) { this.focus();} " value="__/__/____"></asp:TextBox></td>
        <td>&nbsp;</td>
        <td><uc1:ucCalendar ID="UcCalendar1" runat="server" /></td>
    </tr>
</table>


