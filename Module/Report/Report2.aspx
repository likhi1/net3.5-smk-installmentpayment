﻿<%@ Page Language="C#" MasterPageFile="~/MasterData/MainMasterPage.master" AutoEventWireup="true" CodeFile="Report2.aspx.cs" Inherits="Module_Report_Report2" Title="Untitled Page" %>
<%@ Register src="../../UserControl/ucTxtDate.ascx" tagname="ucTxtDate" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" Runat="Server">
    <script>
        function goPage(strPage)
        {
            window.location= strPage;
            return false;
        }
        function validateSearchForm() {
            var isRet = true;
            var strMsg = "";

            if (strMsg != "") {
                strMsg = "! กรุณาระบุเงื่อนไขการดึงข้อมูล\n" + strMsg;
            }

            if (isRet == false)
                alert(strMsg);
            return isRet;
        }
        function validateForm()
        {
            var isRet = true;
            var strMsg = "";
            
 
            if (strMsg != "")
            {
                strMsg = "! Please check\n" + strMsg;
            }            
                                    
            if (isRet == false)
                alert(strMsg);

            window.open("../../images/Tmp/notice_smk.gif");
            isRet = false;
            return isRet;
        }
        function openZoom() {
            openShadowBox("../../Module/Zoom/Zoom1.aspx", 500, 600, 'Zoom');
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div id="page-inside">     
    <table width="100%">
        <tr>
            <td>
                <div class="heading1">:: ออกจดหมายทวงหนี้ ::</div>
            </td>
        </tr>
        <tr>
             <td colspan="1">
                <asp:Button ID="btnSave" runat="server" Text="บันทึก" CssClass="awesome" onclick="btnSave_Click" OnClientClick="return validateForm();"/>            
                <asp:Button ID="btnClear" runat="server" Text="เริ่มใหม่" CssClass="awesome" OnClientClick="return goPage('../../Module/Report/Report2.aspx');"/>            
                <asp:Button ID="btnExit" runat="server" Text="ออก" CssClass="awesome"  OnClientClick="return goPage('../../Module/MasterData/MainPage.aspx');"/>
            </td>
        </tr>
        <tr>
            <td>
                <div id="divSearchPolicy">
                <fieldset >
                    <legend>เงื่อนไขการดึงข้อมูล</legend>
                    <table width="75%">
                        <tr>
                            <td align="right" width="20%">ประเภท :</td>
                            <td align="left" width="30%">
                                <asp:RadioButton ID="rdoMotor" GroupName="rdoPolType" runat="server" />ระบบ Motor&nbsp;
                                <asp:RadioButton ID="rdoNonMotor" GroupName="rdoPolType" runat="server" />ระบบ Non-Motor
                            </td>
                            <td width="20%">                            
                            </td>
                            <td width="30%">                            
                            </td>                  
                        </tr>   
                        <tr>
                            <td align="right" >เลขกรมธรรม์ :</td>
                            <td align="left">
                                <asp:TextBox ID="txtSearchPolicy" runat="server" CssClass="TEXTBOX" Width="100" MaxLength="15" ></asp:TextBox>
                            </td>
                            <td align="right"></td>
                            <td>
                                
                            </td> 
                        </tr>                        
                        <tr>
                            <td></td>
                            <td align="right"></td>
                            <td align="right"></td>
                            <td>
                                <asp:Button ID="btnSearch" runat="server" Text="ค้นหาข้อมูล" CssClass="awesome" onclick="btnSearch_Click" OnClientClick="return validateSearchForm();"/>
                                <asp:Button ID="btnClearSearch" runat="server" Text="เคลียร์" CssClass="awesome" OnClientClick="goPage('../../Module/Report/Report2.aspx');"/>
                            </td>
                        </tr>                                             
                    </table>
                </fieldset>
                </div>
            </td>
        </tr> 
        <tr>
            <td>    
                <div id="divPolicy" style="display:none">
                <fieldset>
                    <legend>ผลการค้นหา</legend>
                    <table cellspacing="0" rules="all" border="1" id="ctl00_ContentPlaceHolder1_dtgInstallments" style="width:100%;border-collapse:collapse;">
	<tr class="gridhead">
		<td align="center" style="width:10%;">
                                            งวดที่					                
					                    </td><td align="center" style="width:15%;">
						                    วันครบกำหนด
					                    </td><td align="center" style="width:20%;">
						                    ยอดผ่อน
					                    </td><td align="center" style="width:20%;">
						                    ยอดที่ชำระแล้ว
					                    </td><td align="center" style="width:10%;">
						                    เลขที่ใบนำส่ง
					                    </td><td align="center" style="width:10%;">พิมพ์
					                    </td>
	</tr><tr class="gridrow0" onmouseover="this.className='gridrowSelect'" onmouseout="this.className='gridrow0'">
		<td align="center">
					                        1
					                    </td><td align="center">15/11/2014
					                    </td><td align="right">
					                        8,830.66
					                    </td><td align="right">							                        
					                        8,000.00
					                    </td><td align="center">
					                        129-2999
					                    </td><td align="center">
                                            <asp:CheckBox ID="chkPrint" runat="server" />
					                    </td>
	</tr><tr class="gridrow1" onmouseover="this.className='gridrowSelect'" onmouseout="this.className='gridrow1'">
		<td align="center">
					                        2
					                    </td><td align="center">15/12/2014
					                    </td><td align="right">
					                        8,830.66
					                    </td><td align="right">
					                        0.000
					                    </td><td align="right">							                        
					                        
					                    </td><td align="center">
					                        <asp:CheckBox ID="CheckBox1" runat="server" />
					                    </td>
	</tr><tr class="gridrow0" onmouseover="this.className='gridrowSelect'" onmouseout="this.className='gridrow0'">
		<td align="center">
					                        3
					                    </td><td align="center">15/01/2015
					                    </td><td align="right">
					                        8,830.66
					                    </td><td align="right">
					                        0.000
					                    </td><td align="right">							                        
					                        
					                    </td><td align="center">
					                        <asp:CheckBox ID="CheckBox2" runat="server" />
					                    </td>
	</tr>
</table>
                </fieldset>        
                </div>        
            </td>
        </tr>  
        <tr>
            <td>
            <div style="display:none">
                <asp:TextBox ID="txtMode" runat="server"></asp:TextBox>
            </div>
            </td>
        </tr>          
        <tr>
            <td>            
            </td>
        </tr>
        <tr>
            <td>
                <asp:Literal ID="litJavaScript" runat="server"></asp:Literal>
            </td>
        </tr> 
    </table>    
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
</asp:Content>

