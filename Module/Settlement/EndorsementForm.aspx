﻿<%@ Page Language="C#" MasterPageFile="~/MasterData/MainMasterPage.master" AutoEventWireup="true" CodeFile="EndorsementForm.aspx.cs" Inherits="Module_Settlement_EndorsementForm" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" Runat="Server">
    <script>
        function goPage(strPage)
        {
            window.location= strPage;
            return false;
        }
        function validateSearchForm() {
            var isRet = true;
            var strMsg = "";
            if (document.getElementById("<%=rdoMotor.ClientID %>").checked == false && 
                document.getElementById("<%=rdoNonMotor.ClientID %>").checked == true) {
                isRet = false;
                strMsg += "\t-  ประเภทกรมธรรม์\n";
            }
            if (document.getElementById("<%=txtSearchEndorseNo.ClientID %>").value == "") {
                isRet = false;
                strMsg += "\t-  เลขสลักหลัง\n";
            }

            if (strMsg != "") {
                strMsg = "! กรุณาระบุเงื่อนไขการดึงข้อมูล\n" + strMsg;
            }

            if (isRet == false)
                alert(strMsg);
            return isRet;
        }
        function validateForm()
        {
            var isRet = true;
            var strMsg = "";
            
 
            if (strMsg != "")
            {
                strMsg = "! Please check\n" + strMsg;
            }            
                                    
            if (isRet == false)
                alert(strMsg);
            return isRet;
        }
        function openZoom() {
            openShadowBox("../../Module/Zoom/Zoom1.aspx", 500, 600, 'Zoom');
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div id="page-inside">     
    <table width="100%">
        <tr>
            <td>
                <div class="heading1">:: บันทึกชำระเบี้ยประกันภัย ::</div>
            </td>
        </tr>
        <tr>
             <td colspan="1">
                <asp:Button ID="btnback" runat="server" Text="กลับ" CssClass="awesome" OnClientClick="return goPage('../../Module/Settlement/PayInstallmentList.aspx');"/>
                <asp:Button ID="btnSave" runat="server" Text="บันทึก" CssClass="awesome" onclick="btnSave_Click" OnClientClick="return validateForm();"/>            
                <asp:Button ID="btnClear" runat="server" Text="เริ่มใหม่" CssClass="awesome" OnClientClick="return goPage('../../Module/Settlement/PayInstallmentForm.aspx');"/>            
                <asp:Button ID="btnExit" runat="server" Text="ออก" CssClass="awesome"  OnClientClick="return goPage('../../Module/MasterData/MainPage.aspx');"/>
            </td>
        </tr>
        <tr>
        <td>
            <div id="divSearchPolicy">
            <fieldset >
                <legend>ค้นหากรมธรรม์</legend>
                <table width="75%">
                    <tr>
                        <td align="right" width="20%">ประเภท :</td>
                        <td align="left" width="30%">
                            <asp:RadioButton ID="rdoMotor" GroupName="rdoPolType" runat="server" Checked/>ระบบ Motor&nbsp;
                            <asp:RadioButton ID="rdoNonMotor" GroupName="rdoPolType" runat="server" />ระบบ Non-Motor
                        </td>
                        <td width="20%">                            
                        </td>
                        <td width="30%">                            
                        </td>                  
                    </tr>   
                    <tr>
                        <td align="right">เลขสลักหลัง :</td>
                        <td align="left">
                            <asp:TextBox ID="txtSearchEndorseNo" runat="server" CssClass="TEXTBOX" Width="100" MaxLength="15" ></asp:TextBox>
                        </td>
                        <td align="right"></td>
                        <td>
                            
                        </td> 
                    </tr>
                    <tr>
                        <td></td>
                        <td align="right"></td> 
                        <td align="right"></td>
                        <td>
                            <asp:Button ID="btnSearch" runat="server" Text="ค้นหาสลักหลัง" CssClass="awesome" onclick="btnSearch_Click" OnClientClick="return validateSearchForm();"/>
                            <asp:Button ID="btnClearSearch" runat="server" Text="เคลียร์" CssClass="awesome" OnClientClick="goPage('../../Module/Settlement/SettlementForm.aspx');"/>
                        </td>
                    </tr>                                             
                </table>
            </fieldset>
            </div>
        </td>
    </tr> 
        <tr>
            <td>
                <div id="divPolicy" style="display:none">
                <fieldset >
                    <legend>ข้อมูลกรมธรรม์</legend>
                    <table width="100%">
                        <tr>
                            <td align="right" width="20%">เลขกรมธรรม์ :</td>
                            <td width="30%">
                                <asp:Label ID="lblPolicyNo" runat="server" CssClass="textBlue" ></asp:Label>
                            </td>
                            <td align="right" width="20%">วันที่รับกรมธรรม์ :</td>                        
                            <td width="30%">
                                <asp:Label ID="lblIssueDate" runat="server" CssClass="textBlue" ></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">สาขา :</td>
                            <td>
                                <asp:Label ID="lblBranchName" runat="server" CssClass="textBlue" ></asp:Label>
                            </td>
                            <td align="right">รหัสตัวแทน :</td>
                            <td>
                                <asp:Label ID="lblAgentName" runat="server" CssClass="textBlue" ></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <div id="divPolicyRemark1" style="display:inline">ทะเบียนรถ :</div>
                            </td>
                            <td>
                                <asp:Label ID="lblRemark1" runat="server" CssClass="textBlue" ></asp:Label>
                            </td>
                            <td align="right">วันคุ้มครอง :</td>
                            <td>
                                <asp:Label ID="lblPeriodFromTo" runat="server" CssClass="textBlue" ></asp:Label>
                            </td>
                        </tr> 
                        <tr>
                            <td align="right">ชื่อ - นามสกุลผู้เอาประกันภัย :</td>
                            <td>
                                <asp:Label ID="lblInsurerName" runat="server" CssClass="textBlue" ></asp:Label>
                            </td>
                            <td align="right">เบอร์โทรศัพท์  :</td>
                            <td>
                                <asp:Label ID="lblPhoneNo" runat="server" CssClass="textBlue" ></asp:Label>
                            </td>
                        </tr>                                                            
                        <tr>
                            <td align="right">เบี้ยสุทธิ :</td>
                            <td>
                                <asp:Label ID="lblPolicyPremium" runat="server" CssClass="textBlue" ></asp:Label>
                            </td>  
                            <td align="right">เบี้ยรวม :</td>
                            <td>
                                <asp:Label ID="lblPolicyNet" runat="server" CssClass="textBlue" ></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <hr />
                            </td>
                        </tr>      
                        <tr>
                            <td align="right">เลขกรมธรรม์ (พรบ.) :</td>
                            <td>
                                <asp:Label ID="lblCompulNo" runat="server" CssClass="textBlue" ></asp:Label>
                            </td>  
                            <td>                            
                            </td>
                            <td>                            
                            </td>
                        </tr>
                        <tr>
                            <td align="right">เบี้ยสุทธิ (พรบ.) :</td>
                            <td>
                                <asp:Label ID="lblCompulPremium" runat="server" CssClass="textBlue" ></asp:Label>
                            </td>  
                            <td align="right">เบี้ยรวม (พรบ.) :</td>
                            <td>
                                <asp:Label ID="lblCompulNet" runat="server" CssClass="textBlue" ></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <hr />
                            </td>
                        </tr>      
                        <tr>
                            <td align="right">เลขสลักหลัง :</td>
                            <td>
                                <asp:TextBox ID="txtEndorseNo" runat="server" CssClass="TEXTBOXDIS" ></asp:TextBox>
                            </td>  
                            <td align="right">วันที่สลักหลัง :</td>
                            <td>                            
                                <asp:TextBox ID="txtEndorseDate" runat="server" CssClass="TEXTBOXDIS" ></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">เบี้ยสุทธิเปลี่ยนแปลง :</td>
                            <td>
                                <asp:TextBox ID="txtEndorsePremium" runat="server" CssClass="TEXTBOXMONEYDIS" ></asp:TextBox>
                            </td>  
                            <td align="right">เบี้ยรวมเปลี่ยนแปลง :</td>
                            <td>
                                <asp:TextBox ID="txtEndorseNet" runat="server" CssClass="TEXTBOXMONEYDIS" ></asp:TextBox>
                            </td>
                        </tr>            
                    </table>
                    <table cellspacing="0" rules="all" border="1" id="ctl00_ContentPlaceHolder1_dtgInstallments" style="width:100%;border-collapse:collapse;">
	<tr class="gridhead">
		<td align="center" style="width:10%;">
                                            งวดที่					                
					                    </td><td align="center" style="width:15%;">
						                    วันครบกำหนด
					                    </td><td align="center" style="width:20%;">
						                    ยอดผ่อน
					                    </td><td align="center" style="width:20%;">
						                    ยอดที่ชำระ
					                    </td><td align="center" style="width:10%;">
						                    เลขที่ใบนำส่ง
					                    </td>
	</tr><tr class="gridrow0" onmouseover="this.className='gridrowSelect'" onmouseout="this.className='gridrow0'">
		<td align="center">
					                        1
					                    </td><td align="center">15/11/2014
					                    </td><td align="right">
					                        8,830.66
					                    </td><td align="right">							                        
					                        8,830.66
					                    </td><td align="center">
					                        129-2999
					                    </td>
	</tr><tr class="gridrow1" onmouseover="this.className='gridrowSelect'" onmouseout="this.className='gridrow1'">
		<td align="center">
					                        2
					                    </td><td align="center">
					                        
<script>
    jQuery(function($) {
        //$("#ctl00_ContentPlaceHolder1_dtgInstallments_ctl03_txtDueDate_txtDate").mask("99/99/9999", { placeholder: "_" });
        $("#ctl00_ContentPlaceHolder1_dtgInstallments_ctl03_txtDueDate_txtDate").inputmask("99/99/9999", {
            placeholder: "__/__/____",
            clearMaskOnLostFocus: false,
            showMaskOnHover: true
        });
    });
</script>
<table cellpadding="0" cellspacing="0">
    <tr valign="middle">
        <td><input name="ctl00$ContentPlaceHolder1$dtgInstallments$ctl03$txtDueDate$txtDate" type="text" value="15/12/2014" maxlength="10" id="ctl00_ContentPlaceHolder1_dtgInstallments_ctl03_txtDueDate_txtDate" class="TEXTBOX" mindate="" maxdate="" onpaste="dateOnPaste(this)" onblur="if(isDate(this) == false) { this.focus();} " value="__/__/____" style="width:80px;" /></td>
        <td>&nbsp;</td>
        <td><SCRIPT LANGUAGE="JavaScript">
<!--
                function openCalendarctl00$ContentPlaceHolder1$dtgInstallments$ctl03$txtDueDate$txtDate(txtCalendarName) {
                    openShadowBox("/InstallmentPayment/UserControl/Calendar.aspx?txtDateObj=document.all." + txtCalendarName + "&title=Calendar&postaction=&lang=th&yeartype=AD&firstYear=2004&lastYear=2024&mindate=&maxdate=", 250, 270, 'Calendar');
                }
//-->
</SCRIPT>

<img name="btnCalendar" style="CURSOR:hand" src="/InstallmentPayment/images/ticker.gif" border="0" onclick="openCalendarctl00$ContentPlaceHolder1$dtgInstallments$ctl03$txtDueDate$txtDate('ctl00$ContentPlaceHolder1$dtgInstallments$ctl03$txtDueDate$txtDate');">
</td>
    </tr>
</table>



					                    </td><td align="right">
					                        <input name="ctl00$ContentPlaceHolder1$dtgInstallments$ctl03$txtNet" type="text" value="6,869.36" maxlength="18" id="ctl00_ContentPlaceHolder1_dtgInstallments_ctl03_txtNet" class="TEXTBOXMONEY" onblur="calSumNet();" />
					                    </td><td align="right">							                        
					                        <input name="ctl00$ContentPlaceHolder1$dtgInstallments$ctl03$txtPaid" type="text" value="0" maxlength="18" readonly="readonly" id="ctl00_ContentPlaceHolder1_dtgInstallments_ctl03_txtPaid" class="TEXTBOXMONEY" onblur="calSumPaid();" />
					                    </td><td align="center">
					                        
					                    </td>
	</tr><tr class="gridrow0" onmouseover="this.className='gridrowSelect'" onmouseout="this.className='gridrow0'">
		<td align="center">
					                        3
					                    </td><td align="center">
					                        
<script>
    jQuery(function($) {
        //$("#ctl00_ContentPlaceHolder1_dtgInstallments_ctl04_txtDueDate_txtDate").mask("99/99/9999", { placeholder: "_" });
        $("#ctl00_ContentPlaceHolder1_dtgInstallments_ctl04_txtDueDate_txtDate").inputmask("99/99/9999", {
            placeholder: "__/__/____",
            clearMaskOnLostFocus: false,
            showMaskOnHover: true
        });
    });
</script>
<table cellpadding="0" cellspacing="0">
    <tr valign="middle">
        <td><input name="ctl00$ContentPlaceHolder1$dtgInstallments$ctl04$txtDueDate$txtDate" type="text" value="15/01/2015" maxlength="10" id="ctl00_ContentPlaceHolder1_dtgInstallments_ctl04_txtDueDate_txtDate" class="TEXTBOX" mindate="" maxdate="" onpaste="dateOnPaste(this)" onblur="if(isDate(this) == false) { this.focus();} " value="__/__/____" style="width:80px;" /></td>
        <td>&nbsp;</td>
        <td><SCRIPT LANGUAGE="JavaScript">
<!--
                function openCalendarctl00$ContentPlaceHolder1$dtgInstallments$ctl04$txtDueDate$txtDate(txtCalendarName) {
                    openShadowBox("/InstallmentPayment/UserControl/Calendar.aspx?txtDateObj=document.all." + txtCalendarName + "&title=Calendar&postaction=&lang=th&yeartype=AD&firstYear=2004&lastYear=2024&mindate=&maxdate=", 250, 270, 'Calendar');
                }
//-->
</SCRIPT>

<img name="btnCalendar" style="CURSOR:hand" src="/InstallmentPayment/images/ticker.gif" border="0" onclick="openCalendarctl00$ContentPlaceHolder1$dtgInstallments$ctl04$txtDueDate$txtDate('ctl00$ContentPlaceHolder1$dtgInstallments$ctl04$txtDueDate$txtDate');">
</td>
    </tr>
</table>



					                    </td><td align="right">
					                        <input name="ctl00$ContentPlaceHolder1$dtgInstallments$ctl04$txtNet" type="text" value="7,074.28" maxlength="18" id="ctl00_ContentPlaceHolder1_dtgInstallments_ctl04_txtNet" class="TEXTBOXMONEY" onblur="calSumNet();" />
					                    </td><td align="right">							                        
					                        <input name="ctl00$ContentPlaceHolder1$dtgInstallments$ctl04$txtPaid" type="text" value="0" maxlength="18" readonly="readonly" id="ctl00_ContentPlaceHolder1_dtgInstallments_ctl04_txtPaid" class="TEXTBOXMONEY" onblur="calSumPaid();" />
					                    </td><td align="center">
					                        
					                    </td>
	</tr><tr>
		<td>&nbsp;</td><td>&nbsp;</td><td align="right" valign="middle"><div id="divSumNet" class="textBlue">20,706.00&nbsp;</div></td><td align="right" valign="middle"><div id="divSumPaid" class="textBlue">8,830.66&nbsp;</div></td><td>&nbsp;</td>
	</tr>
</table>
                </fieldset>
                </div>
            </td>
        </tr>
        <tr>
            <td>
            <div style="display:none">
                <asp:TextBox ID="txtMode" runat="server"></asp:TextBox>
            </div>
            </td>
        </tr>          
        <tr>
            <td>            
            </td>
        </tr>
        <tr>
            <td>
                <asp:Literal ID="litJavaScript" runat="server"></asp:Literal>
            </td>
        </tr> 
    </table>    
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
</asp:Content>

