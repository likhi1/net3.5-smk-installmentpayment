﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Module_Settlement_SyncDataList : System.Web.UI.Page
{
    public string[] ColumnName = { "NO", "sync_datetime", "sync_policy", "sync_endorse", "sync_name" };
    public string[] ColumnType = { "string", "string", "string", "string", "string" };
    public string[] ColumnTitle = { "ลำดับที่", "วัน-เวลาที่ดึงข้อมูล", "จำนวนกรมธรรม์", "จำนวนสลักหลัง", "ผู้ที่ดึงข้อมูล"};
    string[] ColumnWidth = { "5%", "15%", "15%", "15%", "50%"};
    string[] ColumnAlign = { "center", "center", "right", "right", "left"};

    public string tranPicUrl = "";
    DataView dv = new DataView();

    protected void Page_Load(object sender, EventArgs e)
    {
        ProfileData loginData = FormatStringApp.getLoginSession(Page, Session);
        MasterDataManager cm = new MasterDataManager();
        DataSet ds = new DataSet();
        if (!IsPostBack)
        {
            initialDropDownList();
            initialDataGrid();
        }
        else
        {
            litJavaScript.Text = "";
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        ProfileData loginData = FormatStringApp.getLoginSession(Page, Session);
        SyncManager cmSync = new SyncManager();
        DataSet ds = new DataSet();

        ds = cmSync.searchDataSyncHistory(txtFromDate.Text, txtToDate.Text);
        ViewState.Remove("SortDirection");
        ViewState.Remove("SortString");
        ViewState.Remove("SortIndex");
        ViewState.Add("DSMasterTable", ds);
        ShowData();
    }
    protected void btnSync_Click(object sender, EventArgs e)
    {
        ProfileData loginData = FormatStringApp.getLoginSession(Page, Session);
        SyncManager cmSync = new SyncManager();
        IfxManager cmIfx = new IfxManager();
        SQLManager cmSQL = new SQLManager();
        DataSet ds = new DataSet();
        DataSet dsIfxMotor = new DataSet();
        DataSet dsSQLNon = new DataSet();
        DataSet dsOldPolicy = new DataSet();
        DataRow[] drOldPolicy;
        string strLastSyncDateTime = "";
        string strSelect = "";
        string strRetPolicy = "";

        // 1. หา sync_datetime ล่าสุด
        ds = cmSync.searchDataSyncHistory("__/__/____", FormatStringApp.FormatDate(DateTime.Today.AddDays(1)));
        if (ds.Tables[0].Rows.Count > 0)
        {
            // เอาแถวสุดท้าย >> sync ล่าสุด
            strLastSyncDateTime = FormatStringApp.FormatDate(ds.Tables[0].Rows[ds.Tables[0].Rows.Count - 1]["sync_datetime"]);
        }
        else
        {
            strLastSyncDateTime = FormatStringApp.FormatDate(DateTime.Today.AddDays(-7));
        }

        // 2. เอา sync_datetime ล่าสุดไปหาใน Motor (ifx)
        dsIfxMotor = cmIfx.getDataIfxVolun(strLastSyncDateTime);

        // 3. เอา sync_datetime ล่าสุดไปหาใน Non motor(ifx)>> ยังไม่มี

        // 4. เอา sync_datetime ล่าสุดไปหาใน Non motor (sql)
        dsSQLNon = cmSQL.getDataSQLNonMotor(strLastSyncDateTime);        

        // 5. เอา sync_datetime ล่าสุดไปหาใน sync_policy 
        dsOldPolicy = cmSync.searchDataSyncPolicy(strLastSyncDateTime, "__/__/____");
        // 6. เอารายการ ซ้ำออก
        dsIfxMotor.Tables[0].Merge(dsSQLNon.Tables[0], false, MissingSchemaAction.Ignore);
        if (dsOldPolicy.Tables[0].Rows.Count > 0)
        {
            for (int iOldPolicy = 0; iOldPolicy <= dsOldPolicy.Tables[0].Rows.Count - 1; iOldPolicy++)
            {
                strSelect = "spol_policy = '" + dsOldPolicy.Tables[0].Rows[iOldPolicy]["spol_policy"].ToString() + "' ";
                // IFX Motor
                drOldPolicy = dsIfxMotor.Tables[0].Select(strSelect, "spol_issuedate");
                if (drOldPolicy.Length > 0)
                {
                    for (int iDup = 0; iDup <= drOldPolicy.Length - 1; iDup++)
                    {
                        dsIfxMotor.Tables[0].Rows.Remove(drOldPolicy[iDup]);
                    }
                }
                //// SQL Non Motor
                //drOldPolicy = dsSQLNon.Tables[0].Select(strSelect, "spol_issuedate");
                //if (drOldPolicy.Length > 0)
                //{
                //    for (int iDup = 0; iDup <= drOldPolicy.Length - 1; iDup++)
                //    {
                //        dsSQLNon.Tables[0].Rows.Remove(drOldPolicy[iDup]);
                //    }
                //}
            }
        }
        DataGrid3.DataSource = dsIfxMotor;
        DataGrid3.DataBind();
        // 7. เพิ่มข้อมูลในตาราง sync_policy
        strRetPolicy = cmSync.addDataSyncPolicyEndorse(dsIfxMotor, dsIfxMotor, loginData.loginName, "Y");

        litJavaScript.Text = "<script>alert('! พบข้อมูล \\n\\tกรมธรรม์ : " + strRetPolicy + " รายการ');</script>";

    }


    protected void PageIndexChanged(Object source, DataGridPageChangedEventArgs e)
    {
        DataView dv = new DataView();
        DataSet ds = new DataSet();
        if (!(ViewState["DSMasterTable"] == null))
        {
            ds = (DataSet)ViewState["DSMasterTable"];
            if (e.NewPageIndex > dtgData.PageCount - 1)
                dtgData.CurrentPageIndex = dtgData.PageCount - 1;
            else
                dtgData.CurrentPageIndex = e.NewPageIndex;

            String sortBy = "";
            if (!(ViewState["DSMasterTable"] == null))
            {
                sortBy = (string)(ViewState["SortString"]);
            }
            dv.Table = ds.Tables[0];
            dv.Sort = sortBy;
            ShowData();

        }
    }

    public void initialDropDownList()
    {
        DataSet ds;
        MasterDataManager cmMaster = new MasterDataManager();
    }
    public void initialDataGrid()
    {
        DataSet ds = new DataSet();
        DataTable dt = new DataTable();
        int iPageSize = 0;
        dt = TDS.Utility.MasterUtil.AddColumnToDataTable(dt, ColumnType, ColumnName);
        ds.Tables.Add(dt);
        ViewState.Remove("SortDirection");
        ViewState.Remove("SortString");
        ViewState.Remove("SortIndex");
        ViewState.Add("DSMasterTable", ds);
        ShowData();
        GridViewUtil.setGridStyle(dtgData, ColumnWidth, ColumnAlign, ColumnTitle);
        try
        {
            iPageSize = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["pageSize"].ToString());
        }
        catch (Exception e)
        {
            iPageSize = 10;
        }
        dtgData.PageSize = iPageSize;
    }
    public void ShowData()
    {
        DataSet ds = new DataSet();
        if (ViewState["DSMasterTable"] != null)
            ds = (DataSet)ViewState["DSMasterTable"];

        string sortBy = "";
        if (ViewState["SortString"] != null)
            sortBy = (String)ViewState["SortString"];

        if (ds.Tables.Count > 0)
        {
            dv.Table = ds.Tables[0];
            dv.Sort = sortBy;

            int newPageCount = (Int32)(Math.Ceiling((double)dv.Table.Rows.Count / (double)dtgData.PageSize));
            if (dtgData.CurrentPageIndex >= newPageCount && newPageCount > 0)
                dtgData.CurrentPageIndex = newPageCount - 1;
        }
        dtgData.DataSource = dv;
        dtgData.DataBind();

        GridViewUtil.ItemDataBound(dtgData.Items);

        ucPageNavigator1.dvData = ds.Tables[0].DefaultView;
        ucPageNavigator1.dtgData = dtgData;
    }
    public void sortColumn(object Sender, EventArgs e)
    {
        string[] SortDirection = new string[ColumnTitle.Length];
        for (int i = 0; i < SortDirection.Length; i++)
        {
            SortDirection[i] = "";
        }
        if (ViewState["SortDirection"] != null)
            SortDirection = (string[])ViewState["SortDirection"];

        LinkButton lb = (LinkButton)Sender;
        string sortBy = "";
        int index = 0;
        while ((index < ColumnName.Length) && !((lb.Text).Equals(ColumnTitle[index])))
        {
            index++;
        }
        sortBy = ("" + ColumnName[index] + " " + SortDirection[index]);
        SortDirection[index] = (SortDirection[index].Equals("") ? "DESC" : "");
        ViewState.Add("SortDirection", SortDirection);
        ViewState.Add("SortString", sortBy);
        ViewState.Add("SortIndex", index);
        ShowData();
    }
    public string getPicUrl(int colNo)
    {
        string returnValue = Request.ApplicationPath + "/Images/tran.gif";
        if (ViewState["SortIndex"] != null)
        {
            DataGridItem dgi = (DataGridItem)(dtgData.Controls[0].Controls[1]);

            string[] SortDirection = new string[Title.Length];
            SortDirection = (string[])ViewState["SortDirection"];

            if ((int)(ViewState["SortIndex"]) == colNo)
            {
                returnValue = Request.ApplicationPath + "/Images/" + (SortDirection[colNo].Equals("") ? "sortDown.gif" : "sortUp.gif");
            }
        }
        return returnValue;
    }
}
