﻿<%@ Page Language="C#" MasterPageFile="~/MasterData/MainMasterPage.master" AutoEventWireup="true" CodeFile="SyncDataList.aspx.cs" Inherits="Module_Settlement_SyncDataList" Title="Untitled Page" %>
<%@ Register src="../../UserControl/ucPageNavigator.ascx" tagname="ucPageNavigator" tagprefix="uc1" %>
<%@ Register src="../../UserControl/ucTxtDate.ascx" tagname="ucTxtDate" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" Runat="Server">
    <script language="javascript">
        function goPage(strPage)
        {
            window.location= strPage;
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div id="page-inside">     
        <table width="100%">
            <tr>
                <td>
                    <div class="heading1">:: ดึงข้อมูลจากระบบภายนอก ::</div>
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                <asp:Button ID="btnSearch" runat="server" Text="ค้นหา" cssClass="awesome"
                                    onclick="btnSearch_Click"/>
                            </td>
                            <td>
                                <asp:Button ID="btnClear" runat="server" Text="เคลียร์" CssClass="awesome" OnClientClick="return goPage('../../Module/Settlement/SyncDataList.aspx');"/>
                            </td>
                            <td>
                                <asp:Button ID="btnSync" runat="server" Text="ดึงข้อมูล" CssClass="awesome" onclick="btnSync_Click"/>
                            </td>
                            <td>
                                <asp:Button ID="btnExit" runat="server" Text="ออก" CssClass="awesome" OnClientClick="return goPage('../../Module/MasterData/MainPage.aspx');"/>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <fieldset>
                        <legend>เงื่อนไขการค้นหา</legend>
                        <table width="100%">                            
                            <tr>
                                <td align="right" width="20%">วันดึงข้อมูลตั้งแต่ :</td>
                                <td align="left" width="30%">
                                    <uc1:ucTxtDate ID="txtFromDate" runat="server" />
                                </td>
                                <td align="right" width="20%">ถึงวันที่ :</td>
                                <td align="left" width="30%">
                                    <uc1:ucTxtDate ID="txtToDate" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </fieldset>            
                </td>
            </tr>
            <tr>
                <td>            
                    <fieldset>
                        <legend>ผลการค้นหา</legend>
                        <table width="100%">
                            <tr>
                                <td>                            
                                    <uc1:ucPageNavigator ID="ucPageNavigator1" runat="server" OnPageIndexChanged="PageIndexChanged"/>                            
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:DataGrid ID="dtgData" runat="server" AutoGenerateColumns="false" Width="100%"
                                        HeaderStyle-CssClass="gridhead" ItemStyle-CssClass="gridrow0" AlternatingItemStyle-CssClass="gridrow1"
                                        AllowPaging="true" PagerStyle-Visible="false">
                                        <Columns>
                                            <asp:TemplateColumn>
						                        <HeaderTemplate>
                                                    <%#ColumnTitle[0]%>					                
						                        </HeaderTemplate>
						                        <ItemTemplate>
						                            <%# Container.DataSetIndex +1%>
						                        </ItemTemplate>
					                        </asp:TemplateColumn>
                                            <asp:TemplateColumn>
						                        <HeaderTemplate>
							                        <asp:LinkButton ID="lnkID1" Runat="server" OnClick="sortColumn" text="<%#ColumnTitle[1]%>">
							                        </asp:LinkButton>
							                        <asp:Image ID="imgSortCol1" Runat="server" ImageUrl="<%#getPicUrl(1)%>">
							                        </asp:Image>
						                        </HeaderTemplate>
						                        <ItemTemplate>
						                            <%#FormatStringApp.FormatDateTime(Eval(ColumnName[1]))%>
						                        </ItemTemplate>
					                        </asp:TemplateColumn>
					                        <asp:TemplateColumn>
						                        <HeaderTemplate>
							                        <asp:LinkButton ID="lnkID2" Runat="server" OnClick="sortColumn" text="<%#ColumnTitle[2]%>">
							                        </asp:LinkButton>
							                        <asp:Image ID="imgSortCol2" Runat="server" ImageUrl="<%#getPicUrl(2)%>">
							                        </asp:Image>
						                        </HeaderTemplate>
						                        <ItemTemplate>
						                            <%#Eval(ColumnName[2])%>
						                        </ItemTemplate>
					                        </asp:TemplateColumn>
					                        <asp:TemplateColumn>
						                        <HeaderTemplate>
							                        <asp:LinkButton ID="lnkID3" Runat="server" OnClick="sortColumn" text="<%#ColumnTitle[3]%>">
							                        </asp:LinkButton>
							                        <asp:Image ID="imgSortCol3" Runat="server" ImageUrl="<%#getPicUrl(3)%>">
							                        </asp:Image>
						                        </HeaderTemplate>
						                        <ItemTemplate>
						                            <%#Eval(ColumnName[3])%>
						                        </ItemTemplate>
					                        </asp:TemplateColumn>
					                        <asp:TemplateColumn>
						                        <HeaderTemplate>
							                        <asp:LinkButton ID="lnkID4" Runat="server" OnClick="sortColumn" text="<%#ColumnTitle[4]%>">
							                        </asp:LinkButton>
							                        <asp:Image ID="imgSortCol4" Runat="server" ImageUrl="<%#getPicUrl(4)%>">
							                        </asp:Image>
						                        </HeaderTemplate>
						                        <ItemTemplate>
						                            <%#Eval(ColumnName[4])%>
						                        </ItemTemplate>
					                        </asp:TemplateColumn>					                        
					                    </Columns>
                                    </asp:DataGrid>
                                </td>
                            </tr>
                        </table>
                    </fieldset>        
                </td>
            </tr>    
            <tr>
                <td>
                    <asp:DataGrid ID="DataGrid3" runat="server" AutoGenerateColumns="true" Width="100%"
                        HeaderStyle-CssClass="gridhead" ItemStyle-CssClass="gridrow0" AlternatingItemStyle-CssClass="gridrow1"
                        AllowPaging="false" PagerStyle-Visible="false" Visible="false">
                    </asp:DataGrid>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Literal ID="litJavaScript" runat="server"></asp:Literal>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
</asp:Content>

