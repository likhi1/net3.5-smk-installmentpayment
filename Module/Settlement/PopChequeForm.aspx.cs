﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Module_Settlement_PopChequeForm : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        litJavaScript.Text = "<script>paymenttype_click();</script>";
        if (!IsPostBack)
        {
            rdoCheque.Attributes.Add("onclick", "paymenttype_click();");
            rdoCashierCheque.Attributes.Add("onclick", "paymenttype_click();");
            rdoTransferCheque.Attributes.Add("onclick", "paymenttype_click();");
            rdoDraftCheque.Attributes.Add("onclick", "paymenttype_click();");
            rdoTransfer.Attributes.Add("onclick", "paymenttype_click();");
            rdoCash.Attributes.Add("onclick", "paymenttype_click();");
            rdoCreditCard.Attributes.Add("onclick", "paymenttype_click();");
            rdoAP.Attributes.Add("onclick", "paymenttype_click();");
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        DataSet dsRV = new DataSet();
        DataTable dt;
        string[] ColumnNameRV = { "rv_no", "rv_type", "rv_desc", "rv_net", "" };
        string[] ColumnTypeRV = { "string", "string", "string", "string", "string" };
        if (Session["DSMasterTableRV"] != null)
            dsRV = (DataSet)Session["DSMasterTableRV"];
        else
        {
            dsRV = new DataSet();
            dt = new DataTable();
            dt = TDS.Utility.MasterUtil.AddColumnToDataTable(dt, ColumnTypeRV, ColumnNameRV);
            dsRV.Tables.Add(dt);
        }

        dsRV.Tables[0].Rows.Add(dsRV.Tables[0].NewRow());
        if (rdoCheque.Checked == true)
        {
            // เช็ค
            dsRV.Tables[0].Rows[dsRV.Tables[0].Rows.Count - 1]["rv_type"] = "";
            dsRV.Tables[0].Rows[dsRV.Tables[0].Rows.Count - 1]["rv_desc"] = "เลขที่เช็ค:12345  วันที่ในเช็ค:15/11/2014  ธนาคาร:กรุงศรีอยุธยา จำกัด(มหาชน)  สาขา:สีลม";
            dsRV.Tables[0].Rows[dsRV.Tables[0].Rows.Count - 1]["rv_net"] = "6,310.00";
        }
        else
        {
            dsRV.Tables[0].Rows[dsRV.Tables[0].Rows.Count - 1]["rv_type"] = "";
            dsRV.Tables[0].Rows[dsRV.Tables[0].Rows.Count - 1]["rv_desc"] = "เลขที่เช็ค:12345  วันที่ในเช็ค:15/11/2014  ธนาคาร:กรุงศรีอยุธยา จำกัด(มหาชน)  สาขา:สีลม";
            dsRV.Tables[0].Rows[dsRV.Tables[0].Rows.Count - 1]["rv_net"] = "6,310.00";
        }
        Session.Add("DSMasterTableRV", dsRV);
        litJavaScript.Text = "<script>parent.addRVSubmit();parent.closeShadowBox();</script>";
    }
}
