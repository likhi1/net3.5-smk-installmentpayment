﻿<%@ Page Language="C#" MasterPageFile="~/MasterData/MainMasterPage.master" AutoEventWireup="true" CodeFile="PayInstallmentForm.aspx.cs" Inherits="Module_Settlement_PayInstallmentForm" Title="Untitled Page" %>
<%@ Register src="../../UserControl/ucTxtDate.ascx" tagname="ucTxtDate" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" Runat="Server">
<script>
        function goPage(strPage)
        {
            window.location= strPage;
            return false;
        }
        function validateSearchForm() {
            var isRet = true;
            var strMsg = "";
            if (document.getElementById("<%=rdoMotor.ClientID %>").checked == true) {
                if (document.getElementById("<%=txtVolunNo.ClientID %>").value == "") {
                    isRet = false;
                    strMsg += "\t-  เลขกรมธรรม์สมัครใจ\n";
                }
            }
            if (document.getElementById("<%=rdoNonMotor.ClientID %>").checked == true) {
                if (document.getElementById("<%=txtNonMotorNo.ClientID %>").value == "") {
                    isRet = false;
                    strMsg += "\t-  เลขกรมธรรม์\n";
                }
            }

            if (strMsg != "") {
                strMsg = "! กรุณาระบุเงื่อนไขการดึงข้อมูล\n" + strMsg;
            }

            if (isRet == false)
                alert(strMsg);
            return isRet;
        }
        function validateForm()
        {
            var isRet = true;
            var strMsg = "";
            
 
            if (strMsg != "")
            {
                strMsg = "! Please check\n" + strMsg;
            }            
                                    
            if (isRet == false)
                alert(strMsg);
            return isRet;
        }
        function openZoom() {
            openShadowBox("../../Module/Zoom/Zoom1.aspx", 500, 600, 'Zoom');
            return false;
        }
        function openPopChequeForm() {
            openShadowBox("../../Module/Settlement/PopChequeForm.aspx", 750, 700, 'PopCheque');
        }
        function addRVSubmit() {
            <%=Page.ClientScript.GetPostBackEventReference(btnRVSave, string.Empty) %>
            //__doPostBack('<%=btnRVSave.ClientID %>', '');
        }
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div id="page-inside">     
    <table width="100%">
        <tr>
            <td>
                <div class="heading1">:: บันทึกชำระเบี้ยประกันภัย ::</div>
            </td>
        </tr>
        <tr>
             <td colspan="1">
                <asp:Button ID="btnback" runat="server" Text="กลับ" CssClass="awesome" OnClientClick="return goPage('../../Module/Settlement/PayInstallmentList.aspx');"/>
                <asp:Button ID="btnSave" runat="server" Text="บันทึก" CssClass="awesome" onclick="btnSave_Click" OnClientClick="return validateForm();"/>            
                <asp:Button ID="btnClear" runat="server" Text="เริ่มใหม่" CssClass="awesome" OnClientClick="return goPage('../../Module/Settlement/PayInstallmentForm.aspx');"/>            
                <asp:Button ID="btnExit" runat="server" Text="ออก" CssClass="awesome"  OnClientClick="return goPage('../../Module/MasterData/MainPage.aspx');"/>
            </td>
        </tr>
        <tr>
        <td>
            <div id="divSearchPolicy">
            <fieldset >
                <legend>ค้นหากรมธรรม์</legend>
                <table width="75%">
                    <tr>
                        <td align="right" width="20%">ประเภท :</td>
                        <td align="left" width="20%">
                            <asp:RadioButton ID="rdoMotor" GroupName="rdoPolType" runat="server" Checked/>ระบบ Motor
                        </td>
                        <td width="18%">
                            เลขกรมธรรม์สมัครใจ :
                        </td>
                        <td width="20%">
                            <asp:TextBox ID="txtVolunNo" runat="server" CssClass="TEXTBOX" Width="100" MaxLength="15" ></asp:TextBox>
                        </td>                        
                        <td width="22%"></td>
                    </tr>                                                 
                    <tr>
                        <td></td>
                        <td align="left">
                            <asp:RadioButton ID="rdoNonMotor" GroupName="rdoPolType" runat="server" />ระบบ Non-Motor
                        </td>
                        <td>
                            เลขกรมธรรม์ :
                        </td>
                        <td>
                            <asp:TextBox ID="txtNonMotorNo" runat="server" CssClass="TEXTBOX" Width="100" MaxLength="15" ></asp:TextBox>
                        </td>
                        <td>
                        </td>
                    </tr> 
                    <tr>
                        <td></td>
                        <td align="right"></td>
                        <td>
                            
                        </td>  
                        <td align="right"></td>
                        <td>
                            <asp:Button ID="btnSearch" runat="server" Text="ค้นหากรมธรรม์" CssClass="awesome" onclick="btnSearch_Click" OnClientClick="return validateSearchForm();"/>
                            <asp:Button ID="btnClearSearch" runat="server" Text="เคลียร์" CssClass="awesome" OnClientClick="goPage('../../Module/Settlement/SettlementForm.aspx');"/>
                        </td>
                    </tr>                                             
                </table>
            </fieldset>
            </div>
        </td>
    </tr> 
        <tr>
            <td>
                <div id="divPolicy" style="display:none">
                <fieldset >
                    <legend>ข้อมูลกรมธรรม์</legend>
                    <table width="100%">
                        <tr>
                            <td align="right" width="20%">เลขกรมธรรม์ :</td>
                            <td width="30%">
                                <asp:Label ID="lblPolicyNo" runat="server" CssClass="textBlue" ></asp:Label>
                            </td>
                            <td align="right" width="20%">วันที่รับกรมธรรม์ :</td>                        
                            <td width="30%">
                                <asp:Label ID="lblIssueDate" runat="server" CssClass="textBlue" ></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">สาขา :</td>
                            <td>
                                <asp:Label ID="lblBranchName" runat="server" CssClass="textBlue" ></asp:Label>
                            </td>
                            <td align="right">รหัสตัวแทน :</td>
                            <td>
                                <asp:Label ID="lblAgentName" runat="server" CssClass="textBlue" ></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <div id="divPolicyRemark1" style="display:inline">ทะเบียนรถ :</div>
                            </td>
                            <td>
                                <asp:Label ID="lblRemark1" runat="server" CssClass="textBlue" ></asp:Label>
                            </td>
                            <td align="right">วันคุ้มครอง :</td>
                            <td>
                                <asp:Label ID="lblPeriodFromTo" runat="server" CssClass="textBlue" ></asp:Label>
                            </td>
                        </tr> 
                        <tr>
                            <td align="right">ชื่อ - นามสกุลผู้เอาประกันภัย :</td>
                            <td>
                                <asp:Label ID="lblInsurerName" runat="server" CssClass="textBlue" ></asp:Label>
                            </td>
                            <td align="right">เบอร์โทรศัพท์  :</td>
                            <td>
                                <asp:Label ID="lblPhoneNo" runat="server" CssClass="textBlue" ></asp:Label>
                            </td>
                        </tr>                                                            
                        <tr>
                            <td align="right">เบี้ยสุทธิ :</td>
                            <td>
                                <asp:Label ID="lblPolicyPremium" runat="server" CssClass="textBlue" ></asp:Label>
                            </td>  
                            <td align="right">เบี้ยรวม :</td>
                            <td>
                                <asp:Label ID="lblPolicyNet" runat="server" CssClass="textBlue" ></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <hr />
                            </td>
                        </tr>      
                        <tr>
                            <td align="right">เลขกรมธรรม์ (พรบ.) :</td>
                            <td>
                                <asp:Label ID="lblCompulNo" runat="server" CssClass="textBlue" ></asp:Label>
                            </td>  
                            <td>                            
                            </td>
                            <td>                            
                            </td>
                        </tr>
                        <tr>
                            <td align="right">เบี้ยสุทธิ (พรบ.) :</td>
                            <td>
                                <asp:Label ID="lblCompulPremium" runat="server" CssClass="textBlue" ></asp:Label>
                            </td>  
                            <td align="right">เบี้ยรวม (พรบ.) :</td>
                            <td>
                                <asp:Label ID="lblCompulNet" runat="server" CssClass="textBlue" ></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <hr />
                            </td>
                        </tr>      
                        <tr>
                            <td align="right">เลขสลักหลัง :</td>
                            <td>
                                <asp:Label ID="lblEndorseNo" runat="server" CssClass="textBlue" ></asp:Label>
                            </td>  
                            <td align="right">วันที่สลักหลัง :</td>
                            <td>                            
                                <asp:Label ID="lblEndorseDate" runat="server" CssClass="textBlue" ></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">เบี้ยสุทธิเปลี่ยนแปลง :</td>
                            <td>
                                <asp:Label ID="lblEndorsePremium" runat="server" CssClass="textBlue" ></asp:Label>
                            </td>  
                            <td align="right">เบี้ยรวมเปลี่ยนแปลง :</td>
                            <td>
                                <asp:Label ID="lblEndorseNet" runat="server" CssClass="textBlue" ></asp:Label>
                            </td>
                        </tr>            
                    </table>
                </fieldset>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div id="divSendDoc" style="display:none">
                <fieldset >
                    <legend>ที่อยู่จัดส่งเอกสาร</legend>
                    <table width="100%">
                        <tr>
                            <td align="right" width="20%">ชื่อ - นามสกุล :</td>
                            <td width="30%">
                                <asp:Label ID="lblContactName" runat="server" CssClass="textBlue" ></asp:Label>
                            </td>
                            <td align="right" width="20%">เบอร์โทรศัพท์ :</td>                        
                            <td width="30%">
                                <asp:Label ID="lblContactPhoneNo" runat="server" CssClass="textBlue" ></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">ที่อยู่ 1 :</td>
                            <td colspan="3">
                                <asp:Label ID="lblContactAddr1" runat="server" CssClass="textBlue" ></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">ที่อยู่ 2 :</td>
                            <td colspan="3">
                                <asp:Label ID="lblContactAddr2" runat="server" CssClass="textBlue" ></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">จังหวัด :</td>
                            <td>
                                <asp:Label ID="lblContactProvince" runat="server" CssClass="textBlue" ></asp:Label>
                            </td>
                            <td align="right">รหัสไปรษณีย์ :</td>
                            <td>
                                <asp:Label ID="lblContactPostCode" runat="server" CssClass="textBlue" ></asp:Label>
                            </td>
                        </tr>                                
                    </table>
                </fieldset>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div id="divInstallments" style="display:none">
                <fieldset >
                    <legend>ข้อมูลผ่อนชำระ</legend>
                    <table width="100%">
                        <tr>
                            <td align="right" width="20%">เลขที่ผ่อนชำระ :</td>
                            <td width="30%">
                                <asp:Label ID="lblSettlementID" runat="server" CssClass="textBlue" ></asp:Label>
                            </td>
                            <td width="20%" align="right">วันที่บันทึกผ่อนชำระ :</td>                        
                            <td width="30%">
                                <asp:Label ID="lblSettlementDate" runat="server" CssClass="textBlue" ></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">รวมเบี้ยกรมธรรม์ :</td>
                            <td>
                                <asp:Label ID="txtPolicyTotal" runat="server" CssClass="textBlue"></asp:Label> บาท
                            </td>
                            <td align="right">รวมเบี้ยสลักหลัง :</td>
                            <td>
                                <asp:Label ID="txtEndorseTotal" runat="server" CssClass="textBlue"></asp:Label> บาท
                            </td>
                        </tr>
                        <tr>
                            <td align="right">รวมยอดเบี้ย :</td>
                            <td>
                                <asp:Label ID="txtPolicyBalance" runat="server" CssClass="textBlue"></asp:Label> บาท
                            </td>
                            <td align="right">ส่วนลด :</td>
                            <td>
                                <asp:Label ID="lblDiscountRate" runat="server" CssClass="textBlue" ></asp:Label> %
                                <asp:Label ID="lblDiscount" runat="server" CssClass="textBlue" ></asp:Label> บาท
                            </td>
                        </tr> 
                        <tr>
                            <td align="right">ยอดผ่อนชำระ :</td>
                            <td>
                                <asp:Label ID="lblBalance" runat="server" CssClass="textBlue" ></asp:Label> บาท
                            </td>
                            <td align="right">จำนวนงวด :</td>
                            <td>
                                <asp:TextBox ID="txtInstallments" runat="server" CssClass="TEXTBOXMONEY" Width="50" MaxLength="2"></asp:TextBox>
                                ราย
                                <asp:Label ID="lblMonths" runat="server" CssClass="textBlue" ></asp:Label> เดือน
                            </td>
                        </tr>    
                        <tr>
                            <td align="right">วันที่เริ่มจ่าย :</td>                        
                            <td>
                                <asp:Label ID="lblStartDate" runat="server" CssClass="textBlue" ></asp:Label>
                            </td>
                            <td align="right">เลข AR :</td>
                            <td>
                                <asp:Label ID="txtARNo" runat="server" CssClass="textBlue"></asp:Label>
                            </td>
                        </tr>  
                        <tr>
                            <td align="right">ยอดที่จ่ายมาแล้ว :</td>
                            <td>
                                <asp:TextBox ID="txtPaid" runat="server" CssClass="TEXTBOXMONEYDIS"></asp:TextBox> บาท
                            </td>
                            <td align="right">ยอดค้างชำระ :</td>
                            <td>
                                <asp:TextBox ID="txtArrear" runat="server" CssClass="TEXTBOXMONEYDIS"></asp:TextBox> บาท
                            </td>
                        </tr>    
                        <tr>
                            <td align="right">หมายเหตุ :</td>
                            <td>
                                <asp:TextBox ID="txtRemark" runat="server" textmode="MultiLine" Width="300" Rows="3"></asp:TextBox>
                            </td>
                        </tr>  
                        <tr>
                            <td>
                                <asp:Button ID="btnAddInst" runat="server" Text="เพิ่มงวดการชำระ" CssClass="awesome" onclick="btnAddInst_Click"/>
                            </td>                      
                        </tr>
                        <tr>
                            <td colspan="4">
                                <asp:DataGrid ID="dtgInstallments" runat="server" AutoGenerateColumns="false" Width="100%"
                                    HeaderStyle-CssClass="gridhead" ItemStyle-CssClass="gridrow0" AlternatingItemStyle-CssClass="gridrow1"
                                    AllowPaging="false" PagerStyle-Visible="false" OnItemDataBound="dtgInstallments_ItemDataBound"
                                    ShowFooter="true" Visible="false">
                                    <Columns>
                                        <asp:TemplateColumn>
					                        <HeaderTemplate>
                                                <%#ColumnTitle[0]%>					                
					                        </HeaderTemplate>
					                        <ItemTemplate>
					                            <%#Eval(ColumnName[0])%>
					                        </ItemTemplate>
				                        </asp:TemplateColumn>
                                        <asp:TemplateColumn>
					                        <HeaderTemplate>
						                        <%#ColumnTitle[1]%>
					                        </HeaderTemplate>
					                        <ItemTemplate>
					                            <uc1:ucTxtDate ID="txtDueDate" runat="server" Text="<%#FormatStringApp.FormatDate(Eval(ColumnName[1]))%>" />
					                        </ItemTemplate>
				                        </asp:TemplateColumn>
				                        <asp:TemplateColumn>
					                        <HeaderTemplate>
						                        <%#ColumnTitle[2]%>
					                        </HeaderTemplate>
					                        <ItemTemplate>
					                            <asp:TextBox ID="txtRate" runat="server" CssClass="TEXTBOXMONEY" Width="50" MaxLength="5" Text="<%#Eval(ColumnName[2])%>" inst="<%#Eval(ColumnName[0])%>" onblur="calNetInstallment(this)"></asp:TextBox>
					                        </ItemTemplate>
				                        </asp:TemplateColumn>	
				                        <asp:TemplateColumn>
					                        <HeaderTemplate>
						                        <%#ColumnTitle[3]%>
					                        </HeaderTemplate>
					                        <ItemTemplate>
					                            <asp:TextBox ID="txtNet" runat="server" CssClass="TEXTBOXMONEY" MaxLength="18" Text="<%#FormatStringApp.FormatMoney(Eval(ColumnName[3]))%>" onblur="calSumNet();"></asp:TextBox>
					                        </ItemTemplate>
				                        </asp:TemplateColumn>
				                        <asp:TemplateColumn>
					                        <HeaderTemplate>
						                        <%#ColumnTitle[4]%>
					                        </HeaderTemplate>
					                        <ItemTemplate>							                        
					                            <asp:TextBox ID="txtPaid" runat="server" CssClass="TEXTBOXMONEYDIS" MaxLength="18" Text="<%#Eval(ColumnName[4])%>" onblur="calSumPaid();"></asp:TextBox>
					                        </ItemTemplate>
				                        </asp:TemplateColumn>
				                        <asp:TemplateColumn>
					                        <HeaderTemplate>
						                        <%#ColumnTitle[5]%>
					                        </HeaderTemplate>
					                        <ItemTemplate>
					                            <%#Eval(ColumnName[5])%>
					                        </ItemTemplate>
				                        </asp:TemplateColumn>	
				                        <asp:TemplateColumn>
					                        <HeaderTemplate>						                    
					                        </HeaderTemplate>
					                        <ItemTemplate>
                                                <asp:ImageButton ID="imgDelete" runat="server" ImageUrl="../../images/btnDelete.GIF" ToolTip="ลบรายการ" onclick="imgDeleteInst_Click" dsindex="<%# Container.DataSetIndex%>"/>                                            
					                        </ItemTemplate>
				                        </asp:TemplateColumn>		                    
				                    </Columns>
                                </asp:DataGrid>
                                <table cellspacing="0" rules="all" border="1" id="ctl00_ContentPlaceHolder1_dtgInstallments" style="width:100%;border-collapse:collapse;">
	<tr class="gridhead">
		<td align="center" style="width:10%;">
                                            งวดที่					                
					                    </td><td align="center" style="width:15%;">
						                    วันครบกำหนด
					                    </td><td align="center" style="width:15%;">
						                    %
					                    </td><td align="center" style="width:20%;">
						                    ยอดผ่อน
					                    </td><td align="center" style="width:20%;">
						                    ยอดที่ชำระ
					                    </td><td align="center" style="width:10%;">
						                    เลขที่ใบนำส่ง
					                    </td><td align="center" style="width:10%;">						                    
					                    </td>
	</tr><tr class="gridrow0" onmouseover="this.className='gridrowSelect'" onmouseout="this.className='gridrow0'">
		<td align="center">
					                        1
					                    </td><td align="center">15/11/2014
					                    </td><td align="right">
					                        33
					                    </td><td align="right">
					                        8,830.66
					                    </td><td align="right">							                        
					                        8,830.66
					                    </td><td align="center">
					                        129-2999
					                    </td><td align="center">
					                    </td>
	</tr><tr class="gridrow1" onmouseover="this.className='gridrowSelect'" onmouseout="this.className='gridrow1'">
		<td align="center">
					                        2
					                    </td><td align="center">
					                        
<script>
    jQuery(function($) {
        //$("#ctl00_ContentPlaceHolder1_dtgInstallments_ctl03_txtDueDate_txtDate").mask("99/99/9999", { placeholder: "_" });
        $("#ctl00_ContentPlaceHolder1_dtgInstallments_ctl03_txtDueDate_txtDate").inputmask("99/99/9999", {
            placeholder: "__/__/____",
            clearMaskOnLostFocus: false,
            showMaskOnHover: true
        });
    });
</script>
<table cellpadding="0" cellspacing="0">
    <tr valign="middle">
        <td><input name="ctl00$ContentPlaceHolder1$dtgInstallments$ctl03$txtDueDate$txtDate" type="text" value="15/12/2014" maxlength="10" id="ctl00_ContentPlaceHolder1_dtgInstallments_ctl03_txtDueDate_txtDate" class="TEXTBOX" mindate="" maxdate="" onpaste="dateOnPaste(this)" onblur="if(isDate(this) == false) { this.focus();} " value="__/__/____" style="width:80px;" /></td>
        <td>&nbsp;</td>
        <td><SCRIPT LANGUAGE="JavaScript">
<!--
                function openCalendarctl00$ContentPlaceHolder1$dtgInstallments$ctl03$txtDueDate$txtDate(txtCalendarName) {
                    openShadowBox("/InstallmentPayment/UserControl/Calendar.aspx?txtDateObj=document.all." + txtCalendarName + "&title=Calendar&postaction=&lang=th&yeartype=AD&firstYear=2004&lastYear=2024&mindate=&maxdate=", 250, 270, 'Calendar');
                }
//-->
</SCRIPT>

<img name="btnCalendar" style="CURSOR:hand" src="/InstallmentPayment/images/ticker.gif" border="0" onclick="openCalendarctl00$ContentPlaceHolder1$dtgInstallments$ctl03$txtDueDate$txtDate('ctl00$ContentPlaceHolder1$dtgInstallments$ctl03$txtDueDate$txtDate');">
</td>
    </tr>
</table>



					                    </td><td align="right">
					                        <input name="ctl00$ContentPlaceHolder1$dtgInstallments$ctl03$txtRate" type="text" value="33" maxlength="5" id="ctl00_ContentPlaceHolder1_dtgInstallments_ctl03_txtRate" class="TEXTBOXMONEY" onblur="calNetInstallment(this)" inst="2" style="width:50px;" />
					                    </td><td align="right">
					                        <input name="ctl00$ContentPlaceHolder1$dtgInstallments$ctl03$txtNet" type="text" value="6,762.36" maxlength="18" id="ctl00_ContentPlaceHolder1_dtgInstallments_ctl03_txtNet" class="TEXTBOXMONEY" onblur="calSumNet();" />
					                    </td><td align="right">							                        
					                        <input name="ctl00$ContentPlaceHolder1$dtgInstallments$ctl03$txtPaid" type="text" value="0" maxlength="18" readonly="readonly" id="ctl00_ContentPlaceHolder1_dtgInstallments_ctl03_txtPaid" class="TEXTBOXMONEY" onblur="calSumPaid();" />
					                    </td><td align="center">
					                        
					                    </td><td align="center">
                                            <input type="image" name="ctl00$ContentPlaceHolder1$dtgInstallments$ctl03$imgDelete" id="ctl00_ContentPlaceHolder1_dtgInstallments_ctl03_imgDelete" title="ลบรายการ" dsindex="1" src="../../images/btnDelete.GIF" style="border-width:0px;" />                                            
					                    </td>
	</tr><tr class="gridrow0" onmouseover="this.className='gridrowSelect'" onmouseout="this.className='gridrow0'">
		<td align="center">
					                        3
					                    </td><td align="center">
					                        
<script>
    jQuery(function($) {
        //$("#ctl00_ContentPlaceHolder1_dtgInstallments_ctl04_txtDueDate_txtDate").mask("99/99/9999", { placeholder: "_" });
        $("#ctl00_ContentPlaceHolder1_dtgInstallments_ctl04_txtDueDate_txtDate").inputmask("99/99/9999", {
            placeholder: "__/__/____",
            clearMaskOnLostFocus: false,
            showMaskOnHover: true
        });
    });
</script>
<table cellpadding="0" cellspacing="0">
    <tr valign="middle">
        <td><input name="ctl00$ContentPlaceHolder1$dtgInstallments$ctl04$txtDueDate$txtDate" type="text" value="15/01/2015" maxlength="10" id="ctl00_ContentPlaceHolder1_dtgInstallments_ctl04_txtDueDate_txtDate" class="TEXTBOX" mindate="" maxdate="" onpaste="dateOnPaste(this)" onblur="if(isDate(this) == false) { this.focus();} " value="__/__/____" style="width:80px;" /></td>
        <td>&nbsp;</td>
        <td><SCRIPT LANGUAGE="JavaScript">
<!--
                function openCalendarctl00$ContentPlaceHolder1$dtgInstallments$ctl04$txtDueDate$txtDate(txtCalendarName) {
                    openShadowBox("/InstallmentPayment/UserControl/Calendar.aspx?txtDateObj=document.all." + txtCalendarName + "&title=Calendar&postaction=&lang=th&yeartype=AD&firstYear=2004&lastYear=2024&mindate=&maxdate=", 250, 270, 'Calendar');
                }
//-->
</SCRIPT>

<img name="btnCalendar" style="CURSOR:hand" src="/InstallmentPayment/images/ticker.gif" border="0" onclick="openCalendarctl00$ContentPlaceHolder1$dtgInstallments$ctl04$txtDueDate$txtDate('ctl00$ContentPlaceHolder1$dtgInstallments$ctl04$txtDueDate$txtDate');">
</td>
    </tr>
</table>



					                    </td><td align="right">
					                        <input name="ctl00$ContentPlaceHolder1$dtgInstallments$ctl04$txtRate" type="text" value="34" maxlength="5" id="ctl00_ContentPlaceHolder1_dtgInstallments_ctl04_txtRate" class="TEXTBOXMONEY" onblur="calNetInstallment(this)" inst="3" style="width:50px;" />
					                    </td><td align="right">
					                        <input name="ctl00$ContentPlaceHolder1$dtgInstallments$ctl04$txtNet" type="text" value="6,967.28" maxlength="18" id="ctl00_ContentPlaceHolder1_dtgInstallments_ctl04_txtNet" class="TEXTBOXMONEY" onblur="calSumNet();" />
					                    </td><td align="right">							                        
					                        <input name="ctl00$ContentPlaceHolder1$dtgInstallments$ctl04$txtPaid" type="text" value="0" maxlength="18" readonly="readonly" id="ctl00_ContentPlaceHolder1_dtgInstallments_ctl04_txtPaid" class="TEXTBOXMONEY" onblur="calSumPaid();" />
					                    </td><td align="center">
					                        
					                    </td><td align="center">
                                            <input type="image" name="ctl00$ContentPlaceHolder1$dtgInstallments$ctl04$imgDelete" id="ctl00_ContentPlaceHolder1_dtgInstallments_ctl04_imgDelete" title="ลบรายการ" dsindex="2" src="../../images/btnDelete.GIF" style="border-width:0px;" />                                            
					                    </td>
	</tr><tr>
		<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td align="right" valign="middle"><div id="divSumNet" class="textBlue">20,492.00&nbsp;</div></td><td align="right" valign="middle"><div id="divSumPaid" class="textBlue">0.00&nbsp;</div></td><td>&nbsp;</td><td>&nbsp;</td>
	</tr>
</table>

                            </td>
                        </tr>
                    </table>
                </fieldset>
                </div>
            </td>
        </tr> 
        <tr>
            <td>
                <div id="divRV" style="display:none">
                <fieldset>
                    <legend>ข้อมูลรับชำระ</legend>
                    <table width="100%">
                        <tr>
                            <td align="right" width="15%">เลขที่นำส่ง :</td>
                            <td width="35%">
                                <asp:TextBox ID="txtRVNO" runat="server" CssClass="TEXTBOXDIS"></asp:TextBox>
                            </td>
                            <td align="right" width="20%">วันที่รับเงิน :</td>                        
                            <td width="30%">
                                <uc1:ucTxtDate ID="txtRVDate" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right">จากฝ่าย :</td>
                            <td>
                                <asp:TextBox ID="txtTmpCode1" runat="server" CssClass="TEXTBOX" width="80"></asp:TextBox>
                                <asp:ImageButton ID="imgPopup1" runat="server" 
                                    ImageUrl="../../Images/binoculars.gif" ToolTip="ค้นหาฝ่าย"
                                    style="width: 15px"  type="script" OnClientClick="return openZoom();"/>
                                <asp:TextBox ID="txtTmpName1" runat="server" CssClass="TEXTBOXDIS" width="200"></asp:TextBox>
                            </td>
                            <td align="right">เล่ม - เลขที่ :</td>
                            <td>
                                <asp:TextBox ID="txtBookNO" runat="server" CssClass="TEXTBOXDIS" Width="150"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">สาขารับเงิน :</td>
                            <td>
                                <asp:TextBox ID="txtTmpCode2" runat="server" CssClass="TEXTBOX" width="80"></asp:TextBox>
                                <asp:ImageButton ID="imgPopup2" runat="server" 
                                    ImageUrl="../../Images/binoculars.gif" ToolTip="ค้นหาสาขา"
                                    style="width: 15px"  type="script"/>
                                <asp:TextBox ID="txtTmpName2" runat="server" CssClass="TEXTBOXDIS" width="200"></asp:TextBox>
                            </td>
                            <td align="right">หลักฐานการรับเงิน :</td>
                            <td>
                                <asp:TextBox ID="txtDocument" runat="server" CssClass="TEXTBOXDIS" width="200"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">รหัสตัวแทน :</td>
                            <td>
                                <asp:TextBox ID="txtTmpCode3" runat="server" CssClass="TEXTBOX" width="80"></asp:TextBox>
                                <asp:ImageButton ID="imgPopup3" runat="server" 
                                    ImageUrl="../../Images/binoculars.gif" ToolTip="ค้นหาตัวแทน"
                                    style="width: 15px"  type="script"/>
                                <asp:TextBox ID="txtTmpName3" runat="server" CssClass="TEXTBOXDIS" width="200"></asp:TextBox>
                            </td>
                            <td align="right">หัวหน้าสาย :</td>
                            <td>
                                <asp:TextBox ID="txtHead" runat="server" CssClass="TEXTBOXDIS" width="200"></asp:TextBox>
                            </td>
                        </tr>    
                        <tr>
                            <td align="right">รับเงินเพื่อ :</td>
                            <td>
                                <asp:TextBox ID="txtTmpCode4" runat="server" CssClass="TEXTBOX" width="80"></asp:TextBox>
                                <asp:ImageButton ID="imgPopup4" runat="server" 
                                    ImageUrl="../../Images/binoculars.gif" ToolTip="ค้นหารับเงินเพื่อ"
                                    style="width: 15px"  type="script"/>
                                <asp:TextBox ID="txtTmpName4" runat="server" CssClass="TEXTBOXDIS" width="200"></asp:TextBox>
                            </td>
                            <td align="right">หัวหน้าสาย :</td>
                            <td>
                                <asp:TextBox ID="TextBox3" runat="server" CssClass="TEXTBOXDIS" width="200"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">ชื่อผู้นำส่ง :</td>
                            <td>
                                <asp:TextBox ID="txtTmpCode5" runat="server" CssClass="TEXTBOX" width="80"></asp:TextBox>
                                <asp:ImageButton ID="imgPopup5" runat="server" 
                                    ImageUrl="../../Images/binoculars.gif" ToolTip="ค้นหาผู้นำส่ง"
                                    style="width: 15px"  type="script"/>
                                <asp:TextBox ID="txtTmpName5" runat="server" CssClass="TEXTBOXDIS" width="200"></asp:TextBox>
                            </td>
                            <td align="right">ยอดที่ชำระครั้งนี้ :</td>
                            <td>
                                <asp:TextBox ID="txtSumPaid" runat="server" CssClass="TEXTBOXMONEYDIS"></asp:TextBox>
                            </td>
                        </tr>  
                        <tr>
                            <td>
                                <asp:Button ID="btnAddRV" runat="server" Text="เพิ่มรายการรับชำระ" CssClass="awesome" OnClientClick="openPopChequeForm();return false;"/>
                            </td>                      
                        </tr> 
                        <tr>
                            <td colspan="4">
                                <asp:DataGrid ID="dtgRV" runat="server" AutoGenerateColumns="false" Width="100%"
                                    HeaderStyle-CssClass="gridhead" ItemStyle-CssClass="gridrow0" AlternatingItemStyle-CssClass="gridrow1"
                                    AllowPaging="false" PagerStyle-Visible="false" OnItemDataBound="dtgRV_ItemDataBound">
                                    <Columns>
                                        <asp:TemplateColumn>
					                        <HeaderTemplate>
                                                <%#ColumnTitleRV[0]%>					                
					                        </HeaderTemplate>
					                        <ItemTemplate>
					                            <%# Container.DataSetIndex +1%>
					                        </ItemTemplate>
				                        </asp:TemplateColumn>
                                        <asp:TemplateColumn>
					                        <HeaderTemplate>
						                        <%#ColumnTitleRV[1]%>
					                        </HeaderTemplate>
					                        <ItemTemplate>
					                            <%#(Eval(ColumnNameRV[1]))%>
					                        </ItemTemplate>
				                        </asp:TemplateColumn>
				                        <asp:TemplateColumn>
					                        <HeaderTemplate>
						                        <%#ColumnTitleRV[2]%>
					                        </HeaderTemplate>
					                        <ItemTemplate>
					                            <%#Eval(ColumnNameRV[2])%>
					                        </ItemTemplate>
				                        </asp:TemplateColumn>	
				                        <asp:TemplateColumn>
					                        <HeaderTemplate>
						                        <%#ColumnTitleRV[3]%>
					                        </HeaderTemplate>
					                        <ItemTemplate>
					                            <%#Eval(ColumnNameRV[3])%>
					                        </ItemTemplate>
				                        </asp:TemplateColumn>
				                        <asp:TemplateColumn>
					                        <HeaderTemplate>						                    
					                        </HeaderTemplate>
					                        <ItemTemplate>
					                            <asp:ImageButton ID="imgEdit" runat="server" ImageUrl="../../images/btnEDIT.GIF" ToolTip="แก้ไขเงินนำส่ง" OnClientClick="openPopChequeForm(); return false;" onclick="imgEditRV_Click" dsindex="<%# Container.DataSetIndex%>"/>
                                                <asp:ImageButton ID="imgDelete" runat="server" ImageUrl="../../images/btnDelete.GIF" ToolTip="ลบรายการ" onclick="imgDeleteRV_Click" dsindex="<%# Container.DataSetIndex%>"/>                                            
					                        </ItemTemplate>
				                        </asp:TemplateColumn>		                    
				                    </Columns>
                                </asp:DataGrid>
                            </td>
                        </tr>                           
                    </table>
                </fieldset>
                </div>
            </td>
        </tr>
        <tr>
            <td>
            <div style="display:none">
                <asp:TextBox ID="txtMode" runat="server"></asp:TextBox>
                <asp:TextBox ID="txtPayID" runat="server"></asp:TextBox>
                <asp:Button ID="btnRVSave" runat="server" Text="เพิ่มรายการรับชำระ" CssClass="awesome" onclick="btnRVSave_Click"/>
            </div>
            </td>
        </tr>          
        <tr>
            <td>            
            </td>
        </tr>
        <tr>
            <td>
                <asp:Literal ID="litJavaScript" runat="server"></asp:Literal>
            </td>
        </tr> 
    </table>    
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
</asp:Content>

