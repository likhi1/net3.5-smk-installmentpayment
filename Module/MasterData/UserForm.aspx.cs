using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Common.Lib.Login;

public partial class Module_MasterData_UserForm : System.Web.UI.Page
{
    public string[] ColumnName = { "NO",  "branch_name", "branch_code" };
    public string[] ColumnType = { "string",  "string",  "string" };
    public string[] ColumnTitle = { "ลำดับที่",  "สาขา",  "" };
    string[] ColumnWidth = { "20%",  "40%",  "40%" };
    string[] ColumnAlign = { "center", "left", "center" };
    public string[] ColumnName2 = { "NO", "menu_name", "urmenu_flagadd", "urmenu_flagedit", "urmenu_flagdelete", "urmenu_flaginquiry","", "menu_id" };
    public string[] ColumnType2 = { "string", "string", "string", "string", "string", "string", "string", "string" };
    public string[] ColumnTitle2 = { "ลำดับที่", "เมนู", "บันทึก", "แก้ไข", "ยกเลิก", "สอบถาม", "" };
    string[] ColumnWidth2 = { "10%", "20%", "15%", "15%", "15%", "15%", "10%" };
    string[] ColumnAlign2 = { "center", "left", "center", "center", "center", "center", "center" };
    DataView dv = new DataView();
    DataView dv2 = new DataView();

    protected void Page_Load(object sender, EventArgs e)
    {
        ProfileData loginData = FormatStringApp.getLoginSession(Page, Session);
        MasterDataManager cm = new MasterDataManager();
        DataSet ds = new DataSet();
        DataSet dsAuth = new DataSet();
        DataSet dsMenu = new DataSet();
        if (!IsPostBack)
        {
            initialDropDownList();
            initialDataGrid();
            initialDataGrid2();
            ddlStatus.SelectedValue = "A";
            txtMode.Text = "A";
            if (Request.QueryString["username"] != null)
            {
                txtMode.Text = "E";
                txtUserName.Text = Request.QueryString["username"];
                ds = cm.getDataUserByUserName(txtUserName.Text);                
                dsAuth = cm.getDataUserAuthByUserName(txtUserName.Text);
                dsMenu = cm.getDataUserMenuByUserName(txtUserName.Text);
                docToUI(ds, dsAuth,dsMenu);
            }
            setMode();    
        }
        else
        {
            litJavaScript.Text = "";
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        bool isRet = true;
        DataSet ds;
        DataSet dsAuth = new DataSet();
        DataSet dsMenu = new DataSet();
        ProfileData loginData = FormatStringApp.getLoginSession(Page, Session);
        MasterDataManager cm = new MasterDataManager();
        litJavaScript.Text = "<script>";
        ds = UIToDoc();
        dsAuth = (DataSet)ViewState["DSMasterTable"];
        dsMenu = (DataSet)ViewState["DSMasterTable2"];
        if (txtMode.Text == "A")
        {
            isRet = cm.addDataUser(ds, dsAuth,dsMenu);
            if (isRet == false)
            {
                litJavaScript.Text += "alert('" + cm.getDataErrorByPageNameNo("","002") + "');";
            }
            else
            {
                litJavaScript.Text += "alert('" + cm.getDataErrorByPageNameNo("", "001") + "');";
                txtMode.Text = "E";
            }
        }
        else if (txtMode.Text == "E")
        {
            isRet = cm.editDataUser(ds, dsAuth, dsMenu);
            if (isRet == false)
            {
                litJavaScript.Text += "alert('" + cm.getDataErrorByPageNameNo("", "002") + "');";
            }
            else
            {
                litJavaScript.Text += "alert('" + cm.getDataErrorByPageNameNo("", "001") + "');";
            }
        }
        litJavaScript.Text += "</script>";
        ds = cm.getDataUserByUserName(txtUserName.Text);
        dsAuth = cm.getDataUserAuthByUserName(txtUserName.Text);
        dsMenu = cm.getDataUserMenuByUserName(txtUserName.Text);
        docToUI(ds, dsAuth,dsMenu);
        setMode();
    }
    protected void btnAddItem_Click(object sender, EventArgs e)
    {
        DataSet ds;
        ds = (DataSet)ViewState["DSMasterTable"];
        ds.Tables[0].Rows.InsertAt(ds.Tables[0].NewRow(), 0);
        ViewState["DSMasterTable"] = ds;
        dtgData.EditItemIndex = 0;
        ShowData();
    }
    protected void btnAddItem2_Click(object sender, EventArgs e)
    {
        DataSet ds2;
        TextBox txtDtgMenuMode;
        
        ds2 = (DataSet)ViewState["DSMasterTable2"];
        ds2.Tables[0].Rows.InsertAt(ds2.Tables[0].NewRow(), 0);
        ViewState["DSMasterTable2"] = ds2;
        dtgData2.EditItemIndex = 0;
        ShowData2();
        txtDtgMenuMode = (TextBox)dtgData2.Items[0].FindControl("txtMenuMode");
        txtDtgMenuMode.Text = "A";
    }
    protected void imgEdit_Click(object sender, ImageClickEventArgs e)
    {
        ImageButton imgClick = (ImageButton)sender;
        dtgData.EditItemIndex = Convert.ToInt32(imgClick.Attributes["dsindex"]);
        ShowData();
    }
    protected void imgEdit2_Click(object sender, ImageClickEventArgs e)
    {
        ImageButton imgClick2 = (ImageButton)sender;
        DropDownList ddlDtgMenu;
        TextBox txtDtgMenu;
        CheckBox chkAdd;
        CheckBox chkEdit;
        CheckBox chkDelete;
        CheckBox chkInquiry;
        TextBox txtFlagAdd;
        TextBox txtFlagEdit;
        TextBox txtFlagDelete;
        TextBox txtFlagInquiry;
        TextBox txtDtgMenuMode;
        int iItem = 0;

        dtgData2.EditItemIndex = Convert.ToInt32(imgClick2.Attributes["dsindex"]);
        ShowData2();

        iItem = dtgData2.EditItemIndex;
        ddlDtgMenu = (DropDownList)dtgData2.Items[iItem].Cells[0].FindControl("ddlMenu");
        txtDtgMenu = (TextBox)dtgData2.Items[iItem].Cells[0].FindControl("txtMenu");
        chkAdd = (CheckBox)dtgData2.Items[iItem].Cells[0].FindControl("chkFlagAdd");
        chkEdit = (CheckBox)dtgData2.Items[iItem].Cells[0].FindControl("chkFlagEdit");
        chkDelete = (CheckBox)dtgData2.Items[iItem].Cells[0].FindControl("chkFlagDelete");
        chkInquiry = (CheckBox)dtgData2.Items[iItem].Cells[0].FindControl("chkFlagInquiry");
        txtFlagAdd = (TextBox)dtgData2.Items[iItem].Cells[0].FindControl("txtFlagAdd");
        txtFlagEdit = (TextBox)dtgData2.Items[iItem].Cells[0].FindControl("txtFlagEdit");
        txtFlagDelete = (TextBox)dtgData2.Items[iItem].Cells[0].FindControl("txtFlagDelete");
        txtFlagInquiry = (TextBox)dtgData2.Items[iItem].Cells[0].FindControl("txtFlagInquiry");
        txtDtgMenuMode = (TextBox)dtgData2.Items[iItem].FindControl("txtMenuMode");
        txtDtgMenuMode.Text = "E";

        if (ddlDtgMenu != null)
        {
            ddlDtgMenu.Enabled = false;
            initialDropDownListMenu(ddlDtgMenu, txtDtgMenu.Text, chkAdd, chkEdit, chkDelete, chkInquiry);
            chkAdd.Checked = (chkAdd.Visible ? (txtFlagAdd.Text == "Y") : false);
            chkEdit.Checked = (chkEdit.Visible ? (txtFlagEdit.Text == "Y") : false);
            chkDelete.Checked = (chkDelete.Visible ? (txtFlagDelete.Text == "Y") : false);
            chkInquiry.Checked = (chkInquiry.Visible ? (txtFlagInquiry.Text == "Y") : false);
        }
    }
    protected void imgCancel_Click(object sender, ImageClickEventArgs e)
    {
        dtgData.EditItemIndex = -1;
        ShowData();
    }
    protected void imgCancel2_Click(object sender, ImageClickEventArgs e)
    {
        dtgData2.EditItemIndex = -1;
        ShowData2();
    }
    protected void imgSave_Click(object sender, ImageClickEventArgs e)
    {
        ImageButton imgClick = (ImageButton)sender;
        DataSet ds;
        DropDownList ddlBranchCode;

        //DropDownList ddlStatus;
        int iIndex = Convert.ToInt32(imgClick.Attributes["dsindex"]);
        ds = (DataSet)ViewState["DSMasterTable"];
        ddlBranchCode = (DropDownList)dtgData.Items[iIndex].FindControl("ddlBranchCode");
        if (ddlBranchCode.SelectedValue  == "" )
        {
            litJavaScript.Text = "<script>alert('!ยังไม่ได้ระบุสาขาที่ต้องการ ');</script>";
        }
        else if (ds.Tables[0].Select("branch_code = '" + ddlBranchCode.SelectedValue   + "'", "").Length > 0)
        {
            litJavaScript.Text = "<script>alert('!สาขาที่ต้องการ มีการกำหนดสิทธิ์แล้ว ');</script>";
        }
        else
        {
            ds.Tables[0].Rows[iIndex]["branch_name"] = ddlBranchCode.SelectedItem;
            ds.Tables[0].Rows[iIndex]["branch_code"] = ddlBranchCode.SelectedValue;
            ViewState["DSMasterTable"] = ds;
            dtgData.EditItemIndex = -1;
            ShowData();
        }
    }
    protected void imgSave2_Click(object sender, ImageClickEventArgs e)
    {
        ImageButton imgClick2 = (ImageButton)sender;
        DataSet ds2;
        DropDownList ddlMenu;
        CheckBox chkFlagAdd;
        CheckBox chkFlagEdit;
        CheckBox chkFlagDelete;
        CheckBox chkFlagInquiry;
        TextBox txtDtgMenuMode;
        
        int iIndex2 = Convert.ToInt32(imgClick2.Attributes["dsindex"]);
        ds2 = (DataSet)ViewState["DSMasterTable2"];
        ddlMenu = (DropDownList)dtgData2.Items[iIndex2].FindControl("ddlMenu");
        chkFlagAdd = (CheckBox)dtgData2.Items[iIndex2].FindControl("chkFlagAdd");
        chkFlagEdit = (CheckBox)dtgData2.Items[iIndex2].FindControl("chkFlagEdit");
        chkFlagDelete = (CheckBox)dtgData2.Items[iIndex2].FindControl("chkFlagDelete");
        chkFlagInquiry = (CheckBox)dtgData2.Items[iIndex2].FindControl("chkFlagInquiry");
        txtDtgMenuMode = (TextBox)dtgData2.Items[iIndex2].FindControl("txtMenuMode");

        if ( ds2.Tables[0].Select("menu_id = '" + ddlMenu.SelectedValue + "'", "").Length > 0)
        {
            litJavaScript.Text = "<script>alert('!เมนูที่ระบุ มีการกำหนดสิทธิ์แล้ว ');</script>";
        }
        else if (ddlMenu.SelectedValue =="")
        {
            litJavaScript.Text = "<script>alert('!ยังไม่ได้ระบุเมนูที่ต้องการ ');</script>";
        }
        else if ( chkFlagAdd.Checked == false && chkFlagEdit.Checked == false && chkFlagDelete.Checked == false && chkFlagInquiry.Checked == false )
        {
            litJavaScript.Text = "<script>alert('!ยังไม่ได้ระบุคำสั่งการทำงานที่ต้องการ ');</script>";
        }
        else
        {
            ds2.Tables[0].Rows[iIndex2]["menu_id"] = ddlMenu.SelectedValue;
            ds2.Tables[0].Rows[iIndex2]["menu_name"] = ddlMenu.Items[ddlMenu.SelectedIndex].Text;
            if (chkFlagAdd.Checked == true && chkFlagAdd.Visible == true)
            {
                ds2.Tables[0].Rows[iIndex2]["urmenu_flagadd"] = "Y";
            }
            else
            {
                ds2.Tables[0].Rows[iIndex2]["urmenu_flagadd"] = "N";
            }
            if (chkFlagEdit.Checked == true && chkFlagEdit.Visible == true)
            {
                ds2.Tables[0].Rows[iIndex2]["urmenu_flagedit"] = "Y";
            }
            else
            {
                ds2.Tables[0].Rows[iIndex2]["urmenu_flagedit"] = "N";
            }
            if (chkFlagDelete.Checked == true && chkFlagDelete.Visible == true)
            {
                ds2.Tables[0].Rows[iIndex2]["urmenu_flagdelete"] = "Y";
            }
            else
            {
                ds2.Tables[0].Rows[iIndex2]["urmenu_flagdelete"] = "N";
            }
            if (chkFlagInquiry.Checked == true && chkFlagInquiry.Visible == true)
            {
                ds2.Tables[0].Rows[iIndex2]["urmenu_flaginquiry"] = "Y";
            }
            else
            {
                ds2.Tables[0].Rows[iIndex2]["urmenu_flaginquiry"] = "N";
            }

            ViewState["DSMasterTable2"] = ds2;
            dtgData2.EditItemIndex = -1;
            ShowData2();
        }
        //ShowData2();
    }
    protected void btnConfirmChangePwd_Click(object sender, EventArgs e)
    {
        string strRet = "";
        ProfileData loginData = FormatStringApp.getLoginSession(Page, Session);
        MasterDataManager cm = new MasterDataManager();

        litJavaScript.Text += "<script>";
        strRet = cm.editDataPassword(txtUserName.Text, txtNewPassword.Text, loginData.loginName, "N");
        if (strRet.StartsWith("ERROR"))
        {
            litJavaScript.Text += "alert('" + strRet.Split(':')[1] + "');";
        }
        else
        {
            litJavaScript.Text += "alert('บันทึกข้อมูลเรียบร้อย');";
        }
        litJavaScript.Text += "</script>";
    }
    protected void imgDelete_Click(object sender, ImageClickEventArgs e)
    {
        ImageButton imgClick = (ImageButton)sender;
        int iIndex = Convert.ToInt32(imgClick.Attributes["dsindex"]);
        DataSet ds;
        ds = (DataSet)ViewState["DSMasterTable"];
        ds.Tables[0].Rows.RemoveAt(iIndex);
        ViewState["DSMasterTable"] = ds;
        dtgData.EditItemIndex = -1;
        ShowData();
    }
    protected void imgDelete2_Click(object sender, ImageClickEventArgs e)
    {
        ImageButton imgClick2 = (ImageButton)sender;
        int iIndex = Convert.ToInt32(imgClick2.Attributes["dsindex"]);
        DataSet ds2;
        ds2 = (DataSet)ViewState["DSMasterTable2"];
        ds2.Tables[0].Rows.RemoveAt(iIndex);
        ViewState["DSMasterTable2"] = ds2;
        dtgData2.EditItemIndex = -1;
        ShowData2();
    }
    protected void dtgData_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        MasterDataManager cm = new MasterDataManager();
        DataSet ds; 
        DropDownList ddlBranchCode;
       
        ProfileData loginData = FormatStringApp.getLoginSession(Page, Session);
        if (e.Item.ItemType == ListItemType.EditItem)
        {
           // ddlBranchCode = (DropDownList)e.Item.FindControl("ddlBranchCode");
            ds = cm.getAllDataBranch();
            ddlBranchCode = (DropDownList)e.Item.FindControl("ddlBranchCode");
            ddlBranchCode.DataSource = ds;
            ddlBranchCode.DataTextField = "branch_name";
            ddlBranchCode.DataValueField = "branch_code";
            ddlBranchCode.DataBind();
            ddlBranchCode.Items.Insert(0, new ListItem("-- choose -- ", ""));
        }
         
    }
    protected void dtgData_ItemDataBound2(object sender, DataGridItemEventArgs e)
    {
        MasterDataManager cm = new MasterDataManager();
        DataSet ds;
        DropDownList ddlMenu;
        CheckBox chkFlagAdd;
        CheckBox chkFlagEdit;
        CheckBox chkFlagDelete;
        CheckBox chkFlagInquiry;

        ProfileData loginData = FormatStringApp.getLoginSession(Page, Session);
        if (e.Item.ItemType == ListItemType.EditItem)
        {
            // ddlBranchCode = (DropDownList)e.Item.FindControl("ddlBranchCode");
            ds = cm.getAllMenu();
            ddlMenu = (DropDownList)e.Item.FindControl("ddlMenu");
            ddlMenu.DataSource = ds;
            ddlMenu.DataTextField = "menu_name";
            ddlMenu.DataValueField = "menu_id";
            ddlMenu.DataBind();
            ddlMenu.Items.Insert(0, new ListItem("-- choose -- ", ""));
            chkFlagAdd = (CheckBox)e.Item.FindControl("chkFlagAdd");
            chkFlagEdit = (CheckBox)e.Item.FindControl("chkFlagEdit");
            chkFlagDelete = (CheckBox)e.Item.FindControl("chkFlagDelete");
            chkFlagInquiry = (CheckBox)e.Item.FindControl("chkFlagInquiry");
        }

    }

    public void initialDropDownList()
    {
        MasterDataManager cm = new MasterDataManager();
        DataSet ds;
        ds = cm.getAllDataRole();
        ddlRole.DataSource = ds;
        ddlRole.DataTextField = "ROLE_NAME";
        ddlRole.DataValueField = "ROLE_ID";
        ddlRole.DataBind();
        ddlRole.Items.Insert(0, new ListItem("-- choose -- ", ""));        

        ds = FormatStringApp.getAllDataStatus();
        ddlStatus.DataSource = ds;
        ddlStatus.DataTextField = "status_name";
        ddlStatus.DataValueField = "status_code";
        ddlStatus.DataBind();

        ds = cm.getAllDatateam();
        ddlteam.DataSource = ds;
        ddlteam.DataTextField = "team_name";
        ddlteam.DataValueField = "team_id";
        ddlteam.DataBind();
        ddlteam.Items.Insert(0, new ListItem("-- choose -- ", ""));

        ds = FormatStringApp.getAllDataLevel();
        ddllevel.DataSource = ds;
        ddllevel.DataTextField = "level_name";
        ddllevel.DataValueField = "level_code";
        ddllevel.DataBind();

    }
    public void initialDataGrid()
    {
        DataSet ds = new DataSet();
        DataTable dt = new DataTable();
        dt = TDS.Utility.MasterUtil.AddColumnToDataTable(dt, ColumnType, ColumnName);
        ds.Tables.Add(dt);
        ViewState.Add("DSMasterTable", ds);
        ShowData();
        GridViewUtil.setGridStyle(dtgData, ColumnWidth, ColumnAlign, ColumnTitle);
    }
    public void initialDataGrid2()
    {
        DataSet ds2 = new DataSet();
        DataTable dt2 = new DataTable();
        dt2 = TDS.Utility.MasterUtil.AddColumnToDataTable(dt2, ColumnType2, ColumnName2);
        ds2.Tables.Add(dt2);
        ViewState.Add("DSMasterTable2", ds2);
        ShowData2();
        GridViewUtil.setGridStyle(dtgData2, ColumnWidth2, ColumnAlign2, ColumnTitle2);
    }
    public void docToUI(DataSet dsData, DataSet dsAuth, DataSet dsMenu)
    {
        if (dsData.Tables[0].Rows.Count > 0)
        {
            litJavaScript.Text += "<script>";
            txtUserName.Text = dsData.Tables[0].Rows[0]["user_login"].ToString();
            ddlRole.SelectedValue = dsData.Tables[0].Rows[0]["ROLE_ID"].ToString();            
            txtFName.Text = dsData.Tables[0].Rows[0]["user_fname"].ToString();
            txtLName.Text = dsData.Tables[0].Rows[0]["user_lname"].ToString();
            txtEmail.Text = dsData.Tables[0].Rows[0]["user_email"].ToString();
            ddlStatus.SelectedValue = dsData.Tables[0].Rows[0]["user_status"].ToString();
            if (!Convert.IsDBNull(dsData.Tables[0].Rows[0]["user_lastlogindate"]))
                txtLastLogin.Text = TDS.Utility.MasterUtil.getDateString((DateTime)dsData.Tables[0].Rows[0]["user_lastlogindate"], "dd/MM/yyyy");
            if (!Convert.IsDBNull(dsData.Tables[0].Rows[0]["start_date"]))
                txtStartDate.Text = TDS.Utility.MasterUtil.getDateString((DateTime)dsData.Tables[0].Rows[0]["start_date"], "dd/MM/yyyy");
            if (!Convert.IsDBNull(dsData.Tables[0].Rows[0]["stop_date"]))
                txtEndDate.Text = TDS.Utility.MasterUtil.getDateString((DateTime)dsData.Tables[0].Rows[0]["stop_date"], "dd/MM/yyyy");
            txtInsertLogin.Text = dsData.Tables[0].Rows[0]["insert_name"].ToString();
            txtInsertDate.Text = FormatStringApp.FormatDateTime(dsData.Tables[0].Rows[0]["insert_date"]);
            if (!Convert.IsDBNull(dsData.Tables[0].Rows[0]["update_name"]))
            {
                txtUpdateLogin.Text = dsData.Tables[0].Rows[0]["update_name"].ToString();
                txtUpdateDate.Text = FormatStringApp.FormatDateTime(dsData.Tables[0].Rows[0]["update_date"]);
            }
            //ddlBranch.SelectedValue = dsData.Tables[0].Rows[0]["user_branch"].ToString();
            ddllevel.SelectedValue = dsData.Tables[0].Rows[0]["user_level"].ToString();
            ddlteam.SelectedValue = dsData.Tables[0].Rows[0]["team_id"].ToString();
            litJavaScript.Text += "</script>\n";

            ViewState.Add("DSMasterTable", dsAuth);
            ViewState.Add("DSMasterTable2", dsMenu);
            ShowData();
            ShowData2();
        }
    }
    public DataSet UIToDoc()
    {
        DataSet dsData = new DataSet();
        DataTable dt = new DataTable();
        string[] ColumnType = {"string", "string", "string", "string", "string", 
                                "string", "string", "string", "string", "string", 
                                "string", "string", "string"};
        string[] ColumnName = {"user_login", "user_fname", "user_lname", "user_email", "role_id",
                                "user_status", "LOGINNAME","user_pwd","start_date", "stop_date",
                                "user_branch", "user_level", "team_id"};
        ProfileData loginData = FormatStringApp.getLoginSession(Page, Session);
        string strFilePath = "";
        dt = TDS.Utility.MasterUtil.AddColumnToDataTable(dt, ColumnType, ColumnName);
        dsData.Tables.Add(dt);
        dsData.Tables[0].Rows.Add(dsData.Tables[0].NewRow());
        dsData.Tables[0].Rows[0]["user_login"] = txtUserName.Text;
        dsData.Tables[0].Rows[0]["user_fname"] = txtFName.Text;
        dsData.Tables[0].Rows[0]["user_lname"] = txtLName.Text;
        dsData.Tables[0].Rows[0]["user_email"] = txtEmail.Text;
        dsData.Tables[0].Rows[0]["role_id"] = ddlRole.SelectedValue;
        dsData.Tables[0].Rows[0]["user_status"] = ddlStatus.SelectedValue;
        dsData.Tables[0].Rows[0]["LOGINNAME"] = loginData.loginName;
        if (txtMode.Text == "E")
            dsData.Tables[0].Rows[0]["user_pwd"] = hddPassword.Value;
        else
            dsData.Tables[0].Rows[0]["user_pwd"] = txtPassword.Text;
        dsData.Tables[0].Rows[0]["start_date"] = txtStartDate.Text;
        dsData.Tables[0].Rows[0]["stop_date"] = txtEndDate.Text;
        dsData.Tables[0].Rows[0]["user_level"] = ddllevel.SelectedValue;
        dsData.Tables[0].Rows[0]["team_id"] = ddlteam.SelectedValue;
        //dsData.Tables[0].Rows[0]["user_branch"] = ddlBranch.SelectedValue;

        return dsData;
    }
    public void setMode()
    {
        if (txtMode.Text == "A")
        {
            txtUserName.CssClass = "TEXTBOX";
            txtUserName.ReadOnly = false;
            btnAddNew.Visible = false;
            btnChangePwd.Visible = false;
        }
        else if (txtMode.Text == "E")
        {
            txtUserName.CssClass = "TEXTBOXDIS";
            txtUserName.ReadOnly = true;
            btnClear.Visible = false;
            btnAddNew.Visible = true;
            divLablePassword.Visible = false;
            divLblRePassword.Visible = false;
            divtxtPassword.Visible = false;
            divtxtRePassword.Visible = false;
        }        
    }
    public void ShowData()
    {
        DataSet ds = new DataSet();
        if (ViewState["DSMasterTable"] != null)
            ds = (DataSet)ViewState["DSMasterTable"];

        if (ds.Tables.Count > 0)
        {
            dv.Table = ds.Tables[0];
        }
        dtgData.DataSource = dv;
        dtgData.DataBind();
        GridViewUtil.ItemDataBound(dtgData.Items);
    }
    public void ShowData2()
    {
        DataSet ds2 = new DataSet();
        if (ViewState["DSMasterTable2"] != null)
            ds2 = (DataSet)ViewState["DSMasterTable2"];

        if (ds2.Tables.Count > 0)
        {
            dv2.Table = ds2.Tables[0];
        }
        dtgData2.DataSource = dv2;
        dtgData2.DataBind();
        GridViewUtil.ItemDataBound(dtgData2.Items);
    }
    protected void ddlMenu_SelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList ddlDtgMenu = (DropDownList)sender;
        CheckBox chkAdd;
        CheckBox chkEdit;
        CheckBox chkDelete;
        CheckBox chkInquiry;
        int iItem = 0;
        MasterDataManager cmMaster = new MasterDataManager();

        if (ddlDtgMenu != null)
        {
            iItem = Convert.ToInt32(ddlDtgMenu.Attributes["indexItem"]);
            chkAdd = (CheckBox)dtgData2.Items[iItem].Cells[0].FindControl("chkFlagAdd");
            chkEdit = (CheckBox)dtgData2.Items[iItem].Cells[0].FindControl("chkFlagEdit");
            chkDelete = (CheckBox)dtgData2.Items[iItem].Cells[0].FindControl("chkFlagDelete");
            chkInquiry = (CheckBox)dtgData2.Items[iItem].Cells[0].FindControl("chkFlagInquiry");

            initialChkBoxAutMenu(ddlDtgMenu, chkAdd, chkEdit, chkDelete, chkInquiry);
        }
    }
    public void initialDropDownListMenu(DropDownList ddlMenu, string strMenuValue,
                                        CheckBox chkFlagAdd, CheckBox chkFlagEdit,
                                        CheckBox chkFlagDelete, CheckBox chkFlagInquiry)
    {
        MasterDataManager cmMaster = new MasterDataManager();
        DataSet ds;

        ds = cmMaster.getAllMenu();
        ddlMenu.DataSource = ds;
        ddlMenu.DataTextField = "menu_name";
        ddlMenu.DataValueField = "menu_id";
        ddlMenu.DataBind();
        ddlMenu.Items.Insert(0, new ListItem("-- เลือก -- ", ""));
        if (strMenuValue != "")
            ddlMenu.SelectedValue = strMenuValue;

        initialChkBoxAutMenu(ddlMenu, chkFlagAdd, chkFlagEdit, chkFlagDelete, chkFlagInquiry);
    }
    public void initialChkBoxAutMenu(DropDownList ddlMenu,
                                        CheckBox chkFlagAdd, CheckBox chkFlagEdit,
                                        CheckBox chkFlagDelete, CheckBox chkFlagInquiry)
    {
        MasterDataManager cmMaster = new MasterDataManager();
        DataSet dsMenu;
        
        dsMenu = cmMaster.getModuleByID(ddlMenu.SelectedValue);        
        if (dsMenu.Tables[0].Rows[0]["menu_flagadd"].ToString() != "Y")
        {
            chkFlagAdd.Visible = false;
            chkFlagAdd.Checked = false;
        }
        if (dsMenu.Tables[0].Rows[0]["menu_flagedit"].ToString() != "Y")
        {
            chkFlagEdit.Visible = false;
            chkFlagEdit.Checked = false;
        }
        if (dsMenu.Tables[0].Rows[0]["menu_flagdelete"].ToString() != "Y")
        {
            chkFlagDelete.Visible = false;
            chkFlagDelete.Checked = false;
        }
        if (dsMenu.Tables[0].Rows[0]["menu_flaginquiry"].ToString() != "Y")
        {
            chkFlagInquiry.Visible = false;
            chkFlagInquiry.Checked = false;
        }

    }
}
