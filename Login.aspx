﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="UserControl_AutoLogin" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge"> 
    <title>SYN MUN KONG Insurance : Installment payment</title> 

    <link rel="stylesheet" href="css/globalUse.css"/>
    <link rel="stylesheet"  href="css/login.css"  />
</head>     
<body onKeyPress="callSubmit()">
    
        <script language="javascript">
        function goPage(strPage){
            var param = "";
			window.location = "/<%=strAppPath%>"+strPage + param;
		}
		function validateForm() {
		    var isRet = true;
		    var strMsg = "";
		    var strFocus = "";

		    if (document.getElementById("<%=txtUserName.ClientID %>").value == "") {
		        isRet = false;
		        strMsg += "    - ชื่อผู้ใช้งาน\n";
		        if (strFocus == "")
		            strFocus = document.getElementById("<%=txtUserName.ClientID %>");
		    }
		    if (document.getElementById("<%=txtPassword.ClientID %>").value == "") {
		        isRet = false;
		        strMsg += "    - รหัสผ่าน\n";
		        if (strFocus == "")
		            strFocus = document.getElementById("<%=txtPassword.ClientID %>");
		    }


		    if (isRet == false) {
		        alert("! กรุณาระบุข้อมูล : \n" + strMsg);
		        if (strFocus != "")
		            eval(strFocus.focus());
		    }

		    return isRet;
		}
		function callSubmit() {
		    if (event.keyCode == 13) {
		        if (validateForm()) __doPostBack('<%=btnLogin.ClientID %>', '');
		    }
		}
    </script>
<body>
    <form id="form1" runat="server">
        <div id="container"><!---start container --->
        <div id="header">
        <div class="logo"><a href="#"><img src="images/Installment/logo.png"   /></a></div>
        <div class="name-app">Installment Payment</div>
        <div class="status">
        </div>
         
         

        </div>
         
        <div id="wrapper"><!---start warp --->
        <div id="content"><!---start content --->
        
        <div id="page-home"> 
        <div class="login-area">
        <div id="divhead"> กรุณา Login เพื่อเข้าสู่ระบบ  </div>  
        <div id="divUser">  <label> User : </label> 
            <asp:TextBox ID="txtUserName" runat="server" Width="100"></asp:TextBox> 
        </div>    
        <div id="divPassword">  <label> Password : </label> 
            <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" Width="100"></asp:TextBox> 
        </div>  
        <div  id="divBtn"> 
            <asp:Button ID="btnLogin" runat="server" CssClass="submitlogin" OnClick="btnLogin_Click" OnClientClick="return validateForm();" Text="เข้าสู่ระบบ"></asp:Button>
            <%--<asp:Button ID="btnClear" runat="server" CssClass="submitlogin" OnClientClick="return goPage('Login.aspx');" Text="เคลียร์">  </asp:Button>--%>   
        </div>
            <div class="clear"></div>
        <div  class="warning">
            <span class="warningLeft"></span> 
             <span class="warningRight"> 
            
        </span> 
        </div>
        </div> <!-- /.login-area -->
        </div><!-- /#page-home --->
        </div><!--/#content --->
             
       </div><!-- /#wrapper --->
       
         <div id="footer">
             <div class="copy">
                 Copyright © 2014 SYN MUN KONG Insurance. 
             </div>	
         </div><!-- /#footer --->

         </div><!-- /#container --->
    
  
    <asp:literal ID="litJS" runat="server" ></asp:literal>
       <asp:Literal ID="litScript" runat="server"></asp:Literal>
    </form>
</body>
</html>
