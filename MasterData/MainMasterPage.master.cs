using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class MasterPage_MainMasterPage : System.Web.UI.MasterPage
{
    public string[] columnName = { "MODU_ID", "MODU_NAME", "ROLE_ID" };
    public string strAppName = ConfigurationManager.AppSettings["appName"];
    public ProfileData loginData;
    protected void Page_Load(object sender, EventArgs e)
    {
        loginData = FormatStringApp.getLoginSession(Page, Session);
        MasterDataManager cm = new MasterDataManager();
        DataSet dsMenu;
        Page.Title = "��͹��������";
        if (!IsPostBack)
        {
            loginData = (ProfileData)Session["LOGINDATA"];
            if (loginData != null)
            {                
                lblUserName.Text = loginData.loginName + ":" + loginData.userName  ;
                lblRoleName.Text = loginData.roleName;
                //lblBranch.Text = loginData.userBranch;
                dsMenu = cm.getModuleByRoleID(loginData.roleID);
                repMenu.DataSource = dsMenu;
                repMenu.DataBind();
            }
            //else
            //{
            //    lblUserName.Text = "���ͺ ��.��.�Ϳ������";
            //    lblGroup.Text = "P1";
            //}                
        }
        Page.RegisterClientScriptBlock("xx", "<script>hideSubMenu();</script>");
    }
    public string getSubMenu(object strRole, object strModule, object strLogin)
    {
        string strRet = "";
        //ProfileData loginData = FormatStringApp.getLoginSession(Page, Session);
        MasterDataManager cm = new MasterDataManager();
        DataSet dsMenu;
        dsMenu = cm.getMenuByRoleModuleUser(strRole.ToString(), strModule.ToString(), strLogin.ToString());
        for (int i = 0; i <= dsMenu.Tables[0].Rows.Count - 1; i++)
        {
            if (i == 0)
            {
                strRet = "<ul class=\"subNav subNav-01\"   >";
            }
            strRet += "<li><a href=\"/" + strAppName + dsMenu.Tables[0].Rows[i]["menu_filepath"].ToString() + "\">" + dsMenu.Tables[0].Rows[i]["menu_name"].ToString() + "</a></li>";
            if (i == dsMenu.Tables[0].Rows.Count - 1)
            {
                strRet += "</ul>";
            }
        }
        return strRet;
    }
}
